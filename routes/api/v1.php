<?php

Route::post('auth', "AuthController@postLogin");

Route::group(['namespace' => "Resources"], function () {
    Route::get('users', "Users@index");
    Route::get('users/{id}', "Users@show");
    Route::put('users/{id}', "Users@update");
    Route::delete('users/{id}', "Users@delete");
    Route::post('users', "Users@create");
});

