<?php

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */


if (($response = App\Base\System\Library\Networks\API::validateRequest()) !== true)
    exit($response);

// Version 1.0
Route::group(["domain" => env("APP_URL_API"), 'prefix' => "v1", "namespace" => "API\\v1"], function () {
    //Documentacion
    Route::get("/", function(Illuminate\Http\Request $request) {
        $data["lang"] = strtolower($request->get("lang") ?? "es");
        return view("doc/api/v1", $data);
    });
    require app_path("../routes/api/v1.php");
});


