<?php

use App\Base\Data\RouteData;

/*
  |--------------------------------------------------------------------------
  | Login Routes
  |--------------------------------------------------------------------------
 */


/*
  |--------------------------------------------------------------------------
  | App's Route (Previous Login)
  |--------------------------------------------------------------------------
 */
Route::group(['middleware' => array('auth', 'app.setting', "app.wildo"), 'namespace' => "Web\\Main"], function () {
    Route::get("/", function() {
        return redirect()->route("dashboard");
    });

    // Dashboard
    Route::get('/dashboard', "DashboardController@index")->name(RouteData::DASHBOARD);
    //Typayments
    Route::resource("typayment", "TypaymentController");
    Route::delete("/ext/typayment/{id}/cancel", "TypaymentController@cancel")->name(RouteData::TYPAYMENT_CANCEL);
    //Flow
    Route::resource("flow", "FlowController");
    Route::get("/ext/flow/crons", "FlowController@cron_index")->name(RouteData::FLOW_CRON_INDEX);
    Route::delete("/ext/flow/crons/{id}", "FlowController@cron_delete")->name(RouteData::FLOW_CRON_DELETE);
    //Credits
    Route::resource("credit", "CreditController");
    Route::get("/credit/{id}/track/create", "CreditController@createTrack")->name(RouteData::CREDIT_TRACK_CREATE);
    Route::get("/credit/{id}/record", "CreditController@record")->name(RouteData::CREDIT_RECORD);
    Route::post("/credit/{id}/track/create", "CreditController@storeTrack")->name(RouteData::CREDIT_TRACK_STORE);
    //Budget
    Route::resource("budget", "BudgetController");
    //Goals
    Route::resource("goal", "GoalController");
    //Autoread
    Route::get("/typayment/autoread/add", "AutoReadController@add")->name(RouteData::AUTOREAD_ADD);
    Route::get("/typayment/autoread/index", "AutoReadController@index")->name(RouteData::AUTOREAD_INDEX);
    Route::delete("/ext/typayment/autoread/{id}", "AutoReadController@destroy")->name(RouteData::AUTOREAD_DELETE);
});




/*
  |--------------------------------------------------------------------------
  | ACCOUNT USER
  |--------------------------------------------------------------------------
 */


Route::group(['middleware' => array('auth', 'app.setting', "app.wildo"), 'namespace' => "Web\\Account"], function () {
    Route::get('/my-account', "DashboardController@index")->name(RouteData::ACCOUNT_DASHBOARD);
    Route::get('/my-account/profile/edit', "ProfileController@edit")->name(RouteData::ACCOUNT_PROFILE_EDIT);
    Route::put('/my-account/profile/edit', "ProfileController@update")->name(RouteData::ACCOUNT_PROFILE_EDIT);
    Route::get('/my-account/settings/general', "SettingsController@general")->name(RouteData::ACCOUNT_SETTINGS_GENERAL);
    Route::get('/my-account/settings/notifications', "SettingsController@notifications")->name(RouteData::ACCOUNT_SETTINGS_NOTIFICATIONS);
    Route::put('/my-account/settings/notifications', "SettingsController@postNotifications")->name(RouteData::ACCOUNT_SETTINGS_NOTIFICATIONS);
    Route::put('/my-account/settings/general', "SettingsController@postGeneral")->name(RouteData::ACCOUNT_SETTINGS_GENERAL);
});




/*
  |--------------------------------------------------------------------------
  | AUTH
  |--------------------------------------------------------------------------
 */
Route::group(['namespace' => "Web\\Auth"], function () {
    Route::get('/login', "LoginController@index")->name("auth.login");
    Route::post('/login', "LoginController@handler");
    Route::get('/signup', "RegisterController@signup")->name(RouteData::AUTH_SIGNUP);
    Route::post('/signup', "RegisterController@postSignup");
    Route::get('/signup/confirm/{hash}', "RegisterController@confirm")->name(RouteData::AUTH_CONFIRM);
    Route::get("/forgotten", "ForgotPasswordController@view")->name(RouteData::AUTH_FORGOTTEN);
    Route::post("/forgotten", "ForgotPasswordController@post");
    Route::get("/forgotten/reset/{token}", "ForgotPasswordController@reset")->name(RouteData::AUTH_FORGOTTEN_RESET);
    Route::post("/forgotten/reset/{token}", "ForgotPasswordController@postReset");
    //Ajax
    Route::post("/signup/ajax/resend/email/confirm", "RegisterController@ajaxPostResendEmailConfirm")->name(RouteData::AUTH_AJAX_RESEND_EMAIL_CONFIRM);
});
Route::group(['middleware' => 'auth', 'namespace' => "Web\\Auth"], function () {
    Route::get('/auth/logout', "LoginController@logout")->name(RouteData::USER_LOGOUT);
    //Seguridad
    Route::get('/my-account/security/reset/password', "ResetPasswordController@view")->name(RouteData::ACCOUNT_SECURY_RESET_PASSWORD);
    Route::put('/my-account/security/reset/password', "ResetPasswordController@post")->name(RouteData::ACCOUNT_SECURY_RESET_PASSWORD);
});

/*
  |--------------------------------------------------------------------------
  | SERVICIOS CON CONEXIÓN EXTERNA
  |--------------------------------------------------------------------------
 */


Route::group(['namespace' => "Web\\Services"], function () {
    Route::get("/services/flow/cron/email/{user_id}/{currency}/{lang}/{hash}", "ServiceFlowCronController@confirmFromEmail")->name(RouteData::SERVICES_FLOW_CRON_EMAIL);
    Route::post("/services/flow/cron/email/{user_id}/{currency}/{lang}/{hash}", "ServiceFlowCronController@postConfirmFromEmail")->name(RouteData::SERVICES_FLOW_CRON_EMAIL);
});

/*
  |--------------------------------------------------------------------------
  | AJAX
  |--------------------------------------------------------------------------
 */

Route::group(['middleware' => array('auth'), 'namespace' => "Web\\Main"], function () {
    Route::post("/ajax/ext/flow/crons/{id}/exec", "FlowController@cron_exec")->name(RouteData::FLOW_CRON_EXEC);
    Route::post("/ajax/ext/flow/crons/{id}/defer", "FlowController@cron_defer")->name(RouteData::FLOW_CRON_DEFER);
    //Autoread
     Route::post("/ajax/ext/autoread/params/add/validate", "AutoReadController@postAjaxValidateParam")->name(RouteData::AUTOREAD_AJAX_VALIDATE_PARAM);
    Route::post("/ajax/ext/autoread/log/success/confirm", "AutoReadController@postAjaxLogConfirm")->name(RouteData::AUTOREAD_AJAX_LOG_CONFIRM);
    Route::post("/ajax/ext/autoread/log/success/skip", "AutoReadController@postAjaxLogSkip")->name(RouteData::AUTOREAD_AJAX_LOG_SKIP);
});

//Requiere autenticación de usuario
Route::group(['middleware' => array('auth'), 'namespace' => "Web\\Data"], function () {
    Route::post("/ajax/get/data/banks", "AjaxController@getDataBanks")->name(RouteData::AJAX_GET_BANKS);
    Route::post("/ajax/get/data/model/by/id", "AjaxController@getDataModelById")->name(RouteData::AJAX_GET_DATA_MODEL_BY_ID);
    Route::post("/ajax/post/budget/enable", "AjaxController@postBudgetEnable")->name(RouteData::AJAX_POST_BUDGET_ENABLE);
    Route::post("/ajax/post/budget/disable", "AjaxController@postBudgetDisable")->name(RouteData::AJAX_POST_BUDGET_DISABLE);
    Route::post("/ajax/post/set/meta/user", "AjaxController@postSetMetaUser")->name(RouteData::AJAX_POST_SET_META_USER);
    Route::post("/ajax/post/eloquent/toggle/active", "AjaxController@postEloquentToggleActive")->name(RouteData::AJAX_POST_ELOQUENT_TOGGLE_ACTIVE);
    Route::post("/ajax/post/ui/render/view","AjaxController@postRenderView")->name(RouteData::AJAX_POST_UI_RENDER_VIEW);
});

Route::group(['middleware' => array('auth'), 'namespace' => "Web\\Services"], function () {
    Route::post("/services/api/gmail/ajax/get/url/auth", "InterfaceMailAPIController@ajaxGmailGetUrlAuth")->name(RouteData::SERVICES_API_GMAIL_AJAX_GET_URL_AUTH);
    Route::get("/services/api/gmail/oauth", "InterfaceMailAPIController@oAuthGmail")->name(RouteData::SERVICES_API_GMAIL_OAUTH);
});

//Sin autenticación de usuario
Route::group(['namespace' => "Web\\Data"], function () {
    Route::post("/ajax/get/eloquent/exists/data", "AjaxController@getEloquentExistData")->name(RouteData::AJAX_GET_ELOQUENT_EXISTS_DATA);
});


