$(document).ready(function () {

    if ($(".onload-render-view").length == 0)
        $("page-loader").fadeOut();

    $("a").not("a[href='#'],a.dropdown-toggle,a.collapsible-header,a[data-toggle='dropdown'],a.btn-destroy,a[not-loader],a.btn-toggle-active").click(function () {
        $("page-loader").show();
    });

});