ready(function () {

    var comp = "goal-progress";

    var date_current = new Date();
    $(comp).each(function () {
        goal_progress_adapt_display(this);
        $(comp).find("year[value='" + date_current.getFullYear() + "']").trigger("click");
    });

});



function goal_progress_adapt_display(comp) {
    var total_intervals = $(comp).find("interval").length;
    var total_years = $(comp).find("year").length;
    var width = ((100 - (total_years * 2)) / 12);
    var anim_time = 500;
    var anim_easing = "swing";

    if (total_intervals <= 12)
        return;

    $(comp).find("interval").each(function (i) {
        if (i > 11) {
            $(this).css("width", "0%");
            return true;
        }
        $(this).css("width", width + "%");
    });


    /**
     * Al hacer click en el año, este despliega los meses asociados y desplaza los demas
     */
    $(comp).find("year").click(function () {

        $(comp).find("year").removeClass("active");
        $(this).addClass("active");
        var year_value = parseInt($(this).attr("value"));
        var total_intervals = $(this).find("interval").length;

        $(comp).find("year[value!=" + (year_value) + "]").each(function (j) {

            var intervals = (parseInt($(this).attr("value")) < year_value) ? $(this).find("interval").reverse() : $(this).find("interval");

            intervals.each(function (i) {
                i += 1;
                if ((12 - total_intervals) < i) {
                    $(this).animate({"width": "0%"}, {duration: anim_time, easing: anim_easing});
                }
            });
        });

        $(this).find("interval").animate({"width": width + "%"}, {duration: anim_time, easing: anim_easing, complete: function () {
                if (!$(comp).find("display").isVisible()) {
                    $(comp).find("progress-sand-time").hide();
                    $(comp).find("goal").fadeIn().css('display', 'block');
                    $(comp).find("display").fadeIn().css('display', 'block');
                    $(comp).find(".title").fadeIn().css('display', 'block');
                    $(comp).find(".progress").fadeIn().css('display', 'block');
                    $(comp).find(".msg-complete").fadeIn().css('display', 'block');
                }


            }});
    });
}


function goal_progress_adapt_text_month(comp) {

    $(comp).find("interval").each(function () {

        var width = $(this).width();

        if (width < 50 && width > 15) {
            $(this).attr("label-month-name", $(this).attr("label-month-name").toString().substr(0, 3));
        }

        if (width <= 15) {
            $(this).attr("label-month-name", $(this).attr("label-month-name").toString().substr(0, 1));
            $(this).addClass("min");
        }
    });

}


function goal_callback_success_complete_from_widget() {

    /**
     * Elimina el widget en pantalla si no hay más metas para mostrar
     */
    setTimeout(function () {
        if ($(".goals-widget goal-progress").length == 0) {
            $(".goals-widget").fadeOut(function () {
                $(this).remove();
            })
        }
    }, 1000);
}