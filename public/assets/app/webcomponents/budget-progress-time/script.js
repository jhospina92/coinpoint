ready(function () {
    var comp = "budget-progress-time";

    $(comp).each(function () {
        var count = parseInt($(this).attr("count"));
        var date_start = (($(this).attr("date-start").toString().split(" "))[0]).split("-");
        var date_end = (($(this).attr("date-end").toString().split(" "))[0]).split("-");
        var date = new Date();

        var day_start = date_start[2];
        var day_end = date_end[2];

        date_start = new Date(date_start[0], parseInt(date_start[1]) - 1, parseInt(date_start[2]));
        date_end = new Date(date_end[0], parseInt(date_end[1]) - 1, parseInt(date_end[2]));

        $(this).append("<date start>" + day_start + " " + getDateMonthName(date_start.getMonth()) + " " + date_start.getFullYear() + "</date>");
        $(this).append("<date end>" + day_end + " " + getDateMonthName(date_end.getMonth()) + " " + date_end.getFullYear() + "</date>");

        for (var i = 0; i < count; i++) {
            $(this).append("<item " + ((date_start.getMonth() < date.getMonth() && date_start.getFullYear() <= date.getFullYear()) ? "class='active'" : "") + " title='" + getDateMonthName(date_start.getMonth()) + "' style='width:" + (100 / count) + "%;' data-toggle='tooltip' data-placement='top'>" + ((date_start.getMonth() == date.getMonth() && date_start.getFullYear() == date.getFullYear()) ? "<progress-bar style='width:" + ((date.getDate() / (new Date(date.getFullYear(), date.getMonth() + 1, 0)).getDate()) * 100) + "%'></progress>" : "") + "</item>");
            date_start.setMonth(date_start.getMonth() + 1);

        }

        $(comp + " item").tooltip();
    });

});