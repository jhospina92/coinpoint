
// 0=No se ha mostrado; 1=Se esta mostrando; 2=Se ha visto y ocultado
var wildo_report_state = 0;

window.onload = function () {

    if (!$("#wildo").hasAttr("data-original-title"))
        return;

    $('#wildo').tooltip({template: '<div class="tooltip cloud wildo" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>', trigger: "manual"});

    playSound("nots");

    setInterval(function () {
        var style = window.getComputedStyle($('#slide-out').get(0));  // Need the DOM object
        var matrix = new WebKitCSSMatrix(style.webkitTransform);
        var x = matrix.m41;

        if (wildo_report_state == 2)
            return;

        if (x >= 0) {
            if (wildo_report_state == 0) {
                $('#wildo').tooltip('show');
                wildo_report_state = 1;
            }

        } else {
            if (wildo_report_state == 1) {
                $('#wildo').tooltip('hide');
                wildo_report_state = 0;
            }
        }
    }, 1000);

    $(document).on("click", ".cloud.wildo", function () {
        $('#wildo').tooltip('hide');
        wildo_report_state = 2;
    });
};