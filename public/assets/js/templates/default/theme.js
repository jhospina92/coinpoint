/*
 |--------------------------------------------------------------------------
 |GLOBAL PARAMS
 |--------------------------------------------------------------------------
 */


//INIT
$(document).ready(function () {
    initMaterialDesign();
    onLoadCanvasGraphs();
});



/** 
 *  SELECCIÓN DE CATEGORIA
 */


$(document).ready(function () {

    //Muestra el listado de categorias de un tipo
    if ($("button[data-target='#modal-category'").length > 0) {
        if ($(this).hasAttr("data-show-initial")) {
            $("#modal-category .list-categories").hide();
            $("#categories-" + $(this).attr("data-show-initial")).show();
        } else {
            $("#modal-category .list-categories").hide();
            $("#categories-SP").show();
        }
    }
    $("button[data-target='#modal-category']").click(function () {
        if ($(this).hasAttr("data-show-initial")) {
            $("#modal-category .list-categories").hide();
            $("#categories-" + $(this).attr("data-show-initial")).show();
        } else {
            $("#modal-category .list-categories").hide();
            $("#categories-SP").show();
        }
    });


    $("#modal-category .list-categories .list-group-item").click(function () {
        var numCats = $("#list-categories .list-group-item").length;
        var code = $(this).attr("data-code");
        var height = $("#list-categories").height();

        $(".list-categories .list-group-item").removeClass("active");
        $(this).addClass("active");
        $(".list-subcat").hide();
        $("#subcat-" + code).show();

        if ($("#subcat-" + code + " .list-group-item").length > numCats)
            $("#subcat-" + code).css("height", height + "px");
        else
            $("#subcat-" + code).css("height", "100%");

    });

    $("#modal-category .list-subcat .list-group-item").click(function () {
        var subcat = $(this).text();
        $(".list-subcat .list-group-item").removeClass("active");
        $(this).addClass("active");

        var input_target = ($("button[data-target='#modal-category'").parent().parent()).children("input");

        if (/CREDCAR/.test($(this).attr("data-code"))) {
            subcat = subcat.toString().substr(4, subcat.length) + " (" + subcat.toString().substr(0, 4) + ")";
        }

        input_target.val(subcat);

        if ($("#category").length == 0) {
            input_target.parent().append('<input type="hidden" name="category" id="category" value="' + $(this).attr("data-code") + '">');
        } else {
            $("#category").val($(this).attr("data-code")).trigger("change");
        }
        $("#modal-category").modal("hide");
    });
});


/*
 * ELIMINAR DE ELEMENTOS
 */

$(document).ready(function () {
    $(".btn-destroy").click(function () {

        var action = $(this).attr("data-action");
        var title = $(this).attr("data-title");
        var msg = $(this).attr("data-msg");
        var tag = $(this).attr("data-tag");
        var msg_success = ($(this).hasAttr("data-msg-success")) ? $(this).attr("data-msg-success") : app.msg.success.system.request.default;
        var msg_error = ($(this).hasAttr("data-msg-error")) ? $(this).attr("data-msg-error") : app.msg.error.system.request.default;
        var func_success = ($(this).hasAttr("data-func-success")) ? $(this).attr("data-func-success") : null


        var runDelete = function () {
            $.ajax({
                method: "DELETE",
                url: action,
                data: {_token: app_token}
            }).done(function (msg) {
                var data = $.parseJSON(msg);
                if (data.response) {

                    if (/modal/.test($(tag).attr("class"))) {
                        $(tag).modal("hide");
                    }

                    $(tag).fadeOut(function () {
                        $(this.remove());
                        toastr.success((data.message != undefined) ? data.message : msg_success);
                    });

                    if (func_success != undefined && func_success != null)
                        window[func_success]();

                } else {
                    toastr.error((data.message != undefined) ? data.message : msg_error);
                }
            });
        }

        if (msg == "false") {
            runDelete();
        } else {
            dialogConfirm(msg, title, function () {
                runDelete();
            });
        }
    });

});


/**
 * ELOQUENT TOGGLE ACTIVE
 */

$(document).ready(function () {

    $("#mode-secure").change(function () {
        $(this).parent().find("i").removeClass("active");

        if ($(this).is(":checked")) {
            $(this).parent().find("i.fa-lock").addClass("active");
            setModeSecure(true);
        } else {
            setModeSecure(false);
            $(this).parent().find("i.fa-unlock").addClass("active");
        }

        $("amount").css("visibility", "visible");
    }).trigger("change");


});


$(document).ready(function () {

    $(".btn-toggle-active").click(function () {

        var model = $(this).data("model");
        var id = $(this).data("id");
        var callback = $(this).data("callback");
        var msg_error = ($(this).hasAttr("data-msg-error")) ? $(this).attr("data-msg-error") : app.msg.error.system.request.default;
        var text_state_true = $(this).data("text-state-true");
        var text_state_false = $(this).data("text-state-false");
        var parent = this;

        $.ajax({
            method: "POST",
            url: app.route.ajax.post.eloquent.toggle.active,
            data: {_token: app_token, model: model, id: id}
        }).done(function (msg) {

            var data = $.parseJSON(msg);

            if (data.response == "error") {
                toastr.error((data.message != undefined) ? data.message : msg_error);
                return true;
            }

            var text = (data.response) ? text_state_true : text_state_false;


            $(parent).html(text);


            window[callback](data.response, id, text);
        });

    });

});



/*
 |--------------------------------------------------------------------------
 | MODAL RENDER VIEW
 |--------------------------------------------------------------------------
 */


$(document).ready(function () {

    $(document).on("click", ".btn-modal-render-view", function () {

        var callback = $(this).data("callback");
        var view = $(this).data("view");
        var title = ($(this).data("title") != undefined) ? $(this).data("title") : "";
        var params = ($(this).data("params") != undefined) ? $(this).data("params") : [];
        var show_footer = $(this).data("footer");

        $("#ui-modal-render-view .modal-title").html(title);
        if (show_footer)
            $("#ui-modal-render-view .modal-footer").show();
        else
            $("#ui-modal-render-view .modal-footer").hide();
        $("#ui-modal-render-view").modal("show");
        $("#ui-modal-render-view progress-sand-time").show();
        $("#ui-modal-render-view .modal-body .render-view").html("");

        uiRenderView("#ui-modal-render-view .modal-body", view, params, callback, true);

    });

});

/*
 |--------------------------------------------------------------------------
 | CONTENT RENDER VIEW
 |--------------------------------------------------------------------------
 */

$(document).on("click", ".btn-render-view", onUIRenderView);
$(function () {
    $(".onload-render-view").each(function () {
        onUIRenderView(this);
    });
});



/*
 |--------------------------------------------------------------------------
 | PAGINATION
 |--------------------------------------------------------------------------
 */

$(document).on("click", "button[data-pagination]", function () {

    $(this).parent().children("button").removeClass("active");


    var data = $(this).data("pagination");

    $(this).parent().children("button").each(function () {
        if (data.page == $(this).text())
            $(this).addClass("active");
    });

});

$(document).on("ajax:render-view", function () {
    $(".content-pagination").each(function () {
        if (!$(this).hasAttr("catch")) {
            handlerRenderPagination(this);
        }
    });
});