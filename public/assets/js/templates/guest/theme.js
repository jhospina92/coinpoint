/*
 |--------------------------------------------------------------------------
 |GLOBAL PARAMS
 |--------------------------------------------------------------------------
 */


//INIT
$(document).ready(function () {
    initMaterialDesign();
});

$(document).ready(function () {

    $("[open-dialog-confirm]").click(function () {
        var title = $(this).attr("data-msg-title");
        var content = $(this).attr("data-msg-content");
        var callback = $(this).attr("data-callback");
        var label_ok = $(this).attr("data-btn-ok");
        var label_cancel = $(this).attr("data-btn-cancel");

        var label = {};

        if (label_ok) {
            label.ok = label_ok;
        }

        if (label_cancel) {
            label.cancel = label_cancel;
        }
        dialogConfirm(content, title, function () {
            if (callback)
                window[callback]();
        }, label);
    });

});


var sg_captcha = false;

function sg_onSubmit(token) {
    sg_captcha = true;
    sg_validSubmit();
}

//Valida el envio del formulario. 
function sg_validSubmit() {
    var val = true;

    if (!sg_captcha)
        return;

    $("input[validation]").each(function () {
        var inp = new InputValidation(this, $(this).attr("validation"));
        if (!inp.run() && val) {
            val = false;
        }
    });

    if (val && sg_captcha) {
        $("#progress-bar-g").show();
        $("#subtn").attr("disabled","disabled");
        $("form").submit();
    } else
        toastr.error(app.msg.error.form.inputs.invalid);
}



function sg_success_dialog_resend_email() {

    var user_id = getUrlParameter("u");

    if (!user_id)
        user_id = $("#resend-mail").attr("data-u");

    $.ajax({
        url: sg_rse,
        type: "POST",
        data: {_token: app_token, "user_id": user_id},
        success: function (msg) {
            var data = $.parseJSON(msg);
            $("#resend-mail").remove();
            toastr.success(data.response);
        }
    });
}