$(document).ready(function () {

    $("button#subtn").click(function () {

        var val = true;

        $("input[validation]").each(function () {
            var inp = new InputValidation(this, $(this).attr("validation"));
            if (!inp.run() && val) {
                val = false;
            }
        });

        if (val) {
            $("#progress-bar-g").show();
            $("form").submit();
        } else
            toastr.error(app.msg.error.form.inputs.invalid);
    });
});
