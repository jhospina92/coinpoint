var separator_mil;
var separator_dec;

$(document).ready(function () {

    separator_mil = (typeof currency_format_sep_millar !== 'undefined') ? currency_format_sep_millar : (typeof app.currency !== 'undefined') ? app.currency.format[1] : ",";
    separator_dec = (typeof currency_format_sep_decimal !== 'undefined') ? currency_format_sep_decimal : (typeof app.currency !== 'undefined') ? app.currency.format[2] : ".";


    /**
     * Formatea el número de un campo en notación con puntos de mil y decimal
     */
    $(document).on("keypress", "input.input-currency", function (e) {
        if (!$.isNumeric(e.key))
            return false;

        var value = extractInteger($(this).val() + e.key);

        //Valida si tiene un valor maximo
        if ($(this).is("[data-max-value]") && value > parseFloat($(this).attr("data-max-value"))) {
            showErrorInput(this, app.msg.error.input.numeric.exceed_value_to + formatNumber($(this).attr("data-max-value"), separator_mil, separator_dec));
            return false;
        }
    });


    $(document).on("keyup", "input.input-currency", function () {
        /*var currency = app.currency.code;
         var decimal = app.currency.format[0];*/

        var value = $(this).extractInteger();

        if (value > 0)
            $(this).val(formatNumber(value, separator_mil, separator_dec));

    });

    $("input[data-required='number']").keypress(function (e) {
        if (!$.isNumeric(e.key))
            return false;

        if ($(this).hasAttr("maxlength") && $.isNumeric($(this).attr("maxlength"))) {
            if ($(this).val().length > parseInt($(this).attr("maxlength")) - 1) {
                return false;
            }
        }
    });

    $("input[data-required='decimal']").keypress(function (e) {

        var decimal = ".";

        if (e.key != decimal && !$.isNumeric(e.key))
            return false;

        if (e.key == decimal && ($(this).val().toString().count(decimal) >= 1 || $(this).val().length == 0)) {
            return false;
        }
    });

    /*
     * Loader submit Form
     */

    $("form").submit(function (e) {

        var form = this;

        showLoadingSubmit(this);

        if (!validateInputs($(this))) {
            e.preventDefault();
            hideLoadingSubmit(this);
            toastr.error(app.msg.error.form.inputs.invalid);
        }

        if ($(this).attr("method") == "ajax") {

            var postData = $(this).serializeArray();
            var formURL = $(this).attr("action");
            var func_success = $(this).attr("data-func-success");

            $.ajax({
                url: formURL,
                type: "POST",
                data: postData,
                success: function (msg) {
                    var data = $.parseJSON(msg);

                    if (data.response)
                    {
                        window[func_success]();
                        hideLoadingSubmit(this);
                    } else {
                        toastr.error((data.message != undefined) ? data.message : app.msg.error.system.request.default);
                    }

                    $(form).find(".progress.loader").remove();
                }
            });


            e.preventDefault();
            return false;
        }

    });

    $("input.input-currency").keyup();

});


