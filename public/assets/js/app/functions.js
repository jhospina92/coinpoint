
/** Retorna una cadena con un numero formateado en notación monetaria
 * 
 * @param {type} number
 * @param {type} separtorMillar
 * @param {type} separatorDecimal
 * @returns {String}
 */
function formatNumber(number, separtorMillar, separatorDecimal) {

    number += "";
    if (separtorMillar == undefined)
        separtorMillar = app.currency.format[1];
    if (separatorDecimal == undefined)
        separatorDecimal = app.currency.format[2];


    // Variable que contendra el resultado final
    var result = "";

    // Si el numero empieza por el valor "-" (numero negativo)
    if (number[0] == "-")
    {
        // Cogemos el numero eliminando los posibles puntos que tenga, y sin
        // el signo negativo
        if (separtorMillar == ",")
            newNumber = number.replace(/\,/g, '').substring(1);
        else
            newNumber = number.replace(/\./g, '').substring(1);
    } else {
        // Cogemos el numero eliminando los posibles puntos que tenga
        if (separtorMillar == ",")
            newNumber = number.replace(/\,/g, '');
        else
            newNumber = number.replace(/\./g, '');

    }

    // Si tiene decimales, se los quitamos al numero
    if (number.indexOf(separatorDecimal) >= 0)
        newNumber = newNumber.substring(0, newNumber.indexOf(separatorDecimal));

    // Ponemos un punto cada 3 caracteres
    for (var j, i = newNumber.length - 1, j = 0; i >= 0; i--, j++)
        result = newNumber.charAt(i) + ((j > 0) && (j % 3 == 0) ? separtorMillar : "") + result;

    // Si tiene decimales, se lo añadimos al numero una vez forateado con
    // los separadores de miles
    if (number.indexOf(separatorDecimal) >= 0)
        result += number.substring(number.indexOf(separatorDecimal));

    if (number[0] == "-")
    {
        // Devolvemos el valor añadiendo al inicio el signo negativo
        return "-" + result;
    } else {
        return result;
    }
}


function initMaterialDesign() {

    $('.mdb-select').material_select('destroy');
    $('.mdb-select').material_select();
    // Initialize collapse button
    // SideNav init
    $(".button-collapse").sideNav();

// Custom scrollbar init
    if ($(".custom-scrollbar").length > 0) {
        var el = document.querySelector('.custom-scrollbar');
        Ps.initialize(el);
    }

    if (app.trans) {
        var attr = {
            monthsFull: [app.trans.date.january.name, app.trans.date.febrary.name, app.trans.date.march.name, app.trans.date.april.name, app.trans.date.may.name, app.trans.date.june.name, app.trans.date.july.name, app.trans.date.august.name, app.trans.date.september.name, app.trans.date.october.name, app.trans.date.november.name, app.trans.date.december.name],
            monthsShort: [app.trans.date.january.abv, app.trans.date.febrary.abv, app.trans.date.march.abv, app.trans.date.april.abv, app.trans.date.may.abv, app.trans.date.june.abv, app.trans.date.july.abv, app.trans.date.august.abv, app.trans.date.september.abv, app.trans.date.october.abv, app.trans.date.november.abv, app.trans.date.december.abv],
            weekdaysFull: [app.trans.date.sunday.name, app.trans.date.monday.name, app.trans.date.tuesday.name, app.trans.date.wednesday.name, app.trans.date.thursday.name, app.trans.date.friday.name, app.trans.date.saturday.name],
            weekdaysShort: [app.trans.date.sunday.abv, app.trans.date.monday.abv, app.trans.date.tuesday.abv, app.trans.date.wednesday.abv, app.trans.date.thursday.abv, app.trans.date.friday.abv, app.trans.date.saturday.abv],
            today: app.trans.date.today.name,
            clear: app.trans.ui.label.clear,
            close: app.trans.ui.label.close,
            labelMonthNext: app.trans.ui.date.next_month,
            labelMonthPrev: app.trans.ui.date.prev_month,
            labelMonthSelect: app.trans.ui.date.select_month,
            labelYearSelect: app.trans.ui.date.select_year,
            format: app.format.date.text,
            formatSubmit: app.format.date.submit,
        };

        // Data Picker Initialization
        $('.datepicker').each(function () {
            if ($(this).data("attr-min") != undefined) {
                var date = $(this).data("attr-min").split("-");
                attr.min = [date[0], parseInt(date[1]) - 1, date[2]];
            }
            if ($(this).data("attr-max") != undefined) {
                var date = $(this).data("attr-max").split("-");
                attr.max = [date[0], parseInt(date[1]) - 1, date[2]];
            }

            $(this).pickadate(attr);
        });
        $('.datepicker-month').each(function () {
            $(this).pickadatemonth(attr);
        })
    }
    // Toast Alerts
    toastr.options = {
        "closeButton": true, // true/false
        "debug": false, // true/false
        "newestOnTop": false, // true/false
        "progressBar": false, // true/false
        "positionClass": "toast-bottom-right", // toast-top-right / toast-top-left / toast-bottom-right / toast-bottom-left
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300", // in milliseconds
        "hideDuration": "1000", // in milliseconds
        "timeOut": "5000", // in milliseconds
        "extendedTimeOut": "1000", // in milliseconds
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    $('[data-toggle="tooltip"]').each(function () {
        var custom_class = $(this).attr("data-custom-class");
        $(this).tooltip({template: '<div class="tooltip ' + custom_class + '" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'});
    });

    $(".mdb-select.select-group ul.multiple-select-dropdown li").each(function () {
        if ($(this).children("img").length > 0)
            $(this).addClass("main");
        else
            $(this).addClass("subitem");

    });

}

/** Valida todos los requisitos de los campos 
 *  
 * @returns {undefined}
 */


function validateInputs(form) {

    resetFormError();

    //Validación select
    form.find("select[data-required]").each(function () {

        if (!$(this).isVisible())
            return;


        var msg = "";
        if ($(this).val() == null || $(this).val().length == 0) {
            msg = app.msg.error.select;
        }

        showErrorInput(this, msg);

    }).change(function () {
        $(".mdb-select").removeClass("invalid");
        $(".error-select").remove();
    });


    //Validación input - Número formateado
    form.find("input[data-required='balance']").each(function () {

        if (!$(this).isVisible())
            return;

        var val = parseFloat($(this).val().replace(/[^\d]/g, ''));
        var msg = "";

        if (val < parseInt($(this).attr("data-error-attr"))) {
            msg = app.msg.error.input.numeric.attr_minor_to + " " + $(this).attr("data-error-attr");
        }
        if (!$.isNumeric(val)) {
            msg = app.msg.error.input.numeric.not_valid;
        }
        if ($(this).val().length == 0) {
            msg = app.msg.error.input.any.empty;
        }

        //Valida si el valor es menor
        if ($(this).is("[data-min-value]") && extractInteger($(this).val()) <= parseFloat($(this).attr("data-min-value"))) {
            msg = sprintf(app.msg.error.input.numeric.attr_minor_to, formatNumber($(this).attr("data-min-value"), separator_mil, separator_dec));
        }
        showErrorInput(this, msg);

    });


    //Validación input - Númerico
    form.find("input[data-required='number']").each(function () {

        if (!$(this).isVisible())
            return;

        var val = $(this).val();
        var msg = "";

        if (val < parseInt($(this).attr("data-error-attr"))) {
            msg = (app.msg.error.input.numeric.attr_minor_to).replace("%s", $(this).attr("data-error-attr"));
        }
        if (!$.isNumeric(val)) {
            msg = app.msg.error.input.numeric.not_valid;
        }

        if ($(this).hasAttr("maxlength") && $.isNumeric($(this).attr("maxlength")) && $(this).val().length > parseInt($(this).attr("maxlength"))) {
            msg = sprintf(app.msg.error.input.format.invalid, $(this).parent().find("label").html());
        }


        if ($(this).val().length == 0) {
            msg = app.msg.error.input.any.empty;
        }

        showErrorInput(this, msg);

    });

    //Validación input - texto
    form.find("input[data-required='text'],input[data-required]").each(function () {
        if (!$(this).isVisible())
            return;
        var msg = "";
        if ($(this).val().length == 0) {
            msg = app.msg.error.input.any.empty;
        }
        showErrorInput(this, msg);
    });


    //Validación de comparación
    form.find("input[data-compare]").each(function () {
        if (!$(this).isVisible())
            return;
        var msg = "";
        var value_compare = $($(this).attr("data-compare")).val();

        if ($(this).val() != value_compare)
            msg = app.msg.error.input.compare.values;

        showErrorInput(this, msg);
    });

    //Validación de longitud
    form.find("input[min-length]").each(function () {
        if (!$(this).isVisible())
            return;
        var msg = "";

        if ($(this).val().toString().length < parseInt($(this).attr("min-length")))
            msg = (app.msg.error.input.min.length).replace("%s", $(this).attr("min-length"));

        showErrorInput(this, msg);
    });

    return (!$("form.form-main").is("[invalid]"));
}

function resetFormError() {
    $(".mdb-select").removeClass("invalid");
    $(".error-select").remove();
    $("form.form-main").removeAttr("invalid");
}


/** Muestra un mensaje de error en un campo
 * 
 * @returns {undefined}
 */
function showErrorInput(obj, msg) {

    if (msg.length == 0)
        return;

    var parent = $(obj).parent();

    if (parent.children("label").length == 0)
        parent.append("<label></label>");


    var label = parent.children("label");


    if (!$(obj).hasClass("validate")) {
        $(obj).addClass("validate");
    }
    if (!$(obj).hasClass("invalid")) {
        $(obj).addClass("invalid");
        if ($(obj).tagName() == "input")
            label.attr("data-error", msg);
        if ($(obj).tagName() == "select")
        {
            parent.addClass("invalid");
            parent.append("<div class='error-select'>" + msg + "</div>");
        }

    }
    $("form.form-main").attr("invalid", "");

}

function showLoadingSubmit(form) {
    $(form).find("button.btn").attr("disabled", "disabled").addClass("disabled");
    if ($(form).attr("method") == "ajax") {
        if ($(form).find(".progress.loader").length > 0)
            $(form).find(".progress.loader").show();
        else
            $(form).append('<div class="progress loader"><div class="indeterminate"></div></div>');
    } else {
        $("#progress-bar-g").show();
    }
}

function hideLoadingSubmit(form) {
    $(form).find("button.btn").removeAttr("disabled").removeClass("disabled");
    $("#progress-bar-g").hide();
}

function extractInteger(text) {
    return parseFloat(text.replace(/[^\d]/g, ''));
}

function dialogConfirm(msg, title, callback, labels) {
    title = (title == undefined) ? app.trans.ui.label.warning : title;
    $("#dialog-confirm .modal-title").html(title);
    $("#dialog-confirm .modal-body h5").html(msg);
    $("#dialog-confirm").modal("show");

    if (labels && labels.ok) {
        $("#dialog-confirm button.accept").attr("data-msg", $("#dialog-confirm button.accept").html());
        $("#dialog-confirm button.accept").html(labels.ok);
    } else {
        if ($("#dialog-confirm button.accept").hasAttr("data-msg"))
            $("#dialog-confirm button.accept").html($("#dialog-confirm button.accept").attr("data-msg"));
    }

    if (labels && labels.cancel) {
        $("#dialog-confirm button.cancel").attr("data-msg", $("#dialog-confirm button.cancel").html());
        $("#dialog-confirm button.cancel").html(labels.cancel);
    } else {
        if ($("#dialog-confirm button.cancel").hasAttr("data-msg"))
            $("#dialog-confirm button.cancel").html($("#dialog-confirm button.cancel").attr("data-msg"));
    }

    if ($("#ui-message-modal").isVisible()) {
        $("#ui-message-modal").css("z-index", "1000");
    }

    $("#dialog-confirm button.accept").unbind("click").click(function () {
        callback();
        $("#dialog-confirm").modal("hide");
        if ($("#ui-message-modal").isVisible()) {
            $("#ui-message-modal").css("z-index", "1050");
        }
    });

    $("#dialog-confirm button.cancel").unbind("click").click(function () {
        if ($("#ui-message-modal").isVisible()) {
            $("#ui-message-modal").css("z-index", "1050");
        }
    });
}

function playSound(filename) {
    filename = app_url_site + "/assets/sounds/" + filename;
    document.getElementById("sound").innerHTML = '<audio autoplay="autoplay"><source src="' + filename + '.mp3" type="audio/mpeg" /><source src="' + filename + '.ogg" type="audio/ogg" /><embed hidden="true" autostart="true" loop="false" src="' + filename + '.mp3" /></audio>';
}


/** Ejecuta una funcion callback indica cuando el DOM este cargado (Aprueba de JQUERY)
 * 
 * @param {type} callback
 * @returns {undefined}
 */

var app_flag_ready = false;

function ready(callback) {

    if (app_flag_ready)
        return;

    setTimeout(function () {
        if (typeof jQuery != 'undefined') {
            app_flag_ready = true;
        }
        if (!app_flag_ready) {
            ready(callback);
            return;
        }
        $(document).ready(function () {
            callback();
        });

    }, 2000);
}

function getDateMonthName(val) {
    switch (val) {
        case 0:
            return app.trans.date.january.name;
        case 1:
            return app.trans.date.febrary.name;
        case 2:
            return app.trans.date.march.name;
        case 3:
            return app.trans.date.april.name;
        case 4:
            return app.trans.date.may.name;
        case 5:
            return app.trans.date.june.name;
        case 6:
            return app.trans.date.july.name;
        case 7:
            return app.trans.date.august.name;
        case 8:
            return app.trans.date.september.name;
        case 9:
            return app.trans.date.october.name;
        case 10:
            return app.trans.date.november.name;
        case 11:
            return app.trans.date.december.name;
    }
}

/** Retorna la diferencia en dias entre dos fechas con formato YY/MM/DD
 * 
 * @param String date1
 * @param String date2 Si no se define, se toma con la fecha actual
 * @returns {Number}
 */
function gapDates(date1, date2) {

    var date = new Date();

    date2 = (date2 == undefined) ? date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() : date2;

    var aDate1 = date2.split('-');
    var aDate2 = date1.split('-');
    var fDate1 = Date.UTC(aDate1[0], aDate1[1] - 1, aDate1[2]);
    var fDate2 = Date.UTC(aDate2[0], aDate2[1] - 1, aDate2[2]);
    var dif = fDate2 - fDate1;
    var dias = Math.floor(dif / (1000 * 60 * 60 * 24));
    return dias;
}


/** Al activarse el modo seguro, todos los valores monetarios en pantalla se ocultan
 * 
 * @param {type} flag
 * @returns {undefined}
 */

function setModeSecure(flag) {

    $("amount").each(function () {

        if (!flag) {
            $(this).html($(this).attr("data-val"));
            return;
        }
        var amount = $(this).text();
        $(this).attr("data-val", amount);
        var text = (amount).split("");
        var exit = "";
        for (var i = 0; i < text.length; i++) {
            if (text[i] != " ")
                exit += '#';
            else
                exit += text[i];
        }

        $(this).html(exit);
    });


    $.ajax({
        method: "POST",
        url: app.route.ajax.set.meta.user,
        data: {_token: app_token, key: "mode-secure", value: flag}
    });
}

/** Imprime un texto con formato
 * 
 * @returns {undefined}
 */
function sprintf(text, ...args) {

    for (var i = 0; i < args.length; i++) {
        text = text.replace("%s", args[i]);
    }

    return text;

}

/** Retorna el valor de una variable pasada por _GET en la URL actual
 * 
 * @param {type} sParam
 * @returns {getUrlParameter.sParameterName|Boolean}
 */
function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}


/** Establece el evento de Render View
 * 
 * @returns {undefined}
 */
function onUIRenderView(obj) {

    obj = (obj == undefined) ? this : obj

    var callback = $(obj).data("callback");
    var container = $(obj).data("container");
    var view = $(obj).data("view");
    var params = ($(obj).data("params") != undefined) ? $(this).data("params") : {};
    var pagination = ($(obj).data("pagination") != undefined) ? $(this).data("pagination") : {};

    $.each(pagination, function (key, value) {
        params[key] = value;
    });
    uiRenderView(container, view, params, callback);
}

/** Carga en un contenedor una vista llamada por ajax
 * 
 * @param {type} container
 * @param {type} view
 * @param {type} params
 * @param {type} callback
 * @param {type} isModal
 * @returns {undefined}
 */
function uiRenderView(container, view, params, callback, isModal) {

    if (!isModal)
        $(container).addClass("content-render-view");

    if ($(container).height() > 0)
        $(container).css("overflow", "hidden").css("height", $(container).height() + "px");

    //Muestra un loader dentro del contenedor en donde se mostrara el contenido
    if (!$(container).hasClass("onload-render-view"))
        $(container).html(app.ui.component.loader.sand);

    $.ajax({
        method: "POST",
        url: app.route.ajax.post.ui.modal.render.view,
        data: {_token: app_token, _view: app_view, render_view: view, params: params},

        success: function (msg) {

            var data = $.parseJSON(msg);

            if (data.response == "error") {
                toastr.error(app.msg.error.system.request.default);
                return true;
            }
            if (!isModal) {
                $(container).css("opacity", "0").html("<div class='_render'>" + data.view + "</div>");
            }

            initMaterialDesign();
            onLoadCanvasGraphs();

            //Carga en el contenedor la vista renderizada
            setTimeout(function () {

                if (!isModal) {
                    $(container).animate({height: $(container).children(":first-child").height() + "px", opacity: 1}, 500);
                } else {
                    $(container).html(data.view);
                }

                if (callback != undefined && $.type(callback) === "string")
                    window[callback](data);

                if (varIsFunction(callback)) {
                    callback(data);
                }

                $(document).trigger("ajax:render-view");
                $("#mode-secure").trigger("change");

                if ($("page-loader").css("display") == "block")
                    $("page-loader").fadeOut();


            }, 500);

        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr.error(app.msg.error.system.request.default);
        }
    });
}

/** Indica si una variable es una funcion
 * 
 * @param {type} v
 * @returns {Boolean}
 */
function varIsFunction(v) {
    return (typeof v === "function");
}

/** Asigna un controlador de paginación en tiempo real
 * 
 * @param parent Objeto contenedor con class .content-pagination
 * @returns {undefined}
 */
function handlerRenderPagination(parent) {

    var param_num_btns = 5;
    var btnlist = $(parent).find(".btn-render-view:not(.btn-pag-nav)");
    var data = $(parent).find(".btn-pag-nav:first-child").data("pagination");
    var btn_page_first = $(parent).find(".btn-render-view[data-label='1']");
    var btn_page_last = $(parent).find(".btn-render-view[data-label='" + data.total_page + "']");
    var btn_nav_left = $(parent).find(".btn-pag-nav:first-child");
    var btn_nav_right = $(parent).find(".btn-pag-nav:last-child");

    $(parent).attr("catch", "");
    $(parent).find(".btn-pag-nav:first-child").hide();


    if (data.total_page > param_num_btns) {
        btnlist.each(function (i) {
            if ((i + 1) > param_num_btns)
                $(this).hide();
        });
    }

    //Controlador de visualización de botones de paginación
    btnlist.click(function () {
        var page = $(this).data("label");
        btnlist.hide();

        //Muestra los botones seguidos hacia abajo de la paginación
        for (var i = page; (i > 0 && i > page - Math.ceil(param_num_btns / 2)); i--) {
            $(parent).find(".btn-render-view[data-label='" + i + "']").show();
        }
        var i = page;
        //Muestra los botones seguidos hacia arriba de la paginación
        for (var i = page; (i <= data.total_page && i < page + Math.ceil(param_num_btns / 2)); i++) {
            $(parent).find(".btn-render-view[data-label='" + i + "']").show();
        }

        if (btn_page_last.isVisible()) {
            btn_nav_right.removeClass("show-bullet").hide();
        } else {
            btn_nav_right.addClass("show-bullet").show();
        }

        if (btn_page_first.isVisible()) {
            btn_nav_left.removeClass("show-bullet").hide();
        } else {
            btn_nav_left.addClass("show-bullet").show();
        }

    });


    btn_nav_right.click(function () {
        btn_page_last.trigger("click");
    });
    btn_nav_left.click(function () {
        btn_page_first.trigger("click");
    });


    btn_page_first.trigger("click");

}

function onLoadCanvasGraphs() {

    $("div.graph").each(function (i) {
        var id = "canvas-graph-" + (parseInt(Date.now()) + i);
        var canvas = $(this);
        var type = $(this).attr("data-type");
        var labels = $(this).attr("data-labels");
        var cols = $(this).attr("data-cols");
        //var ctx = canvas.get(0).getContext("2d");
        var dataset = [];

        $(this).attr("id", id);

        var option = {
            responsive: true
        };

        //Etiquetas por defecto
        if (labels == undefined)
            labels = [app.trans.date.january.name, app.trans.date.febrary.name, app.trans.date.march.name, app.trans.date.april.name, app.trans.date.may.name, app.trans.date.june.name, app.trans.date.july.name, app.trans.date.august.name, app.trans.date.september.name, app.trans.date.october.name, app.trans.date.november.name, app.trans.date.december.name];
        else
            labels = labels.split(",");

        canvas.children("item").each(function () {
            //Params
            var values = $(this).attr("data-values");

            if (values == undefined)
                values = [];
            else
                values = values.split(",");

            dataset.push({
                fillColor: ($(this).attr("data-attr-fillColor") == undefined) ? "rgba(220,220,220,0.2)" : $(this).attr("data-attr-fillColor"),
                strokeColor: ($(this).attr("data-attr-strokeColor") == undefined) ? "rgba(220,220,220,1)" : $(this).attr("data-attr-strokeColor"),
                pointColor: ($(this).attr("data-attr-pointColor") == undefined) ? "rgba(220,220,220,1)" : $(this).attr("data-attr-pointColor"),
                pointStrokeColor: ($(this).attr("data-attr-pointStrokeColor") == undefined) ? "#fff" : $(this).attr("data-attr-pointStrokeColor"),
                pointHighlightFill: ($(this).attr("data-attr-pointHighlightFill") == undefined) ? "#fff" : $(this).attr("data-attr-pointHighlightFill"),
                pointHighlightStroke: ($(this).attr("data-attr-pointHighlightStroke") == undefined) ? "rgba(220,220,220,1)" : $(this).attr("data-attr-pointHighlightStroke"),
                data: values
            });
        });
        
   
        var values = [cols.split(",")];
        var series = {};

        $.each(labels, function (i, val) {

            var data = [val];

            for (var f = 0; f < dataset.length; f++) {
                data.push(parseInt(dataset[f].data[i]));
                series[f] = {color: dataset[f].fillColor};
            }

            values.push(data);

        });
        

        var separator_mil = (typeof currency_format_sep_millar !== 'undefined') ? currency_format_sep_millar : (typeof app.currency !== 'undefined') ? app.currency.format[1] : ",";
        var separator_dec = (typeof currency_format_sep_decimal !== 'undefined') ? currency_format_sep_decimal : (typeof app.currency !== 'undefined') ? app.currency.format[2] : ".";



        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(function () {

            var data = google.visualization.arrayToDataTable(values);

            var options = {
                vAxis: {minValue: 0},
                legend: 'none',
                series: series,
                allowHtml: true, showRowNumber: true
            };

            var chart = new google.visualization.AreaChart(document.getElementById(id));

            var formatter = new google.visualization.NumberFormat(
                    {negativeParens: true, decimalSymbol: separator_dec, groupingSymbol: separator_mil, suffix: " " + app.currency.code, prefix: decodeHtml(app.currency.symbol)});
            formatter.format(data, 1); // Apply formatter to second column

            chart.draw(data, options);
        });


    });
}

/** Decodifica un formato HTML
 * 
 * @param {type} value
 * @returns {.document@call;createElement.value|textArea.value}
 */
function decodeHtml(value) {
    var textArea = document.createElement('textarea');
    textArea.innerHTML = value;
    return textArea.value;
}