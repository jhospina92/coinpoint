var gc_num_month, gc_total, gc_rate_saving, gc_money_current, gc_saving_min;


$(document).ready(function () {



    $("#rate_saving").change(function () {
        $("#display-rate-saving").html($(this).val() + "%");
        $("#input-value").keyup();
    });

    $("#date").change(function () {
        var date = $("#date").parent().find("input[name='date_submit']").val().toString().replace(/\//g, "-");
        var num_months = Math.ceil(gapDates(date) / 30);
        $("#st-months").html(num_months);
        $("#input-value").keyup();
    });

    //Calcula los valores estimados
    $("#input-value").keyup(function () {

        gc_num_month = Math.ceil(gapDates($("#date").parent().find("input[name='date_submit']").val().toString() + "-28") / 30);
        gc_total = $(this).val().extractInteger();
        gc_rate_saving = $("#rate_saving").val();
        gc_money_current = Math.ceil(parseInt($("#st-current-amount").attr("data-value")) * (gc_rate_saving / 100));
        gc_saving_min = Math.ceil(((gc_total - gc_money_current) / parseInt(gc_num_month)) / (gc_rate_saving / 100));

        $("#perc-saving").html(gc_rate_saving + "%");
        $("#st-months").html(gc_num_month);
        $("#st-current-amount").html(formatNumber(gc_money_current, app.currency.format[1], app.currency.format[2]) + " " + app.currency.code)
        $("#st-min-saving").html(isFinite(gc_saving_min) ? formatNumber(gc_saving_min, app.currency.format[1], app.currency.format[2]) + " " + app.currency.code : "&#x221e;");
        $("#st-goal-value").html(formatNumber(gc_total, app.currency.format[1], app.currency.format[2]) + " " + app.currency.code);

        if (gc_validate()) {
            if (!$("#calculate").isVisible())
                $("#calculate").show();
            $("form.form-main button[type='submit']").removeAttr("disabled");
        } else {
            $("form.form-main button[type='submit']").attr("disabled", "disabled");
            $("#calculate").hide();
        }

    });

    setTimeout(function () {
        if ($("form.form-main input[name='_method'][value='PUT']").length > 0)
            $("#input-value").keyup();
    }, 1000);

});


function gc_validate() {

    if (gc_num_month <= 0)
        return false;


    if (gc_total <= gc_money_current || isNaN(gc_total))
        return false;

    if (gc_rate_saving == 0)
        return false;

    return true;
}