


function goal_callback_success_index_terminate() {

    /**
     * Elimina el widget en pantalla si no hay más metas para mostrar
     */
    setTimeout(function () {
        if ($("#container-main table .goal").length == 0) {
            $("#container-main table").fadeOut(function () {
                $(this).remove();
                $("#container-main").html('<div class="card-block center-block text-xs-center"><h2><i class="fa fa-info-circle" aria-hidden="true"></i> '+app.msg.general.system.not.data+'</h2></div>');
            });
        }
    }, 1000);
}