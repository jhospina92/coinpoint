$(document).ready(function () {
    $("a.active-budget").click(function () {
        var id = $(this).attr("data-id");

        if ($("#budget-" + id).hasClass("active")) {

            $.ajax({
                method: "POST",
                url: ajax_budget_disable,
                data: {_token: app_token, user_id: app.user.data.id, budget_id: id}
            });

            $("#budget-" + id).removeClass("active");
            $(this).children("span").html(app.trans.ui.label.enable);

        } else {

            //Desactiva todos los checks
            $("a.active-budget").children("span").html(app.trans.ui.label.enable);
            $(".budget-index").removeClass("active");

            $(this).children("span").html(app.trans.ui.label.disable);
            $("#budget-" + id).addClass("active");
            $.ajax({
                method: "POST",
                url: ajax_budget_enable,
                data: {_token: app_token, user_id: app.user.data.id, budget_id: id}
            });

        }
    });
});
