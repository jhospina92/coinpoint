$(document).ready(function () {

    var type_current = null;


    $("button[data-target='#modal-category']").click(function () {
        if ($(this).attr("data-show-initial") == "SP")
            type_current = "spend";
        else
            type_current = "revenue";
    });


    /**
     * Agrega un campo en el formulario con la categoria correspondiente en el presupuesto
     */
    $("#modal-category .list-subcat li").click(function () {
        var code = $(this).attr("data-code");
        var text = $(this).text();
        var catmain_code = code.split("-")[0];
        var catmain_text = $("li.list-group-item[data-code='" + code.split("-")[0] + "']").text();
        if ($("#list-" + type_current + " div[data-code='" + catmain_code + "']").length == 0) {
            var content = list_item_main.replace("%code%", catmain_code).replace("%name%", catmain_text);
            if (type_current == "spend")
                content = content.replace(/%color-class%/g, "red");
            else
                content = content.replace(/%color-class%/g, "green");
            $("#list-" + type_current + "").append(content);
        }

        if ($("#list-" + type_current + " div[data-code='" + code + "']").length == 0) {
            $("#list-" + type_current + " div[data-code='" + catmain_code + "'] .list-subcats-content").append(list_item.replace(/%code%/g, code).replace("%name%", text).replace("%input-name%", type_current).replace("%code-main%", catmain_code));
        }

    });

    /**
     * Elimina un elemento del formulario de prespuesto
     */
    $(document).on("click", "button.remove-item", function () {
        var code = $(this).parent().parent().attr("data-code");
        var catmain_code = code.split("-")[0];
        if ($("div[data-code='" + catmain_code + "'] .list-subcats-content .row").length == 1) {
            $("div[data-code='" + catmain_code + "']").remove();
        } else {
            $(this).parent().parent().parent().remove();
        }
        reload_progress_bars();
    });

    /**
     * Calcula el total de categoria
     */
    $(document).on("keyup", "input.input-currency", function () {

        var code = $(this).attr("data-code");
        var catmain_code = $(this).attr("data-code-main");
        var value = $(this).extractInteger();
        var totalvalue = 0;
        $("input[data-code-main='" + catmain_code + "']").each(function () {
            totalvalue += ($(this).extractInteger() > 0) ? $(this).extractInteger() : 0;
        });

        $("div[data-code='" + catmain_code + "'] .level span").html(formatNumber(totalvalue, currency_format_sep_millar, currency_format_sep_decimal));
        reload_progress_bars();
    });

    $("input.input-currency").keyup();

});


/**
 * Recarga y calcula las barras de medición del presupuesto
 */
function reload_progress_bars() {
    
    $(".list-budget").each(function () {

        var balance = 0;
        var id = $(this).attr("id");

        $("#" + id + " input").each(function () {
            balance += ($(this).extractInteger() > 0) ? $(this).extractInteger() : 0;
        });

        $("#" + id + " div.section .level span").each(function () {
            var total = $(this).text().extractInteger();
            if (total / balance > 0) {
                var porcent = ((total / balance) * 100).toFixed(1) + "%";
                var section = $(this).parent().parent().parent().parent();
                section.find(".bar").stop().animate({"width": porcent}, 1000);
                section.find(".bar span").html(porcent);
            }
        });

        $("div[data-type='" + id + "'] .total").html(currency_symbol + " " + formatNumber(balance, currency_format_sep_millar, currency_format_sep_decimal) + " " + currency_code);
    });
    reload_balace();
}


/**
 * Gestión la barra de progreso del balance
 */
function reload_balace() {
    var total_revenue = $("div[data-type='list-revenue'] .total").text().extractInteger();
    var total_spend = $("div[data-type='list-spend'] .total").text().extractInteger();
    var total_saving = total_revenue - total_spend;


    if (total_saving >= 0) {

        var porcent_saving = ((Math.abs(total_saving) / total_revenue) * 100);
        var porcent_revenue = ((100 - Math.abs(porcent_saving)) / 2);
        var porcent_spend = porcent_revenue;

        if (porcent_saving > 60) {
            porcent_revenue += porcent_saving / 2;
            porcent_saving -= porcent_saving / 2;
        }

        if (porcent_saving < 40) {
            porcent_spend += porcent_saving / 2;
            porcent_saving -= porcent_saving / 2;
        }

        $("#progress-balance .saving").animate({width: porcent_saving + "%"}, function () {
            track_label_progress_bar(this)
        }).html("<label>" + currency_symbol + " " + formatNumber(total_saving, currency_format_sep_millar, currency_format_sep_decimal) + " " + currency_code + "</label>");
        $("#progress-balance .deficit").animate({width: 0 + "%"}).html("");

        //El presupuesto esta en Deficit
    } else {

        var porcent_saving = ((Math.abs(total_saving) / total_spend) * 100);
        var porcent_revenue = ((100 - Math.abs(porcent_saving)) / 2);
        var porcent_spend = porcent_revenue;

        if (porcent_saving < 40) {
            porcent_revenue += porcent_saving / 2;
            porcent_saving -= porcent_saving / 2;
        }

        if (porcent_saving < 60) {
            porcent_spend += porcent_saving / 2;
            porcent_saving -= porcent_saving / 2;
        }

        $("#progress-balance .saving").animate({width: 0 + "%"}).html("");
        $("#progress-balance .deficit").animate({width: porcent_saving + "%"}, function () {
            track_label_progress_bar(this)
        }).html("<label>- " + currency_symbol + " " + formatNumber(Math.abs(total_saving), currency_format_sep_millar, currency_format_sep_decimal) + " " + currency_code + "</label>");
    }


    $("#progress-balance .revenue").animate({width: porcent_revenue + "%"}, function () {
        track_label_progress_bar(this)
    }).html("<label>" + currency_symbol + " " + formatNumber(total_revenue, currency_format_sep_millar, currency_format_sep_decimal) + " " + currency_code + "</label>");
    $("#progress-balance .spend").animate({width: porcent_spend + "%"}, function () {
        track_label_progress_bar(this)
    }).html("<label>" + currency_symbol + " " + formatNumber(total_spend, currency_format_sep_millar, currency_format_sep_decimal) + " " + currency_code + "</label>");
    $("#saving-porcent").html((((total_saving / total_revenue) * 100).toFixed(1)) + "%");
    $("#saving").html(currency_symbol + " " + formatNumber(total_saving, currency_format_sep_millar, currency_format_sep_decimal) + " " + currency_code);
    $("#balance").val(total_saving);
}


function track_label_progress_bar(obj) {
    if ($(obj).children("label").text().length * 13 > $(obj).width()) {
        $(obj).children("label").css("width", ($(obj).children("label").text().length * 13) + "px").addClass("floating");
    }
}