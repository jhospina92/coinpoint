
$(document).ready(function () {
    $(".card").addClass("spend");
});

var transactions_type = "spend";
var typayment_type = null;

/**
 * FUNCIONES
 */

/** Controla el state de chequeo del tipo de transacción indicado
 * 
 * @param {type} val
 * @returns {undefined}
 */
function flow_push_state_type(btn, val) {

    var obj = $("#type-" + val);
    var style = $(btn).attr("data-class-style");
    transactions_type = val;

    $("input[name='type']:radio").attr("checked", false);
    $("#type-" + val).attr("checked", true).trigger("change");

    $(".card").removeClass("spend").removeClass("entry").removeClass("exchange").removeClass("investment").addClass(style);
    $(".list-categories").hide();
    $("#categories-" + obj.val()).show();

    switch (val) {
        case "spend":
            $("button[data-target='#modal-category']").attr("data-show-initial", "SP");
            $("#typayment").trigger("change");
            break;
        case "entry":
            $("button[data-target='#modal-category']").attr("data-show-initial", "EN");
            $("#input-value").removeAttr("data-max-value");
            break;
        case "exchange":
            $("#origin").change();
            break;
    }


    $("div[data-show-type]").each(function () {
        var types = ($(this).attr("data-show-type")).split(",");
        if (types.indexOf(obj.val()) > -1)
            $(this).show();
        else
            $(this).hide();
    });
}


/**
 * CONTROL DEL FORMULARIO
 */

$(document).ready(function () {

    /**
     * MEDIO DE PAGO: INGRESO - GASTO
     */

    $("#typayment").change(function () {

        try {
            var features = $.parseJSON($(this).find("option[value='" + $(this).val() + "']").data("features").toString().replace(/\'/g, "\""));
        } catch (err) {
            var features = {};
        }

        $(".currency-symbol").html($(this).find("option[value='" + $(this).val() + "']").attr("data-currency-symbol"));
        $(".currency-code").html($(this).find("option[value='" + $(this).val() + "']").attr("data-currency-code"));
        var balance = $(this).find("option[value='" + $(this).val() + "']").attr("data-balance");
        var type = typayment_type = $(this).find("option[value='" + $(this).val() + "']").attr("data-type");
        if (transactions_type != "entry")
            $("#input-value").attr("data-max-value", balance);

        //Valida que el valor ingresado no supere el saldo disponible del medio de pago
        if ($("#input-value").extractInteger() > balance) {
            $("#input-value").val(formatNumber(balance, app.currency.format[1], app.currency.format[2]));
        }

        //Valida si el tipo de pago usados es una tarjeta de credito
        if (type == "CC") {
            if (features["default_fees"] != undefined) {
                $("#input-fees").val(features["default_fees"]).change();
            } else {
                $("#input-fees").val("").change();
            }
            $(".content-type-cc").show();
        } else {
            $(".content-type-cc").hide();
        }

    }).trigger("change");

    /*
     * EXCHANGE
     */

    //Desactiva el medio de pago correspondiente al destino de la transferencia y vicerveza
    $("#origin").change(function () {

        var type = $(this).find("option[value='" + $(this).val() + "']").data("type");

        if (type == undefined)
            return;

        try {
            var features = $.parseJSON($(this).find("option[value='" + $(this).val() + "']").data("features").toString().replace(/\'/g, "\""));
        } catch (err) {
            var features = {};
        }
        $(".currency-symbol").html($(this).find("option[value='" + $(this).val() + "']").attr("data-currency-symbol"));
        $(".currency-code").html($(this).find("option[value='" + $(this).val() + "']").attr("data-currency-code"));

        $("#dest").enabledSelectOption();
        $("#dest").disabledSelectOption($("#origin").val());
        var balance = $(this).find("option[value='" + $(this).val() + "']").attr("data-balance");
        $("#input-value").attr("data-max-value", balance);

        //Valida que el valor ingresado no supere el saldo disponible del medio de pago
        if ($("#input-value").extractInteger() > balance) {
            $("#input-value").val(formatNumber(balance, app.currency.format[1], app.currency.format[2]));
        }

        //Si un avance con tarjeta de credito se indica el numero cuotas
        if (type == "CC") {

            if (features["default_fees"] != undefined) {
                $("#input-fees").val(features["default_fees"]).change();
            }

            $(".content-type-cc").show();
        } else {
            $(".content-type-cc").hide();
        }


    });

    $("#dest").change(function () {
        $("#origin").enabledSelectOption();
        $("#origin").disabledSelectOption($("#dest").val());
    });
});


/**
 * EDIT
 */

$(document).ready(function () {
    if (typeof transaction === 'undefined')
        return;

    var typayment_type = $("#typayment option[value='" + transaction.typayment_id + "']").attr("data-type");

    //Tipo
    $("input[value='" + transaction.type + "']").parent().click();
    $("input[value='" + transaction.type + "']").change();

    //Categoria
    var category = transaction.category;

    switch (transaction.category) {
        case "CREDCAR":
            category = "CREDCAR" + "-" + transaction.metadata.credit_card;
            break;
        case "CREDTCK":
            category = "CREDTCK" + "-" + transaction.metadata.credit_id;
            break;
    }

    $("li[data-code='" + category + "']").click();
    //Medio de pago
    $("#typayment").parent().find("ul li:nth-child(" + ($("#typayment option[value='" + transaction.typayment_id + "']").index() + 1) + ")").click();
    //Descripción
    $("#input-description").val(transaction.description);
    //Valor
    $("#input-value").val(transaction.value).trigger("keyup");

    //Tarjeta de credito
    if (typayment_type == app.typayment.type.credit_card) {
        //Cuota
        $("#input-fees").val(transaction.metadata.fees);
    }

    if (transaction.type == app.flow.type.exchange) {
        $("#origin").parent().find("ul li:nth-child(" + ($("#origin option[value='" + transaction.typayment_id + "']").index() + 1) + ")").click();
        $("#dest").parent().find("ul li:nth-child(" + ($("#dest option[value='" + transaction.metadata.dest + "']").index() + 1) + ")").click();

        $.ajax({
            method: "POST",
            url: ajax_get_data_model_by_id,
            data: {_token: app_token, model: app.model.type.flow, id: transaction.metadata.transaction}
        }).done(function (msg) {
            var data = $.parseJSON(msg);
            $("#exchange-cost").val(data.value).keyup();
            $("#exchange-cost").parent().find("label").addClass("active");
        });

    }
});


/*
 |--------------------------------------------------------------------------
 | CRONS TASK
 |--------------------------------------------------------------------------
 */

$(document).ready(function () {
    $("#cron-check").change(function () {
        if ($(this).is(":checked")) {
            $("#cron-content span.title").html(trans_label_cron_enabled);
            $("#form-cron").stop().clearQueue().slideDown();
            return;
        }

        $("#cron-content span.title").html(trans_label_cron_disabled);
        $("#form-cron").stop().clearQueue().slideUp();
    });
});

/*
 |--------------------------------------------------------------------------
 | RESTRICCIONES ADICIONALES
 |--------------------------------------------------------------------------
 */
$(document).ready(function () {

    /** [1]
     * Al seleccionar el pago de la cuota de una tarjeta de credito, esta debe quitarse del listado de medios de pagos.
     */
    $("#modal-category .list-subcat .list-group-item").click(function () {
        var code = $(this).attr("data-code");

        if (/CREDCAR/.test(code)) {
            var id = code.toString().replace("CREDCAR-", "");

            if ($("#typayment option:selected").val() == id) {
                $("#typayment").setValueSelect("");
            }

            $("#typayment").disabledSelectOption(id);
        } else {
            $("#typayment").enabledSelectOption();
        }

    });

    /** [2]
     *   //Si se va ingresar un valor a la tarjeta de credito, esta no tiene cuotas
     */

    $("input[name='type'],#typayment").change(function () {
        if (transactions_type == "entry" && typayment_type == "CC") {
            $(".content-type-cc").hide();
        }
    }).trigger("change");



    /** [3]
     * 
     */


});



