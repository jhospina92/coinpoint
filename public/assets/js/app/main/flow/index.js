
$(document).ready(function () {
    $(".card-transaction").mouseenter(function () {
        $(this).find(".btn-actions").show();
    }).mouseleave(function () {
        $(this).find(".btn-actions").hide();
    });

    $("#open-filter").click(function () {
        $(this).slideUp();
        $("#filter").attr("data-status", "open");
    });

    $("#select-type").change(function () {
        var data = $(this).val();
        $("#type").val(data.join(","));
    });

    $("#select-typayments").change(function () {
        var data = $(this).val();
        $("#typayments").val(data.join(","));
    });

    $("#select-category").change(function () {
        var data = $(this).val();
        $("#cats").val(data.join(","));
    });

    //$("page-loader .cs-ms").html(app.msg.general.system.loading);

    $("form.form-main").submit(function () {
        $("page-loader").show();
    });
});