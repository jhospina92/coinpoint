
ready(function () {

    $("form#form-fc-not button[type='submit']").click(function () {
        $("form#form-fc-not").attr("action", $(this).attr("data-action"));
        $("form#form-fc-not").attr("data-func-success", $(this).attr("data-func-success"));
    });

});


function fc_defer_success() {
    $(message_modal_id).modal("hide");
    toastr.success(msg_flow_cron_defer_success);
}

function fc_exec_success() {
    $(message_modal_id).modal("hide");
    toastr.success(msg_flow_cron_exec_success);
}