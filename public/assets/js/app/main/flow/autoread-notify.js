/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var aut_id_log = 0;

ready(function () {

    $(".aut-flow-input-category[data-type='SP']").mdb_autocomplete({
        data: app.dataset.user.categories.spend.sub
    });

    $(".aut-flow-input-category[data-type='EN']").mdb_autocomplete({
        data: app.dataset.user.categories.revenue.sub
    });

    // BTN - Confirm
    $(".btn-flowaut-confirm").click(function () {

        var parent = this;
        var id = $(this).data("id");
        var category = $("#input-aut-category-log-" + id).val();
        var dataset_cat = ($("#input-aut-category-log-" + id).data("type") == "SP") ? app.dataset.user.categories.spend.sub : app.dataset.user.categories.revenue.sub;
        var key_category = aut_get_key_from_dataset(category, dataset_cat);


        if (key_category == null) {
            showErrorInput($("#input-aut-category-log-" + id), "error");
            toastr.error(msg_flow_aut_error_input_cat);
            return false;
        }

        $(parent).setState("btn-default", "fa-spinner fa-loader-btn").attr("disabled", "disabled");

        $.ajax({
            method: "POST",
            url: url_ajax_aut_confirm,
            data: {_token: app_token, id: id, category: key_category}
        }).done(function (msg) {

            var data = $.parseJSON(msg);

            if (data.response == true) {

                $("#aut-log-" + id + "-1").fadeOut(function () {
                    $(this).remove();
                });

                $("#aut-log-" + id + "-2").fadeOut(function () {
                    $(this).remove();
                    aut_flow_check_logs();
                });

                toastr.success(msg_flow_aut_confirm);
            }else{
                toastr.error(app.msg.error.system.request.default);
            }
        });

    });

    // BTN - Skip
    $(".btn-flowaut-skip").click(function () {

        var parent = this;
        var id = $(this).data("id");

        $(parent).setState("btn-default", "fa-spinner fa-loader-btn").attr("disabled", "disabled");

        $.ajax({
            method: "POST",
            url: url_ajax_aut_skip,
            data: {_token: app_token, id: id}
        }).done(function (msg) {

            var data = $.parseJSON(msg);

            if (data.response == true) {

                $("#aut-log-" + id + "-1").fadeOut(function () {
                    $(this).remove();
                });

                $("#aut-log-" + id + "-2").fadeOut(function () {
                    $(this).remove();
                    aut_flow_check_logs();
                });

                toastr.error(msg_flow_aut_skip);
            }else{
                toastr.error(app.msg.error.system.request.default);
            }
        });

    });



    $("input.aut-flow-input-category").focus(function () {
        aut_id_log = $(this).data("id");
    });


    /**
     * Muestra en pantalla una etiqueda con la categoria principal seleccionada
     */
    $(document).on("click", "li", function () {
        var cat = $(this).text();

        var code_cat = aut_get_key_from_dataset(cat, app.dataset.user.categories.spend.sub);

        if (code_cat == null)
            code_cat = aut_get_key_from_dataset(cat, app.dataset.user.categories.revenue.sub);

        if (code_cat == null)
            return false;

        $.each(app.dataset.user.categories.spend.main, function (key, val) {
            if (code_cat.search(key) == 0) {
                $("#aut-log-" + aut_id_log + "-2 .tag-cat-main").html(val).show();
                return false;
            }
        });

        $.each(app.dataset.user.categories.revenue.main, function (key, val) {
            if (code_cat.search(key) == 0) {
                $("#aut-log-" + aut_id_log + "-2 .tag-cat-main").html(val).show();
                return false;
            }
        });
    });


});


function aut_flow_check_logs() {

    if ($("#table-aut-log .tr-log").length == 0) {
        $("#ui-message-modal").modal("hide");
    }
}


function aut_get_key_from_dataset(value, dataset) {

    var retr = null;

    $.each(dataset, function (key, val) {
        if (val == value) {
            retr = key;
            return false;
        }
    });

    return retr;
}


