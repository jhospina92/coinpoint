$(document).ready(function () {

    $("#select-client").change(function () {
        if ($(this).val().length > 0) {
            $("#connect").removeAttr("disabled");
        } else {
            $("#connect").attr("disabled", "disabled");
        }
    });
    
    
      $("#connect").click(function () {

        var client = $("#select-client").val();

        var url = $("#select-client option[value='" + client + "']").data("url");

        var parent = this;

        $(parent).setState("btn-default", "fa-spinner fa-loader-btn", msg_btn_state_connecting).attr("disabled", "disabled");

        $.ajax({
            method: "POST",
            url: url,
            data: {_token: app_token}
        }).done(function (msg) {

            var data = $.parseJSON(msg);
            window.location.href = data.response;
        });
    });
    
});