

function callback_toggle_active(response, id, text) {
    if (response) {
        $("#client-" + id + " .state").removeClass("danger-color").addClass("success-color").html('<i class="fa fa-link" aria-hidden="true"></i>').attr("title",app.trans.ui.label.linked);
    } else {
        $("#client-" + id + " .state").removeClass("success-color").addClass("danger-color").html('<i class="fa fa-unlink" aria-hidden="true"></i>').attr("title",app.trans.ui.label.unlinked);
    }
}