
var typayments_nobanks = ["CA"]; //Medios de pagos sin asociacion bancaria

$(document).ready(function () {
    //Actualiza la seleccion del tipo de moneda seleccionado
    $("#select-currency").change(function () {
        var currency = $(this).val();

        //$("#bank-assoc").attr("disabled", "disabled");
        $("#bank-assoc").html("<option>...</option>");

        $.ajax({
            method: "POST",
            url: ajax_get_banks,
            data: {_token: app_token, currency: currency}
        }).done(function (msg) {
            var data = $.parseJSON(msg);
            var set_value = $("#bank-assoc").data("value");

            $("#bank-assoc").html("<option value='' disabled selected>" + app.trans.ui.label.select + "</option>");
            $.each(data, function (i, e) {
                $("#bank-assoc").append("<option value=" + e[0] + " " + ((set_value == e[0]) ? "selected" : "") + ">" + e[1] + "</option>");
            });

            //$("#bank-assoc").removeAttr("disabled");
            initMaterialDesign();
        });
    }).trigger("change");


    $("#type").change(function () {

        /**
         * Muestra el campo de asociación bancaria con el producto seleccionado
         */


        if (typayments_nobanks.indexOf($("#type").val()) != -1 || $("#type").val() == null) {
            $("#select-bank").hide();
            $("#bank-assoc").removeAttr("name");
        } else {
            $("#select-bank").show();
            $("#bank-assoc").attr("name", "bank-assoc");
        }


        $("div[data-show-type]").each(function () {

            if ($(this).hasAttr("inload"))
                return true;

            var types = ($(this).attr("data-show-type")).split(",");
            if (types.indexOf($("#type").val()) > -1 && ($(this).data("vals") > 0 || $(this).data("vals") == undefined))
                $(this).show();
            else
                $(this).hide();
        });

    }).trigger("change");

});



/*
 |--------------------------------------------------------------------------
 | SECCIÓN AUTOREAD
 |--------------------------------------------------------------------------
 */

var aut_params = {};
var aut_type;
var aut_types;

$(document).ready(function () {

    if ($("textarea[name='autoread-params']").val().length > 0)
        aut_params = $.parseJSON($("textarea[name='autoread-params']").val());


    aut_types = [app.flow.type.spend, app.flow.type.revenue, app.flow.type.exchange];

    //Validación de asociación: 
    $("form").submit(function () {
        if ($("#autoread-check").is(":checked")) {

            if (Object.keys(aut_params).length == 0) {
                toastr.error(msg_error_not_params);
                hideLoadingSubmit(this);
                return false;
            }
            
            
            for (var i = 0; i < aut_types.length; i++) {
                
                if (aut_params[aut_types[i]] !== undefined && Object.keys(aut_params[aut_types[i]]).length < ($("#aut-select-param option").length - 1)) {
                    toastr.error(sprintf(msg_error_params_miss, app.trans.attr.flow[aut_types[i]]));
                    hideLoadingSubmit(this);
                    return false;
                }
            }

        }
    });

    $("#autoread-check").change(function () {
        if ($(this).is(":checked"))
            $("#sec-autoread").slideDown();
        else
            $("#sec-autoread").slideUp();
    }).trigger("change");

    $(".aut-btn-add-param").click(function () {
        $("#modal-params-autoread").modal("show");
        aut_type = $(this).data("type");
    });


    $("#aut-select-param").change(function () {
        var name = $(this).val();

        $("div#modal-params-autoread .help-default-msg .col-md-12").hide();
        $("div#modal-params-autoread div[data-name-help='" + name + "']").show();
        $("div#modal-params-autoread .alert").hide();

        if (name == null) {
            $("div#modal-params-autoread .help-default-msg div:first-child").show();
        }

    });


    $("#aut-btn-add").click(function () {
        var param = $("#aut-select-param").val();
        var url_validate = $(this).data("url-validate");
        var parent = this;

        var id_param = aut_type + "-param-" + param;

        $("div#modal-params-autoread .alert").hide();

        if (param == null) {
            $("div#modal-params-autoread .alert[data-type='not-select']").show();
            return false;
        }

        if ($("#" + id_param).length > 0) {
            toastr.error(msg_error_params_exists);
            return false;
        }

        var val = $("#input-text-add").val();

        $(parent).setState("btn-default", "fa-spinner fa-loader-btn").attr("disabled", "disabled");

        $.ajax({
            method: "POST",
            url: url_validate,
            data: {_token: app_token, param: param, val: val}
        }).done(function (msg) {

            var data = $.parseJSON(msg);

            if (typeof data.response === 'boolean' && data.response) {

                $("#" + aut_type + "-table-params").append("<tr id='" + id_param + "' class='param'><td>" + $("#aut-select-param option[value='" + param + "']").text() + "</td><td>" + val + "</td><td><i class='fa fa-remove' onclick=\"aut_remove_param('" + id_param + "','" + aut_type + "')\"></i></td></tr>");
                toastr.success(app.msg.success.default);

                $("#aut-select-param").setValueSelect("");
                $("#input-text-add").val("")
                $("#modal-params-autoread").modal("hide");

                if (aut_params[aut_type] === undefined)
                    aut_params[aut_type] = {};
                aut_params[aut_type][param] = val;

                $("textarea[name='autoread-params']").val(JSON.stringify(aut_params));

                $("#" + aut_type + "-msg-not-params").hide();
            } else {
                $("div#modal-params-autoread .alert[data-type='param-invalid']").html(data.response).show();
            }

            $(parent).setState("btn-success", "fa-check").removeAttr("disabled");

        });
    });

    //ENTER
    /*
     $("#input-text-add").keypress(function (e) {
     if (e.which == 13) {
     $("#aut-btn-add").trigger("click");
     }
     });*/
});



/** Elimina un parametro ingresado en pantalla
 * 
 * @param {type} id_param
 * @returns {undefined}
 */
function aut_remove_param(id_param, type) {


    $("#" + id_param).fadeOut(function () {
        $(this).remove();

        if ($("#" + type + "-table-params tr.param").length == 0) {
            $("#" + type + "-msg-not-params").show();
        }
    });

    delete aut_params[type][id_param.replace(type + "-param-", "")];
    $("textarea[name='autoread-params']").val(JSON.stringify(aut_params));

}