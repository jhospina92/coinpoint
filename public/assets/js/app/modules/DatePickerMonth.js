
$.fn.pickadatemonth = function (attrs) {
    var picker = new DatePickerMonth(this, attrs);
    picker.init();
}

function DatePickerMonth(obj, data) {

    this.date = new Date();
    this.date.setDate(1)

    this.displayFormat = "mm, yyyy";

    this.obj = obj;


    this.value = ($(obj).attr("data-value") != undefined) ? $(obj).attr("data-value") : null;
    this.format = ($(obj).attr("data-attr-format") != undefined) ? $(obj).attr("data-attr-format") : null;

    this.name = $(obj).attr("name");

    this.modalid = "#datem_root_" + this.name;

    var set_y_default = [];

    for (var i = this.date.getFullYear() - 5; i < (this.date.getFullYear() + 20); i++) {
        set_y_default.push(i);
    }

    this.data_set_y = ($(obj).attr("data-set-y") != undefined) ? $(obj).data("set-y") : set_y_default;
    this.data_set_m = ($(obj).attr("data-set-m") != undefined) ? $(obj).data("set-m") : [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];



    this.attrs = {
        monthsFull: data.monthsFull,
        monthsShort: data.monthsShort,
        weekdaysFull: data.weekdaysFull,
        weekdaysShort: data.weekdaysShort,
        today: data.today,
        clear: data.clear,
        close: data.close,
        labelMonthNext: data.labelMonthNext,
        labelMonthPrev: data.labelMonthPrev,
        labelMonthSelect: data.labelMonthSelect,
        labelYearSelect: data.labelYearSelect,
        format: data.format,
        formatSubmit: data.formatSubmit
    };
}

/** Inicializa
 * 
 * @returns {undefined}
 */
DatePickerMonth.prototype.init = function () {


    this.deploy();

    var parent = this;

    if (this.value == "today") {
        this.setDisplay(this.date);
    }

    if (this.value != null && this.value.length > 0 && this.value != "today") {

        var sep_val = this.getSeparator(this.value);
        var sep_format = this.getSeparator(this.format);

        var formatdata = this.format.split(sep_format);
        var data = this.value.split(sep_val);

        for (var i = 0; i < formatdata.length; i++) {

            if (formatdata[i].length == 4)
                this.date.setFullYear(data[i]);
            if (formatdata[i].length == 2)
                this.date.setMonth(parseInt(data[i] - 1));
        }

        this.setDisplay(this.date);
    }


    $(this.obj).focusin(function () {
        parent.openModal();
        $(parent.obj).focusout();
    });

    $(this.obj).keypress(function (e) {
        e.preventDefault();
    }).keyup(function (e) {
        e.preventDefault();
    }).keydown(function (e) {
        e.preventDefault();
    });


    $(this.modalid + " .picker__button--close").click(function () {
        parent.closeModal();
    });


    $(this.modalid + " .picker__button--today").click(function () {
        parent.date.setMonth($(parent.modalid + " .picker__select--month").val());
        parent.date.setFullYear($(parent.modalid + " .picker__select--year").val());
        parent.setDisplay(parent.date);
        parent.closeModal();
    });

    $(parent.modalid + " .picker__select--year").change(function () {

    });

    $(parent.modalid + " .picker__select--month").change(function () {
        $(parent.modalid + " .picker__month-display div").html(parent.getMonthNameFull($(this).val()))
    });

    $(parent.modalid + " .picker__select--year").change(function () {
        $(parent.modalid + " .picker__day-display div").html($(this).val());
    });

}

DatePickerMonth.prototype.getSeparator = function (value) {
    var data = value.split("");
    var separator = "";
    for (var i = 0; i < data.length; i++) {

        if (!/[0-9]/.test(data[i]) && !/[a-z]/.test(data[i]))
        {
            separator = data[i];
            break;
        }
    }

    return separator;
}

/** Despliega las opciones del HTML
 * 
 * @returns {undefined}
 */
DatePickerMonth.prototype.deploy = function () {

    $(this.obj).parent().append('<input type="hidden" id="' + this.name + '_submit" name="' + this.name + '_submit" value="">');

    var parent = this;

    var modal = "<div class='picker' id='" + this.modalid.replace("#", "") + "'>" +
            "<div class='picker__holder' tabindex='-1'><div class='picker__frame'>" +
            "<div class='picker__wrap'><div class='picker__box'><div class='picker__header'><div class='picker__date-display'>" +
            "<div class='picker__month-display'>" +
            "<div>" + this.getMonthNameFull(this.date.getMonth()) + "</div></div>" +
            "<div class='picker__day-display'><div>" + this.date.getFullYear() + "</div></div></div>" +
            "<select class='picker__select--month' aria-controls='date_table' title='" + this.attrs.labelMonthSelect + "' style='width:90%;'>";

    $.each(this.data_set_m, function (i, val) {
        var m = (val - 1);
        modal += "<option value='" + m + "' " + ((m == parent.date.getMonth()) ? "selected" : "") + ">" + parent.getMonthNameFull(m) + "</option>";
    });

    modal += "</select>";

    modal += "<select class='picker__select--year' aria-controls='date_table' title='" + this.attrs.labelYearSelect + "' style='width:90%;margin-top: 5px;'>";

    var y = this.date.getFullYear();

    $.each(this.data_set_y, function (i, y) {
        modal += "<option value='" + y + "' " + ((y == parent.date.getFullYear()) ? "selected" : "") + ">" + y + "</option>";
    });

    modal += "</select></div>" +
            "<div class='picker__footer'><button class='picker__button--close' style='width: 25%;' type='button' data-close='true' aria-controls='date'>" + this.attrs.close + "</button>" +
            "<button class='picker__button--today' type='button' style='width:75%;text-align:right;'>" + this.attrs.labelMonthSelect + "</button></div></div></div></div></div></div>";

    $(this.obj).parent().append(modal);
}

/** Retorna el nombre completo del mes dado por su valor nymero
 * 
 * @param {type} val
 * @returns {unresolved}
 */
DatePickerMonth.prototype.getMonthNameFull = function (val) {
    return this.attrs.monthsFull[val];
}

/*
 * Establece un valor de fecha en el campo del objecto objetivo dato por una clase de tipo Date(Fecha). 
 */
DatePickerMonth.prototype.setDisplay = function (date) {
    $(this.obj).parent().children("label").addClass("active");
    $(this.obj).val((this.displayFormat).replace("mm", this.getMonthNameFull(date.getMonth())).replace("yyyy", date.getFullYear()));
    $('#' + this.name + '_submit').val((this.format).replace("mm", (((date.getMonth() + 1) < 10) ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1))).replace("yyyy", date.getFullYear()));
}

DatePickerMonth.prototype.closeModal = function () {
    $(this.obj).removeClass("datepicker-month picker__input picker__input--active");
    $(this.modalid).removeClass("picker--opened picker--focused");
}

DatePickerMonth.prototype.openModal = function () {


    this.setModalYear(this.date.getFullYear());
    this.setModalMonth(this.date.getMonth());

    $(this.obj).addClass("datepicker-month picker__input picker__input--active");
    $(this.modalid).addClass("picker--opened picker--focused");
}

DatePickerMonth.prototype.setModalYear = function (y) {
    $(this.modalid + " .picker__select--year option").removeAttr("selected");
    $(this.modalid + " .picker__select--year option[value='" + y + "']").attr("selected", "selected").change();
}

DatePickerMonth.prototype.setModalMonth = function (m) {
    $(this.modalid + " .picker__select--month option").removeAttr("selected");
    $(this.modalid + " .picker__select--month option[value='" + m + "']").attr("selected", "selected").change();
}