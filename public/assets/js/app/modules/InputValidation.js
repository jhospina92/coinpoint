
$(document).ready(function () {
    $("input[validation]").focusout(function () {
        var val = new InputValidation(this, $(this).attr("validation"));
        val.run();
    });
});


function InputValidation(obj, type) {
    this.obj = obj;
    this.type = type;
}

InputValidation.prototype.run = function () {
    switch (this.type) {
        case "name":
            if (!this.vName())
                return false;
            break;
        case "email":
            if (!this.vEmail())
                return false;
            break;
        case "password":
            if (!this.vPassword())
                return false;
            break;
        case "compare":
            if (!this.vCompare())
                return false;
            break;
        case "check":
            if (!this.vCheck())
                return false;
            break;
    }

    this.showSuccess(app.msg.success.default);
    return true;
}

/** Valida si el campo de tipo nombre es valido
 * 
 * @returns {Boolean}
 */
InputValidation.prototype.vName = function () {
    var val = $(this.obj).val();

    if (!(/^[a-zA-Z ]+$/.test(val)) || val.length < 5) {
        this.showError(sprintf(app.msg.error.input.format.invalid, this.getLabel()));
        return false;
    }
    return true;
}

InputValidation.prototype.vEmail = function () {
    var val = $(this.obj).val();

    if (!(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(val))) {
        this.showError(sprintf(app.msg.error.input.format.invalid, this.getLabel()));
        return false;
    }

    return true;
}

InputValidation.prototype.vPassword = function () {

    var val = $(this.obj).val();

    if (!(/[A-Z]/g.test(val)) || val.length < 8) {
        this.showError(app.msg.error.input.password.invalid);
        return false;
    }


    return true;
}

InputValidation.prototype.vCompare = function () {
    var val = $(this.obj).val();
    var inputcompare = $(this.obj).attr("data-compare");

    if ($(inputcompare).val() != val || $(inputcompare).val().length < 8) {
        this.showError(app.msg.error.input.compare.values);
        return false;
    }

    return true;
}

InputValidation.prototype.vCheck = function () {

    var msg_error = $(this.obj).attr("data-msg-error");

    if (!$(this.obj).is(":checked")) {
        toastr.error(msg_error);
        return false;
    }

    return true;
}


InputValidation.prototype.getLabel = function (obj) {
    var parent = (obj == undefined) ? $(this.obj).parent() : $(obj).parent();

    if (parent.children("label").length == 0)
        parent.append("<label></label>");

    var label = parent.children("label");

    return label.text();
}

InputValidation.prototype.showSuccess = function (msg) {
    var obj = this.obj;

    if (msg.length == 0)
        return;

    $(obj).removeClass("invalid");

    var parent = $(obj).parent();

    if (parent.children("label").length == 0)
        parent.append("<label></label>");

    var label = parent.children("label");

    if (!$(obj).hasClass("valid")) {
        $(obj).addClass("valid");
    }

    if ($(obj).tagName() == "input")
        label.attr("data-success", msg);

    if ($(obj).tagName() == "select")
    {
        parent.addClass("valid");
        parent.append("<div class='success-select'>" + msg + "</div>");
    }
}

InputValidation.prototype.showError = function (msg) {

    var obj = this.obj;

    if (msg.length == 0)
        return;

    $(obj).removeClass("valid");

    var parent = $(obj).parent();

    if (parent.children("label").length == 0)
        parent.append("<label></label>");


    var label = parent.children("label");

    if (!$(obj).hasClass("invalid")) {
        $(obj).addClass("invalid");
    }

    if ($(obj).tagName() == "input")
        label.attr("data-error", msg);

    //if ($(obj).val().length == 0)
    //   label.attr("data-error", "");

    if ($(obj).tagName() == "select")
    {
        parent.addClass("invalid");
        parent.append("<div class='error-select'>" + msg + "</div>");
    }

}