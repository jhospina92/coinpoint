/** Retorna el nombre de la etiqueta del elemento HTML
 * 
 * @returns {$.fn@call;prop@call;toLowerCase}
 */
$.fn.tagName = function () {
    return this.prop("tagName").toLowerCase();
};

/** Deshabilita una opción del objetivo tipo select, dado por su indice de posición. 
 * @param {type} index
 * @returns {undefined}
 */
$.fn.disabledSelectOption = function (val) {
    if ($(this).tagName() != "select")
        return;
    $(this).parent().find("ul li[data-value='" + val + "']").addClass("disabled");
    $(this).find("option[value='val']").addClass("disabled selected");
}
/** Habilita una opción del objetivo tipo select, dado por su indice de posición. 
 * @param {type} val
 * @returns {undefined}
 */
$.fn.enabledSelectOption = function (val) {

    var select = this;

    if ($(this).tagName() != "select")
        return;

    if (val == undefined)
    {
        $(this).find("option").each(function () {
            //Si no es un elemento que no tiene valor (Por defecto el elemento de "Seleccionar")
            if ($(this).val().length > 0) {
                $(this).removeClass("disabled selected");
                $(select).parent().children("ul").children("li").removeClass("disabled");
            }
        });
    } else {
        $(this).parent().find("ul li[data-value='" + val + "']").removeClass("disabled");
        $(this).find("option[value='" + val + "']").removeClass("disabled selected");
    }
}

$.fn.setValueSelect = function (val) {
    $(this).val(val).change();
    $(this).parent().children("ul").children("li").removeClass("active");
    $(this).parent().children("ul").children("li:nth-child(" + ($(this).children("option[value='" + val + "']").index() + 1) + ")").addClass("active");
    $(this).parent().children("input[type='text']").val($(this).children("option[value='" + val + "']").text());
}

/** Indica si un elemento es visible en pantalla
 * 
 * @returns {Boolean}
 */
$.fn.isVisible = function () {

    if ($(this).length == 0)
        return false;

    if ($(this).tagName() == "select") {
        var element = $(this).parent().children(".select-dropdown");
        return (element.length > 0 && element.css('visibility') !== 'hidden' && element.css('display') !== 'none') && (element.is(":visible"));
    }
    return $(this).is(":visible");
}

/** Extrae los numeros de un texto
 * 
 * @returns {unresolved}
 */
$.fn.extractInteger = function () {
    var num = parseFloat($(this).val().replace(/[^\d]/g, ''));
    return isFinite(num) ? num : 0;
}


String.prototype.extractInteger = function () {
    return parseFloat(this.replace(/[^\d]/g, ''));
}

/** Pone un texto en vertical
 * 
 * @returns String
 */
String.prototype.verticalText = function () {
    return ((this).split("")).join("<br/>");
}



/** Indica si un elemento contiene un atributo
 * 
 * @param {type} name
 * @returns {Boolean}
 */
$.fn.hasAttr = function (name) {
    return this.attr(name) !== undefined;
};

/** Cuenta la cantidad de veces que aparece un cadena de texto
 * 
 * @param {type} s1
 * @returns {Number}
 */
String.prototype.count = function (s1) {
    return this.split(s1).length - 1;
}

$.fn.reverse = function () {
    return this.pushStack(this.get().reverse(), arguments);
};


/** Establece un nuevo estado a un Botón
 * 
 * @param String btnClass La nueva clase de estado dado por btn-{estado}
 * @param String iconClass La nueva clase del icono (Si existe)
 * @param String text Un nuevo texto para el botón
 * @returns void
 */
$.fn.setState = function (btnClass, iconClass, text) {
    btnClass = (btnClass === undefined) ? null : btnClass;
    iconClass = (iconClass === undefined) ? null : iconClass;
    text = (text === undefined) ? null : text;

    parent = this;

    if (btnClass !== null) {
        $.each(["btn-default", "btn-info", "btn-success", "btn-warning", "btn-danger", "btn-destroy"], function (i, valClass) {
            $(parent).removeClass(valClass);
        });
        $(parent).addClass(btnClass);
    }
    //If icon exists
    if ($(parent).children("i").length > 0 && iconClass !== null) {
        var icon = $(parent).children("i").attr("class", "fa " + iconClass).get()[0].outerHTML;
    }

    if (text !== null) {
        $(parent).html(((icon !== undefined) ? icon.toString() : "") + " " + text);
    }

    $(parent).removeAttr("disabled");

    return $(parent);
};