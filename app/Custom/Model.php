<?php

namespace App\Custom;

use \Illuminate\Database\Eloquent\Model as ModelEloquent;
use Illuminate\Support\Facades\Auth;

class Model extends ModelEloquent {

    /**
     *  Indica si el usuario en sesion es el propietario del registro dado su id
     */
    public static function isOwnerAuth($id) {
        $class = get_called_class();

        $model = $class::find($id);

        if (empty($model))
            return false;

        if (empty($model->user_id))
            return false;

        return ($model->user_id == Auth::user()->id);
    }

}
