<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Wildo\Resources\UI;

use App\Models\Wildo\WReport;
use Illuminate\Support\Facades\Session;

/**
 * Description of Message
 *
 * @author jospina
 */
class Message {

    public $message;
    private $object;
    private $type;
    private $data;
    private $time;

    public function __construct(WReport $report) {
        $this->object = $report;
        $this->type = $report[WReport::ATTR_TYPE];
        $this->message = $report[WReport::ATTR_MESSAGE];
        $this->data = json_decode($report[WReport::ATTR_DATA]);
        $this->time = $report[WReport::ATTR_TIME];
        $this->assignMessage();
    }

    private function assignMessage() {

        $data = array();

        foreach ($this->data as $index => $value) {
            $data[] = $value;
        }

        if (count($data) > 0)
            $this->message = vsprintf(trans("wildo." . $this->message), $data);
        else
            $this->message = trans("wildo." . $this->message);
    }

    /**
     * Establece el mensaje a mostrar en una variable de sesión
     */
    public function load() {
        Session::flash("wildo.report.type", $this->type);
        Session::flash("wildo.report.message", $this->message);
        if (is_null($this->time)) {
            $this->object[WReport::ATTR_VIEWED] = true;
            $this->object->save();
        }
    }

}
