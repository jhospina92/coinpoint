<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Wildo;

use Illuminate\Support\Facades\Auth;
use App\Models\Wildo\WReport;
use App\Wildo\Resources\UI\Message;

/**
 * Description of Wildo
 *
 * @author jospina
 */
class Wildo {

    const ENV_REPORT = "report";

    public static function run() {
        self::reports();
    }

    private static function reports() {
        if (!Auth::check())
            return;

        $reports = WReport::get();

        foreach ($reports as $report) {

            $message = new Message($report);
            $message->load();
        }
    }

}
