<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        //Crear una variable que se pasa a todas las vistas cargadas para indicar el nombre de la vista
        view()->composer('*', function($view) {
            view()->share('view_name', $view->getName());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

}
