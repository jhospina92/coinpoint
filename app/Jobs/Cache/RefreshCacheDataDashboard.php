<?php

namespace App\Jobs\Cache;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use App\Models\Flow;

class RefreshCacheDataDashboard implements ShouldQueue {

    use InteractsWithQueue,
        Queueable,
        SerializesModels;

    protected $user_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $user_id) {
        $this->user_id = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        $cachekey = $this->user_id . "-dashboard";
        Cache::forget($cachekey);
        Cache::forever($cachekey, Flow::getDataDashboard($this->user_id));
    }

}
