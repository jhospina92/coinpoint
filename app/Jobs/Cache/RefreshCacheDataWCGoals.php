<?php

namespace App\Jobs\Cache;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Cache;
use App\Base\UI\WebComponents\WCGoalProgress;

class RefreshCacheDataWCGoals implements ShouldQueue {

    use InteractsWithQueue,
        Queueable,
        SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $user_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $user_id) {
        $this->user_id = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        $cachekey = $this->user_id . "-wc-goals";
        Cache::forget($cachekey);
        Cache::forever($cachekey, WCGoalProgress::getHtml($this->user_id));
    }

}
