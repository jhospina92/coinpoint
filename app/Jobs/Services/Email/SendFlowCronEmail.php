<?php

namespace App\Jobs\Services\Email;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\FlowCron;

class SendFlowCronEmail implements ShouldQueue {

    use InteractsWithQueue,
        Queueable,
        SerializesModels;

    private $users = [];
    
    public $tries = 3;

    public function __construct(array $users) {
        $this->users = $users;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {

        for ($i = 0; $i < count($this->users); $i++) {
            FlowCron::handler($this->users[$i]);
        }
    }

}
