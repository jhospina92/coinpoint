<?php

namespace App\Jobs\Services\Auto;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;
use App\Base\System\Services\ReaderEmail\ReaderEmail;
use App\Models\Auto\AutoRead;

class ProcessAutoRead implements ShouldQueue {

    use InteractsWithQueue,
        Queueable,
        SerializesModels;

    private $users = [];
    public $tries = 3;

    public function __construct(array $users) {
        $this->users = $users;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {

        for ($i = 0; $i < count($this->users); $i++) {
            $user = User::find($this->users[$i]);
            $typayments = $user->typayments;

            foreach ($typayments as $typayment) {

                foreach ($typayment->autoreads as $autoread) {

                    if (!$autoread[AutoRead::ATTR_ACTIVE])
                        continue;

                    $reader = new ReaderEmail($autoread);
                    $reader->run();
                }
            }
        }
    }

}
