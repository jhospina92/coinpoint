<?php

namespace App\Jobs\Providers;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Base\System\Library\DataProviders\CurrencyDataProvider;

class RefreshCurrencyRates implements ShouldQueue {

    use InteractsWithQueue,
        Queueable,
        SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        CurrencyDataProvider::refresh();
    }

}
