<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Base\System\Services\ReaderEmail;

use ReflectionClass;
use App\Base\System\Library\DataProviders\APIs\Mails\Build\MailClient;
use App\Base\System\Library\DataProviders\APIs\Mails\Build\Payload\EmailMessage;
use App\Base\System\Services\ReaderEmail\DataParam;
use App\Models\Auto\AutoRead;
use App\Models\Logs\LogAutoReadFailed as LogError;
use App\Models\Logs\LogAutoReadProcessed as LogSuccess;
use Exception;
use App\Base\Data\Currency;
use App\Models\Typayment;
use App\Base\System\Library\Comp\FineDiff;
use App\Base\System\Library\Comp\RegexExtract;
use Illuminate\Support\Facades\Log;

/**
 * Description of ReaderEmail
 *
 * @author jospina
 */
class ReaderEmail {

    /**
     *
     * @var MailClient; 
     */
    private $mailclient;

    /**
     *
     * @var DataParam
     */
    private $params;

    /**
     *
     * @var AutoRead
     */
    private $autoread;
    private $type;
    private $id;
    private $content;
    private $typayment_id;

    public function __construct(AutoRead $autoread, $params = []) {
        $this->mailclient = $autoread->getClientObject();
        $this->params = new DataParam((!empty($autoread->pivot->params)) ? json_decode($autoread->pivot->params, true) : $params);
        $this->autoread = $autoread;
        $this->type = $autoread->pivot->type ?? null;
        $this->typayment_id = $autoread->pivot->typayment_id ?? null;
    }

    public function run() {

        if (is_null($this->mailclient)) {
            Log::warning("The mail client for [Autoread:" . $this->autoread->id . "] could not be loaded");
            return;
        }

        $param_subject = $this->params->getInput_subject();

        //Retorna un array con el listado de los ultimos 5 correos del usuario con la fecha actual
        $messages = $this->mailclient->getMessages($this->params->getFrom(), $param_subject, (env('APP_DEBUG', false)) ? false : date("Y-m-d"), 5);

        foreach ($messages as $_message) {

            //[HELPER IDE]
            $message = new EmailMessage(null, null, null, null, null);
            $message = $_message;

            //Verifica que el correo no haya sido procesado anteriormente
            if ($this->autoread->existsMessageIdInLogSuccess($message->getId()) || $this->autoread->existsMessageIdInLogFail($message->getId()))
                continue;

            //Si el parametro del asunto esta definido lo filtrara por este
            if (!is_null($param_subject)) {
                if ($message->getSubject() != $param_subject)
                    continue;
            }

            $this->id = $message->getId();
            $this->content = mb_strtolower(strip_tags($message->getBody()));

            //Si estos valores no se extraen correctamente sera registrado como un error
            try {
                $date = $this->extractDate();
                $value = $this->extractValue();
                $description = $this->extractDescription();
            } catch (Exception $e) {
                $this->autoread->addLogError($this->id, $e->getMessage(), $this->type, $this->typayment_id);
                break;
            }

            $hour = $this->extractHour();

            $data = ["typayment" => $this->typayment_id,
                "type" => $this->type,
                "date" => $date . " " . ($hour ?? "00:00:00"),
                "value" => $value,
                "description" => $description];

            $this->autoread->addLogSuccess($this->id, $data);
        }
    }

    /** Extrae la fecha del mensaje dado un formato en (YYYY-MM-DD)
     * 
     * @return type
     * @throws Exception
     */
    private function extractDate() {
        $date = RegexExtract::date($this->content);

        if ($date === false)
            throw new Exception(LogError::ERROR_DATE_NOT_FOUND);

        return $date;
    }

    /** Extrae la hora del mensaje dado el formato por defecto HH:MM (Disponible por el momento solo para el formato de 24 horas)
     * 
     * @return type
     * @throws Exception
     */
    private function extractHour() {

        $hour = RegexExtract::time($this->content);

        if ($hour === false)
            throw new Exception(LogError::ERROR_HOUR_NOT_FOUND);

        return $hour;
    }

    /** Extrae el valor de la transacción del mensaje
     * 
     * @return type
     * @throws Exception
     */
    private function extractValue() {

        $typayment = Typayment::find($this->autoread->pivot->typayment_id);

        $currency = new Currency($typayment->currency);

        $currency_symbol = html_entity_decode($currency->getSymbol());

        //Normaliza el texto
        $text = str_replace(array(" ", "-"), "", $this->content);

        $val = RegexExtract::moneyValue($text);


        if ($val === false)
            throw new Exception(LogError::ERROR_VALUE_NOT_FOUND);

        $val = str_replace($currency_symbol, "", $val); //"21.050,00";
        
        //Verifica que el valor sea inferior a 1000
        if (preg_match('/^[0-9]{1,3}(\,|\.)?[0-9]{0,2}$/', $val)) {
            return floatval(str_replace(",", ".", $val));
        }

        if (!preg_match('/(\,|\.)/', $val, $sep)) {
            throw new Exception(LogError::ERROR_VALUE_NOT_IDENTIFIED);
        }

        $sep = $sep[0]; //[.]

        return floatval(str_replace($sep, "", $val)); //21050.00
    }

    //Extraer la descripción de la transacción
    private function extractDescription() {
        $content = $this->content;

        $format = $this->params->getInput_description_format();

        if (is_null($format))
            return null;

        if (strpos($format, "XYZ") === false) {
            throw new Exception(LogError::ERROR_DESCRIPTION_FORMAT_INVALID);
        }

        $regex = "";
        foreach (str_split($format) as $index => $char) {
            if (!preg_match('/[a-zA-Z]/', $char))
                $regex .= "\\$char";
            else
                $regex .= "$char";
        }

        $regex = str_replace("XYZ", "(\w.+)", $regex);

        if (!preg_match("/" . $regex . "/", $content, $match)) {
            throw new Exception(LogError::ERROR_DESCRIPTION_NOT_FOUND);
        }

        $text = ucfirst(mb_strtolower($match[1] ?? null));


        return $text;
    }

    /** Autogenera el parametro del formato de descripción del a transacción
     * 
     * @global type $format
     * @return type
     */
    public function autogenerateParamForDescription() {

        $param_subject = $this->params->getInput_subject();

        $messages = $this->mailclient->getMessages($this->params->getFrom(), $param_subject, false, 2);

        if (count($messages) < 2) {
            return null;
        }

        $body1 = mb_strtolower(strip_tags($messages[0]->getBody()));
        $body2 = mb_strtolower(strip_tags($messages[1]->getBody()));

        $opcodes = FineDiff::getDiffOpcodes($body1, $body2, FineDiff::wordDelimiters);

        $diffs = [];

        //Obtiene la diferencia entre los dos mensajes de correo
        FineDiff::renderFromOpcodes($body1, $opcodes, function($opcode, $from, $from_offset, $from_len) use (&$diffs) {
            if ($opcode === 'i') {
                $diffs[] = array($from_offset, $from_len, htmlspecialchars(mb_substr($from, $from_offset, $from_len), ENT_QUOTES));
            }
        });


        global $format;
        //Almacenara el formato
        $format = null;

        foreach ($diffs as $diff) {

            list($offset, $len, $text) = $diff;

            //Saltar si es una fecha, una hora,un valor monetario, o un numero
            if (RegexExtract::date($text) !== false ||
                    RegexExtract::time($text) !== false ||
                    RegexExtract::moneyValue($text) !== false ||
                    is_int($text) ||
                    is_float($text)) {
                continue;
            }

            //Precisión de busqueda de formato de descripción
            $lensearch = 21;

            do {
                $lensearch--;

                $txt1 = mb_substr($body2, strpos($body2, $text) - $lensearch, $len + ($lensearch * 2));
                $txt2 = mb_substr($body1, strpos($body2, $text) - $lensearch, $len + ($lensearch * 2));

                $opcodes = FineDiff::getDiffOpcodes($txt1, $txt2);

                $diffs = [];

                //Construye el formato
                FineDiff::renderFromOpcodes($txt2, $opcodes, function($opcode, $from, $from_offset, $from_len) use ($txt1) {

                    global $format;

                    $text = htmlspecialchars(mb_substr($from, $from_offset, $from_len));

                    if (strpos($txt1, $text) === 0 && is_null($format)) {
                        $format = $text;
                    }

                    if ((strpos($txt1, $text) + strlen($text)) === strlen($txt1)) {
                        $format .= "XYZ" . $text;
                    }
                });

                if (strpos($format, "XYZ") === false) {
                    $format = null;
                }
            } while (empty($format) && $lensearch > 0);
        }

        $f = $format;

        unset($format);
        unset($GLOBALS["format"]);

        return $f;
    }

    /** Retorna un array con el listado de parametros de lectura
     * 
     * @param type $index
     * @return type
     */
    public static function getParams($index = null) {
        return DataParam::getList($index);
    }

    /** Retorna el texto legible del nombre del parametro
     * 
     * @param type $data
     * @return type
     */
    public static function getNameParam($data) {
        return DataParam::getName($data);
    }

    /** Valida si el valor de un parametro es valido
     * 
     * @param string $param
     * @param string $val
     */
    public static function isValidParam(string $param, string $val) {
        return DataParam::isValidParam($param, $val);
    }

}
