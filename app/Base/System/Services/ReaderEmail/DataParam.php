<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Base\System\Services\ReaderEmail;

use ReflectionClass;

class DataParam {

    const PARAM_FROM = "from";
    const PARAM_INPUT_SUBJECT = "input-subject";
    const AUTOPARAM_INPUT_DESCRIPTION_FORMAT = "input-description-format";

    private $from = null;
    private $input_subject = null;
    private $input_description_format = null;

    function __construct($params = array()) {

        foreach ($params as $param => $value) {
            if (self::isExist($param)) {
                $this->{trim(str_replace("-", "_", $param))} = $value;
            }
        }
    }

    /** Indica si el paramentro existe
     * 
     * @param type $param
     * @return type
     */
    public static function isExist($param) {
        return (isset(self::getList()[$param]) || isset(self::getListAuto()[$param]));
    }

    public static function getList($index = null) {
        $class = new ReflectionClass(__CLASS__);

        $return = [];

        foreach ($class->getConstants() as $name => $data) {
            if (strpos($name, "PARAM_") !== false && strpos($name, "PARAM_") == 0)
                $return[$data] = self::getName($data . $index);
        }

        return $return;
    }

    public static function getListAuto($index = null) {
        $class = new ReflectionClass(__CLASS__);

        $return = [];

        foreach ($class->getConstants() as $name => $data) {
            if (strpos($name, "AUTOPARAM_") !== false && strpos($name, "AUTOPARAM_") == 0)
                $return[$data] = self::getName($data . $index);
        }

        return $return;
    }

    public static function getName($data) {
        return trans("attr.reader.email.param." . $data);
    }

    public static function isValidParam(string $param, string $val) {

        $params = self::getList();

        if (!isset($params[$param]))
            return false;

        switch ($param) {
            case self::PARAM_FROM:

                if (!filter_var($val, FILTER_VALIDATE_EMAIL)) {
                    return trans("ui.msg.error.email.invalid");
                }

                break;
            case self::PARAM_INPUT_SUBJECT:
                if (!is_string($val) || strlen($val) > 200) {
                    return trans("ui.msg.error.email.invalid");
                }
                break;
        }


        return true;
    }

    
    function getFrom() {
        return $this->from;
    }

    function getInput_subject() {
        return $this->input_subject;
    }

    function getInput_description_format() {
        return $this->input_description_format;
    }

    function setFrom($from) {
        $this->from = $from;
    }

    function setInput_subject($input_subject) {
        $this->input_subject = $input_subject;
    }

    function setInput_description_format($input_description_format) {
        $this->input_description_format = $input_description_format;
    }


}
