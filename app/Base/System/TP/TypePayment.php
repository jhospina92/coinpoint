<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Base\System\TP;

use App\Base\Data\Currency;
use App\Base\Data\Bank;
use App\Base\Data\TypePaymentData;
use App\Base\System\TP\{
    Cash,
    CreditCard,
    BankAccount
};
use ReflectionClass;

/**
 * Description of TypePayment
 *
 * @author jospina
 */
class TypePayment {

    protected $id; //Identificador del tipo de pago
    protected $balance = 0; //Saldo
    protected $currency; // Tipo de moneda
    protected $bank = null; //Banco

    /** Constructor por defecto
     * 
     * @param int $balance
     * @param Currency $currency
     */

    function __construct(float $balance, Currency $currency, $features = null) {
        $this->balance = $balance;
        $this->currency = $currency;
    }

    public static function load($type, $balance, $currency, array $data): TypePayment {
        $types = TypePaymentData::getList("class");

        foreach ($types as $index => $class) {
            if ($index == $type) {
                break;
            }
        }

        $typayment = new $class($balance, new Currency($currency));
        $typayment->setID($type);

        if (method_exists($typayment, "features")) {
            $typayment->features($data);
        }

        if (isset($data[TypePaymentData::BANK]))
            $typayment->setBank(new Bank($data[TypePaymentData::BANK]));

        return $typayment;
    }

    /** Obtiene un array con el listado de campos adicionales de un tipo de pago
     * 
     * @return type
     */
    public static function getFields() {
        $class = new ReflectionClass(get_called_class());

        $return = [];

        foreach ($class->getConstants() as $const => $data) {
            if (strpos($const, "FIELD_") !== false)
                $return[$data["name"]] = array_merge($data, ["name" => self::getFieldName($data["name"]), "help" => self::getFieldHelpText($data["name"])]);
        }

        return $return;
    }

    /** Obtiene el titulo de un campo dado por su nombre
     * 
     * @param type $value
     * @return type
     */
    public static function getFieldName($value) {
        return trans("attr.typepayment." . get_called_class()::ID . ".field." . $value);
    }

    /** Obtiene la descripción de ayuda de un campo dado por su nombre
     * 
     * @param type $value
     * @return type
     */
    public static function getFieldHelpText($value) {
        $index = "attr.typepayment." . get_called_class()::ID . ".field." . $value . ".help";
        return \Illuminate\Support\Facades\Lang::has($index) ? trans($index) : null;
    }

    /** Indica si algun parametro del tipo de pago puede ser editado. Si el paramentro no tiene la directiva establecida retornara True por defecto
     * 
     * @param type $name
     * @return boolean
     */
    public static function paramManageEdit($name) {
        $class = get_called_class();
        $param = $class . "::PARAM_MANAGE_EDIT_" . mb_strtoupper($name);

        if (defined($param))
            return constant($param);

        return true;
    }

    public function setBank(Bank $bank) {
        $this->bank = $bank;
    }

    /** Retorna el nombre de un tipo de pago
     * 
     * @param $bank [false] Indica si se debe imprimir junto con el nombre del banco (Si aplica)
     * @return type
     */
    public static function getName($bank = false) {
        $class = get_called_class();

        if (!defined($class . "::ID"))
            return null;

        $name = trans("attr.typepayment.name." . $class::ID);

        return $name;
    }

    /** Retorna el nombre del tipo de pago con sus datos asociados
     * 
     * @return type
     */
    public function getFullName() {

        $class = get_called_class();

        if (!defined($class . "::ID"))
            return null;

        if ($this->hasBankAssoc()) {
            return trans("attr.typepayment.name." . $class::ID) . " (" . $this->getBankName() . "" . (($class == CreditCard::class && !is_null($numbers = $this->getEndNumbers())) ? " - " . $numbers : "") . ")";
        } else {
            return trans("attr.typepayment.name." . $class::ID);
        }
    }

    public function getBalance() {
        return $this->balance;
    }

    public function setBalance($balance) {
        return $this->balance = $balance;
    }

    /** Imprime el valor del saldo en formato de moneda legible
     * 
     * @return type
     */
    public function getTextBalance() {

        list($decimals, $t, $f) = $this->currency->getFormat();

        return $this->currency->getSymbol() . " " . number_format($this->balance, $decimals, $f, $t) . " " . $this->currency->getCode();
    }

    /* Indica si tiene un banco asociado
     * 
     * @return type
     */

    public function hasBankAssoc(): bool {
        return (!is_null($this->bank));
    }

    /** Retorna el objeto del banco asociado
     * 
     * @return Bank
     */
    public function getBank(): Bank {
        return $this->bank;
    }

    /** Retorna el nombre del banco asociado al tipo de pago
     * 
     * @return type
     */
    public function getBankName() {
        if (!$this->hasBankAssoc())
            return null;

        return $this->bank->getName();
    }

    public function getID() {
        return $this->id;
    }

    public function setID($id) {
        $this->id = $id;
    }

    public function getCurrency() {
        return $this->currency;
    }

    public function setCurrency(Currency $currency) {
        $this->currency = $currency;
    }

    /** Retorna un array con las caracteristicas del medio de pago, asociado los medios de pagos
     * 
     * @return type
     */
    public function getDataFeatures() {
        return [];
    }

    /** Retorna un String JSON con parametros que definen la clase
     * 
     * @return string
     */
    public function __toString(): string {
        return $this->getAttrsJSON();
    }

    protected function getAttrsJSON() {
        $data = array(
            TypePaymentData::BALANCE => $this->balance,
            TypePaymentData::CURRENCY => $this->currency->getCode()
        );

        if ($this->hasBankAssoc())
            $data[TypePaymentData::BANK] = $this->bank->getCode();

        return json_encode($data, true);
    }

}
