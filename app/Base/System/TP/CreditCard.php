<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Base\System\TP;

use App\Base\System\TP\TypePayment;
use ReflectionClass;
use App\Base\System\Library\Comp\Util;
use App\Base\Data\Lang;

/**
 * Description of CreditCard
 *
 * @author jospina
 */
class CreditCard extends TypePayment {

    const ID = "CC";

    private $quouta = 0;
    private $date_day_cut = null; //El día de corte de la tarjeta
    private $date_day_pay = null; // El día de pago de la tarjeta
    private $franchise = null; // La franquicia
    private $end_numbers = null; // Los ultimos 4 numeros de la tarjeta
    private $default_fees = null; //Numero de cuotas por defecto

    //Features

    const FIELD_DATE_DAY_CUT = ["name" => "date-cut", "type" => "select", "required" => true, "data" => "parse::_html_data_select_date_day"];
    const FIELD_DATE_DAY_PAY = ["name" => "date-pay", "type" => "select", "required" => true, "data" => "parse::_html_data_select_date_day"];
    const FIELD_FRANCHISE = ["name" => "franchise", "type" => "select", "required" => true, "data" => "parse::_html_data_select_franchises"];
    const FIELD_END_NUMBERS = ["name" => "end_numbers", "type" => "number", "required" => true, "html" => ["attrs" => ["maxlength" => "4"]]];
    const FIELD_DEFAULT_FEES = ["name" => "default_fees", "type" => "number", "required" => true, "html" => ["attrs" => ["maxlength" => "2"]]];
    //Params
    const PARAM_MANAGE_EDIT_TYPE = false;
    const PARAM_MANAGE_EDIT_CURRENCY = false;
    const PARAM_MANAGE_EDIT_VALUE = true;
    const PARAM_MANAGE_EDIT_BANK = false;

    /** Establece las caracteristicas de la tarjeta de credito pasando un array asociativo con los datos.
     * 
     * @param type $data
     */
    public function features($data) {
        $this->quouta = $data["balance"] ?? 0;
        $this->date_day_cut = $data["date-cut"] ?? null;
        $this->date_day_pay = $data["date-pay"] ?? null;
        $this->franchise = $data["franchise"] ?? null;
        $this->end_numbers = $data["end_numbers"] ?? null;
        $this->default_fees = $data["default_fees"] ?? 12;
    }

    /** [Override] Retorna un array con las caracteristicas del medio de pago 
     * 
     * @return Array
     */
    function getDataFeatures() {

        $data = [];
        $data["date-cut"] = $this->date_day_cut;
        $data["date-pay"] = $this->date_day_pay;
        $data["franchise"] = $this->franchise;
        $data["end_numbers"] = $this->end_numbers;
        $data["default_fees"] = $this->default_fees;

        return $data;
    }

    /** Retorna el valor del cupo
     * 
     * @return type
     */
    public function getQuota() {
        return $this->quouta;
    }

    public function getDateDayCut() {
        return $this->date_day_cut;
    }

    public function getDateDayPay() {
        return $this->date_day_pay;
    }

    function getFranchise() {
        return $this->franchise;
    }

    function getEndNumbers() {
        return $this->end_numbers;
    }

    function getDefaultFees() {
        return $this->default_fees;
    }

    public static function _parse($data) {

        if (strpos($data, "parse::") !== false) {
            return call_user_func(__CLASS__ . "::" . str_replace("parse::", "", $data));
        }

        return $data;
    }

    public function __toString(): string {
        $data = json_decode($this->getAttrsJSON(), true);

        if (!empty($this->date_day_cut))
            $data["date-cut"] = $this->date_day_cut;

        if (!empty($this->date_day_pay))
            $data["date-pay"] = $this->date_day_pay;

        if (!empty($this->franchise))
            $data["franchise"] = $this->franchise;

        if (!empty($this->date_day_pay))
            $data["end_numbers"] = $this->end_numbers;

        if (!empty($this->default_fees))
            $data["default_fees"] = $this->default_fees;

        return json_encode($data);
    }

    private static function _html_data_select_date_day() {
        $data = [];
        $data[""] = trans("ui.label.select");
        for ($i = 1; $i <= 30; $i++)
            $data[$i] = sprintf(trans("ui.label.every.day.month"), $i);
        return $data;
    }

    private static function _html_data_select_franchises() {
        $data = [];
        $data[""] = trans("ui.label.select");

        $franchises = Franchise::getList();

        foreach ($franchises as $index => $value) {
            $data[$value[0]] = [$value[1], $value[2]];
        }

        return $data;
    }

}

/**
 * Agregado en la versión 0.8.0
 *  -> Franquicias de tarjetas de credito
 */
class Franchise {

    const MASTERCARD = ["MT", "Mastercard", "mastercard.png"];
    const VISA = ["VI", "Visa", "visa.png"];
    const AMERICAN_EXPRESS = ["AE", "American Express", "americanexpress.jpg"];
    const DINERS_CLUB = ["DC", "Diners Club", "dinersclub.png"];
    const DISCOVER_CARD = ["DD", "Discover Card", "discovercard.jpg"];
    const OTHER = ["OT", "trans[ui.label.other]", "franchise.png"];

    private $name;
    private $code;
    private $image;

    function __construct(string $code) {

        foreach (self::getList() as $index => $data) {
            list($codeGiv, $name, $image) = $data;
            if ($code == $codeGiv) {
                $this->name = Lang::adapt($name);
                $this->code = $code;
                $this->image = url("assets/images/res/data/franchises/$image");
                return;
            }
        }

        throw new \Exception("Franchise don't exists");
    }

    function getName() {
        return $this->name;
    }

    function getCode() {
        return $this->code;
    }

    function getImage() {
        return $this->image;
    }

    public static function getList() {
        $class = new ReflectionClass(__CLASS__);

        $return = [];

        foreach ($class->getConstants() as $name => $data) {
            list($code, $name, $image) = $data;
            $return[] = [$code, Lang::adapt($name), url("assets/images/res/data/franchises/$image")];
        }

        return $return;
    }

}
