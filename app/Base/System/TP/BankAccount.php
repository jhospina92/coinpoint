<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Base\System\TP;

use App\Base\System\TP\TypePayment;

/**
 * Description of BankAccount
 *
 * @author jospina
 */
class BankAccount extends TypePayment{
    const ID="BA";
    
    //Params
    const PARAM_MANAGE_EDIT_TYPE = false;
    const PARAM_MANAGE_EDIT_CURRENCY = false;
    const PARAM_MANAGE_EDIT_VALUE = false;
    const PARAM_MANAGE_EDIT_BANK = false;
    
}
