<?php

namespace App\Base\System\Library\Networks;

use Illuminate\Support\Facades\Mail;

class Email {

    const VAR_HEADER = "header";
    const VAR_NAME = "name";
    const VAR_DESCRIPTION = "description";
    const VAR_EMAIL = "email";
    const EMAIL_FROM = "no-reply@myrald.com";

    public $header;
    public $description;
    public $email;
    public $subject;
    public $template;

    /** Inicializa los datos del correo electronico que se va ha enviar
     * 
     * @param type $subject // Asunto
     * @param type $email // Correo electronico
     * @param type $data //Datos del contenido
     * @param type $template //Plantilla del correo
     */
    function __construct($subject, $email, $data = null, $template = "basic") {
        $this->setSubject($subject);

        $this->setEmail($email);

        $this->setHeader($data[Email::VAR_HEADER] ?? $subject);
        $this->setDescription($data[Email::VAR_DESCRIPTION] ?? null);
        $this->setTemplate($template);

        $this->debug();
    }

    /** Activa el sistema de depuración al enviar email solo cuando el ambiente es de  pruebas.
     * 
     * @return type
     */
    private function debug() {

        if (!env('APP_DEBUG', false))
            return;

         $this->setEmail(env('DEBUG_EMAIL_TO', "myrald.inc@gmail.com"));
    }

    /** Envia el correo
     * 
     * @return boolean
     */
    function send() {

        if (!env('APP_ENABLE_SEND_EMAILS', true)) {
            return;
        }

        $data = $this->getData();
        

        return Mail::send('emails.' . $this->getTemplate(), $data, function($message) {
                    $message->to($this->getEmail())->subject($this->getSubject());
                });
    }

    /** Pone en cola el correo para enviar
     * 
     * @return type
     */
    function queue() {

        if (!env('APP_ENABLE_SEND_EMAILS', true)) {
            return;
        }

        $data = $this->getData();
        return Mail::queue('emails.' . $this->getTemplate(), $data, function($message) {
                    $message->to($this->getEmail())->subject($this->getSubject());
                });
    }

    private function getData() {
        return [
            self::VAR_HEADER => $this->getHeader(),
            self::VAR_DESCRIPTION => $this->getDescription(),
            self::VAR_EMAIL => $this->getEmail()
        ];
    }

    function getHeader() {
        return $this->header;
    }

    function getDescription() {
        return $this->description;
    }

    function getEmail() {
        return $this->email;
    }

    function getSubject() {
        return $this->subject;
    }

    function getTemplate() {
        return $this->template;
    }

    function setTemplate($template) {
        $this->template = $template;
    }

    function setHeader($header) {
        $this->header = $header;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setSubject($subject) {
        $this->subject = $subject;
    }

}
