<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools - Templates
 * and open the template in the editor.
 */

namespace App\Base\System\Library\Networks;

use App\Base\System\Library\Comp\Util;

/**
 * Description of API
 *
 * @author jospina
 */
class API {

    const TYPE_ERROR_LOGIN_FAILED = APIResponse::TYPE_ERROR_LOGIN_FAILED;
    const TYPE_ERROR_VALIDATION_FAILED = APIResponse::TYPE_ERROR_VALIDATION_FAILED;
    const TYPE_ERROR_PARAMETERS_VALIDS_NOT_EXISTS = APIResponse::TYPE_ERROR_PARAMETERS_VALIDS_NOT_EXISTS;

    /**
     * Valida si la peticion a la API es correcta
     */
    public static function validateRequest() {
        $url = Util::filtrateUrl(Util::getCurrentUrl());
        $url_base = url("");
        $url_path = str_replace($url_base, "", $url);

        //Valida si no esta accediendo a la documentacion de la api a traves de un navegador
        if (!Util::isRequestAPI() || (preg_match("/^\/?v[0-9]$/i", str_replace("/", "", $url_path)) && isset($_SERVER["HTTP_USER_AGENT"])))
            return true;

        //Metodo de autenticación
        if (preg_match("/\/?(v[0-9])\/auth/i", $url_path))
            return true;

        if (!preg_match("/\/?(v[0-9])\/((\/?[a-z]*\/?[0-9]*)*)/i", $url_path, $match))
            return self::response(400);


        list($pattern, $version, $params) = $match;


        $handler = "App\\Http\\Controllers\\API\\" . strtoupper($version) . "\\Handler";

        //Verifica si la versión existe
        if (!class_exists($handler)) {
            return self::response(400);
        }


        return call_user_func($handler . "::validate", $params);
    }

    /** Retorna una respuesta JSON
     * 
     * @param type $data Los datos a mostrar | Puede ser un codigo de estatus HTTP
     * @param type $status 
     * @return type
     */
    public static function response($data, $status = 200): APIResponse {

        if (is_int($data)) {
            $status = $data;
            $data = null;
        }

        $content_type = "application/json; charset=utf-8";

        header("HTTP/1.1 $status");
        header("Content-type: $content_type");
        header("Accept: $content_type");

        $response_data = ["status_code" => $status];
        if (!is_null($data))
            $response_data["details"] = APICodeError::getHTTP($status);

        $response_data["response"] = $data ?? ["description" => APICodeError::getHTTP($status)];


        return (new APIResponse($response_data, $status));
    }

}

class APICodeError {

    const LOGIN_ACCCOUNT_USER_NOT_ACTIVE = 101;
    const LOGIN_UNAUTHORIZED = 102;

    /** Retorna una respuesta JSON de un error de la API dado el codigo del error
     * 
     * @param type $code
     * @return type
     */
    public static function get($code) {
        return ["error" => $code, "details" => self::getDetail($code)];
    }

    private static function getDetail($code) {
        switch ($code) {
            case self::LOGIN_ACCCOUNT_USER_NOT_ACTIVE:
                return "The user account have not been activated.";
            case self::LOGIN_UNAUTHORIZED:
                return "Login incorrect, the email or password is invalid.";
        }
        return null;
    }

    /** Retorna la descripción de un codigo de error HTTP
     * 
     * @param type $status_code El codigo de Error HTTP
     * @return string
     */
    public static function getHTTP($status_code) {
        switch ($status_code) {
            case 200:
                return "OK";
            case 201:
                return "Created - The resource has been created successfully.";
            case 400:
                return "Bad Request - The request was unacceptable, missing a required parameter or some invalid parameter.";
            case 401:
                return "Unauthorized - No valid API Token provided.";
            case 403:
                return "Forbidden - Yoy don't have permission to access on this resource.";
            case 404:
                return "Not Found - The requested resource doesn't exist.";
            case 405:
                return "Method Not Allowed -  Do not have permission to access this method.";
            case 422:
                return "Unprocessable Entity - The request was well-formed but was unable to be followed due to semantic errors.";
            case 429:
                return "Too Many Requests - Too many requests hit the API too quickly. We recommend an exponential backoff of your requests.";
        }

        throw new \Exception("Response HTTP code $status_code not allowed");
    }

}

class APIResponse {

    const TYPE_ERROR_LOGIN_FAILED = 1001;
    const TYPE_ERROR_VALIDATION_FAILED = 2001;
    const TYPE_ERROR_PARAMETERS_VALIDS_NOT_EXISTS = 2002;

    private $data;
    private $status;

    function __construct(array $data, $status = 200) {
        $this->data = $data;
        $this->status = $status;
    }

    /** Retorna un objeto de respuesta con un tipo de error especificado
     * 
     * @param type $type
     * @param type $errors
     * @return \App\Base\System\Library\Networks\APIResponse
     * @throws Exception
     */
    function withErrors($type, $errors = []): APIResponse {

        if (isset($this->data["response"]["_errors"]))
            throw new \Exception("WithErrors Method not allowed");

        unset($this->data["response"]);

        $this->data["details"] = APICodeError::getHTTP($this->status);

        $this->data["response"] = ["_code" => $type, "_message" => $this->getTypeError($type), "_errors" => $errors];

        return (new APIResponse($this->data, $this->status));
    }

    /** Obtiene la descripción del codigo de error
     * 
     * @param type $code
     * @return string
     */
    private function getTypeError($code) {
        switch ($code) {
            case self::TYPE_ERROR_LOGIN_FAILED:
                return "Login failed";
            case self::TYPE_ERROR_VALIDATION_FAILED:
                return "Validation Failed";
            case self::TYPE_ERROR_PARAMETERS_VALIDS_NOT_EXISTS:
                return "There are no valid parameters in the request";
        }
        return null;
    }

    private function catchPagination() {

        $data = $this->data;

        if (!isset($data["response"]["-links"]) && !isset($data["response"]["-meta"])) {
            return;
        }

        $data["links"] = $data["response"]["-links"];
        $data["meta"] = $data["response"]["-meta"];

        unset($data["response"]["-links"]);
        unset($data["response"]["-meta"]);

        $this->data = $data;
    }

    function get() {
        $this->catchPagination();
        return response()->json($this->data, $this->status);
    }

    function __toString() {
        $this->catchPagination();
        return json_encode($this->data, JSON_FORCE_OBJECT);
    }

}
