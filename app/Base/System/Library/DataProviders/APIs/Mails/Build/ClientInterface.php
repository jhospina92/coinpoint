<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Base\System\Library\DataProviders\APIs\Mails\Build;

/**
 * Description of ClientInterface
 *
 * @author jospina
 */
interface ClientInterface {

    /**
     * Retorna el correo electrónico
     */
    public function getProfileEmail();

    /** Retorna un array con los ultimos mensajes dado por la cantidad a retornar
     * 
     * @param type $from [null|Optional] Correo electrónico de quien envia
     * @param type $subject [null|Optional] Buscar por asunto
     * @param type $num [10|Optional] Número de mensajes a retornar
      @return array[App\Base\System\Library\DataProviders\APIs\Mails\Build\Payload\EmailMessage...]
     */
    public function getMessages($from = null, $subject = null, $date = null, $num = 1);

    public static function loadClient(int $autoread_client_id);
}
