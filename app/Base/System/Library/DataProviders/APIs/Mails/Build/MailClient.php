<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Base\System\Library\DataProviders\APIs\Mails\Build;

use App\Base\System\Library\DataProviders\APIs\Mails\Build\ClientInterface;

/**
 * Description of MailClient
 *
 * @author jospina
 */
class MailClient {

    public static function load($client_class, $client_autoread_id){

        $class = "App\Base\System\Library\DataProviders\APIs\Mails\\" . $client_class;

        if (!class_exists($class))
            throw new \Exception($class . " not exists");



        return call_user_func($class . "::loadClient", $client_autoread_id);
    }

}
