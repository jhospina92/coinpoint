<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Base\System\Library\DataProviders\APIs\Mails\Build\Payload;

/**
 * Description of EmailMessage
 *
 * @author jospina
 */
class EmailMessage {
    
    private $id;
    private $body;
    private $from;
    private $date;
    private $subject;
    
    function __construct($id, $body, $from, $date, $subject) {
        $this->id = $id;
        $this->body = $body;
        $this->from = $from;
        $this->date = $date;
        $this->subject = $subject;
    }
    
    
    function getId() {
        return $this->id;
    }

    function getBody() {
        return $this->body;
    }

    function getFrom() {
        return $this->from;
    }

    function getDate() {
        return $this->date;
    }

    function getSubject() {
        return $this->subject;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setBody($body) {
        $this->body = $body;
    }

    function setFrom($from) {
        $this->from = $from;
    }

    function setDate($date) {
        $this->date = $date;
    }

    function setSubject($subject) {
        $this->subject = $subject;
    }



    
}
