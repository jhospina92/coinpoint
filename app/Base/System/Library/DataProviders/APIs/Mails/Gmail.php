<?php

namespace App\Base\System\Library\DataProviders\APIs\Mails;

use Illuminate\Support\Facades\Storage;
use Google_Service_Gmail;
use Google_Client;
use App\Base\System\Library\Comp\Util;
use App\Models\Auto\AutoRead;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Exception;
use App\Base\System\Library\DataProviders\APIs\Mails\Build\ClientInterface;
use App\Base\System\Library\DataProviders\APIs\Mails\Exceptions\ExceptionMailClientlExist;
use App\Base\System\Library\DataProviders\APIs\Mails\Build\Payload\EmailMessage;
use App\Base\System\Library\Comp\DateUtil;
use App\Base\System\Library\DataProviders\APIs\Mails\Build\MailClient;

class Gmail extends MailClient implements ClientInterface {

    const APP_NAME = "Myrald - Smart Financial Control";
    const CREDENTIAL_PATH = '~/.credentials/gmail-php-quickstart.json';
    const API_CLIENT_ID = "1080526574805-hlhca6quho7tlqa231v7j0169loimpa1.apps.googleusercontent.com";
    const API_KEY_SECRET = "lJRc4UV33cA8yuWp6SGfP1xd";
    const API_FILE_JSON = "1080526574805-hlhca6quho7tlqa231v7j0169loimpa1.apps.googleusercontent.com_secreto_cliente.json";
    const CLIENT = AutoRead::CLIENT_GMAIL;

    private $_api_client_secret_path;
    private $_api_client;
    private $_api_service = null;
    private $_api_scopes;
    private $storage_path;
    private $object;

    //AUTH DATA
    const AUTH_CODE = "auth_code";
    const AUTH_TOKEN_ACCESS = "access_token";
    const AUTH_TOKEN_REFRESH = "refresh_token";
    //PROFILE DATA
    const PROFILE_EMAIL = "profile_email";

    /**
     * Inicializa el construtor de la API
     */
    function __construct($data = null, $redirect = null) {
        $path = $this->storage_path = Storage::disk("local")->getDriver()->getAdapter()->getPathPrefix();
        $this->_api_client_secret_path = $path . 'apis_auth/' . self::API_FILE_JSON;
        $this->_api_scopes = [Google_Service_Gmail::GMAIL_READONLY];
        
        $client = new Google_Client();
        $client->setApplicationName(self::APP_NAME);
        $client->setScopes($this->_api_scopes);
        $client->setAuthConfig($this->_api_client_secret_path);
        $client->setAccessType('offline');
        $client->setIncludeGrantedScopes(true);
        $client->setApprovalPrompt("force");

        if (is_array($data)) {
            if (isset($data[self::AUTH_TOKEN_ACCESS]))
                $client->setAccessToken($data);

            $this->_api_service = new Google_Service_Gmail($client);
        }

        if (!is_null($data) && !is_array($data)) {

            $authcode = $data;

            $client->setRedirectUri($redirect ?? Util::filtrateUrl(Util::getCurrentUrl()));

            $accessToken = $client->fetchAccessTokenWithAuthCode($authcode);

            if (isset($accessToken["error"])) {
                $accessToken = self::getTokenWithAuthCode($authcode);
            }

            if (empty($accessToken))
                throw new Exception("No access token");

            $client->setAccessToken($accessToken);

            $this->_api_service = new Google_Service_Gmail($client);

            $accessToken[self::AUTH_CODE] = $authcode;
            $accessToken[self::PROFILE_EMAIL] = $this->_api_service->users->getProfile('me')["emailAddress"];

            //Comprueba que este correo no este vinculado al usuario en sesión
            if (AutoRead::isEmailExist($accessToken[self::PROFILE_EMAIL])) {
                throw new ExceptionMailClientlExist();
            }

            $this->object = AutoRead::add(self::CLIENT, $accessToken);
        }

        $this->_api_client = $client;
    }

    /** Carga el clase dinamicamente dato por el id del autoread en la base de datos
     * 
     * @param int $autoread_client_id
     * @return type
     */
    public static function loadClient(int $autoread_client_id) {
        $autoread = Autoread::find($autoread_client_id);
        $gmail = (new Gmail($autoread->{AutoRead::ATTR_AUTH}));
        $gmail->setObject($autoread);

        if ($gmail->getClient()->isAccessTokenExpired())
            if (!$gmail->refreshToken())
                return null;

        return $gmail;
    }

    public function setObject(Autoread $object) {
        $this->object = $object;
    }

    private function setService(Google_Client $client) {
        $this->_api_service = new Google_Service_Gmail($client);
    }

    public function getService(): Google_Service_Gmail {
        return $this->_api_service;
    }

    //Recarga el token de acceso
    public function refreshToken() {
        $auth = $this->object->{AutoRead::ATTR_AUTH};

        if (!isset($auth[self::AUTH_TOKEN_REFRESH])) {
            return false;
        }

        $this->_api_client->refreshToken($auth[self::AUTH_TOKEN_REFRESH]);
        $token_new = $this->_api_client->getAccessToken();
        $this->_api_client->setAccessToken($token_new);
        $this->_api_service = new Google_Service_Gmail($this->_api_client);

        $token_new[self::AUTH_CODE] = $this->object->{AutoRead::ATTR_AUTH}[self::AUTH_CODE];
        $token_new[self::PROFILE_EMAIL] = $this->_api_service->users->getProfile('me')["emailAddress"];

        $this->object->{AutoRead::ATTR_AUTH} = $token_new;
        $this->object->update();

        return true;
    }

    /** Obtiene la url de autencación de permisos
     * 
     * @param type $redirect
     * @return type
     */
    function getUrlAuth($redirect) {
        $this->_api_client->setRedirectUri($redirect);
        return $this->_api_client->createAuthUrl();
    }

    /** Retorna el cliente autorizado de la API
     * 
     * @return type
     */
    function getClient() {
        return $this->_api_client;
    }

    /** Indica si el token de autenticación del usuario ha expirado
     * 
     * @return type
     */
    public function isTokenExpired() {
        return $this->_api_client->isAccessTokenExpired();
    }

    /** Retorna el correo electrónico de la cuenta
     * 
     * @return type
     */
    public function getProfileEmail() {
        if (!$this->isServiceActive())
            return null;


        if (!empty($this->object)) {
            return $this->object->{AutoRead::ATTR_AUTH}[self::PROFILE_EMAIL];
        }

        $profile = $this->_api_service->users->getProfile('me');
        return $profile["emailAddress"];
    }

    /** Retorna un array con los ultimos mensajes dado por la cantidad a retornar
     * 
     * @param type $from [null|Optional] Correo electrónico de quien envia
     * @param type $date [null|Optional] Busca desde una fecha, por defecto la fecha actual
     * @param type $subject [null|Optional] Buscar por asunto
     * @param type $num [10|Optional] Número de mensajes a retornar
      @return array[App\Base\System\Library\DataProviders\APIs\Mails\Build\Payload\EmailMessage...]
     */
    public function getMessages($from = null, $subject = null, $date = null, $num = 1): array {

        if (!$this->isServiceActive())
            return array();

        $search = ['maxResults' => $num];

        if (filter_var($from, FILTER_VALIDATE_EMAIL))
            $search["q"] = "from:" . $from;

        //Date
        if ($date !== false)
            $search["q"] = ($search["q"] ?? "") . " date-begin: " . ($date ?? date("Y-m-d"));

        if (!is_null($subject))
            $search["q"] = ($search["q"] ?? "") . " subject: " . $subject;


        $list = $this->_api_service->users_messages->listUsersMessages('me', $search);

        $messageList = $list->getMessages();

        $inbox = [];

        foreach ($messageList as $mlist) {

            $optParamsGet2['format'] = 'full';
            $single_message = $this->_api_service->users_messages->get('me', $mlist->id, $optParamsGet2);

            $message_id = $mlist->id;
            $headers = $single_message->getPayload()->getHeaders();

            foreach ($headers as $single) {

                if ($single->getName() == 'Subject') {

                    $message_subject = $single->getValue();
                } else if ($single->getName() == 'Date') {

                    $message_date = $single->getValue();
                    $message_date = date(DateUtil::FORMAT_STANDARD, strtotime($message_date));
                } else if ($single->getName() == 'From') {

                    $message_sender = $single->getValue();
                    $message_sender = str_replace('"', '', $message_sender);
                }
            }

            $parts = $single_message->getPayload()->getParts();

            $message_body = null;

            if (isset($parts[0]['body'])) {
                $message_body = $parts[0]['body'];
                $rawData = $message_body->data;
                $sanitizedData = strtr($rawData, '-_', '+/');
                $message_body = base64_decode($sanitizedData);
            }

            $inbox[] = new EmailMessage($message_id, $message_body, $message_sender, $message_date, $message_subject);
        }


        return $inbox;
    }

    public function isServiceActive() {
        return (!is_null($this->_api_service));
    }

    /** Retorna el token de acceso dato el codigo de autorización
     * 
     * @param type $code
     * @param type $user_id
     * @return type
     * @throws Exception
     */
    public static function getTokenWithAuthCode($code, $user_id = null) {

        $user_id = $user_id ?? Auth::user()->id;

        $clients = AutoRead::where(AutoRead::ATTR_FK_USER_ID, $user_id)->where(AutoRead::ATTR_CLIENT, self::CLIENT)->get();

        foreach ($clients as $client) {
            $auth = $client->{AutoRead::ATTR_AUTH};

            if (!isset($auth[self::AUTH_CODE]))
                throw new Exception("Auth Code Not defined in data client Gmail");

            if ($auth[self::AUTH_CODE] == $code) {
                return $auth[self::AUTH_TOKEN_ACCESS];
            }
        }

        return null;
    }

    /** Retorna el modelo de objecto Autoread
     * 
     * @param type $token
     * @return type
     */
    private static function getModelAutoread($token) {

        $user_id = $user_id ?? Auth::user()->id;

        $clients = AutoRead::where(AutoRead::ATTR_FK_USER_ID, $user_id)->where(AutoRead::ATTR_CLIENT, self::CLIENT)->get();

        foreach ($clients as $client) {
            $auth = $client->{AutoRead::ATTR_AUTH};


            if ($auth[self::AUTH_TOKEN_ACCESS] == $token) {
                return $client;
            }
        }

        return null;
    }

}
