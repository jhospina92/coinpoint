<?php

namespace App\Base\System\Library\DataProviders\APIs\Mails\Exceptions;

/**
 * Description of ExceptionMailClientlExist
 *
 * @author jospina
 */

class ExceptionMailClientlExist extends \Exception{
    
    function __construct(string $message = "The Email client is associate in the user account", int $code = 0, \Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}
