<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Base\System\Library\DataProviders;

/**
 * Description of GCaptcha
 *
 * @author jospina
 */
class GCaptcha {

    const KEY_SECRET = "6Ld8pBsUAAAAAOuQ9nI5gL7QEs8in06A5febolHc";
    const KEY_PUBLIC = "6Ld8pBsUAAAAAJn_WkF62RBH71cExhPwRdY3d_cL";
    const URL_VERIFY = "https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s";

    public static function valid($response) {
        $request = json_decode(file_get_contents(sprintf(self::URL_VERIFY, self::KEY_SECRET, $response)), true);
        return ($request["success"]);
    }

}
