<?php

namespace App\Base\System\Library\DataProviders;

use Illuminate\Support\Facades\Storage;
use App\Base\System\Library\Comp\DateUtil;
use App\Base\Data\Currency;

class CurrencyDataProvider {

    const PROVIDER_URL = "https://openexchangerates.org/api/latest.json?app_id=";
    const KEY = "c42186bd18ee4999bba89618d3d17d38";
    const FILEPATH = "files-providers/data-currencies/";
    const FILENAME = "rates-%s.json";
    const DISK = "s3";
    const FORMAT_ID = "YmdH";
    //Sessions var
    const SESSION_VAR_DATE = "currency.data.rate.date";
    const SESSION_VAR_RATE = "currency.data.rate.value";
    const SESSION_VAR_CURRENCIES_RATES_HANDLER = "currency.collection.data.handler";
    const SESSION_VAR_CURRENCIES_RATES = "currency.collection.data.rates";

    /**
     * Refresca el archivo de cache donde se almacenan los datos de los Rates de cada moneda en base al USD.
     */
    public static function refresh() {
        Storage::disk(self::DISK)->put(self::getFilename(), file_get_contents(self::PROVIDER_URL . self::KEY));
    }

    /** Retorna el nombre del archivo JSON dado por si ID
     * 
     * @param type $id
     */
    private static function getFilename($id = null) {
        $id = $id ?? date(self::FORMAT_ID);
        return self::FILEPATH . sprintf(self::FILENAME, $id);
    }

    /** Retorna un array con los datos del archivo dado por su ID, si no retorna el ultimo encontrado
     * 
     * @param type $id
     * @return type
     */
    private static function getData() {

        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $idhandler = date(self::FORMAT_ID);

        //Evita que tenga que volver a leer los datos del archivo comprobando si los datos no han cambiado
        if (isset($_SESSION[self::SESSION_VAR_CURRENCIES_RATES_HANDLER]) && $_SESSION[self::SESSION_VAR_CURRENCIES_RATES_HANDLER] == $idhandler)
            return $_SESSION[self::SESSION_VAR_CURRENCIES_RATES];

        $id = date(self::FORMAT_ID);

        $date = new DateUtil($id, self::FORMAT_ID);

        if (count(Storage::disk(self::DISK)->files(self::FILEPATH)) == 0) {
            $_SESSION[self::SESSION_VAR_CURRENCIES_RATES] = null;
            $_SESSION[self::SESSION_VAR_CURRENCIES_RATES_HANDLER] = $id;
            return null;
        }

        while (!Storage::disk(self::DISK)->exists(self::getFilename($id))) {
            $id = $date->subtractHours(1);
        }

        $data = json_decode(Storage::disk(self::DISK)->get(self::getFilename($id)), true);

        $_SESSION[self::SESSION_VAR_CURRENCIES_RATES] = $data;
        $_SESSION[self::SESSION_VAR_CURRENCIES_RATES_HANDLER] = $idhandler;

        return $data;
    }

    /** Retorna el Ratio de entre las monedas indicadas
     * 
     * @param type $from
     * @param type $to
     * @return int
     */
    public static function getRate($from, $to): float {

        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $id = date(self::FORMAT_ID) . "-$from-$to";

        //Evita que tenga que volver a leer los datos del archivo comprobando si los datos no han cambiado
        if (isset($_SESSION[self::SESSION_VAR_DATE]) && $_SESSION[self::SESSION_VAR_DATE] == $id)
            return floatval($_SESSION[self::SESSION_VAR_RATE]);

        $data = self::getData();

        if (is_null($data))
            return 0;

        $rates = $data["rates"];

        if (!isset($rates[$from]) || !isset($rates[$to])) {
            return 1;
        }

        $rate = $rates[$to] / $rates[$from];

        $_SESSION[self::SESSION_VAR_RATE] = $rate;
        $_SESSION[self::SESSION_VAR_DATE] = $id;

        return $rate;
    }

    /** Indica si una modena exista en el provedor de datos
     * 
     * @param type $currency
     * @return boolean
     */
    public static function exist($currency) {

        if ($currency == Currency::defect())
            return true;


        $data = self::getData();

        if (is_null($data))
            return false;

        $rates = $data["rates"];


        return (isset($rates[$currency]));
    }

}
