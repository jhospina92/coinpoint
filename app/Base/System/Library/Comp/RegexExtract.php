<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Base\System\Library\Comp;

use App\Base\System\Library\Comp\DateUtil;
use App\Base\Data\Currency;

/**
 * Description of RegexExtract
 *
 * @author jospina
 */
class RegexExtract {

    /** Extrae una fecha de un texto [https://github.com/etiennetremel/PHP-Find-Date-in-String]
     * 
     * @param type $string El texto
     * @param type ["Y-m-d"] $output_format El formato de salida
     * @return boolean
     */
    public static function date($string, $output_format = "Y-m-d") {

        $shortenize = function( $string ) {
            return substr($string, 0, 3);
        };
// Define month name:
        $month_names = array(
            "january",
            "february",
            "march",
            "april",
            "may",
            "june",
            "july",
            "august",
            "september",
            "october",
            "november",
            "december"
        );
        $short_month_names = array_map($shortenize, $month_names);
// Define day name
        $day_names = array(
            "monday",
            "tuesday",
            "wednesday",
            "thursday",
            "friday",
            "saturday",
            "sunday"
        );
        $short_day_names = array_map($shortenize, $day_names);
// Define ordinal number
        $ordinal_number = ['st', 'nd', 'rd', 'th'];
        $day = "";
        $month = "";
        $year = "";
// Match dates: 01/01/2012 or 30-12-11 or 1 2 1985
        preg_match('/([0-9]?[0-9])[\.\-\/ ]+([0-1]?[0-9])[\.\-\/ ]+([0-9]{2,4})/', $string, $matches);
        if ($matches) {
            if ($matches[1])
                $day = $matches[1];
            if ($matches[2])
                $month = $matches[2];
            if ($matches[3])
                $year = $matches[3];
        }
        // Match dates: 2012/01/01 or 11-12-01 or 1985 2 1
        preg_match('/([0-9]{2,4})[\.\-\/ ]+([0-1]?[0-9])[\.\-\/ ]+([0-9]?[0-9])/', $string, $matches);
        if ($matches) {
            if ($matches[3])
                $day = $matches[3];
            if ($matches[2])
                $month = $matches[2];
            if ($matches[1])
                $year = $matches[1];
        }
// Match dates: Sunday 1st March 2015; Sunday, 1 March 2015; Sun 1 Mar 2015; Sun-1-March-2015
        preg_match('/(?:(?:' . implode('|', $day_names) . '|' . implode('|', $short_day_names) . ')[ ,\-_\/]*)?([0-9]?[0-9])[ ,\-_\/]*(?:' . implode('|', $ordinal_number) . ')?[ ,\-_\/]*(' . implode('|', $month_names) . '|' . implode('|', $short_month_names) . ')[ ,\-_\/]+([0-9]{4})/i', $string, $matches);
        if ($matches) {
            if (empty($day) && $matches[1])
                $day = $matches[1];
            if (empty($month) && $matches[2]) {
                $month = array_search(strtolower($matches[2]), $short_month_names);
                if (!$month)
                    $month = array_search(strtolower($matches[2]), $month_names);
                $month = $month + 1;
            }
            if (empty($year) && $matches[3])
                $year = $matches[3];
        }
// Match dates: March 1st 2015; March 1 2015; March-1st-2015
        preg_match('/(' . implode('|', $month_names) . '|' . implode('|', $short_month_names) . ')[ ,\-_\/]*([0-9]?[0-9])[ ,\-_\/]*(?:' . implode('|', $ordinal_number) . ')?[ ,\-_\/]+([0-9]{4})/i', $string, $matches);
        if ($matches) {
            if (empty($month) && $matches[1]) {
                $month = array_search(strtolower($matches[1]), $short_month_names);
                if (!$month)
                    $month = array_search(strtolower($matches[1]), $month_names);
                $month = $month + 1;
            }
            if (empty($day) && $matches[2])
                $day = $matches[2];
            if (empty($year) && $matches[3])
                $year = $matches[3];
        }
// Match month name:
        if (empty($month)) {
            preg_match('/(' . implode('|', $month_names) . ')/i', $string, $matches_month_word);
            if ($matches_month_word && $matches_month_word[1])
                $month = array_search(strtolower($matches_month_word[1]), $month_names);
// Match short month names
            if (empty($month)) {
                preg_match('/(' . implode('|', $short_month_names) . ')/i', $string, $matches_month_word);
                if ($matches_month_word && $matches_month_word[1])
                    $month = array_search(strtolower($matches_month_word[1]), $short_month_names);
            }
            $month = $month + 1;
        }
// Match 5th 1st day:
        if (empty($day)) {
            preg_match('/([0-9]?[0-9])(' . implode('|', $ordinal_number) . ')/', $string, $matches_day);
            if ($matches_day && $matches_day[1])
                $day = $matches_day[1];
        }
// Match Year if not already setted:
        if (empty($year)) {
            preg_match('/[0-9]{4}/', $string, $matches_year);
            if ($matches_year && $matches_year[0])
                $year = $matches_year[0];
        }
        if (!empty($day) && !empty($month) && empty($year)) {
            preg_match('/[0-9]{2}/', $string, $matches_year);
            if ($matches_year && $matches_year[0])
                $year = $matches_year[0];
        }
// Day leading 0
        if (1 == strlen($day))
            $day = '0' . $day;
// Month leading 0
        if (1 == strlen($month))
            $month = '0' . $month;
// Check year:
        if (2 == strlen($year) && $year > 20)
            $year = '19' . $year;
        else if (2 == strlen($year) && $year < 20)
            $year = '20' . $year;

        $date = array(
            'year' => $year,
            'month' => $month,
            'day' => $day
        );

        // Return false if nothing found:
        if (empty($year) || empty($month) || empty($day))
            return false;
        else
            return (new DateUtil($date["year"] . "-" . $date["month"] . "-" . $date["day"], "Y-m-d"))->changeFormat($output_format);
    }

    /** Extrae la hora de un texto dado
     * 
     * @param type $string
     * @return boolean
     */
    public static function time($string) {
        //Match 24:00, 7:00:01, 3:00pm, 
        if (!preg_match('/([0-9]{1,2})[\:]([0-9]{1,2})[\:]?([0-9]{1,2})?[ ]?(am|pm)?/i', $string, $matches))
            return false;

        $time = mb_strtolower($matches[0]);

        $format = "H:i";

        if (substr_count($time, ":") > 1)
            $format .= ":i:s";
        if (strpos("am", $time) !== false || strpos("pm", $time) !== false)
            $format .= ":i:s a";


        return date("H:i:s", strtotime($time));
    }

    public static function moneyValue($text) {

        $text = str_replace(array(" ", "-"), "", $text);
        $content = "";

        preg_match_all("/.+/", $text, $match);

        for ($i = 0; $i < count($match[0]); $i++) {
            $content .= trim($match[0][$i]);
        }

        $symbols = Currency::getListSymbols();

        //Expresion que busca un valor monetario dentro del texto
         $regex = '/(' . str_replace(array("$","/","||","|||","."),array("\\$","","","",""), implode("|", $symbols)) . ')?[ ]?[0-9]{1,3}(\,|\.)([0-9]{2,3}(\,|\.)?)+([0-9]{2,3})?/';

    
        if (!preg_match($regex, $content, $val)) {
            return false;
        }


        return $val[0];
    }

}
