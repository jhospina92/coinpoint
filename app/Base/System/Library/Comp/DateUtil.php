<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Base\System\Library\Comp;

use DateTime;
use App\Base\System\Library\Comp\Util;

class DateUtil {

    const FORMAT_STANDARD = "Y-m-d H:i:s";

    var $format;
    var $year;
    var $month;
    var $day;
    var $hour;
    var $min;
    var $sec;
    var $object;

    /** EL constructor debe recibir una cadena de fecha en formato Y-m-d H:i:s
     * 
     * @param type $date
     */
    function __construct($date = null, $format = "Y-m-d H:i:s") {
        $this->format = $format;
        $this->assignAttributes((is_null($date)) ? date($format) : $date);
    }

    function __toString() {
        return $this->object->format($this->format);
    }

    /** Retorna un String con la fecha
     * 
     * @return type
     */
    function getString($format = null) {
        if (is_null($format))
            return $this->object->format($this->format);

        $format = str_replace("m", "%x%", $format);

        return str_replace("%x%", $this->getMonth(), $this->object->format($format));
    }

    /** Cambia el formato de la fecha dado por un formato de fecha valido
     * 
     * @param type $newFormat
     */
    function changeFormat($newFormat) {
        $newDate = date($newFormat, strtotime($this->object->format($this->format)));
        $this->format = $newFormat;
        $this->assignAttributes($newDate);
        return $this->object->format($newFormat);
    }

    /** Agrega un numero definido de meses a la fecha objeto y lo reasigna a la fecha
     * 
     * @param type $num El numero de meses a añadir
     * @return String Retorna la new fecha
     */
    function addMonths(int $num) {

        $date_current = new DateUtil($this->object->format($this->format));

        $this->object->modify("first day of +$num month");

        $date_aux = new DateUtil($this->object->format($this->format));
        $date_new = new DateUtil($this->object->format($this->format));

        $day_dif = $date_current->day - $date_new->day;

        $date_aux->object->modify('last day of');
        $last_day = $date_aux->object->format("d");

        $days = (($date_new->day + $day_dif) > $last_day) ? ($last_day - $date_new->day) : $day_dif;

        $new_date = $date_new->addDays($days);

        $this->assignAttributes($new_date);

        return $date_new;
    }

    /** Agrega un numero definido de semanas a la fecha objeto y lo reasigna a la fecha
     * 
     * @param type $num El numero de semanas a añadir
     * @return String Retorna la new fecha
     */
    function addWeeks($num) {
        $this->object->add(date_interval_create_from_date_string("$num weeks"));
        $new_date = $this->object->format($this->format);
        $this->assignAttributes($this->object->format($this->format));
        return $new_date;
    }

    /** Agrega un numero definido de dias a la fecha objeto y lo reasigna a la fecha
     * 
     * @param type $num El numero de dias a añadir
     * @return String Retorna la new fecha
     */
    function addDays(int $num) {
        $this->object->add(date_interval_create_from_date_string("$num days"));
        $new_date = $this->object->format($this->format);
        $this->assignAttributes($this->object->format($this->format));
        return $new_date;
    }

    /** Agrega un numero definido de horas a la fecha objeto y lo reasigna a la fecha
     * 
     * @param type $num El numero de horas a añadir
     * @return String Retorna la new fecha
     */
    function addHours($num) {
        $this->object->add(date_interval_create_from_date_string("$num hours"));
        $new_date = $this->object->format($this->format);
        $this->assignAttributes($this->object->format($this->format));
        return $new_date;
    }

    /** Agrega un numero definido de minutos a la fecha objeto y lo reasigna a la fecha
     * 
     * @param type $num El numero de minutos a añadir
     * @return String Retorna la new fecha
     */
    function addMinutes($num) {
        $this->object->add(date_interval_create_from_date_string("$num minutes"));
        $new_date = $this->object->format($this->format);
        $this->assignAttributes($this->object->format($this->format));
        return $new_date;
    }

    /** Agrega un numero definido de segundos a la fecha objeto y lo reasigna a la fecha
     * 
     * @param type $num El numero de dias a añadir
     * @return String Retorna la new fecha
     */
    function addSeconds($num) {
        $this->object->add(date_interval_create_from_date_string("$num seconds"));
        $new_date = $this->object->format($this->format);
        $this->assignAttributes($this->object->format($this->format));
        return $new_date;
    }

    /** Sustrae un numero definido de horas a la fecha objeto
     * 
     * @param type $num
     * @return type
     */
    function subtractHours($num) {
        $this->object->sub(date_interval_create_from_date_string("$num hours"));
        $new_date = $this->object->format($this->format);
        $this->assignAttributes($this->object->format($this->format));
        return $new_date;
    }

    /** Sustrae un numero definido de dias a la fecha objeto
     * 
     * @param type $num
     * @return type
     */
    function subtractDays($num) {
        $this->object->sub(date_interval_create_from_date_string("$num days"));
        $new_date = $this->object->format($this->format);
        $this->assignAttributes($this->object->format($this->format));
        return $new_date;
    }

    /** Sustrae un numero definido de meses a la fecha objeto
     * 
     * @param type $num
     * @return type
     */
    function subtractMonth($num) {

        $date_current = new DateUtil($this->object->format($this->format));

        $this->object->modify("first day of $num month ago");

        $date_aux = new DateUtil($this->object->format($this->format));
        $date_new = new DateUtil($this->object->format($this->format));

        $day_dif = $date_current->day - $date_new->day;

        $date_aux->object->modify('last day of');
        $last_day = $date_aux->object->format("d");

        $days = (($date_new->day + $day_dif) > $last_day) ? ($last_day - $date_new->day) : $day_dif;

        $new_date = $date_new->addDays($days);


        $this->assignAttributes($new_date);

        return $date_new;
    }

    /** Asigna los atributos de fecha al objeto de la clase
     * 
     * @param type $date Cadena de fecha en formato Y-m-d H:i:s
     */
    private function assignAttributes($date) {
        //"Y-m-d H:i:s" => "%d-%d-%d %d:%d:%d" 

        if ($this->format != self::FORMAT_STANDARD) {
            $this->object = DateTime::createFromFormat($this->format, $date);
            $date = $this->object->format(self::FORMAT_STANDARD);
        }
        $format = str_replace("d", "X", self::FORMAT_STANDARD);
        $spe = str_replace(
                array("Y", "m", "X", "H", "i", "s", "j", "n"), "%d", $format);

        list($year, $month, $day, $hour, $min, $sec) = @sscanf($date, $spe);

        $this->year = $year;
        $this->month = $month;
        $this->day = $day;
        $this->hour = $hour;
        $this->min = $min;
        $this->sec = $sec;
        $this->object = new DateTime($date);
    }

    static function format($date) {
        $monthes = DateUtil::getMonthsNames();

        $ft = explode(" ", $date);

        $date = explode("-", $ft[0]);

        return trans("gen.date.format.01", array("dia" => $date[2], "mes" => $monthes[$date[1]], "ano" => $date[0], "hora" => $ft[1]));
    }

    static function calculateDifference($date1, $date2 = null) {
        $date2 = (is_null($date2)) ? DateUtil::getCurrentTime() : $date2;
        $minute = 60;
        $hour = $minute * 60;
        $day = $hour * 24;
        $month = $day * 30;
        $year = $month * 12;

//formateamos las fechas a segundos tipo 1374998435
        $difference = DateUtil::difSec($date1, $date2);
//comprobamos el tiempo que ha pasado en segundos entre las dos fechas
//floor devuelve el número entero anterior
        if ($difference <= $minute) {
            $time = floor($difference) . " " . trans("ui.label.date." . str_plural("second", $difference));
        } elseif ($difference >= $minute && $difference < $minute * 2) {
            $time = "1 " . trans("ui.label.date.minute");
        } elseif ($difference >= $minute * 2 && $difference < $hour) {
            $time = floor($difference / $minute) . " " . trans("ui.label.date." . str_plural("minute", $difference));
        } elseif ($difference >= $hour && $difference < $hour * 2) {
            $time = "1 " . trans("ui.label.date.hour");
        } elseif ($difference >= $hour * 2 && $difference < $day) {
            $time = floor($difference / $hour) . " " . trans("ui.label.date." . str_plural("hour", $difference));
        } elseif ($difference >= $day && $difference < $day * 2) {
            $time = "1 " . trans("ui.label.date.day");
        } elseif ($difference >= $day * 2 && $difference < $month) {
            $time = floor($difference / $day) . " " . trans("ui.label.date." . str_plural("day", $difference));
        } elseif ($difference >= $month && $difference < $month * 2) {
            $time = "1 " . trans("ui.label.date.month") . " " . DateUtil::calculateDifference($date1, date(DateUtil::FORMAT_STANDARD, intval($difference - $month) + strtotime($date1)));
        } elseif ($difference >= $month * 2 && $difference < $year) {
            $time = floor($difference / $month) . " " . trans("ui.label.date." . str_plural("month", $difference)) . " " . DateUtil::calculateDifference($date1, date(DateUtil::FORMAT_STANDARD, intval($difference - $month * floor($difference / $month) + strtotime($date1))));
        } elseif ($difference >= $year && $difference < $year * 2) {
            $time = "1 " . trans("ui.label.date.year") . " " . DateUtil::calculateDifference($date1, date(DateUtil::FORMAT_STANDARD, intval($difference - $year) + strtotime($date1)));
        } elseif ($difference >= $year * 2) {
            $time = floor($difference / $year) . " " . trans("ui.label.date." . str_plural("year", $difference));
        }


        return (intval($time) <= 0 ) ? false : mb_strtolower($time);
    }

    /** Lenguaje de modelamiento Unificiado que procesa un lenguaje de fecha [URL de la documentación aquí]
     * 
     * @param type $attr
     * @return type
     */
    public static function uml($attr) {

        switch ($attr) {
            //Retorna la fecha actual en el formato [Y-m-d]
            case "current":
                return date("Y-m-d");

            //Retorna el total de días del mes actual
            case "total":
                $date = new DateTime(date("Y-m-d"));
                $date->modify('last day of');
                return $date->format("d");
            //Realiza una operación entre dos fechas dada en el formato attr+attr
            default:
                //Operation
                if (preg_match("/(.+)(\+|\-)(.+)/", $attr, $match)) {
                    $attr1 = self::uml($match[1]);
                    $oper = $match[2];
                    $attr2 = self::uml($match[3]);

                    $date = new DateUtil($attr1, "Y-m-d");

                    if ($oper == "+")
                        $date->addDays($attr2);
                    if ($oper == "-")
                        $date->subtractDays($attr2);

                    return $date;
                }
        }

        return $attr;
    }

    /** Obtiene la diferencia en segundo de dos fechas
     * 
     * @param type $date1
     * @param type $date2
     * @return type
     */
    static function difSec($date1, $date2) {
        return intval(strtotime($date2) - strtotime($date1));
    }

    /** Obtiene la diferencia en menses de dos fechas
     * 
     * @param type $date1
     * @param type $date2
     * @return type
     */
    static function difMon($date1, $date2) {
        return floor(intval(strtotime($date2) - strtotime($date1)) / (60 * 60 * 24 * 28));
    }

    static function numberAdapt($num) {
        return ($num < 10) ? "0" . $num : $num;
    }

    /** Obtiene el timeStamp del servidor indicado por año-mes-dia-hora-minutos-segundos
     * 
     * @return String
     */
    static function getTimeStamp($zone = 'America/Bogota') {
        date_default_timezone_set($zone);
        return date("YmdGis");
    }

    /** Obtiene la fecha actual de una zona horaria
     * 
     * @param type $format24 [True] Indica si la fecha es en horario de 24 horas
     * @param String $zone [America/Bogota] Zona Horaria
     * @return type
     */
    public static function getCurrentTime($format24 = true, $zone = 'America/Bogota') {
        date_default_timezone_set($zone);
        return ($format24) ? date('Y-m-d H:i:s') : date('Y-m-d h:i:sa');
    }

    /** Obtiene un array asociativo con los nombre de los mess del año
     * 
     */
    static function getMonthsNames() {
        return array("01" => trans("ui.date.january.name"),
            "02" => trans("ui.date.febrary.name"),
            "03" => trans("ui.date.march.name"),
            "04" => trans("ui.date.april.name"),
            "05" => trans("ui.date.may.name"),
            "06" => trans("ui.date.june.name"),
            "07" => trans("ui.date.july.name"),
            "08" => trans("ui.date.august.name"),
            "09" => trans("ui.date.september.name"),
            "10" => trans("ui.date.october.name"),
            "11" => trans("ui.date.november.name"),
            "12" => trans("ui.date.december.name"));
    }

    /** Obtiene el nombre de un mes dado por su numero
     * 
     * @param type $month
     * @return type
     */
    static function getNameMonth(int $month) {
        if ($month <= 0 || $month > 12)
            return null;
        $monthes = DateUtil::getMonthsNames();
        return $monthes[Util::addZeroLeft($month)];
    }

    function getFormat() {
        return $this->format;
    }

    function getYear() {
        return $this->year;
    }

    function getMonth() {
        return DateUtil::getNameMonth(DateUtil::numberAdapt($this->month));
    }

    function getDay() {
        return $this->day;
    }

    function getHour() {
        return $this->hour;
    }

    function getMin() {
        return $this->min;
    }

    function getSec() {
        return $this->sec;
    }

    function getObject() {
        return $this->object;
    }

    function setFormat($format) {
        $this->format = $format;
    }

    function setYear($year) {
        $this->year = $year;
    }

    function setMonth($month) {
        $this->month = $month;
    }

    function setDay($day) {
        $this->day = $day;
    }

    function setHour($hour) {
        $this->hour = $hour;
    }

    function setMin($min) {
        $this->min = $min;
    }

    function setSec($sec) {
        $this->sec = $sec;
    }

    function setObject($object) {
        $this->object = $object;
    }

}
