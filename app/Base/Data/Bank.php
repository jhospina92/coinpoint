<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Base\Data;

use ReflectionClass;
use Exception;

/**
 * Description of Bank
 *
 * @author jospina
 */
class Bank {
    /*
      |--------------------------------------------------------------------------
      |Bank List
      |--------------------------------------------------------------------------
     */

    const BANCO_AGRARIO_COLOMBIA = ["BAC", "Banco Agrario de Colombia"];
    const BANCO_AV_VILLAS = ["BAV", "Banco AV Villas"];
    const BANCAMIA = ["BAI", "Bancamia"];
    const BBVA = ["BVA", "BBVA"];
    const BANCO_DE_BOGOTA = ["BDB", "Banco de Bogotá"];
    const BANCO_CAJA_SOCIAL = ["BCS", "Banco Caja Social"];
    const CITIBANK = ["CTB", "Citibank"];
    const BANCOLOMBIA = ["BCA", "Bancolombia"];
    const BANCO_COLPATRIA = ["BCT", "Banco Colpatria"];
    const BANCO_COOMEVA = ["BCO", "Banco Coomeva"];
    const BANCO_CORPBANCA = ["BCR", "Banco Corpbanca"];
    const DAVIVIENDA = ["DVD", "Davivienda"];
    const BANCO_FALABELLA = ["FAL", "Banco Falabella"];
    const BANCO_FINANDINA = ["FIN", "Banco Finandina"];
    const BANCO_GNB_SUDAMERIS = ["GNB", "Banco GNB Sudameris"];
    const HELM_BANK = ["HEB", "Helm Bank"];
    const HSBC = ["HSBC", "HSBC"];
    const BANCO_DE_OCCIDENTE = ["BOC", "Banco de Occidente"];
    const BANCO_PICHINCHA = ["BPICH", "Banco Pichincha"];
    const BANCO_POPULAR = ["BCP", "Banco Popular"];
    const BANCO_PROCREDITO = ["PROC", "Banco Procredito"];
    const SCOTIABANK = ["SCTB", "Scotiabank"];
    const BANCO_WWB = ["BWWB", "Banco WWB"];

    /*
      |--------------------------------------------------------------------------
      | Currencies Bank Assoc
      |--------------------------------------------------------------------------
     */
    //Colombia
    const CR_COP = [self::BANCO_AGRARIO_COLOMBIA, self::BANCO_AV_VILLAS, self::BANCAMIA, self::BBVA, self::BANCO_DE_BOGOTA, self::BANCO_CAJA_SOCIAL, self::CITIBANK, self::BANCOLOMBIA, self::BANCO_COLPATRIA, self::BANCO_COOMEVA, self::BANCO_CORPBANCA, self::DAVIVIENDA, self::BANCO_FALABELLA, self::BANCO_FINANDINA, self::BANCO_GNB_SUDAMERIS, self::HELM_BANK, self::HSBC, self::BANCO_DE_OCCIDENTE, self::BANCO_PICHINCHA, self::BANCO_POPULAR, self::BANCO_PROCREDITO, self::SCOTIABANK, self::BANCO_WWB];

    private $name;
    private $code;

    function __construct(string $code) {

        foreach (self::getList() as $index => $data) {
            list($codeGiv, $name) = $data;
            if ($code == $codeGiv) {
                $this->name = $name;
                $this->code = $code;
                return;
            }
        }

        throw new Exception("Bank code don't exist");
    }

    /** Retorna un array con un listado de bancos
     * 
     * @return type
     */
    public static function getList() {
        $class = new ReflectionClass(__CLASS__);

        $return = [];

        foreach ($class->getConstants() as $name => $data) {
            if (strpos($name, "CR_") === false) {
                $return[] = $data;
            }
        }

        return $return;
    }

    /** Retorna un listado de Bancos dado por el tipo de moneda asociada que identifica al pais
     * 
     * @param type $currencyCode
     * @return type
     */
    public static function getListFromCurrency($currencyCode): array {
        if (!defined(Bank::class . "::CR_" . $currencyCode))
            return array();

        return constant(Bank::class . "::CR_" . $currencyCode);
    }

    function getName() {
        return $this->name;
    }

    function getCode() {
        return $this->code;
    }

}
