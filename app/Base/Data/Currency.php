<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Base\Data;

use ReflectionClass;
use App\Base\System\Library\DataProviders\CurrencyDataProvider;

/**
 * Description of Currency
 *
 * @author jospina
 */
class Currency {

    const AED = "AED";
    const AFN = "AFN";
    const ALL = "ALL";
    const AMD = "AMD";
    const ANG = "ANG";
    const AOA = "AOA";
    const ARS = "ARS";
    const AUD = "AUD";
    const AWG = "AWG";
    const AZN = "AZN";
    const BAM = "BAM";
    const BBD = "BBD";
    const BDT = "BDT";
    const BGN = "BGN";
    const BHD = "BHD";
    const BIF = "BIF";
    const BMD = "BMD";
    const BND = "BND";
    const BOB = "BOB";
    const BRL = "BRL";
    const BSD = "BSD";
    const BTN = "BTN";
    const BWP = "BWP";
    const BYR = "BYR";
    const BZD = "BZD";
    const CAD = "CAD";
    const CDF = "CDF";
    const CHF = "CHF";
    const CLF = "CLF";
    const CLP = "CLP";
    const CNY = "CNY";
    const COP = "COP";
    const CRC = "CRC";
    const CUP = "CUP";
    const CVE = "CVE";
    const CZK = "CZK";
    const DJF = "DJF";
    const DKK = "DKK";
    const DOP = "DOP";
    const DZD = "DZD";
    const EGP = "EGP";
    const ETB = "ETB";
    const EUR = "EUR";
    const FJD = "FJD";
    const FKP = "FKP";
    const GBP = "GBP";
    const GEL = "GEL";
    const GHS = "GHS";
    const GIP = "GIP";
    const GMD = "GMD";
    const GNF = "GNF";
    const GTQ = "GTQ";
    const GYD = "GYD";
    const HKD = "HKD";
    const HNL = "HNL";
    const HRK = "HRK";
    const HTG = "HTG";
    const HUF = "HUF";
    const IDR = "IDR";
    const ILS = "ILS";
    const INR = "INR";
    const IQD = "IQD";
    const IRR = "IRR";
    const ISK = "ISK";
    const JEP = "JEP";
    const JMD = "JMD";
    const JOD = "JOD";
    const JPY = "JPY";
    const KES = "KES";
    const KGS = "KGS";
    const KHR = "KHR";
    const KMF = "KMF";
    const KPW = "KPW";
    const KRW = "KRW";
    const KWD = "KWD";
    const KYD = "KYD";
    const KZT = "KZT";
    const LAK = "LAK";
    const LBP = "LBP";
    const LKR = "LKR";
    const LRD = "LRD";
    const LSL = "LSL";
    const LTL = "LTL";
    const LVL = "LVL";
    const LYD = "LYD";
    const MAD = "MAD";
    const MDL = "MDL";
    const MGA = "MGA";
    const MKD = "MKD";
    const MMK = "MMK";
    const MNT = "MNT";
    const MOP = "MOP";
    const MRO = "MRO";
    const MUR = "MUR";
    const MVR = "MVR";
    const MWK = "MWK";
    const MXN = "MXN";
    const MYR = "MYR";
    const MZN = "MZN";
    const NAD = "NAD";
    const NGN = "NGN";
    const NIO = "NIO";
    const NOK = "NOK";
    const NPR = "NPR";
    const NZD = "NZD";
    const OMR = "OMR";
    const PAB = "PAB";
    const PEN = "PEN";
    const PGK = "PGK";
    const PHP = "PHP";
    const PKR = "PKR";
    const PLN = "PLN";
    const PYG = "PYG";
    const QAR = "QAR";
    const RON = "RON";
    const RSD = "RSD";
    const RUB = "RUB";
    const RWF = "RWF";
    const SAR = "SAR";
    const SBD = "SBD";
    const SCR = "SCR";
    const SDG = "SDG";
    const SEK = "SEK";
    const SGD = "SGD";
    const SHP = "SHP";
    const SLL = "SLL";
    const SOS = "SOS";
    const SRD = "SRD";
    const STD = "STD";
    const SVC = "SVC";
    const SYP = "SYP";
    const SZL = "SZL";
    const THB = "THB";
    const TJS = "TJS";
    const TMT = "TMT";
    const TND = "TND";
    const TOP = "TOP";
    const TTD = "TTD";
    const TWD = "TWD";
    const TZS = "TZS";
    const UAH = "UAH";
    const UGX = "UGX";
    const USD = "USD";
    const UYU = "UYU";
    const UZS = "UZS";
    const VEF = "VEF";
    const VND = "VND";
    const VUV = "VUV";
    const WST = "WST";
    const XAF = "XAF";
    const XCD = "XCD";
    const XDR = "XDR";
    const XOF = "XOF";
    const XPF = "XPF";
    const YER = "YER";
    const ZAR = "ZAR";
    const ZMK = "ZMK";
    const ZWL = "ZWL";

    private $code;
    public static $_symbols = array(self::AED => '&#1583;.&#1573;', self::AFN => '&#65;&#102;', self::ALL => '&#76;&#101;&#107;', self::AMD => '', self::ANG => '&#402;', self::AOA => '&#75;&#122;', self::ARS => '&#36;', self::AUD => '&#36;', self::AWG => '&#402;', self::AZN => '&#1084;&#1072;&#1085;', self::BAM => '&#75;&#77;', self::BBD => '&#36;', self::BDT => '&#2547;', self::BGN => '&#1083;&#1074;', self::BHD => '&#1583;', self::BIF => '&#70;&#66;&#117;', self::BMD => '&#36;', self::BND => '&#36;', self::BOB => '&#36;&#98;', self::BRL => '&#82;&#36;', self::BSD => '&#36;', self::BTN => '&#78;&#117;&#46;', self::BWP => '&#80;', self::BYR => '&#112;&#46;', self::BZD => '&#66;&#90;&#36;', self::CAD => '&#36;', self::CDF => '&#70;&#67;', self::CHF => '&#67;&#72;&#70;', self::CLF => '', self::CLP => '&#36;', self::CNY => '&#165;', self::COP => '&#36;', self::CRC => '&#8353;', self::CUP => '&#8396;', self::CVE => '&#36;', self::CZK => '&#75;&#269;', self::DJF => '&#70;&#100;&#106;', self::DKK => '&#107;&#114;', self::DOP => '&#82;&#68;&#36;', self::DZD => '&#1583;&#1580;', self::EGP => '&#163;', self::ETB => '&#66;&#114;', self::EUR => '&#8364;', self::FJD => '&#36;', self::FKP => '&#163;', self::GBP => '&#163;', self::GEL => '&#4314;', self::GHS => '&#162;', self::GIP => '&#163;', self::GMD => '&#68;', self::GNF => '&#70;&#71;', self::GTQ => '&#81;', self::GYD => '&#36;', self::HKD => '&#36;', self::HNL => '&#76;', self::HRK => '&#107;&#110;', self::HTG => '&#71;', self::HUF => '&#70;&#116;', self::IDR => '&#82;&#112;', self::ILS => '&#8362;', self::INR => '&#8377;', self::IQD => '&#1593;.&#1583;', self::IRR => '&#65020;', self::ISK => '&#107;&#114;', self::JEP => '&#163;', self::JMD => '&#74;&#36;', self::JOD => '&#74;&#68;', self::JPY => '&#165;', self::KES => '&#75;&#83;&#104;', self::KGS => '&#1083;&#1074;', self::KHR => '&#6107;', self::KMF => '&#67;&#70;', self::KPW => '&#8361;', self::KRW => '&#8361;', self::KWD => '&#1583;.&#1603;', self::KYD => '&#36;', self::KZT => '&#1083;&#1074;', self::LAK => '&#8365;', self::LBP => '&#163;', self::LKR => '&#8360;', self::LRD => '&#36;', self::LSL => '&#76;', self::LTL => '&#76;&#116;', self::LVL => '&#76;&#115;', self::LYD => '&#1604;.&#1583;', self::MAD => '&#1583;.&#1605;.', self::MDL => '&#76;', self::MGA => '&#65;&#114;', self::MKD => '&#1076;&#1077;&#1085;', self::MMK => '&#75;', self::MNT => '&#8366;', self::MOP => '&#77;&#79;&#80;&#36;', self::MRO => '&#85;&#77;', self::MUR => '&#8360;', self::MVR => '&#1923;', self::MWK => '&#77;&#75;', self::MXN => '&#36;', self::MYR => '&#82;&#77;', self::MZN => '&#77;&#84;', self::NAD => '&#36;', self::NGN => '&#8358;', self::NIO => '&#67;&#36;', self::NOK => '&#107;&#114;', self::NPR => '&#8360;', self::NZD => '&#36;', self::OMR => '&#65020;', self::PAB => '&#66;&#47;&#46;', self::PEN => '&#83;&#47;&#46;', self::PGK => '&#75;', self::PHP => '&#8369;', self::PKR => '&#8360;', self::PLN => '&#122;&#322;', self::PYG => '&#71;&#115;', self::QAR => '&#65020;', self::RON => '&#108;&#101;&#105;', self::RSD => '&#1044;&#1080;&#1085;&#46;', self::RUB => '&#1088;&#1091;&#1073;', self::RWF => '&#1585;.&#1587;', self::SAR => '&#65020;', self::SBD => '&#36;', self::SCR => '&#8360;', self::SDG => '&#163;', self::SEK => '&#107;&#114;', self::SGD => '&#36;', self::SHP => '&#163;', self::SLL => '&#76;&#101;', self::SOS => '&#83;', self::SRD => '&#36;', self::STD => '&#68;&#98;', self::SVC => '&#36;', self::SYP => '&#163;', self::SZL => '&#76;', self::THB => '&#3647;', self::TJS => '&#84;&#74;&#83;', self::TMT => '&#109;', self::TND => '&#1583;.&#1578;', self::TOP => '&#84;&#36;', self::TTD => '&#36;', self::TWD => '&#78;&#84;&#36;', self::TZS => '', self::UAH => '&#8372;', self::UGX => '&#85;&#83;&#104;', self::USD => '&#36;', self::UYU => '&#36;&#85;', self::UZS => '&#1083;&#1074;', self::VEF => '&#66;&#115;', self::VND => '&#8363;', self::VUV => '&#86;&#84;', self::WST => '&#87;&#83;&#36;', self::XAF => '&#70;&#67;&#70;&#65;', self::XCD => '&#36;', self::XDR => '', self::XOF => '', self::XPF => '&#70;', self::YER => '&#65020;', self::ZAR => '&#82;', self::ZMK => '&#90;&#75;', self::ZWL => '&#90;&#36;');

    public function __construct(string $code) {
        $list = self::getList();
        if (!isset($list[$code]))
            throw new \Exception('The code currency specified does not exist');
        $this->code = $code;
    }

    /**
     * Obtiene un String con un valor de moneda formateado
     */
    public static function getFormattedValue($value, $code, $tag = true) {

        $negative = ($value < 0);
        
        $value=abs($value);

        $currency = new Currency($code);

        list($decimals, $t, $f) = $currency->getFormat();

        return (($tag) ? "<amount>" : "") . (($negative) ? "- " : "") . $currency->getSymbol() . "" . number_format($value, $decimals, $f, $t) . " " . $currency->getCode() . (($tag) ? "</amount>" : "");
    }

    /** Obtiene un array con el listado de Codigos de Moneda
     * 
     * @return array
     */
    public static function getList() {
        $class = new ReflectionClass(__CLASS__);

        $list = array();

        foreach ($class->getConstants() as $index => $value) {
            if (CurrencyDataProvider::exist($value))
                $list[$index] = $value;
        }

        return $list;
    }

    /** Retorna una array con el listado de simbolos monetarios
     * 
     * @param type $unicode [false]
     * @return type
     */
    public static function getListSymbols($unicode = false) {

        $list = array();

        foreach (self::$_symbols as $index => $value) {
            $list[] = (!$unicode) ? html_entity_decode($value) : $value;
        }

        return $list;
    }

    public static function convert($value, $from, $to) {
        if ($from == $to)
            return $value;
        return $value * CurrencyDataProvider::getRate($from, $to);
    }

    /** Obtiene en formato HTML el simbolo del tipo de moneda seleccionado
     * 
     * @param type $currency
     * @return string
     */
    public function getSymbol() {
        $symbols = self::$_symbols;
        return (isset($symbols[$this->code])) ? $symbols[$this->code] : null;
    }

    /** Devuelve un array indicando el formato de la moneda (Numero de decimales, Separador de Mil, Separador Decimal)
     * 
     * @return array
     */
    public function getFormat() {
        $formats = array(self::AED => array(2, ",", "."), self::AFN => array(2, ",", "."), self::ALL => array(2, ",", "."), self::AMD => array(2, ",", "."), self::ANG => array(2, ",", "."), self::AOA => array(2, ",", "."), self::ARS => array(0, ".", ","), self::AUD => array(2, ",", "."), self::AWG => array(2, ",", "."), self::AZN => array(2, ",", "."), self::BAM => array(2, ",", "."), self::BBD => array(2, ",", "."), self::BDT => array(2, ",", "."), self::BGN => array(2, ",", "."), self::BHD => array(2, ",", "."), self::BIF => array(2, ",", "."), self::BMD => array(2, ",", "."), self::BND => array(2, ",", "."), self::BOB => array(2, ",", "."), self::BRL => array(2, ",", "."), self::BSD => array(2, ",", "."), self::BTN => array(2, ",", "."), self::BWP => array(2, ",", "."), self::BYR => array(2, ",", "."), self::BZD => array(2, ",", "."), self::CAD => array(2, ",", "."), self::CDF => array(2, ",", "."), self::CHF => array(2, ",", "."), self::CLF => array(2, ",", "."), self::CLP => array(0, ".", ","), self::CNY => array(2, ",", "."), self::COP => array(0, ".", ","), self::CRC => array(2, ",", "."), self::CUP => array(2, ",", "."), self::CVE => array(2, ",", "."), self::CZK => array(2, ",", "."), self::DJF => array(2, ",", "."), self::DKK => array(2, ",", "."), self::DOP => array(2, ",", "."), self::DZD => array(2, ",", "."), self::EGP => array(2, ",", "."), self::ETB => array(2, ",", "."), self::EUR => array(2, ",", "."), self::FJD => array(2, ",", "."), self::FKP => array(2, ",", "."), self::GBP => array(2, ",", "."), self::GEL => array(2, ",", "."), self::GHS => array(2, ",", "."), self::GIP => array(2, ",", "."), self::GMD => array(2, ",", "."), self::GNF => array(2, ",", "."), self::GTQ => array(2, ",", "."), self::GYD => array(2, ",", "."), self::HKD => array(2, ",", "."), self::HNL => array(2, ",", "."), self::HRK => array(2, ",", "."), self::HTG => array(2, ",", "."), self::HUF => array(2, ",", "."), self::IDR => array(2, ",", "."), self::ILS => array(2, ",", "."), self::INR => array(2, ",", "."), self::IQD => array(2, ",", "."), self::IRR => array(2, ",", "."), self::ISK => array(2, ",", "."), self::JEP => array(2, ",", "."), self::JMD => array(2, ",", "."), self::JOD => array(2, ",", "."), self::JPY => array(2, ",", "."), self::KES => array(2, ",", "."), self::KGS => array(2, ",", "."), self::KHR => array(2, ",", "."), self::KMF => array(2, ",", "."), self::KPW => array(2, ",", "."), self::KRW => array(2, ",", "."), self::KWD => array(2, ",", "."), self::KYD => array(2, ",", "."), self::KZT => array(2, ",", "."), self::LAK => array(2, ",", "."), self::LBP => array(2, ",", "."), self::LKR => array(2, ",", "."), self::LRD => array(2, ",", "."), self::LSL => array(2, ",", "."), self::LTL => array(2, ",", "."), self::LVL => array(2, ",", "."), self::LYD => array(2, ",", "."), self::MAD => array(2, ",", "."), self::MDL => array(2, ",", "."), self::MGA => array(2, ",", "."), self::MKD => array(2, ",", "."), self::MMK => array(2, ",", "."), self::MNT => array(2, ",", "."), self::MOP => array(2, ",", "."), self::MRO => array(2, ",", "."), self::MUR => array(2, ",", "."), self::MVR => array(2, ",", "."), self::MWK => array(2, ",", "."), self::MXN => array(2, ",", "."), self::MYR => array(2, ",", "."), self::MZN => array(2, ",", "."), self::NAD => array(2, ",", "."), self::NGN => array(2, ",", "."), self::NIO => array(2, ",", "."), self::NOK => array(2, ",", "."), self::NPR => array(2, ",", "."), self::NZD => array(2, ",", "."), self::OMR => array(2, ",", "."), self::PAB => array(2, ",", "."), self::PEN => array(0, ".", ","), self::PGK => array(2, ",", "."), self::PHP => array(2, ",", "."), self::PKR => array(2, ",", "."), self::PLN => array(2, ",", "."), self::PYG => array(0, ".", ","), self::QAR => array(2, ",", "."), self::RON => array(2, ",", "."), self::RSD => array(2, ",", "."), self::RUB => array(2, ",", "."), self::RWF => array(2, ",", "."), self::SAR => array(2, ",", "."), self::SBD => array(2, ",", "."), self::SCR => array(2, ",", "."), self::SDG => array(2, ",", "."), self::SEK => array(2, ",", "."), self::SGD => array(2, ",", "."), self::SHP => array(2, ",", "."), self::SLL => array(2, ",", "."), self::SOS => array(2, ",", "."), self::SRD => array(2, ",", "."), self::STD => array(2, ",", "."), self::SVC => array(2, ",", "."), self::SYP => array(2, ",", "."), self::SZL => array(2, ",", "."), self::THB => array(2, ",", "."), self::TJS => array(2, ",", "."), self::TMT => array(2, ",", "."), self::TND => array(2, ",", "."), self::TOP => array(2, ",", "."), self::TTD => array(2, ",", "."), self::TWD => array(2, ",", "."), self::TZS => array(2, ",", "."), self::UAH => array(2, ",", "."), self::UGX => array(2, ",", "."), self::USD => array(2, ",", "."), self::UYU => array(0, ".", ","), self::UZS => array(2, ",", "."), self::VEF => array(0, ".", ","), self::VND => array(2, ",", "."), self::VUV => array(2, ",", "."), self::WST => array(2, ",", "."), self::XAF => array(2, ",", "."), self::XCD => array(2, ",", "."), self::XDR => array(2, ",", "."), self::XOF => array(2, ",", "."), self::XPF => array(2, ",", "."), self::YER => array(2, ",", "."), self::ZAR => array(2, ",", "."), self::ZMK => array(2, ",", "."), self::ZWL => array(2, ",", "."));
        return (isset($formats[$this->code])) ? $formats[$this->code] : null;
    }

    //Retorna el codigo de la moneda especificada
    public function getCode() {
        return $this->code;
    }

    /** Retorna la moneda por defecto usada en el sistema
     *  
     * @return type
     */
    public static function defect() {
        return self::COP;
    }

}
