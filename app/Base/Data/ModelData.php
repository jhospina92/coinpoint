<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Base\Data;

use App\Models\{
    Budget,
    Flow,
    Category,
    Credit,
    CreditTrack,
    Setting,
    Typayment,
    User,
    UserMeta
};
use ReflectionClass;

/**
 * Description of ModelData
 *
 * @author jospina
 */
class ModelData {

    const ENT_FLOW = Flow::class;
    const ENT_BUDGET = Budget::class;
    const ENT_CATEGORY = Category::class;
    const ENT_CREDIT = Credit::class;
    const ENT_CREDIT_TRACK = CreditTrack::class;
    const ENT_SETTING = Setting::class;
    const ENT_TYPAYMENT = Typayment::class;
    const ENT_USER = User::class;
    const ENT_USER_META = UserMeta::class;

    public static function getAttrsValue($class, $id) {
        if (!class_exists($class))
            return array();

        $model = call_user_func($class . "::find", $id);

        $class = new ReflectionClass($class);
        $data = array();

        foreach ($class->getConstants() as $index => $value) {
            if (strpos($index, "ATTR_") !== false && substr_count($index, "_") == 1) {
                $data[$value] = $model[$value];
            }
        }
        
        $data["id"]=$model->id;

        return $data;
    }

}
