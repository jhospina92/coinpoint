<?php

namespace App\Base\Data;

class RouteData {

    const DASHBOARD = "dashboard";
    const TYPAYMENT_INDEX = "typayment.index";
    const TYPAYMENT_CREATE = "typayment.create";
    const TYPAYMENT_EDIT = "typayment.edit";
    const TYPAYMENT_DELETE = "typayment.destroy";
    const TYPAYMENT_CANCEL = "typayment.cancel";
    const FLOW_INDEX = "flow.index";
    const FLOW_CRON_INDEX = "flow.cron.index";
    const FLOW_CRON_DELETE = "flow.cron.delete";
    const FLOW_CRON_DEFER = "flow.cron.defer";
    const FLOW_CRON_EXEC = "flow.cron.exec";
    const FLOW_CREATE = "flow.create";
    const FLOW_EDIT = "flow.edit";
    const FLOW_DELETE = "flow.destroy";
    const CREDIT_INDEX = "credit.index";
    const CREDIT_CREATE = "credit.create";
    const CREDIT_STORE = "credit.store";
    const CREDIT_EDIT = "credit.edit";
    const CREDIT_RECORD = "credit.record";
    const CREDIT_TRACK_CREATE = "credit.track.create";
    const CREDIT_TRACK_STORE = "credit.track.store";
    const BUDGET_INDEX = "budget.index";
    const BUDGET_CREATE = "budget.create";
    const BUDGET_EDIT = "budget.edit";
    const BUDGET_DELETE = "budget.destroy";
    const BUDGET_STORE = "budget.store";
    const BUDGET_UPDATE = "budget.update";
    const GOAL_INDEX = "goal.index";
    const GOAL_EDIT = "goal.edit";
    const GOAL_CREATE = "goal.create";
    const GOAL_DELETE = "goal.destroy";
    //Autoread
    const AUTOREAD_ADD = "autoread.add";
    const AUTOREAD_INDEX = "autoread.index";
    const AUTOREAD_DELETE = "autoread.delete";
    const AUTOREAD_AJAX_VALIDATE_PARAM = "autoread.ajax.validate.param";
    const AUTOREAD_AJAX_LOG_CONFIRM = "autoread.ajax.log.confirm";
    const AUTOREAD_AJAX_LOG_SKIP = "autoread.ajax.log.skip";



    /*
      |--------------------------------------------------------------------------
      | AUTH
      |--------------------------------------------------------------------------
     */
    const USER_LOGOUT = "user.logout";
    const AUTH_LOGIN = "auth.login";
    const AUTH_SIGNUP = "auth.signup";
    const AUTH_CONFIRM = "auth.confirm";
    const AUTH_FORGOTTEN = "auth.forgotten";
    const AUTH_FORGOTTEN_RESET = "auth.forgotten.reset";
    const AUTH_AJAX_RESEND_EMAIL_CONFIRM = "auth.ajax.resend.email.confirm";

    /*
      |--------------------------------------------------------------------------
      | USER ACCOUNT
      |--------------------------------------------------------------------------
     */
    const ACCOUNT_DASHBOARD = "account.dashboard";
    const ACCOUNT_PROFILE_EDIT = "account.profile.edit";
    const ACCOUNT_SECURY_RESET_PASSWORD = "account.security.reset.password";
    const ACCOUNT_SETTINGS_GENERAL = "account.settings.general";
    const ACCOUNT_SETTINGS_NOTIFICATIONS = "account.settings.notifications";

    /*
      |--------------------------------------------------------------------------
      | AJAX
      |--------------------------------------------------------------------------
     */
    const AJAX_GET_BANKS = "ajax.get.banks";
    const AJAX_POST_BUDGET_ENABLE = "ajax.post.budget.enable";
    const AJAX_POST_SET_META_USER = "ajax.post.set.meta.user";
    const AJAX_POST_BUDGET_DISABLE = "ajax.post.budget.disable";
    const AJAX_GET_DATA_MODEL_BY_ID = "ajax.get.data.model.by.id";
    const AJAX_GET_ELOQUENT_EXISTS_DATA = "ajax.get.eloquent.exists.data";
    const AJAX_POST_ELOQUENT_TOGGLE_ACTIVE = "ajax.post.eloquent.toggle.active";
    const AJAX_POST_UI_RENDER_VIEW = "ajax.post.ui.render.view";

    /*
      |--------------------------------------------------------------------------
      | SERVICES
      |--------------------------------------------------------------------------
     */
    const SERVICES_FLOW_CRON_EMAIL = "services.flow.cron.email";
    const SERVICES_API_GMAIL_AJAX_GET_URL_AUTH = "services.api.gmail.ajax.get.url.auth";
    const SERVICES_API_GMAIL_OAUTH = "services.api.gmail.oauth";

    public static function getMenu() {
        $menu = array(
//Dashboard
            self::DASHBOARD => array(
                "name" => trans("nav.route." . self::DASHBOARD),
                "icon" => "fa fa-area-chart"),
            //Transacciones
            self::FLOW_INDEX => array(
                "name" => trans("nav.route." . self::FLOW_INDEX),
                "icon" => "fa fa-exchange")
            , //Medios de pago
            self::TYPAYMENT_INDEX => array(
                "name" => trans("nav.route." . self::TYPAYMENT_INDEX),
                "icon" => "fa fa-credit-card"),
            self::GOAL_INDEX => array(
                "name" => trans("nav.route." . self::GOAL_INDEX),
                "icon" => "fa fa-flag-checkered"),
            //Presupuestos
            self::BUDGET_INDEX => array(
                "name" => trans("nav.route." . self::BUDGET_INDEX),
                "icon" => "fa fa-cubes"),
            //Prestamos
            self::CREDIT_INDEX => array(
                "name" => trans("nav.route." . self::CREDIT_INDEX),
                "icon" => "fa  fa-gg-circle"),
        );

        return $menu;
    }

    public static function getShortcutes() {
        $menu = array(
//Transacciones
            self::FLOW_CREATE => array(
                "name" => trans("nav.route." . self::FLOW_CREATE),
                "icon" => "fa fa-exchange")
            , //Medios de pago
            self::TYPAYMENT_CREATE => array(
                "name" => trans("nav.route." . self::TYPAYMENT_CREATE),
                "icon" => "fa fa-credit-card"));

        return $menu;
    }

}
