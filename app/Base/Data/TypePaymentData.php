<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Base\Data;

use App\Base\System\TP\{
    Cash,
    CreditCard,
    BankAccount,
    TypePayment
};

class TypePaymentData {

    const BALANCE = "balance";
    const CURRENCY = "currency";
    const BANK = "bank";

    /** Obtiene un array con el listado de tipos de pagos obteniendo su nombre
     * 
     * @return array
     */
    public static function getList($attr = "getName"): array {
        return array(
            Cash::ID =>($attr == "class") ? Cash::class :  call_user_func(Cash::class . "::" . $attr),
            BankAccount::ID => ($attr == "class") ? BankAccount::class : call_user_func(BankAccount::class . "::" . $attr),
            CreditCard::ID => ($attr == "class") ? CreditCard::class : call_user_func(CreditCard::class . "::" . $attr)
        );
    }

}
