<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Base\Data;

use ReflectionClass;

/**
 * Description of Lang
 *
 * @author jospina
 */
class Lang {

    const SPANISH = "es";

    //const ENGLISH = "en";

    public static function getList() {
        $class = new ReflectionClass(__CLASS__);

        $return = [];

        foreach ($class->getConstants() as $name => $data) {
            $return[$data] = self::getName($data);
        }

        return $return;
    }

    public static function getName($code) {
        return trans("attr.lang." . $code);
    }

    public static function adapt($value) {

        if (!preg_match("/trans\[(.+)\]/s", $value, $matches))
            return $value;
        
        return trans($matches[1]);
    }

}
