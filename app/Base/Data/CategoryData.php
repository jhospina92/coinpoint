<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Base\Data;

use ReflectionClass;
use App\Models\Flow;

/**
 * Description of Category
 *
 * @author jospina
 */
class CategoryData {
    /*
      |--------------------------------------------------------------------------
      | GASTOS
      |--------------------------------------------------------------------------
     */

    const SP_TAXES = "TAX"; //Impuestos
    const SP_TAXES_VEHICLE = "TAX-VEH"; //Impuesto vehicular
    const SP_TAXES_BANK_FINANCIAL = "TAX-BAFI";

    /**
     * VIVIENDA
     */
    const SP_HOME = "HOM";
    const SP_HOME_RENT = "HOM-REN"; //Arriendo
    const SP_HOME_SERVICE_ENERGY = "HOM-SEN"; //Servicio Publico - Energia Electrica
    const SP_HOME_SERVICE_WATER_SEWERAGE = "HOM-SWS"; //Servicio Publico - Agua/Alcantarillado
    const SP_HOME_SERVICE_CLEAN = "HOM-SCL"; //Servicio Publico - Aseo/Recogida de basura
    const SP_HOME_SERVICE_GAS = "HOM-SGA"; //Servicio Publico - Gas 
    const SP_HOME_SERVICE_PACK_INTERNET_TELEVISION_PHONE = "HOM-SITP"; //Servicio de Internet/Television/Linea Telefonica
    const SP_HOME_REFORMS_MANTENANCE_REPAIRS = "HOM-RMR"; //Reformas, Mantenimiento y Reparaciiones
    const SP_HOME_FURNITURE_DECORATION = "HOM-FUDE"; //Mobiliario y decoración
    const SP_HOME_APPLIANCES = "HOM-FURN"; //Electrodomesticos

    /**
     * ALIMENTACIÓN
     */
    const SP_FOOD = "FOD"; //Alimentación
    const SP_FOOD_MARKET = "FOD-MARK"; //Mercado
    const SP_FOOD_JUNK = "FOD-JUNK"; // Comida Chatarra/Chucheria 
    const SP_FOOD_RESTAURANT = "FOD-REST"; //Restaurante

    /**
     * TRANSPORTE
     */
    const SP_TRANSPORT = "TRA";
    const SP_TRANSPORT_ARTICULATED_BUS = "TRA-ARBU"; //Bus articulado
    const SP_TRANSPORT_URBAN_BUS = "TRA-URBU"; //Bus Urbano
    const SP_TRANSPORT_TAXI = "TRA-TAXI"; // Taxi
    const SP_TRANSPORT_MANTENANCE_REPAIR_VEHICLE = "TRA-MRV"; // Mantenimiento/Reparación vehicular
    const SP_TRANSPORT_TICKET_AIRWAYS = "TRA-TAIR"; // Pasaje de avión
    const SP_TRANSPORT_TICKET_TRAIN = "TRA-TTRA"; // Pasaje de tren
    const SP_TRANSPORT_TICKET_SUBWAY = "TRA-TSUB"; // Pasaje de metro
    const SP_TRANSPORT_TICKET_INTERSTATE_BUS = "TRA-TINB"; // Pasaje de bus interestatal
    const SP_TRANSPORT_PRIVATE_APP = "TRA-PRAP"; //Servicio de transporte privado a través de Aplicación Móvil
    const SP_TRANSPORT_BUY_VEHICLE = "TRA-BUYV"; //Compra de vehiculo personal
    const SP_TRANSPORT_FUEL = "TRA-FUEL"; // Gasolina/Combustible
    const SP_TRANSPORT_VEHICLE_INSURANCE = "TRA-VINS"; //Seguro de vehiculo
    const SP_TRANSPORT_VEHICLE_INFRACTIONS = "TRA-VINF"; // Multas e infracciones de transito
    const SP_TRANSPORT_VEHICLE_TOLL = "TRA-VTOL"; // Peaje 

    /**
     * DIVERSIÓN Y OCIO
     */
    const SP_FUN = "FUN";
    const SP_FUN_CINEMA_TEATHER_CONCERT = "FUN-CITC"; //Salida a Cine/Teatro/Concierto
    const SP_FUN_MUSEUM_EXPOSITION = "FUN-MUEX"; //Salida a Museo/Exposiciones
    const SP_FUN_SPORT_EVENT = "FUN-SPEV"; //Salida a Evento deportivo
    const SP_FUN_DANCE_BAR = "FUN-BAR"; //Salida a comer/bar/bailar
    const SP_FUN_BOOK_MAGZ_NEWSLETTER = "FUN-BMNE"; //Adquisición de libros/revista/periodicos/comics
    const SP_FUN_VIDEOGAME = "FUN-VID"; //Adquisición de consola/videojuegos 
    const SP_FUN_MUSIC = "FUN-MUSI"; //Adquisición de canción/música/melodia
    const SP_FUN_APP = "FUN-APP"; //Compra/Suscripción de aplicación móvil/web/destop
    const SP_FUN_SUSCRIPTION_MEDIA = "FUN-SUME"; //Suscripción a plataforma multimedia
    const SP_FUN_TRAVEL = "FUN-TRAV"; //Tour de viaje/Hotel/Turismo
    const SP_FUN_TOYS = "FUN-TRAV"; // Juguetes/Figuras/Colecciones

    /**
     * VESTUARIO
     */
    const SP_LOCKER = "LOK";
    const SP_LOCKER_SPORT = "LOK-SPOR"; // Ropa deportiva
    const SP_LOCKER_CASUAL = "LOK-CASU"; // Ropa casual
    const SP_LOCKER_ELEGANT = "LOK-ELEG"; //Ropa elegante
    const SP_LOCKER_KIDS = "LOK-KID"; //Ropa infantil
    const SP_LOCKER_UNDERWEAR = "LOK-UNDE"; //Ropa Interior
    const SP_LOCKER_SHOES = "LOK-SHOE"; // Zapatos
    const SP_LOCKER_PROPS = "LOK-PROP"; // Complementos y Accesorios

    /**
     * EDUCACIÓN
     */
    const SP_EDUCATION = "EDU";
    const SP_EDUCATION_BUY_BOOKS = "EDU-BUBO"; //Compra de libros
    const SP_EDUCATION_COURSES_CERTIFICATIONS = "EDU-COCE"; //Cursos/Certificaciones
    const SP_EDUCATION_CONFERENCES = "EDU-CONF"; //Conferencias/Seminarios
    const SP_EDUCATION_PAY_ENROLLMENT = "EDU-PAEN"; //Pago de matricula
    const SP_EDUCATION_MONTHLY = "EDU-MON"; //Mensualidad Escolar/Educativa
    const SP_EDUCATION_MATERIALS_IMPLEMENTS = "EDU-MAIM"; //Papeleria, materiales e implementos

    /**
     * SALUD
     */
    const SP_HEALTH = "HEA";
    const SP_HEALTH_INSURANCE = "HEA-INS"; //Seguro médico
    const SP_HEALTH_CONSULTATION = "HEA-CONS"; //Consulta médica
    const SP_HEALTH_MEDICATIONS = "HEA-MEDI"; //Medicamentos/Inyeccions/Vacunas/Cremas
    const SP_HEALTH_THERAPIES = "HEA-THER"; //Tratamientos/Terapias/Hospitalizaciones
    const SP_HEALTH_SURGERY = "HEA-SUR"; //Cirujias
    const SP_HEALTH_BEATY = "HEA-BEA"; //Estética y belleza
    const SP_HEALTH_OPTOMETRY = "HEA-OPT"; //Optometria/Óptica
    const SP_HEALTH_ODONTOLOGY = "HEA-ODO"; //Odontología
    const SP_HEALTH_ORTOPEDY = "HEA-ORT"; //Ortopedia
    const SP_HEALTH_AUDIOLOGY = "HEA-AUD"; //Audiologia
    const SP_HEALTH_ALTERNATIVE = "HEA-ALT"; //Médicina Alternativa
    const SP_HEALTH_IMPLEMENTS = "HEA-IMP"; //Implementos médicos

    /**
     * DONACIONES/REGALOS/CONTRIBUCIONES
     */
    const SP_DONATIONS = "DON";
    const SP_DONATIONS_CHARITY = "DON-CHAR"; //Caridad
    const SP_DONATIONS_CONTRIBUTION_FAMILY = "DON-COFA"; //Contribución familiar
    const SP_DONATIONS_CONTRIBUTION_COUPLE = "DON-COCO"; //Contribución matrimonial/sentimental
    const SP_DONATIONS_GIFS = "DON-GIFS"; //Regalo de Cumpleaños/Navidad/Aniversario/Especial
    const SP_DONATIONS_HELP_SUSTENANCE = "DON-HESU"; //Ayuda de sostenimiento
    const SP_DONATIONS_COURTESY = "DON-COUR"; //Cortesia

    /**
     * PRODUCTOS Y SERVICIOS COMPLEMENTARIOS
     */
    const SP_SERVICES = "PSE";
    const SP_SERVICES_INSURANCES = "PSE-INSU"; //Seguros
    const SP_SERVICES_MOBILE = "PSE-MOBI"; //Teléfonia Móvil 
    const SP_SERVICES_LOTTERY = "PSE-LOTT"; //Loterias y Apuestas
    const SP_SERVICES_COMMISSIONS_INTERESTS = "PSE-COIN"; //Comisiones e Intereses 
    const SP_SERVICES_BANKS = "PSE-BANK"; //Servicios Bancarios
    const SP_SERVICES_SOFTWARE = "PSE-SOFT"; //Software
    const SP_SERVICES_LEGALS = "PSE-LEGA"; //Servicios Legales
    const SP_SERVICES_NOTARIALS = "PSE-NOTA"; //Servicios Notariales
    const SP_SERVICES_PAPERS = "PSE-PAPE";  //Papeleria

    /*
      |--------------------------------------------------------------------------
      | INGRESOS
      |--------------------------------------------------------------------------
     */

    /**
     *  Empleo formal
     */
    const EN_JOB = "JOB";
    const EN_JOB_SALARY = "JOB-SAL"; //Salario
    const EN_JOB_BONUS = "JOB-BON"; //Bonos
    const EN_JOB_PERFOMANCE = "JOB-PER"; //Prestaciones

    /**
     * Actividades Profesionales
     */
    const EN_PROFESSIONAL = "PRO";
    const EN_PROFESSIONAL_FEE = "PRO-FEE"; //Honorarios
    const EN_PROFESSIONAL_COMMISSIONS = "PRO-COM"; //Comisiones

    /**
     *  Actividades Empresariales e Inversiones
     */
    const EN_COMPANY = "COM";
    const EN_COMPANY_DIVIDENS = "COM-DIV"; //Dividendos
    const EN_COMPANY_SETTLEMENT = "COM-SET"; //Liquidación
    const EN_COMPANY_COMMISSIONS = "COM-COM"; //Comisiones
    /**
     * Propiedades de Finca raiz
     */
    const EN_PROPERTIES = "PPR";
    const EN_PROPERTIES_SALE = "PPR-SAL"; //Venta
    const EN_PROPERTIES_RENTAL = "PPR-REN"; //Arrendamiento

    /**
     * Liquidacion de bienes
     */
    const EN_SETTLEMENT = "SET";
    const EN_SETTLEMENT_FURNITE = "SET-FUR"; //Mobiliario
    const EN_SETTLEMENT_HOME_APPLIANCE = "SET-HOA"; //Electródomesticos
    const EN_SETTLEMENT_CLOTHING = "SET-CLO"; // Vestimenta
    const EN_SETTLEMENT_VEHICLES = "SET-VEH"; // Vehiculos
    const EN_SETTLEMENT_OTHERS = "SET-OTH"; //Otros

    /**
     * Otros
     */
    const EN_OTHERS = "ENO";
    const EN_OTHERS_AWARDS = "ENO-AWA"; //Obtención de premio
    const EN_OTHERS_INTERESTS = "ENO-INT"; //Intereses
    const EN_OTHERS_GIFT = "ENO-GIF"; //Regalo

    /**
     * EXTENSIONES
     */
    const EXT_CREDITS_TRACK = "CREDTCK";
    const EXT_CREDITS_CARD = "CREDCAR";

    /** Retorna una array con el listado de categorias de gastos
     * 
     * @return array
     */
    public static function getSpendList(): array {
        $array = [];
        $class = new ReflectionClass(__CLASS__);

        foreach ($class->getConstants() as $index => $value) {

            ///Busca la categoria principal
            if (strpos($index, "SP_") !== false && substr_count($index, "_") == 1) {

                $array[$value]["name"] = trans("attr.category." . $value);
                $category_id = $index;

                $subcat = array();
                //Jerarquiza las subcategorias
                foreach ($class->getConstants() as $const => $val) {

                    if (strpos($const, $category_id) !== false && $const != $category_id) {
                        $subcat[$val] = trans("attr.category." . $val);
                    }
                }

                $array[$value]["sub"] = $subcat;
            }
        }

        return $array;
    }

    /** Retorna un array un listado de categorias
     * 
     * @param type $prefix Si se define [SP|EN] Retornara el listado de las subcategorias
     * @return array
     */
    public static function getList($prefix = null):array {
        $array = [];
        $class = new ReflectionClass(__CLASS__);

        foreach ($class->getConstants() as $index => $value) {
            if (!is_null($prefix)) {
                if (strpos($index, $prefix . "_") !== false && substr_count($index, "_") > 1) {
                    $array[$value] = trans("attr.category." . $value);
                    continue;
                }
            } else {
                $array[$value] = trans("attr.category." . $value);
            }
        }

        return $array;
    }
    
    /** Retorna un array con el listado de las categorias principales
     * 
     * @param type $prefix
     * @return array
     */
    public static function getListCatMain($prefix="SP"):array{
         $array = [];
        $class = new ReflectionClass(__CLASS__);

        foreach ($class->getConstants() as $index => $value) {
            if (!is_null($prefix)) {
                if (strpos($index, $prefix . "_") !== false && (substr_count($index, "_") ==1)) {
                    $array[$value] = trans("attr.category." . $value);
                    continue;
                }
            }
        }

        return $array;
    }

    /** Retorna un array con el listado de categorias de tipo ingresos
     * 
     * @return array
     */
    public static function getEntryList(): array {
        $array = [];
        $class = new ReflectionClass(__CLASS__);

        foreach ($class->getConstants() as $index => $value) {

            ///Busca la categoria principa
            if (strpos($index, "EN_") !== false && substr_count($index, "_") == 1) {

                $array[$value]["name"] = trans("attr.category." . $value);
                $category_id = $index;

                $subcat = array();
                //Jerarquiza las subcategorias
                foreach ($class->getConstants() as $const => $val) {

                    if (strpos($const, $category_id) !== false && $const != $category_id) {
                        $subcat[$val] = trans("attr.category." . $val);
                    }
                }

                $array[$value]["sub"] = $subcat;
            }
        }

        return $array;
    }

    /** Retorna el nombre de la categoria principal dado el codigo de una subcategoria
     * 
     * @param type $subcat
     */
    public static function getCatMainName($subcat): string {
        $data = explode("-", $subcat);
        return self::getName($data[0]);
    }

    public static function getCatMain($subcat): string {
        $data = explode("-", $subcat);
        return $data[0];
    }

    /**
     * Obtiene el nombre de la categoria dado su codigo
     */
    public static function getName($code) {

        if (strpos($code, self::EXT_CREDITS_TRACK . "-") !== false) {
            $credit = \App\Models\Credit::find(explode("-", $code)[1]);
            return trans("ui.label.fee") . " " . $credit->organization;
        }

        return trans("attr.category." . $code);
    }

    /** Retorna el tipo de transacción asociado a la categoria dado por el codigo. 
     * 
     * @param String $code
     * @return String
     */
    public static function getType($code) {
        $class = new ReflectionClass(__CLASS__);

        foreach ($class->getConstants() as $index => $value) {

            if ($code != $value)
                continue;


            //Gasto
            if (strpos($index, Flow::TYPE_SPEND) !== false || strpos($index, "EXT_") !== false) {
                return Flow::TYPE_SPEND;
            }
            //Ingreso
            if (strpos($index, Flow::TYPE_REVENUE) !== false) {
                return Flow::TYPE_REVENUE;
            }

            //Transferencia
            if (strpos($index, Flow::TYPE_EXCHANGE) !== false) {
                return Flow::TYPE_EXCHANGE;
            }

            //Inversión
            if (strpos($index, Flow::TYPE_INVESTMENT) !== false) {
                return Flow::TYPE_INVESTMENT;
            }
        }

        return Flow::TYPE_SPEND;
    }

}
