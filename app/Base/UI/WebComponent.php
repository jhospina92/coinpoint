<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Base\UI;

use ReflectionClass;
use App\Base\UI\WebComponents\{
    WCGoalProgress,
    WCBudgetProgressTime,
    WCPageLoader,
    WCLoaderSand
};
use \App\Base\System\Library\Comp\Util;
use \App\Base\UI\HTML;

/**
 * Description of WebComponent
 *
 * @author jospina
 */
class WebComponent {

    //Components names
    const AS_BUDGET_PROGRESS_TIME = "budget-progress-time";
    const AS_GOAL_PROGRESS = "goal-progress";
    const AS_PAGE_LOADER = "page-loader";
    const AS_LOADER_SAND = "loader-sand";
    //Paths
    const PATH_ASSETS = "assets/app/webcomponents/";
    const PATH_VIEW = "ui/ext/webcomponents/";
    const SOURCE = [
        self::AS_GOAL_PROGRESS => WCGoalProgress::class,
        self::AS_BUDGET_PROGRESS_TIME => WCBudgetProgressTime::class,
        self::AS_PAGE_LOADER => WCPageLoader::class,
        self::AS_LOADER_SAND => WCLoaderSand::class,
    ];

    public static function invoke($name, $sources = true) {

        if (!self::exists($name))
            return "<div class='msg-notice'><i class='fa fa-warning' aria-hidden='true'></i> ¡WebComponent <b>$name</b> not found!</div>";

        $class = self::SOURCE[$name];

        $content = $class::getHtml();

        if ($sources)
            $content .= html_entity_decode(self::assets($name));

        return $content;
    }

    /** Imprime los recursos de JS y CSS de un componente web dado por su nombre
     * 
     * @param type $name
     */
    private static function assets($name) {

        global $webcomponent;

        //Evita que se vuelva a importar los recursos JS y CSS si hay más de una invocación en pantalla de un componente
        if (isset($webcomponent[$name]))
            return;

        $webcomponent[$name] = true;

        $url_script = url(self::PATH_ASSETS . "$name/script.js");
        $url_style = url(self::PATH_ASSETS . "$name/style.css");

        $html = "";

        if (Util::UrlExist($url_script))
            $html .= HTML::script($url_script);

        if (Util::UrlExist($url_style))
            $html .= HTML::style(url($url_style), array("media" => "screen"));

        return $html;
    }

    private static function exists($name) {
        $class = new ReflectionClass(__CLASS__);
        foreach ($class->getConstants() as $name_const => $value) {
            if ($value == $name) {
                return true;
            }
        }

        return false;
    }

}
