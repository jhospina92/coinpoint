<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Base\UI;

use Collective\Html\HtmlFacade;
use App\Base\System\Library\Comp\DateUtil;
use Collective\Html\FormFacade as Form;

/**
 * Description of HTML
 *
 * @author jospina
 */
class HTML {

    public static function style($filename, $data = array()) {
        return HtmlFacade::style($filename . "?v=" . trans("app.version"), $data);
    }

    public static function script($filename, $data = array()) {
        return HtmlFacade::script($filename . "?v=" . trans("app.version"), $data);
    }

    /** Retorna una etiqueta IMG con el icono indicado
     * 
     * @param type $filename
     * @return type
     */
    public static function icon($filename) {
        return "<img class='icon' src='" . url("assets/images/app/icons/" . $filename) . ".png'>";
    }

    /** Retorna una plantilla de paginación de resultados enfocado en AJAX
     * 
     * @param type $results
     * @param type $class
     * @param type $data
     * @return string
     */
    public static function pagination($results, $class = null, $data = array()) {

        $page_current = $results->currentPage();
        $total = $results->total();
        $total_page = ceil($results->total() / $results->count());
        $html = "<div class='content-pagination'>";

        $html .= '<div class="btn-group">';

        //Funcion anonima para crear un boton
        $button = function($page, $text, $css_class = null) use ($class, $page_current, $data, $total_page) {

            $button = "<button type='button' data-total='$total_page' data-label='$text' class='btn btn-default btn-sm $class $css_class" . (($page_current == $page && is_int($text)) ? " active" : "") . "'";

            foreach ($data as $key => $value) {
                $button .= " data-$key='$value' ";
            }

            $button .= " data-pagination='" . json_encode(["currentpage" => $page_current, "page" => $page, "total_page" => $total_page]) . "' ";

            return $button .= ">$text</button>";
        };


        //Botón a la izquierda
        $html .= $button(1, '<i class="fa fa-angle-double-left" aria-hidden="true"></i>', 'btn-pag-nav');

        for ($i = 1; $i <= $total_page; $i++) {
            $html .= $button($i, $i);
        }

        //Botton a la derecha
        $html .= $button($i - 1, '<i class="fa fa-angle-double-right" aria-hidden="true"></i>', 'btn-pag-nav');

        $html .= "</div></div>";

        return $html;
    }

    /** Retorna una campo tipo Input HTML de formulario
     * 
     * @param String $type El tipo de campo
     * @param String $name El nombre del campo
     * @param String $title El texto mostrar
     * @param Boolean $required [True] Si es requerido
     * @param String $help_text [null] Texto de ayuda
     * @param String $value El valor [null]
     * @param Array $attrs Atributos adicionales [array]
     * @return string
     */
    public static function getInputForm($type, $name, $title, $required = true, $help_text = null, $value = null, $attrs = array()) {

        $html = "<div class='md-form'>";

        switch ($type) {
            case "date":
                $html .= "<i class='prefix'><li class='fa fa-calendar'></li></i>";
                $html .= "<input id='fd-$name' data-value='" . ($value ?? "today") . "' " . (($required) ? "data-required" : "") . "  type='text' data-attr-format='yyyy/mm/dd' name='" . $name . "' class='form-control datepicker' value='' maxlength='20' ";
                break;
            case "number":
                $html .= "<input id='fd-$name' " . (($required) ? "data-required='number'" : "") . "  type='number' name='" . $name . "' class='form-control' value='$value'";
                break;
        }

        foreach ($attrs as $index => $value) {
            if (preg_match("/date\[(.+)\]/", $value, $match)) {
                $value = DateUtil::uml($match[1]);
            }

            $html .= "$index='$value'";
        }

        $html .= ">";

        $html .= "<label for='fd-$name'>" . $title . " ";
        if (!empty($help_text))
            $html .= "<span class='pull-right help' data-toggle='tooltip' data-placement='top' title='$help_text'><i class='fa fa-question-circle' aria-hidden='true'></i></span>";
        $html .= "</label>";

        $html .= "</div>";
        return $html;
    }

    /** Retorna un campo del tipo Select en HTML para un formulario
     * 
     * @param String $name El nombre del campo
     * @param String $title El titulo a mostrar
     * @param Boolean $required  Si es requerido
     * @param String $help_text Un texto de ayuda
     * @param Array $data [Empty] Los datos de seleccion
     * @param String $value [null] El valor por defecto
     * @return string
     */
    public static function getSelectForm($name, $title, $required = true, $data = [], $help_text = null, $value = null) {
        $html = "<div class='md-form'>";


        $html .= " <select id='sf-$name' name='$name' class='mdb-select' " . (($required) ? "data-required" : "") . ">";
        foreach ($data as $index => $text) {
            if (!is_array($text))
                $html .= "<option value='$index' " . (($index == $value) ? "selected" : "") . ">$text</option>";
            else
                $html .= "<option value='$index' " . (($index == $value) ? "selected" : "") . " data-icon='" . $text[1] . "' class='rounded-circle'>$text[0]</option>";
        }
        $html .= "</select>";

        $html .= "<label for='fd-$name'>" . $title . " ";
        if (!empty($help_text))
            $html .= "<span class='pull-right help' data-toggle='tooltip' data-placement='top' title='$help_text'><i class='fa fa-question-circle' aria-hidden='true'></i></span>";
        $html .= "</label>";
        $html .= "</div>";
        return $html;
    }

}
