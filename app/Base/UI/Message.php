<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Base\UI;

use Illuminate\Support\Facades\Session;
use App\Base\Data\RouteData;

/**
 * Description of Message
 *
 * @author ingen
 */
class Message {

    const ALERT_TYPE_INFO = "info";
    const ALERT_TYPE_SUCCESS = "success";
    const ALERT_TYPE_ERROR = "danger";
    const ALERT_TYPE_WARNING = "warning";

    /** Set a session var for show alert message to user
     * 
     * @param type $type Tipo de mensaje (info,success,error,warning)
     * @param type $message El mensaje a mostrar
     * @param type $params [null] Clases de estilo opcionales
     * @param type $pos [1] Posicion id del mensaje
     * @return type
     */
    public static function alert($message, $type = self::ALERT_TYPE_SUCCESS) {
        Session::flash("ui.message.alert.type", $type);
        Session::flash("ui.message.alert.message", $message);
        return redirect()->back();
    }

    /** Set a session var for show a modal message to user
     * 
     * @param type $message
     * @param type $title
     * @param type $footer
     * @return void
     */
    public static function modal($message, $title = null, $footer = true, $size = "lg") {

        //Defaults params
        $title = (is_null($title)) ? trans("ui.label.message") : $title;
        $footer = ($footer) ? '<button type="button" class="btn btn-primary" data-dismiss="modal">' . trans("ui.label.close") . '</button>' : $footer;

        if (Session::has("ui.message.modal.title"))
            return;

        Session::flash("ui.message.modal.title", $title);
        Session::flash("ui.message.modal.message", $message);
        Session::flash("ui.message.modal.footer", $footer);
        Session::flash("ui.message.modal.size", $size);
    }

    public static function clearSessions() {
        Session::forget("ui.message.modal.title");
        Session::forget("ui.message.modal.message");
        Session::forget("ui.message.modal.footer");
        Session::forget("ui.message.modal.size");
    }

}
