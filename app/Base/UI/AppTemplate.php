<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Base\UI;

use Illuminate\Support\Facades\Request;
use App\Base\Data\RouteData;
use Illuminate\Support\Facades\Lang;
use App\Base\UI\WebComponent;
use App\Models\Flow;

/**
 * Description of AppTemplate
 *
 * @author jospina
 */
class AppTemplate {

    const SN_URL_FACEBOOK = "https://www.facebook.com/";
    const SN_URL_TWITTER = "https://www.twitter.com/";
    const SN_URL_INSTAGRAM = "https://www.instagram.com/";

    public static function getTitle() {
        $base = ((!is_null(Request::route()) && Lang::has("nav.route." . Request::route()->getName())) ? trans("nav.route." . Request::route()->getName()) : trans("ui.page.not.available")) . " | " . trans("app.website.title") . " - " . trans("app.website.description");
        return $base;
    }

    public static function getDataUI() {
        $app = [];

        $app["msg"]["error"]["input"]["compare"]["values"] = trans("ui.msg.error.input.compare.values");
        $app["msg"]["general"]["system"]["not"]["data"] = trans("ui.msg.not.show.data");
        $app["msg"]["general"]["system"]["loading"] = trans("ui.msg.loading.please");
        $app["msg"]["success"]["default"] = trans("ui.msg.suc.fine");
        $app["msg"]["error"]["input"]["format"]["invalid"] = trans("ui.msg.error.input.format.invalid");
        $app["msg"]["error"]["input"]["email"]["exists"] = trans("ui.msg.error.input.email.exists");
        $app["msg"]["error"]["input"]["password"]["invalid"] = trans("ui.msg.error.input.password.invalid");
        $app["msg"]["error"]["input"]["compare"]["invalid"] = trans("ui.msg.error.input.compare.invalid");
        $app["url"]["ajax"]["get"]["eloquent"]["exists"]["data"] = route(RouteData::AJAX_GET_ELOQUENT_EXISTS_DATA);

        /*
          |--------------------------------------------------------------------------
          | MENSAJES DE INTERFAZ DE USUARIO
          |--------------------------------------------------------------------------
         */

        $app["msg"]["error"]["input"]["any"]["empty"] = trans("ui.msg.error.input.numeric.empty");
        $app["msg"]["error"]["input"]["compare"]["values"] = trans("ui.msg.error.input.compare.values");
        $app["msg"]["error"]["input"]["min"]["length"] = trans("ui.msg.error.input.min.length");
        $app["msg"]["error"]["input"]["numeric"]["not_valid"] = trans("ui.msg.error.input.numeric.not.valid");
        $app["msg"]["error"]["input"]["numeric"]["attr_minor_to"] = trans("ui.msg.error.input.numeric.attr.minor.to");
        $app["msg"]["error"]["input"]["numeric"]["exceed_value_to"] = trans("ui.msg.error.input.numeric.exceed.value.to");
        $app["msg"]["error"]["select"] = trans("ui.msg.error.select");
        $app["msg"]["error"]["form"]["inputs"]["invalid"] = trans("ui.msg.error.form.inputs.invalid");
        $app["msg"]["error"]["system"]["request"]["default"] = trans("ui.msg.error.system.request.default");
        $app["msg"]["success"]["system"]["request"]["default"] = trans("ui.msg.success.system.request.default");
        $app["msg"]["general"]["system"]["not"]["data"] = trans("ui.msg.not.show.data");



        /*
          |--------------------------------------------------------------------------
          | Traducciones de texto usandos en aplicaciones javascript
          |--------------------------------------------------------------------------
         */

        //Nombre de meses
        $app["trans"]["date"]["january"]["name"] = trans("ui.date.january.name");
        $app["trans"]["date"]["febrary"]["name"] = trans("ui.date.febrary.name");
        $app["trans"]["date"]["march"]["name"] = trans("ui.date.march.name");
        $app["trans"]["date"]["april"]["name"] = trans("ui.date.april.name");
        $app["trans"]["date"]["may"]["name"] = trans("ui.date.may.name");
        $app["trans"]["date"]["june"]["name"] = trans("ui.date.june.name");
        $app["trans"]["date"]["july"]["name"] = trans("ui.date.july.name");
        $app["trans"]["date"]["august"]["name"] = trans("ui.date.august.name");
        $app["trans"]["date"]["september"]["name"] = trans("ui.date.september.name");
        $app["trans"]["date"]["october"]["name"] = trans("ui.date.october.name");
        $app["trans"]["date"]["november"]["name"] = trans("ui.date.november.name");
        $app["trans"]["date"]["december"]["name"] = trans("ui.date.december.name");

        //Abreviaturas de meses
        $app["trans"]["date"]["january"]["abv"] = trans("ui.date.january.abv");
        $app["trans"]["date"]["febrary"]["abv"] = trans("ui.date.febrary.abv");
        $app["trans"]["date"]["march"]["abv"] = trans("ui.date.march.abv");
        $app["trans"]["date"]["april"]["abv"] = trans("ui.date.april.abv");
        $app["trans"]["date"]["may"]["abv"] = trans("ui.date.may.abv");
        $app["trans"]["date"]["june"]["abv"] = trans("ui.date.june.abv");
        $app["trans"]["date"]["july"]["abv"] = trans("ui.date.july.abv");
        $app["trans"]["date"]["august"]["abv"] = trans("ui.date.august.abv");
        $app["trans"]["date"]["september"]["abv"] = trans("ui.date.september.abv");
        $app["trans"]["date"]["october"]["abv"] = trans("ui.date.october.abv");
        $app["trans"]["date"]["november"]["abv"] = trans("ui.date.november.abv");
        $app["trans"]["date"]["december"]["abv"] = trans("ui.date.december.abv");
        //Nombre de dias de la semana
        $app["trans"]["date"]["monday"]["name"] = trans("ui.date.monday.name");
        $app["trans"]["date"]["tuesday"]["name"] = trans("ui.date.tuesday.name");
        $app["trans"]["date"]["wednesday"]["name"] = trans("ui.date.wednesday.name");
        $app["trans"]["date"]["thursday"]["name"] = trans("ui.date.thursday.name");
        $app["trans"]["date"]["friday"]["name"] = trans("ui.date.friday.name");
        $app["trans"]["date"]["saturday"]["name"] = trans("ui.date.saturday.name");
        $app["trans"]["date"]["sunday"]["name"] = trans("ui.date.sunday.name");
        //Dias de la semana abreviados
        $app["trans"]["date"]["monday"]["abv"] = trans("ui.date.monday.abv");
        $app["trans"]["date"]["tuesday"]["abv"] = trans("ui.date.tuesday.abv");
        $app["trans"]["date"]["wednesday"]["abv"] = trans("ui.date.wednesday.abv");
        $app["trans"]["date"]["thursday"]["abv"] = trans("ui.date.thursday.abv");
        $app["trans"]["date"]["friday"]["abv"] = trans("ui.date.friday.abv");
        $app["trans"]["date"]["saturday"]["abv"] = trans("ui.date.saturday.abv");
        $app["trans"]["date"]["sunday"]["abv"] = trans("ui.date.sunday.abv");
        //-----------
        $app["trans"]["date"]["today"]["name"] = trans("ui.date.label.today");

        //UI Calendario
        $app["trans"]["ui"]["date"]["next_month"] = trans("ui.date.ui.next.month");
        $app["trans"]["ui"]["date"]["prev_month"] = trans("ui.date.ui.prev.month");
        $app["trans"]["ui"]["date"]["select_month"] = trans("ui.date.ui.select.month");
        $app["trans"]["ui"]["date"]["select_year"] = trans("ui.date.ui.select.year");
        //Interfaz
        $app["trans"]["ui"]["label"]["clear"] = trans("ui.label.clear");
        $app["trans"]["ui"]["label"]["close"] = trans("ui.label.close");
        $app["trans"]["ui"]["label"]["enable"] = trans("ui.label.enable");
        $app["trans"]["ui"]["label"]["disable"] = trans("ui.label.disable");
        $app["trans"]["ui"]["label"]["warning"] = trans("ui.label.warning");
        $app["trans"]["ui"]["label"]["select"] = trans("ui.label.select");
        $app["trans"]["ui"]["label"]["linked"] = trans("ui.label.linked");
        $app["trans"]["ui"]["label"]["unliked"] = trans("ui.label.unliked");
        $app["trans"]["attr"]["flow"][Flow::TYPE_REVENUE] = Flow::getUtilTypeName(Flow::TYPE_REVENUE);
        $app["trans"]["attr"]["flow"][Flow::TYPE_SPEND] = Flow::getUtilTypeName(Flow::TYPE_SPEND);
        $app["trans"]["attr"]["flow"][Flow::TYPE_EXCHANGE] = Flow::getUtilTypeName(Flow::TYPE_EXCHANGE);


        /*
          |--------------------------------------------------------------------------
          | FORMATOS
          |--------------------------------------------------------------------------
         */

        $app["format"]["date"]["text"] = trans("app.date.format.text");
        $app["format"]["date"]["submit"] = trans("app.date.format.submit");


        /*
          |--------------------------------------------------------------------------
          | ELEMENTOS DE INTERFAZ DE USUARIO
          |--------------------------------------------------------------------------
         */

        $app["ui"]["component"]["loader"]["sand"] = WebComponent::invoke("loader-sand");

        return $app;
    }

    /** Retorna una vista renderizada de un fragmento de una vista principal obtenida de un directorio /states como complemento de un estado a la vista
     * 
     * @param String $view El nombre de la vista
     * @param Array $states [STATENAME...] Los nombre de los estados
     * @return View
     * @throws \Exception
     */
    public static function getState($view, array $states, $data = array()) {
        $paths = explode(".", $view);
        $view_name = array_pop($paths);
        $dircurrent = implode(".", $paths);

        $viewstate = $dircurrent . ".states." . $view_name . "-" . ((!isset($_GET["st"])) ? $states[0] : $states[array_search($_GET["st"], $states)]);

        if (!view()->exists($viewstate))
            throw new \Exception('The view state not exist in ' . str_replace(".", "/", $viewstate) . \Illuminate\Support\Facades\Route::current()->parameter('user'));

        return view($viewstate, $data)->render();
    }

    /** Aborta la ejecución y muestra una vista de error HTTP
     * 
     * @param type $code Codigo del error HTTP
     * @param type $message Mensaje error
     */
    public static function abort($code, $message = null) {
        exit(view("errors/" . $code)->with("exception", new \Exception($message))->render());
    }

}
