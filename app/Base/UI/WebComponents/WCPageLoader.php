<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Base\UI\WebComponents;

use App\Base\UI\WebComponents\WC;

/**
 * Description of WCPageLoader
 *
 * @author jospina
 */
class WCPageLoader implements WC {

    //put your code here
    public static function getHtml($user_id = null) {

        $html = '<page-loader>';
        $html .= '<display>';
        $html .= '<div id="coin-flip-cont">';
        $html .= '<div id="coin" class="animation">';
        $html .= '<div class="front"></div>';
        $html .= '<div class="back"></div>';
        $html .= '</div>';
        $html .= '<div class="cs-loader">';
        $html .= '<div class="cs-loader-inner">';

        for ($i = 0; $i < 6; $i++) {
            $html .= '<label>●</label>';
        }

        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="cs-ms"></div>';
        $html .= '</display>';
        $html .= '</page-loader>';

        return $html;
    }

}
