<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Base\UI\WebComponents;

use App\Base\UI\WebComponents\WC;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

/**
 * Description of WCBudgetProgressTime
 *
 * @author jospina
 */
class WCBudgetProgressTime implements WC {

    public static function getHtml($user_id = null) {

        $user = User::find($user_id ?? ((Auth::check()) ? Auth::user()->id : 0));

        $html = "";

        if (empty($user))
            return $html;

        $budget = $user->getBudgetActive();


        if (empty($budget))
            return $html;


        $html .= '<budget-progress-time date-start = "' . $budget->date_start . '" date-end = "' . $budget->date_end . '" count = "' . $budget->getCountMonthsValidity() . '"></budget-progress-time>';

        return $html;
    }

}
