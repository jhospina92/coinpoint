<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Base\UI\WebComponents;

use App\Base\Data\Currency;
use App\Models\Flow;
use App\Base\System\Library\Comp\DateUtil;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Base\Data\RouteData;
use Illuminate\Support\Facades\Cache;
use App\Base\UI\WebComponent;

/**
 * Description of WCGoalProgress
 *
 * @author jospina
 */
class WCGoalProgress {

    public static function getHtml($user_id = null) {

        $user = User::find($user_id ?? ((Auth::check()) ? Auth::user()->id : 0));
        $html = "";


        if (empty($user))
            return $html;

        $cachekey = $user->id . "-wc-goals";

        if (Cache::has($cachekey))
            return Cache::get($cachekey);

        $goals = $user->goals;
        $cucu = $user->getMetaCurrency();

        if (empty($goals))
            return $html;

        foreach ($goals as $goal) {

            $date_start = $date_loop = new DateUtil($goal->created_at);
            $date_current = new DateUtil();
            $date_end = new DateUtil($goal->date);
            $numonths = ceil(DateUtil::difSec($date_start, $date_end) / 60 / 60 / 24 / 30);
            $amount_current = ($user->getAbsBalance() * floatval($goal->rate_saving));
            $progress = ($amount_current > $goal->getValue()) ? 100 : ($amount_current / $goal->getValue()) * 100;
            $goal_rest_value = $goal->getValue() - $amount_current;

            $html .= '<goal-progress  id="goal-' . $goal->id . '" ';
            $html .= 'date-start="' . explode(" ", $goal->created_at)[0] . '" ';
            $html .= 'date-end="' . $goal->date . '" ';
            $html .= 'goal-value="' . $goal->value . '" ';
            $html .= 'goal-label="' . Currency::getFormattedValue($goal->value, $cucu) . '" ';
            $html .= '> ';
            $html .= '<div class="title" style="display:none;">' . sprintf(trans("ui.msg.goal.rate.saving"), floatval($goal->rate_saving * 100) . "%", $goal->name) . ' ' . (($goal_rest_value > 0) ? "<span class='pull-right'>" . sprintf(trans("ui.msg.goal.rest.value"), Currency::getFormattedValue($goal_rest_value, $cucu)) . "</span>" : "" ) . '</span></div> ';
            $html .= '<display style="display:none;">';

            $year = $date_loop->year;
            $intervals = array();

            for ($i = 0; $i < $numonths; $i++) {
                $saving_min = $goal->getSavingMin($date_loop->year, $date_loop->month);

                //Obtiene el ahorro actual del mes
                $saving_current = ((Flow::getRevenue($date_loop->year, $date_loop->month, $user->id) - Flow::getSpend($date_loop->year, $date_loop->month, $user->id))) * floatval($goal->rate_saving);

                //Calcula el ahorro que falta para alcanzar el objetivo del mes
                $saving_rest = $saving_min - $saving_current;

                $interval = "<interval month='" . ($date_loop->month) . "' style='width:" . (100 / $numonths) . "%'";
                $interval .= " label-month-name='" . $date_loop->getMonth() . "'";

                if ($date_loop->month == $date_current->month && $date_loop->getYear() == $date_current->getYear())
                    $interval .= " class='active " . (($saving_rest <= 0) ? "complete" : "") . "' " . (($saving_rest > 0) ? 'data-toggle="tooltip" data-placement="bottom" title="' . sprintf(trans("ui.msg.goal.rest.saving.current"), strip_tags(Currency::getFormattedValue($saving_min / $goal->rate_saving, $cucu))) . '"' : "");

                $interval .= ">";

                if ($date_loop->getMonth() == 12)
                    $interval .= "<newyear>" . implode("<br/>", explode("", $date_loop->getMonth())) . "</newyear>";

                //Mes actual
                if ($date_loop->month == $date_current->month && $date_loop->getYear() == $date_current->getYear()) {
                    $interval .= "<progress-saving style='height:" . (100 - (((($saving_rest > 0) ? $saving_rest : 0) / $saving_min) * 100)) . "%'></progress-saving>";
                }

                if (($date_loop->month < $date_current->month && $date_loop->getYear() <= $date_current->getYear()) || $date_loop->getYear() < $date_current->getYear())
                    $interval .= "<result>" . (($saving_rest > 0) ? "<i class='fa fa-remove'></i>" : "<i class='fa fa-check'></i>") . "</result>";
                else
                    $interval .= (($saving_rest > 0) ? "<saving>" . Currency::getFormattedValue($saving_rest, $cucu) . "</saving>" : "<result><i class='fa fa-check'></i></result>" );

                $interval .= "</interval>";

                $date_loop->addMonths(1);
                $intervals[] = $interval;

                if ($year != $date_loop->year || $i == $numonths - 1) {
                    $html .= "<year value='$year' " . ((($date_loop->year - 1) == $date_current->year) ? "class='active'" : "") . ">" . "<div class='open-year' data-value='$year'>" . implode("<br/>", str_split($year)) . "</div>" . implode("", $intervals) . "</year>";
                    $year = $date_loop->year;
                    $intervals = array();
                    continue;
                }
            }

            $html .= '</display>';
            $html .= '<goal>' . $goal->getValue(true) . '</goal>';
            $html .= '<div class="progress" style="display:none;"><div class="progress-bar" style="width:' . $progress . '%;"></div></div>';



            if ($goal_rest_value <= 0) {

                if (is_null($goal->date_complete)) {
                    $goal->date_complete = date(DateUtil::FORMAT_STANDARD);
                    $goal->save();
                }

                $html .= '<div class="msg-complete" style="display:none;">';
                $html .= '<h3>' . trans("ui.msg.goal.complete.congratulations") . '</h3>';
                $html .= '<a class="btn btn-primary btn-sm btn-destroy" data-msg="false" data-msg-success="' . trans("nav.route.goal.delete.terminate") . '" data-action="' . route(RouteData::GOAL_DELETE, $goal->id) . '" data-tag="#goal-' . $goal->id . '" data-func-success="goal_callback_success_complete_from_widget">' . trans("ui.msg.label.terminate") . '</a>';
                $html .= '</div>';
            } else {
                $goal->date_complete = null;
                $goal->save();
            }

            $html .= WebComponent::invoke(WebComponent::AS_LOADER_SAND, false);

            $html .= '</goal-progress>';
        }

        return $html;
    }

}
