<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Base\UI;

use ReflectionClass;

/**
 * Description of Widge
 *
 * @author jospina
 */
class Widget {

    const PATH = "ui/dashboard/widgets/";
    const COMP_GRAPH_SPEND = "graph-spend";
    const COMP_GRAPH_REVENUE = "graph-revenue";
    const COMP_GRAPH_SAVING = "graph-saving";
    const COMP_GRAPH_GROWTH = "graph-growth";
    const COMP_CREDIT_CARDS = "credit-cards";
    const COMP_SPEND_CATEGORIES = "spend-categories";
    const COMP_BUDGET = "budget";

    public static function getList() {

        $class = new ReflectionClass(__CLASS__);
        $return = [];

        foreach ($class->getConstants() as $name => $data) {
            if (strpos($name, "COMP_") !== false) {
                $return[] = self::PATH . $data;
            }
        }
        return $return;
    }

}
