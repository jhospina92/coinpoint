<?php

namespace App\Http\Controllers\API\V1\Resources;

use App\Http\Controllers\API\V1\Controller;
use Illuminate\Http\Request;
use App\Base\System\Library\Networks\API;
use App\Http\Controllers\API\V1\Handler as HandlerAPI;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use App\Models\UserMeta;
use App\Base\Data\Currency;
use App\Base\Data\Lang;

class Users extends Controller {

    const ALIAS = "users";

    function __construct(Request $request) {
        parent::__construct($request, User::class);
    }

    /** [GET] Retorna listado de usuarios limitado a 100 por pagina
     * 
     * @return json
     */
    public function index(Request $request) {
        $params = $request->all();
        return $this->api->index($params);
    }

    /** [GET] Muestra la información de un usuario
     * 
     * @return json
     */
    public function show($id) {

        if (Session::get("api.user.type") == "suscriber" && Session::get("api.user.id") != $id) {
            return API::response(404);
        }

        return $this->api->show($id);
    }

    /** [POST] Crear un nuevo usuario
     * 
     * @param Request $request
     */
    public function create(Request $request) {

        $data = $request->all();

        $rules = [
            User::ATTR_NAME => "required|max:50|string|min:5|regex:/^[a-zA-Z ]*$/",
            User::ATTR_LASTNAME => "required|max:50|string|min:5|regex:/^[a-zA-Z ]*$/",
            User::ATTR_EMAIL => "required|email|max:100|unique:" . User::DB_TABLE,
            User::ATTR_PASSWORD => "required|min:8|max:100"
        ];

        $validator = Validator::make($data, $rules);


        if ($validator->fails()) {
            $errors = [];

            foreach ($rules as $field => $attrs) {
                if ($validator->errors()->get($field))
                    $errors[] = ["field" => $field, "message" => $validator->errors()->get($field)];
            }

            return API::response(422)->withErrors(API::TYPE_ERROR_VALIDATION_FAILED, $errors)->get();
        }

        #Realiza el registro del usuario
        $user_id = User::register($data);

        return API::response(["id" => $user_id], 201)->get();
    }

    /** [PUT] Actualiza los datos de un usuario
     * 
     * @param Request $request
     */
    public function update($id, Request $request) {


        if (Session::get("api.user.type") == "suscriber" && Session::get("api.user.id") != $id) {
            return API::response(405);
        }

        $data = $request->all();
        
 
        unset($data["_method"]);
        

        $user = User::find($id);

        if (!$user->exists())
            return API::response(404);

        #Atributos permitidos para actualizar
        $rules = $rules_or = [
            User::ATTR_NAME => "required|max:50|string|min:5|regex:/^[a-zA-Z ]*$/",
            User::ATTR_LASTNAME => "required|max:50|string|min:5|regex:/^[a-zA-Z ]*$/",
            User::ATTR_PASSWORD => "required|min:8|max:100",
            //Metadatos
            "_meta-" . UserMeta::KEY_CURRENCY => "required|in:" . implode(",", Currency::getList()),
            "_meta-" . UserMeta::KEY_LANG => "required|in:" . implode(",", Lang::getList()),
            "_meta-" . UserMeta::KEY_CURRENCY_UNIFY => "required|boolean",
            "_meta-" . UserMeta::KEY_MODE_SECURE => "required|boolean",
            "_meta-" . UserMeta::KEY_NOTF_FLOWCRON_AUTO_EMAIL => "required|boolean",
            "_meta-" . UserMeta::KEY_NOTF_FLOWCRON_CONFIRM_EMAIL => "required|boolean",
            "_meta-" . UserMeta::KEY_NOTF_FLOW_AUTOREAD_ERROR_EMAIL => "required|boolean",
            "_meta-" . UserMeta::KEY_NOTF_FLOW_AUTOREAD_SUCCESS_AUTO_EMAIL => "required|boolean",
            "_meta-" . UserMeta::KEY_NOTF_FLOW_AUTOREAD_SUCCESS_CONFIRM_EMAIL => "required|boolean",
        ];

        foreach ($rules as $field => $params) {
            if (!isset($data[$field])) {
                unset($rules[$field]);
            }
        }

       

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            $errors = [];

            foreach ($rules as $field => $attrs) {
                if ($validator->errors()->get($field))
                    $errors[] = ["field" => $field, "message" => $validator->errors()->get($field)];
            }

            return API::response(422)->withErrors(API::TYPE_ERROR_VALIDATION_FAILED, $errors)->get();
        }


        //Obtiene solo los datos con atributos existentes
        $auxdata = array_intersect_key($rules, $data);

        if (count($auxdata) == 0) {
            return API::response(422)->withErrors(API::TYPE_ERROR_PARAMETERS_VALIDS_NOT_EXISTS, [sprintf("No valid attribute was found within the request. (%s)", implode(",", array_keys($rules_or)))])->get();
        }

        foreach ($auxdata as $index => $value) {
            //Actualiza la información de un metadato
            if (strpos($index, "_meta-") === 0) {
                $user->setMeta(str_replace("_meta-", "", $index), $data[$index]);
                continue;
            }
            $user[$index] = $data[$index];
        }

        $user->update();

        return API::response(200)->get();
    }

    /** [DELETE] Elimina un usuario del sistema
     * 
     * @param Request $request
     */
    public function delete($id, Request $request) {

        if (Session::get("api.user.type") == "suscriber" && Session::get("api.user.id") != $id) {
            return API::response(405);
        }

        $user = User::find($id);

        if (!$user->exists())
            return API::response(404)->get();

        $user->delete();
        return API::response(200)->get();
    }

}
