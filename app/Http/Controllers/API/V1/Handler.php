<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\API\V1\Resources\Users;
use App\Base\System\Library\Networks\API;
use Illuminate\Http\Request;
use App\Base\System\Library\Comp\Util;
use App\Models\User;
use Illuminate\Support\Facades\Session;
use App\Custom\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use \Illuminate\Pagination\LengthAwarePaginator;

/**
 * Description of Handler
 *
 * @author jospina
 */
class Handler {
    #Este parametro indica que recursos son accesibles desde la API

    const RESOURCES = [
        Users::ALIAS => Users::class
    ];

    #Este parametro espefica los permisos que se puede realizar para cada recurso
    const PERMISSIONS = [Users::ALIAS => ["show", "update"]];
    //Parametros de configuración: 

    const PARAM_PAGINATION_MAX_RESULT_PER_PAGE = "50"; //Numero maximo

    private $resource;
    private $params;

    function __construct($resource) {
        $this->resource = $resource;

        if (!defined($this->resource . "::API_PARAMS")) {
            return API::response(400)->get();
            ;
        } else {
            $this->params = $this->resource::API_PARAMS;
        }
    }

    /** Procesa una peticion para retorna un listado de objetos del recurso solicitado
     * 
     * @param array $filters Parametros de filtros, ordenacion o busqueda
     * @return JSON/OBJECT
     */
    function index(array $filter = []) {

        $query = $this->filter(DB::table($this->resource::DB_TABLE), $filter);

        return API::response($this->make($query, $filter))->get();
    }

    function show($id) {

        if (!is_numeric($id))
            return API::response(400)->get();

        if (!$this->resource::find($id)->exists())
            return API::response(404)->get();

        return API::response($this->make($this->resource::find($id)))->get();
    }

    /** Genera un array con el resultado de una consulta
     * 
     * @param Eloquent $query
     * @return array
     */
    private function make($query, $filter = []): array {
        $_data = [];

        $params = $this->params;

        $fields = (isset($filter["fields"])) ? explode(",", $filter["fields"]) : [];


        $makeData = function($row) use ($params, $fields) {

            $data = [];
            $data["id"] = $row->id;
            foreach ($params as $name => $param) {

                if ($name === "_relations") {
                    array_merge($data, $this->getDataRelations($param, $row->id));
                    continue;
                }


                if ($name === "_meta" && (count($fields) == 0 || in_array("meta", $fields))) {
                    $data["meta"] = $this->getDataMeta($param, $row->id);
                    continue;
                }

                if ($name === "attrs_show_allowed") {
                    foreach ($param as $attr) {
                        if (!empty($row->{$attr})) {
                            $data[$attr] = Util::convertIntToBoolean($row->{$attr});
                        }
                    }
                }
            }

            return $data;
        };


        if (!$query instanceof LengthAwarePaginator && !$query instanceof Collection) {
            return $makeData($query);
        }



        foreach ($query as $row) {
            $_data[] = $makeData($row);
        }


        /**
         * Agrega los metadatos de paginacion
         */
        if ($query instanceof LengthAwarePaginator) {

            $vars = ["page[number]"];

            if ($query->perPage() < self::PARAM_PAGINATION_MAX_RESULT_PER_PAGE)
                $vars[] = "page[size]";

            $url = urldecode(url()->full());
            $total_page = $query->lastPage();
            if ($total_page > 0 && $query->currentPage() > $total_page)
                exit(API::response(400)->get());

            $_data["-links"] = [
                "self" => Util::addVariableToUrl($url, $vars, [$query->currentPage(), $query->perPage()]),
                "first" => Util::addVariableToUrl($url, $vars, [1, $query->perPage()]),
                "prev" => ($query->currentPage() > 1) ? Util::addVariableToUrl($url, $vars, [$query->currentPage() - 1, $query->perPage()]) : null,
                "next" => ($query->hasMorePages()) ? Util::addVariableToUrl($url, $vars, [$query->currentPage() + 1, $query->perPage()]) : null,
                "last" => Util::addVariableToUrl($url, $vars, [$total_page, $query->perPage()])
            ];

            $_data["-meta"] = ["total-pages" => $total_page, "items-per-pages" => $query->count(), "total-items" => $query->total()];
        }

        return $_data;
    }

    /** Retorna los datos relaciones con el objeto
     * 
     * @param type $name
     * @param type $id
     * @return type
     */
    private function getDataRelations($name, $id) {
        return [];
    }

    /** Contruye un array con los metadatos de un objeto
     * 
     * @param array $params ["La clase metadata","El nombre del atributo foreano"]
     * @param int $id El id del objeto padre
     * @return array ["key"=>"value","key"=>"value"...]
     */
    private function getDataMeta(array $params, int $id): array {

        /**
         * Ejemplo: $params ["\App\Models\UserMeta", "user_id"] $id = 1 
         */
        list($class, $fk) = $params;

        $keys = $class::getList();
        $data = [];
        $res = $this->resource::find($id);

        if (is_null($res))
            return $data;

        foreach ($keys as $key) {
            #if(!method_exists($res,"getMeta"))
            $data[$key] = Util::convertIntToBoolean($res->getMeta($key));
        }

        return $data;
    }

    /** Aplica los filtros4
     * .+00000000000000
     * 
     * @param type $query
     * @param type $params
     * @return type
     */
    private function filter($query, $params = []) {


        $fields = null;

        $params_show = $this->params["attrs_show_allowed"];


        /*
          |--------------------------------------------------------------------------
          | Filtra el resultado por atributos especificos {attr}={value}
          |--------------------------------------------------------------------------
         */

        foreach ($params as $index => $value) {


            /**
             * Si el recurso disponible de metadatos, estos pueden ser filtrados por esta información de esta manera: _meta-{attr}={value}
             */
            if (isset($this->params["_meta"]) && strpos($index, "_meta") === 0) {

                $_meta_key = str_replace("_meta-", "", $index);

                list($class, $fk) = $this->params["_meta"];

                $table = $this->resource::DB_TABLE;
                $table_meta = $class::DB_TABLE;


                $query->whereExists(function ($query) use($table, $table_meta, $fk, $_meta_key, $value) {
                    $query->select()->from($table_meta)
                            ->whereRaw("$table_meta.$fk = $table.id and $table_meta.key='$_meta_key' and $table_meta.value='$value'");
                });
            }

            if (in_array($index, $params_show)) {
                $query = $query->where($index, $value);
            }
        }


        #Filtra los datos eliminados
        if (in_array("Illuminate\Database\Eloquent\SoftDeletes", class_uses($this->resource)))
            $query = $query->whereNull("deleted_at");


        /*
          |--------------------------------------------------------------------------
          | FIELDS: Indica los atributos de a retornar (fields=attr1,attr2,...)
          |--------------------------------------------------------------------------
         */

        if (isset($params["fields"]) && strlen($params["fields"]) > 0) {

            $fields = explode(",", $params["fields"]);


            if (in_array("meta", $fields))
                $params_show[] = "meta";

            foreach ($fields as $field) {
                if (!in_array($field, array_merge($params_show, ["id"]))) {
                    exit(API::response(400)->get());
                }
            }

            //Verifica que el ID siempre se muestre
            if (!in_array("id", $fields))
                $fields[] = "id";
        }



        /*
          |--------------------------------------------------------------------------
          | SORT: Ordena el listado retornado ascendentemente/descendemente segun un atributo especificado ([sort={attr}] => ASC [sor=-{attr}])
          |--------------------------------------------------------------------------
         */


        if (isset($params["sort"])) {

            $attr = $params["sort"];
            $order = "ASC";

            #Descendente
            if (strpos($attr, "-") === 0) {
                $attr = str_replace("-", "", $attr);
                $order = "DESC";
            }

            $params_show[] = "id";

            if (!in_array($attr, $params_show)) {
                exit(API::response(400)->get());
            }

            $query = $query->orderBy($attr, $order);
        }



        /*
          |--------------------------------------------------------------------------
          | Paginación:
          |
          | number: El numero de la pagina a retornar
          | size: El tamaño de la pagina
          |--------------------------------------------------------------------------
         */

        $page_number = 1;
        $page_size = self::PARAM_PAGINATION_MAX_RESULT_PER_PAGE;

        if (isset($params["page"])) {
            $page_number = $params["page"]["number"] ?? 1;
            $page_size = $params["page"]["size"] ?? self::PARAM_PAGINATION_MAX_RESULT_PER_PAGE;

            if (!is_numeric($page_number) || !is_numeric($page_size))
                exit(API::response(400)->get());
        }

        $page_size = $page_size > self::PARAM_PAGINATION_MAX_RESULT_PER_PAGE ? self::PARAM_PAGINATION_MAX_RESULT_PER_PAGE : $page_size;


        $query = $query->paginate($page_size, ["*"], 'page', $page_number);

        if (!is_null($fields)) {

            foreach ($query as $row) {
                foreach ($params_show as $field) {
                    if (!in_array($field, $fields)) {
                        unset($row->{$field});
                    }
                }
            }
        }



        return $query;
    }

    /** Valida la solicitud de la API
     * 
     * @param type $params
     * @return type
     */
    public static function validate($params) {

        if (!isset($_SERVER["HTTP_TOKEN"])) {
            return API::response(403)->get();
        }

        $token = $_SERVER["HTTP_TOKEN"];

        $user = User::where(User::ATTR_API_TOKEN, $token)->first();

        if (empty($user)) {
            return API::response(401)->get();
        }

        //Le indica al api que tipo de usuario esta accediendo, en este caso es un suscriptor.
        Session::flash("api.user.type", "suscriber");

        if (env("APP_DEBUG"))
            Session::flash("api.user.type", "debug");

        Session::flash("api.user.id", $user->id);



        if (!preg_match("/([A-Za-z0-9_]+)/i", $params)) {
            return API::response(400)->get();
        }

        $resq = preg_replace("/([A-Za-z0-9_]+)/i", '${1}', $params);

        if (strpos($resq, "/") !== false) {
            $resq = explode("/", $resq . " ")[0];
        }

        if (!isset(self::PERMISSIONS[$resq]))
            return API::response(404)->get();

        return true;
    }

}
