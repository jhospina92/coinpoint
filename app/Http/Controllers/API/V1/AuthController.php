<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Base\System\Library\Networks\API;
use App\Base\System\Library\Networks\APICodeError;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller {

    /** Recibe una petición de login 
     * 
     * @param Request $request
     * @return type
     */
    public function postLogin(Request $request) {

        $data = $request->all();

        if (Auth::attempt([User::ATTR_EMAIL => $data[User::ATTR_EMAIL], User::ATTR_PASSWORD => $data[User::ATTR_PASSWORD]], false)) {
            $user = Auth::user();
            Auth::logout();

            if (!$user->{User::ATTR_ACTIVE}) {
                 return API::response(400)->withErrors(API::TYPE_ERROR_LOGIN_FAILED, APICodeError::get(APICodeError::LOGIN_ACCCOUNT_USER_NOT_ACTIVE))->get();
            }

            $token = $user[User::ATTR_API_TOKEN] = str_random(50);
            $user->save();

            return API::response(["token" => $token])->get();
        }

        return API::response(400)->withErrors(API::TYPE_ERROR_LOGIN_FAILED, APICodeError::get(APICodeError::LOGIN_UNAUTHORIZED))->get();
    }

}
