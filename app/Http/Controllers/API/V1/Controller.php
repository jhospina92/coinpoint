<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller as ControllerDefault;
use Illuminate\Http\Request;
use App\Http\Controllers\API\V1\Handler as HandlerAPI;
use Illuminate\Support\Facades\Session;
use App\Base\System\Library\Networks\API;

/**
 * Description of Controller
 *
 * @author jospina
 */
class Controller extends ControllerDefault {

    //put your code here

    protected $api;

    function __construct(Request $request, $resource) {

        $this->api = new HandlerAPI($resource);

        if (env("APP_DEBUG"))
            return;

        if (Session::get("api.user.type") != "suscriber") {
            return;
        }

        $action = explode("@", $request->route()->getActionName())[1];

        $called_controller = get_called_class();

        //Verifica si los permisos del usuario para acceder al recurso solicitado
        if (!in_array($action, HandlerAPI::PERMISSIONS[$called_controller::ALIAS]))
            exit(API::response(405));
    }

}
