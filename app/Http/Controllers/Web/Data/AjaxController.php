<?php

/*
  |--------------------------------------------------------------------------
  | ESTE CONTROLADOR UNICAMENTE RECIBE PETICIONES LANZADAS POR AJAX
  |--------------------------------------------------------------------------
 */

namespace App\Http\Controllers\Web\Data;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Base\Data\Bank;
use App\Models\Budget;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Base\Data\ModelData;

class AjaxController extends Controller {

    /** Retorna un listado de banco dado por un tipo de moneda(El tipo de moneda identifica el pais)
     * 
     * @param Request $request
     */
    public function getDataBanks(Request $request) {
        if (!$request->ajax())
            return null;

        $data = $request->all();

        $banks = Bank::getListFromCurrency($data["currency"]);

        return json_encode($banks);
    }

    /** Recibe una petición para activar una presupuesto
     * 
     * @param Request $request
     * @return type
     */
    public function postBudgetEnable(Request $request) {
        if (!$request->ajax())
            return null;

        $data = $request->all();

        if (!Budget::isOwnerAuth($data["budget_id"])) {
            return json_encode(false);
        }

//Desactiva todos los presupuestos que se encuentren activos
        Budget::where(Budget::ATTR_FK_USER_ID, $data["user_id"])->update([Budget::ATTR_ACTIVE => false]);
        Budget::find($data["budget_id"])->setActive();

        return json_encode(true);
    }

    /** Recibe una petición para desactivar un presupuesto
     * 
     * @param Request $request
     * @return type
     */
    public function postBudgetDisable(Request $request) {
        if (!$request->ajax())
            return null;

        $data = $request->all();

        if (!Budget::isOwnerAuth($data["budget_id"])) {
            return json_encode(false);
        }

        Budget::find($data["budget_id"])->setActive(false);

        return json_encode(true);
    }

    /** Recibe una petición para obtener los datos de un modelo dado por su Id
     * 
     * @param Request $request
     * @return type
     */
    public function getDataModelById(Request $request) {
        if (!$request->ajax())
            return null;

        $data = $request->all();

        return json_encode(ModelData::getAttrsValue($data["model"], $data["id"]));
    }

    /** Recibe una petición para establecer un metadato asociado a un usuario en sesión
     * 
     * @param Request $request
     * @return type
     */
    public function postSetMetaUser(Request $request) {
        if (!$request->ajax())
            return null;

        $data = $request->all();

        Auth::user()->setMeta($data["key"], $data["value"]);

        return json_encode(true);
    }

    /** Recibe una petición para verificar si un objeto de un modelo existe en la base de datos
     *  
     * @param Request $request
     * @return type
     */
    public function getEloquentExistData(Request $request) {

        $data = $request->all();

        if (!$request->ajax())
            return null;

        if (!isset($data["model"]) || !isset($data["attr"]) || !isset($data["value"]))
            return json_encode(false);

        $class = "App\Models\\" . $data["model"];

        if (class_exists($class))
            return json_encode(false);

        $model = call_user_func($class . "::where(" . $data["attr"] . ",'='," . $data["value"] . ")");

        return json_encode($model->exists());
    }

    public function postEloquentToggleActive(Request $request) {
        $data = $request->all();

        $subs = ["Auto", ""]; //Subcarpetas del caperta Models

        if (!$request->ajax())
            return null;

        if (!isset($data["model"]) || !isset($data["id"]))
            return json_encode(["response" => "error"]);

        do {
            $class = str_replace("\\\\", "\\", "App\Models\\" . array_pop($subs) . "\\" . studly_case($data["model"]));
        } while (!class_exists($class) && !is_null($class));


        if (!class_exists($class))
            return json_encode(["response" => "error"]);

        $id = $data["id"];

        //Verifica la propiedad del elemento Eloquent
        if (!call_user_func($class . "::isOwnerAuth", $id)) {
            return json_encode(["response" => "error"]);
        }


        $model = call_user_func($class . "::find", $id);

        if (!isset($model->active))
            return json_encode(["response" => "error"]);

        $model->active = !$model->active;
        $model->update();

        return json_encode(["response" => $model->active]);
    }


    /** Receibe una petición para renderizar una vista
     * 
     * @param Request $request
     * @return type
     */
    public function postRenderView(Request $request) {
        $data = $request->all();

        if (!$request->ajax())
            return null;

        $view = decrypt($data["_view"]);
        $params = $data["params"] ?? [];       

        $paths = explode(".", $view);
        $view_name = array_pop($paths);
        $dircurrent = implode(".", $paths);

        return json_encode(["response" => "success", "data" => $data, "view" => view($dircurrent . "._render." . $data["render_view"], $params)->render()]);
    }

}
