<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Web\Services;

use Illuminate\Http\Request;
use App\Base\System\Library\DataProviders\APIs\Mails\Gmail as GmailAPI;
use App\Base\System\Library\DataProviders\APIs\Mails\Exceptions\ExceptionMailClientlExist;
use App\Base\Data\RouteData;
use App\Base\UI\Message;

/**
 * Description of MailAPIController
 *
 * @author jospina
 */
class InterfaceMailAPIController {

    private $redirect_gmail_oauth;

    function __construct() {
        $this->redirect_gmail_oauth = route(RouteData::SERVICES_API_GMAIL_OAUTH);
    }

    /** Recibe un petición para realizar una autenticación con la API de Gmail para obtener los permisos del usuario para leer su correo electrónico
     * 
     * @param Request $request
     * @return type
     */
    function oAuthGmail(Request $request) {

        if (isset($_GET["code"])) {
            try {
                $gmail = new GmailAPI($_GET["code"], $this->redirect_gmail_oauth);
            } catch (ExceptionMailClientlExist $e) {
                Message::alert(trans("msg.autoread.associate.error.email.exists"), Message::ALERT_TYPE_ERROR);
                return redirect()->route(RouteData::AUTOREAD_ADD);
            }
            Message::alert(sprintf(trans("msg.autoread.associate.success"), $gmail->getProfileEmail()));
            return redirect()->route(RouteData::AUTOREAD_INDEX);
        }

        if (isset($_GET["error"]) && $_GET["error"] == "access_denied") {
            Message::alert(trans("msg.typayment.sec.autoread.error.access.denied"), Message::ALERT_TYPE_ERROR);
            return redirect()->route(RouteData::AUTOREAD_ADD);
        }

        abort(400);
    }

    function ajaxGmailGetUrlAuth(Request $request) {

        if (!$request->ajax())
            return json_encode(false);

        $gmail = new GmailAPI();

        return json_encode(["response" => filter_var($gmail->getUrlAuth($this->redirect_gmail_oauth), FILTER_SANITIZE_URL)]);
    }

}
