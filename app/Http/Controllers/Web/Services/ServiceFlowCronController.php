<?php

namespace App\Http\Controllers\Web\Services;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FlowCron;
use App\Base\System\Library\Comp\DateUtil;
use App\Base\Data\Currency;
use Exception;
use Illuminate\Support\Facades\App;
use App\Base\Data\Lang;
use App\Base\UI\AppTemplate;
use App\Base\Data\RouteData;

class ServiceFlowCronController extends Controller {

    function __construct(Request $request) {
        //Estable el idioma por defecto
        App::setLocale("en");

        /*
          |--------------------------------------------------------------------------
          | Validación
          |--------------------------------------------------------------------------
         */

        if (is_null($request->route()))
            return;

        $user_id = $request->route()->getParameter("user_id");
        $currency = $request->route()->getParameter("currency");
        $lang = $request->route()->getParameter("lang");
        $hash = $request->route()->getParameter("hash");

        if (!isset(Lang::getList()[$lang]))
            AppTemplate::abort(404);

        //Establece el idioma
        App::setLocale($lang);

        if (isset($_GET["st"])) {
            return;
        }

        if (empty($user_id) || empty($currency) || empty($lang) || empty($hash))
            AppTemplate::abort(400);

        $cron = FlowCron::where(FlowCron::ATTR_HASH_EMAIL, $hash)->where(FlowCron::ATTR_FK_USER_ID, $user_id)->first();
        $date = new DateUtil();


        try {
            (new Currency(strtoupper($currency)));
        } catch (Exception $exc) {
            AppTemplate::abort(400, $exc->getMessage());
        }

        //Verifica que el hash no exista
        if (empty($cron)) {
            AppTemplate::abort(404);
        }

        //Verifica que la fecha de la transacción sea valida para su procesamiento
        if (DateUtil::difSec($date, $cron[FlowCron::ATTR_DATE_NEXT]) > 0) {
            AppTemplate::abort(400, trans("msg.services.flow.cron.confirm.email.error.cron.proccesed"));
        }
    }

    /** Recibe una petición para confirma una transacción programada
     * 
     * @param Request $request
     * @return string
     */
    public function confirmFromEmail($user_id, $currency, $lang, $hash) {

        $cron = FlowCron::where(FlowCron::ATTR_HASH_EMAIL, $hash)->first();

        $currency = new Currency(strtoupper($currency));
        $format = $currency->getFormat();
        $data["cron"] = $cron;
        $data["transaction"] = $transaction = $cron->transaction;
        $data["email"] = $cron->user->email;
        //Datos adicionales pasados para el objeto de la app en la vista
        $data["frontapp"] = 'app["currency"]={"code":"' . $currency->getCode() . '","format":[' . $format[0] . ',"' . $format[1] . '","' . $format[2] . '"],"symbol":"' . $currency->getSymbol() . '"};';

        return view("content/services/flowcron-email", $data);
    }

    public function postConfirmFromEmail(Request $request) {
        $data = $request->all();

        $user_id = $request->route()->getParameter("user_id");
        $hash = $request->route()->getParameter("hash");


        $cron = FlowCron::where(FlowCron::ATTR_HASH_EMAIL, $hash)->where(FlowCron::ATTR_FK_USER_ID, $user_id)->first();

        $url_previous = app('url')->previous();


        //Posponer para mañana
        if ($data["act"] == "postpone") {
            $date = new DateUtil($cron[FlowCron::ATTR_DATE_NEXT]);
            $cron[FlowCron::ATTR_DATE_NEXT] = $date->addDays(1);
            $cron->save();

            return redirect()->to($url_previous . '?' . http_build_query(["st" => "postpone", "dt" => strtotime($cron[FlowCron::ATTR_DATE_NEXT])]));
            //Registro de la transacción
        } else {
            $data["flag-date-next"] = true;
            $date = $cron->exec($data);
            return redirect()->to($url_previous . '?' . http_build_query(["st" => "register", "dt" => strtotime($date)]));
        }
    }

}
