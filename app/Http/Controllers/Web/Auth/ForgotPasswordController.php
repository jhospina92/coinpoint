<?php

namespace App\Http\Controllers\Web\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Base\System\Library\DataProviders\GCaptcha;
use App\Models\User;
use App\Base\UI\Message;
use App\Base\Data\RouteData;
use App\Models\PasswordReset;
use App\Base\System\Library\Comp\DateUtil;

class ForgotPasswordController extends Controller {

    function view() {
        return view("content/auth/forgotten");
    }

    function post(Request $request) {
        $data = $request->all();

        $captcha_response = $data["g-recaptcha-response"];

        //Verifica la validez del captcha
        if (!GCaptcha::valid($captcha_response)) {
            Message::alert(trans("ui.msg.invalid.captcha"), Message::ALERT_TYPE_ERROR);
            return redirect()->back()->withInput();
        }

        $user = User::where(User::ATTR_EMAIL, $data["email"])->first();

        //Verifica que el correo exista
        if (empty($user)) {
            Message::alert(trans("ui.msg.email.not.exists"), Message::ALERT_TYPE_ERROR);
            return redirect()->back()->withInput();
        }

        //Verifica que el usuario haya activado su cuenta
        if (!$user->{User::ATTR_ACTIVE}) {
            Message::alert(trans("msg.gen.account.not.active"), Message::ALERT_TYPE_ERROR);
            return redirect()->back()->withInput();
        }


        if (!PasswordReset::generate($user->id))
            return Message::alert(trans("ui.msg.error.system.request.default"), Message::ALERT_TYPE_ERROR);

        return redirect()->route(RouteData::AUTH_FORGOTTEN, ["email" => $user[User::ATTR_EMAIL], "st" => "confirm"]);
    }

    function reset($token) {

        //Valida el token -> Si retorna un string, es porque es un mensaje de error
        if (is_string($response = PasswordReset::validate($token)))
            abort(400, $response);

        return view("content/auth/reset-password")->with("token", $token);
    }

    function postReset(Request $request) {
        $data = $request->all();
        $token = $data["token"];

        if (is_string($resp = PasswordReset::validate($token)))
            abort(400, $resp);

        if ($data["new"] != $data["repeat"]) {
            return Message::alert(trans("nav.route.account.security.reset.password.error.not.match"), Message::ALERT_TYPE_ERROR);
        }

        $user = PasswordReset::getUserFromToken($token);
        $user[User::ATTR_PASSWORD] = bcrypt($data["new"]);
        $user->save();

        $resp->{PasswordReset::ATTR_VALIDATE} = true;
        $resp->save();

        Message::alert(trans("msg.forgotten.reset.success"), Message::ALERT_TYPE_INFO);

        return redirect()->route(RouteData::AUTH_LOGIN);
    }

}
