<?php

namespace App\Http\Controllers\Web\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Base\UI\Message;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Base\Data\RouteData;

class ResetPasswordController extends Controller {

    public function view() {
        return view("content/account/security/reset-password");
    }

    public function post(Request $request) {
        $data = $request->all();

        if (!Auth::attempt(['email' => Auth::user()->email, 'password' => $data["current"]])) {
            return Message::alert(trans("nav.route.account.security.reset.password.error.current.invalid"), Message::ALERT_TYPE_ERROR);
        }

        if ($data["new"] != $data["repeat"]) {
            return Message::alert(trans("nav.route.account.security.reset.password.error.not.match"), Message::ALERT_TYPE_ERROR);
        }

        $user = Auth::user();
        $user[User::ATTR_PASSWORD] = bcrypt($data["new"]);
        $user->save();

        Message::alert(trans("nav.route.account.security.reset.password.success"), Message::ALERT_TYPE_SUCCESS);
        return redirect()->route(RouteData::ACCOUNT_DASHBOARD);
    }

}
