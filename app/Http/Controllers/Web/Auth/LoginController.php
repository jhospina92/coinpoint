<?php

namespace App\Http\Controllers\Web\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Base\Data\RouteData;
use App\Models\User;
use App\Base\UI\Message;

class LoginController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

    public function index() {
        return view("content/auth/login");
    }

    /** Recieve a POST request for Login access
     * 
     * @param Request $request
     */
    public function handler(Request $request) {
        $data = $request->all();

        if (Auth::attempt(['email' => $data[User::ATTR_EMAIL], 'password' => $data[User::ATTR_PASSWORD]], isset($data["remember"]))) {

            if (!Auth::user()->{User::ATTR_ACTIVE}) {
                $user_id = Auth::user()->id;
                Auth::logout();
                Message::alert(sprintf(trans("msg.login.account.not.active"), '<div class="text-center"><a href="#" id="resend-mail" data-msg-title="' . trans("msg.signup.confirm.resent.link") . '" data-msg-content="' . trans("msg.signup.confirm.resent.link.content") . '" data-callback="sg_success_dialog_resend_email" data-btn-ok="' . trans('ui.label.resend') . '" data-u="' . $user_id . '" open-dialog-confirm>' . trans("msg.signup.confirm.resent.link") . '</a></div>'), Message::ALERT_TYPE_WARNING);
                return redirect()->back()->withInput();
            }
            // Authentication passed...
            return redirect()->route(RouteData::DASHBOARD);
        } else {
            Message::alert(trans("ui.msg.error.login"), Message::ALERT_TYPE_ERROR);
        }

        return redirect()->back()->withInput();
    }

    public function logout() {
        Auth::logout();
        return redirect()->back();
    }

}
