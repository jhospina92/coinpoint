<?php

namespace App\Http\Controllers\Web\Auth;

use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Base\System\Library\DataProviders\GCaptcha;
use App\Base\UI\Message;
use App\Models\User;
use App\Base\System\Library\Networks\Email;
use App\Base\Data\RouteData;
use App\Models\UserMeta;
use App\Base\System\Library\Comp\DateUtil;

class RegisterController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Register Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users as well as their
      | validation and creation. By default this controller uses a trait to
      | provide this functionality without requiring any additional code.
      |
     */

use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest');
    }

    public function signup() {
        return view("content/auth/signup");
    }

    /** Recibe una petición para el registro de usuario
     * 
     * @param Request $request
     * @return type
     */
    public function postSignup(Request $request) {

        if (!env('APP_ENABLE_USER_REGISTER', true)) {
            return Message::alert(trans("msg.signup.disabled"), Message::ALERT_TYPE_ERROR);
        }

        $data = $request->all();

        $captcha_response = $data["g-recaptcha-response"];

        //Verifica la validez del captcha
        if (!GCaptcha::valid($captcha_response)) {
            Message::alert(trans("ui.msg.invalid.captcha"), Message::ALERT_TYPE_ERROR);
            return redirect()->back()->withInput();
        }

        //Verifica que el correo no exista
        if (User::where(User::ATTR_EMAIL, $data["email"])->exists()) {
            Message::alert(trans("ui.msg.email.exists"), Message::ALERT_TYPE_ERROR);
            return redirect()->back()->withInput();
        }

        $user_id = User::register($data, true);

        return redirect()->route(RouteData::AUTH_SIGNUP, ["u" => $user_id, "st" => "confirm"]);
    }

    /** Recibe una petición GET para confirmar el correo de usuario
     * 
     * @param type $hash
     * @return type
     */
    public function confirm($hash) {

        $meta = UserMeta::where(UserMeta::ATTR_KEY, UserMeta::KEY_HASH_EMAIL_CONFIRM)->where(UserMeta::ATTR_VALUE, $hash)->first();

        if (empty($meta)) {
            abort(404);
        }

        $user = User::find($meta->user_id);

        if ($user[User::ATTR_ACTIVE]) {
            abort(404);
        }

        $user[User::ATTR_ACTIVE] = true;
        $user->setMeta(UserMeta::KEY_HASH_EMAIL_CONFIRM, new DateUtil());
        $user->save();

        $data["user"] = $user;

        return view("content/auth/confirm", $data)->with("data", $data);
    }

    /** Recibe una petición Ajax para reenviar el correo de confirmación al usuario
     * 
     * @param Request $request
     * @return type
     */
    public function ajaxPostResendEmailConfirm(Request $request) {

        if (!$request->ajax()) {
            return json_encode(array(false));
        }

        $user = User::find($request->get("user_id"));

        $hash = $user->getMeta(UserMeta::KEY_HASH_EMAIL_CONFIRM);

        //Envia un correo de confirmación
        $email = new Email(trans("mail.signup.activation.account.subject"), $user[User::ATTR_EMAIL]);
        $email->setDescription(sprintf(trans("mail.signup.activation.account.description"), $user[User::ATTR_NAME], route(RouteData::AUTH_CONFIRM, $hash)));
        $email->send();

        return json_encode(array("response" => trans('msg.signup.resend.mail.success')));
    }

}
