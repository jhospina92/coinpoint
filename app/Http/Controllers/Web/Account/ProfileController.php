<?php

namespace App\Http\Controllers\Web\Account;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Http\Request;
use App\Base\Data\RouteData;
use App\Base\UI\Message;

class ProfileController extends Controller {

    const PATH = "content/account/profile/";

    public function edit() {
        $data = array();
        return view(self::PATH . "edit", $data);
    }

    public function update(Request $request) {
        $data = $request->all();

        $user = Auth::user();
        $user[User::ATTR_NAME] = $data["name"];
        $user[User::ATTR_LASTNAME] = $data["lastname"];
        $user->save();

        Message::alert(trans("nav.route.account.profile.edit.success"), Message::ALERT_TYPE_SUCCESS);
        return redirect()->route(RouteData::ACCOUNT_DASHBOARD);
    }

}
