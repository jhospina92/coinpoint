<?php

/*
  |--------------------------------------------------------------------------
  | DASHBOARD ACCOUNT
  |--------------------------------------------------------------------------
 */

namespace App\Http\Controllers\Web\Account;

use App\Http\Controllers\Controller;

class DashboardController extends Controller {

    public function index() {
        $data = array();
        return view("content/account/dashboard", $data);
    }

}
