<?php

namespace App\Http\Controllers\Web\Account;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Base\Data\RouteData;
use App\Base\UI\Message;
use App\Base\Data\Lang;
use App\Base\Data\Currency;
use App\Models\UserMeta;
use App\Jobs\Cache\RefreshCacheDataDashboard;

class SettingsController extends Controller {

    const PATH = "content/account/settings/";

    public function general() {
        $data = array();
        $data["langs"] = Lang::getList();
        $data["currencies"] = Currency::getList();
        return view(self::PATH . "general", $data);
    }

    public function postGeneral(Request $request) {
        $data = $request->all();

        Auth::user()->setMeta(UserMeta::KEY_CURRENCY, $data[UserMeta::KEY_CURRENCY]);
        Auth::user()->setMeta(UserMeta::KEY_LANG, $data[UserMeta::KEY_LANG]);
        Auth::user()->setMeta(UserMeta::KEY_CURRENCY_UNIFY, (isset($data[UserMeta::KEY_CURRENCY_UNIFY])));

        Message::alert(trans("nav.route.account.settings.general.success"), Message::ALERT_TYPE_SUCCESS);
        
         dispatch(new RefreshCacheDataDashboard(Auth::user()->id));

        return redirect()->route(RouteData::ACCOUNT_DASHBOARD);
    }

    public function notifications() {
        return view(self::PATH . "notifications");
    }

    public function postNotifications(Request $request) {
        $data = $request->all();
        Auth::user()->setMeta(UserMeta::KEY_NOTF_FLOWCRON_AUTO_EMAIL, (isset($data[UserMeta::KEY_NOTF_FLOWCRON_AUTO_EMAIL])));
        Auth::user()->setMeta(UserMeta::KEY_NOTF_FLOWCRON_CONFIRM_EMAIL, (isset($data[UserMeta::KEY_NOTF_FLOWCRON_CONFIRM_EMAIL])));
        Auth::user()->setMeta(UserMeta::KEY_NOTF_FLOW_AUTOREAD_SUCCESS_AUTO_EMAIL, (isset($data[UserMeta::KEY_NOTF_FLOW_AUTOREAD_SUCCESS_AUTO_EMAIL])));
        Auth::user()->setMeta(UserMeta::KEY_NOTF_FLOW_AUTOREAD_SUCCESS_CONFIRM_EMAIL, (isset($data[UserMeta::KEY_NOTF_FLOW_AUTOREAD_SUCCESS_CONFIRM_EMAIL])));
        Auth::user()->setMeta(UserMeta::KEY_NOTF_FLOW_AUTOREAD_ERROR_EMAIL, (isset($data[UserMeta::KEY_NOTF_FLOW_AUTOREAD_ERROR_EMAIL])));

        Message::alert(trans("nav.route.account.settings.notifications.success"), Message::ALERT_TYPE_SUCCESS);

        return redirect()->route(RouteData::ACCOUNT_DASHBOARD);
    }

}
