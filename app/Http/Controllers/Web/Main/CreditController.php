<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Web\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Base\UI\Message;
use App\Base\Data\RouteData;
use App\Models\Credit;
use Illuminate\Support\Facades\Auth;
use App\Models\CreditTrack;
use App\Base\System\Library\Comp\Util;
use App\Base\Data\Currency;
use App\Models\UserMeta;

class CreditController {

    const PATH = "content/main/credit/";

    public function index() {
        $data = [];

        $data["credits"] = Auth::user()->credits;

        return view(self::PATH . "index", $data);
    }

    public function create() {
        $data = [];
        $data["currency"] = new Currency(Auth::user()->getMeta(UserMeta::KEY_CURRENCY));
        $data["currencies"] = Currency::getList();
        return view(self::PATH . "create", $data);
    }

    public function record($id) {

        if (!Credit::isOwnerAuth($id))
            return redirect()->route(RouteData::CREDIT_INDEX);

        $data = [];
        $data["credit"] = Credit::find($id);
        return view(self::PATH . "record", $data);
    }

    /** Almacenas todas las transacciones. 
     * 
     * @param Request $request
     * @return type
     */
    public function store(Request $request) {
        $data = $request->all();


        $credit = new Credit();
        $credit[Credit::ATTR_VALUE] = Util::getValue($data[Credit::ATTR_VALUE], $data[Credit::ATTR_CURRENCY]);
        $credit[Credit::ATTR_BALANCE] = Util::getValue($data[Credit::ATTR_VALUE], $data[Credit::ATTR_CURRENCY]);
        $credit[Credit::ATTR_NAME] = $data[Credit::ATTR_NAME];
        $credit[Credit::ATTR_ORGANIZATION] = $data[Credit::ATTR_ORGANIZATION];
        $credit[Credit::ATTR_FEES] = $data[Credit::ATTR_FEES];
        $credit[Credit::ATTR_CATEGORY] = $data[Credit::ATTR_CATEGORY];
        $credit[Credit::ATTR_STATUS] = Credit::STATUS_ACTIVE;
        $credit[Credit::ATTR_FK_USER_ID] = Auth::user()->id;
        $credit[Credit::ATTR_CURRENCY] = $data[Credit::ATTR_CURRENCY];

        $metadata = array();
        //METADOS - Caracteristicas adicionales
        foreach ($data as $index => $value) {
            if (strpos($index, "feature_") !== false) {
                if (!empty($value)) {
                    $metadata[$index] = $value;
                }
            }
        }

        $credit[Credit::ATTR_METADATA] = json_encode($metadata);
        $credit->save();
        Message::alert(sprintf(trans("ui.msg.success.create"), trans("ui.label.credit")), Message::ALERT_TYPE_SUCCESS);
        return redirect()->route(RouteData::CREDIT_INDEX);
    }

    /*
      |--------------------------------------------------------------------------
      | Credit Track
      |--------------------------------------------------------------------------
     */

    public function createTrack($id) {
        $data = [];

        if (!Credit::isOwnerAuth($id))
            return redirect()->route(RouteData::CREDIT_INDEX);

        $data["credit"] = Credit::find($id);
        return view(self::PATH . "create_track", $data);
    }

    public function storeTrack(Request $request) {

        $data = $request->all();

        CreditTrack::register($data);

        Message::alert(sprintf(trans("ui.msg.success.register"), trans("ui.label.track.credit")), Message::ALERT_TYPE_SUCCESS);
        return redirect()->route(RouteData::CREDIT_INDEX);
    }

}
