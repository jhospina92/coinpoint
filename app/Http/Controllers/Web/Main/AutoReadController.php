<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Web\Main;

use App\Http\Controllers\Controller;
use App\Models\Auto\AutoRead;
use App\Base\Data\RouteData;
use App\Base\UI\Message;
use App\Base\System\Library\DataProviders\APIs\Mails\Gmail as GmailAPI;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Base\System\Services\ReaderEmail\ReaderEmail;
use App\Models\Logs\LogAutoReadProcessed as LogSuccess;
use App\Models\Flow;
use App\Models\Relations\TypaymentAutoRead;

/**
 * Description of AutoReadController
 *
 * @author jospina
 */
class AutoReadController extends Controller {

    const PATH = "content/main/typayment/autoread/";

    public function index() {
        $data["clients"] = Auth::user()->autoread_clients;

        return view(self::PATH . "index", $data);
    }

    public function add() {
        $data["clients"] = [AutoRead::CLIENT_GMAIL => route(RouteData::SERVICES_API_GMAIL_AJAX_GET_URL_AUTH)];

        return view(self::PATH . "add", $data);
    }

    public function destroy($id, Request $request) {

        if (!$request->ajax())
            return json_encode(array("response" => false));

        if (!AutoRead::isOwnerAuth($id))
            return json_encode(array("response" => false));

        AutoRead::find($id)->log_success()->forceDelete();
        AutoRead::find($id)->log_error()->forceDelete();
        TypaymentAutoRead::where(TypaymentAutoRead::ATTR_FK_AUTOREAD_ID, $id)->delete();
        AutoRead::find($id)->delete();


        return json_encode(array("response" => true));
    }

    public function postAjaxValidateParam(Request $request) {

        if (!$request->ajax())
            return json_encode(array("response" => "error"));

        $data = $request->all();

        return json_encode(array("response" => ReaderEmail::isValidParam($data["param"], $data["val"])));
    }

    public function postAjaxLogConfirm(Request $request) {

        if (!$request->ajax())
            return json_encode(array("response" => "error"));

        $params = $request->all();

        $log = LogSuccess::find($params["id"]);

        if ($log->autoread->user_id != Auth::user()->id)
            return json_encode(array("response" => "error"));

        $log[LogSuccess::ATTR_VALIDATE] = true;

        $log->save();

        $data = $log->data;

        $data["category"] = $params["category"];
        $data["cc-fees"] = 1;
        $data["date_submit"] = $data["date"];


        Flow::createFromType($data);

        return json_encode(array("response" => true));
    }

    public function postAjaxLogSkip(Request $request) {
        if (!$request->ajax())
            return json_encode(array("response" => "error"));

        $params = $request->all();

        $log = LogSuccess::find($params["id"]);

        if ($log->autoread->user_id != Auth::user()->id)
            return json_encode(array("response" => "error"));

        $log[LogSuccess::ATTR_VALIDATE] = true;
        $log->delete();

        return json_encode(array("response" => true));
    }

}
