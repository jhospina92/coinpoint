<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Web\Main;

use Illuminate\Http\Request;
use App\Models\Flow;
use Illuminate\Support\Facades\Auth;
use App\Base\UI\Message;
use App\Base\Data\RouteData;
use App\Base\System\Library\Comp\DateUtil;
use App\Base\System\Library\Comp\Util;
use App\Models\FlowCron;
use App\Base\System\TP\Cash;
use App\Models\Typayment;
use App\Base\Data\CategoryData;

class FlowController {

    const PATH = "content/main/flow/";

    public function index() {
        $data = [];

        $transactions = array();

        $date = new DateUtil();



        $data["categories"]["revenue"] = CategoryData::getEntryList();
        $data["categories"]["spend"] = CategoryData::getSpendList();

        /**
         * APLICA EL FILTRO DE BUSQUEDA
         */
        if ((isset($_GET["search"]) && strlen($_GET["search"]) > 0) ||
                (isset($_GET["datefrom_submit"]) && strlen($_GET["datefrom_submit"]) > 0) ||
                (isset($_GET["type"]) && strlen($_GET["type"]) > 0) ||
                (isset($_GET["cats"]) && strlen($_GET["cats"]) > 0)) {

            $datefrom_search = $date;

            //Con mes y año
            if (strlen($_GET["datefrom_submit"]) > 0 || strlen($_GET["dateto_submit"]) > 0) {
                $datefrom = $_GET["datefrom_submit"] . "-01 00:00:00";
                $datefrom_search = new DateUtil((strlen($_GET["datefrom_submit"]) > 0) ? $datefrom : (new DateUtil(date("Y-m-01 00:00:00")))->subtractMonth(1));
                $dateto_search = new DateUtil($_GET["dateto_submit"] ?? date("Y-m") . "-01 00:00:00");
                $date = $dateto_search;
            }

            //Calcula el numero de meses entre la fecha Desde y Hasta
            $num = DateUtil::difMon($datefrom_search, $dateto_search) + 1;


            //Valida que el rango de fecha (Desde sea mayor que el Hasta)
            if ($num < 1) {
                $datefrom_search = (new DateUtil(date("Y-m-01 00:00:00")))->subtractMonth(1);
                $dateto_search = new DateUtil(date("Y-m") . "-01 00:00:00");
                Message::alert(trans("msg.flow.error.range.date.invalid"), Message::ALERT_TYPE_ERROR);
                $num = 2;
            }

            $types = explode(",", $_GET["type"]);
            $cats = explode(",", $_GET["cats"]);
            $typayments = explode(",", $_GET["typayments"]);

            /**
             * AGREGAR BUSQUEDA 
             * DESDE - HASTA
             * MEDIO DE PAGO
             */
            for ($i = 0; $i < $num; $i++) {
                $transactions["month"][$i]["name"] = DateUtil::getNameMonth($date->month);
                $transactions["month"][$i]["num"] = Util::addZeroLeft($date->month);
                $transactions["month"][$i]["year"] = $date->year;

                $filters = [];

                $query = Flow::getForMonth($date->year, $date->month, null, $filters, null, false);

                //Tipo de transaccion
                if (strlen($_GET["type"]) > 0 && count($types) > 0)
                    $query->whereIn(Flow::ATTR_TYPE, $types);
                //Categorias
                if (strlen($_GET["cats"]) && count($cats) > 0)
                    $query->whereIn(Flow::ATTR_CATEGORY, $cats);
                //Medios de pagos
                if (strlen($_GET["typayments"]) && count($typayments) > 0)
                    $query->whereIn(Flow::ATTR_FK_TYPAYMENT_ID, $typayments);
                //Palabras
                if (isset($_GET["search"]) && strlen($_GET["search"]))
                    $query->where(Flow::ATTR_DESCRIPTION, "like", "%" . $_GET["search"] . "%");


                $transactions["month"][$i]["list"] = $query->orderBy(Flow::ATTR_DATE, "DESC")->orderBy("id", "DESC")->get();

                $date->subtractMonth(1);
            }
        } else {
            for ($i = 0; $i < 2; $i++) {
                $transactions["month"][$i]["name"] = DateUtil::getNameMonth($date->month);
                $transactions["month"][$i]["num"] = Util::addZeroLeft($date->month);
                $transactions["month"][$i]["year"] = $date->year;
                $transactions["month"][$i]["list"] = Flow::getForMonth($date->year, $date->month);
                $date->subtractMonth(1);
            }
        }

        $data["transactions"] = $transactions;
        $data["currencies"] = Auth::user()->getCurrencies();

        $transactions_count = Auth::user()->transactions()->count();

        //Obtiene la fecha de registro de la ultima transaccón del usuario
        $date_last_transaction = new DateUtil(($transactions_count > 0) ? Auth::user()->transactions()->orderBy(Flow::ATTR_DATE, "DESC")->first([Flow::ATTR_DATE])->{Flow::ATTR_DATE} : null);
        //Obtiene la fecha de registro de la primera transacción del usuario
        $date_first_transaction = new DateUtil(($transactions_count > 0) ? Auth::user()->transactions()->orderBy(Flow::ATTR_DATE, "ASC")->first([Flow::ATTR_DATE])->{Flow::ATTR_DATE} : null);

        $dataset_m = [$date_last_transaction->month];
        $dataset_y = [$date_first_transaction->year];

        $difm = abs(DateUtil::difMon($date_first_transaction, $date_last_transaction));

        if ($difm > 12) {
            $dataset_m = [];
            for ($i = 1; $i <= 12; $i++) {
                $dataset_m[] = $i;
            }
        } else {
            for ($i = $date_last_transaction->month - 1; $i >= $date_first_transaction->month; $i--) {
                $date_last_transaction->subtractMonth(1);
                $dataset_m[] = $date_last_transaction->month;
                if ($date_last_transaction->month == 12)
                    $dataset_y[] = $date_last_transaction->year;
            }
        }

        $data["dataset_y"] = $dataset_y;

        return view(self::PATH . "index", $data);
    }

    public function create() {

        $data = [];
        /**
         * Verifica que el usuario tenga al menos un medios de pago registrado
         */
        if (Auth::user()->typayments()->count() == 0) {
            Message::alert(trans("ui.msg.not.payments.current") . " <a href='" . route(RouteData::TYPAYMENT_CREATE) . "'>" . trans("ui.msg.want.create") . "</a>", Message::ALERT_TYPE_WARNING);
            return redirect()->route(RouteData::DASHBOARD);
        }

        $data["html"] = Typayment::getHtmlSelect(array("typayment", "origin", "dest"));

        return view(self::PATH . "createdit", $data);
    }

    /** Almacenas todas las transacciones. 
     * 
     * @param Request $request
     * @return type
     */
    public function store(Request $request) {

        Flow::createFromType($request->all());

        Message::alert(sprintf(trans("ui.msg.success.create"), trans("ui.label.transaction")), Message::ALERT_TYPE_SUCCESS);

        return redirect()->route(RouteData::FLOW_INDEX);
    }

    public function edit($id) {

        if (!Flow::isOwnerAuth($id))
            return redirect()->route(RouteData::FLOW_INDEX);

        $data = [];
        /**
         * Verifica que el usuario tenga al menos un medios de pago registrado
         */
        if (Auth::user()->typayments()->count() == 0) {
            Message::alert(trans("ui.msg.not.payments.current") . " <a href='" . route(RouteData::TYPAYMENT_CREATE) . "'>" . trans("ui.msg.want.create") . "</a>", Message::ALERT_TYPE_WARNING);
            return redirect()->route(RouteData::DASHBOARD);
        }

        $transaction = Flow::find($id);
        $data["transaction"]["id"] = $transaction->id;
        $data["transaction"]["description"] = $transaction[Flow::ATTR_DESCRIPTION];
        $data["transaction"]["value"] = $transaction[Flow::ATTR_VALUE];
        $data["transaction"]["date"] = $transaction[Flow::ATTR_DATE];
        $data["transaction"]["typayment_id"] = $transaction[Flow::ATTR_FK_TYPAYMENT_ID];
        $data["transaction"]["metadata"] = $transaction[Flow::ATTR_METADATA];
        $data["transaction"]["type"] = $transaction[Flow::ATTR_TYPE];
        $data["transaction"]["category"] = $transaction[Flow::ATTR_CATEGORY];

        $attrs = array("typayment", "origin", "dest");

        switch ($transaction[Flow::ATTR_TYPE]) {
            case Flow::TYPE_EXCHANGE:
                $attrs["origin"] = $transaction[Flow::ATTR_FK_TYPAYMENT_ID];
                $attrs["dest"] = $transaction->{Flow::ATTR_METADATA}["dest"];
                unset($attrs[array_search("origin", $attrs)]);
                unset($attrs[array_search("dest", $attrs)]);
                break;
            case Flow::TYPE_SPEND || Flow::TYPE_REVENUE:
                $attrs["typayment"] = $transaction[Flow::ATTR_FK_TYPAYMENT_ID];
                unset($attrs[array_search("typayment", $attrs)]);
                break;
        }

        $data["html"] = Typayment::getHtmlSelect($attrs);

        return view(self::PATH . "createdit", $data);
    }

    /** Recibe una petición para actualizar una transacción
     * 
     * @param Request $request
     */
    public function update($id, Request $request) {

        if (!Flow::isOwnerAuth($id))
            return redirect()->back();

        $flow = Flow::find($id);
        $data = $request->all();
        $data["id"] = $flow->id;

        //Verifica la validez de la transacción y el propietario
        if (empty($flow) || $flow[Flow::ATTR_FK_USER_ID] != Auth::user()->id)
            return redirect()->route(RouteData::FLOW_INDEX);

        //Reversa la transacción
        $flow->reverse();

        //Registra una nueva transacción con el mismo ID
        Flow::createFromType($data);

        Message::alert(sprintf(trans("ui.msg.success.edit"), trans("ui.label.transaction")), Message::ALERT_TYPE_INFO);
        return redirect()->route(RouteData::FLOW_INDEX);
    }

    /** Recibe una petición para eliminar una transacción
     * 
     * @param type $id
     * @return type
     */
    public function destroy($id, Request $request) {

        if (!Flow::isOwnerAuth($id))
            return json_encode(array("response" => false));

        $transaction = Flow::find($id);

        if (!$request->ajax())
            return json_encode(array("response" => false));

        //Verifica la validez de la transacción y el propietario
        if (empty($transaction) || $transaction[Flow::ATTR_FK_USER_ID] != Auth::user()->id)
            return json_encode(array("response" => false));

        $transaction->reverse();

        return json_encode(array("response" => true));
    }

    /**
     *  Muestra un listado de las transacciones programas
     */
    public function cron_index() {

        $data = array();

        $data["crons"] = FlowCron::where(FlowCron::ATTR_FK_USER_ID, Auth::user()->id)->get();

        return view(self::PATH . "crons", $data);
    }

    /** Recibe una petición para eliminar una transacciones programa
     * 
     * @param type $id
     * @param Request $request
     * @return type
     */
    public function cron_delete($id, Request $request) {

        $cron = FlowCron::find($id);


        if (!$request->ajax())
            return json_encode(array("response" => false));

        //Verifica la validez de la transacción y el propietario
        if (!FlowCron::isOwnerAuth($id))
            return json_encode(array("response" => false));

        $cron->delete();

        //Limpia las sessiones del modal, para que no vuelva aparecer el mismo mensaje
        Message::clearSessions();

        return json_encode(array("response" => true));
    }

    public function cron_defer($id, Request $request) {
        $cron = FlowCron::find($id);

        if (!$request->ajax())
            return json_encode(array("response" => false));

        //Verifica la validez de la transacción y el propietario
        if (!FlowCron::isOwnerAuth($id))
            return json_encode(array("response" => false));


        if (DateUtil::difSec((new DateUtil()), $cron[FlowCron::ATTR_DATE_NEXT]) > 0) {
            return json_encode(array("response" => false, "message" => trans("msg.services.flow.cron.confirm.email.error.cron.proccesed")));
        }

        $date = new DateUtil($cron[FlowCron::ATTR_DATE_NEXT]);
        $cron[FlowCron::ATTR_DATE_NEXT] = $date->addDays(1);
        $cron->save();

        return json_encode(array("response" => true));
    }

    public function cron_exec($id, Request $request) {
        $cron = FlowCron::find($id);

        if (!$request->ajax())
            return json_encode(array("response" => false));

        //Verifica la validez de la transacción y el propietario
        if (empty($cron) || $cron[FlowCron::ATTR_FK_USER_ID] != Auth::user()->id)
            return json_encode(array("response" => false));

        if (DateUtil::difSec((new DateUtil()), $cron[FlowCron::ATTR_DATE_NEXT]) > 0) {
            return json_encode(array("response" => false, "message" => trans("msg.services.flow.cron.confirm.email.error.cron.proccesed")));
        }

        $data = $request->all();
        $data["flag-date-next"] = true;

        $cron->exec($data);

        return json_encode(array("response" => true));
    }

}
