<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Web\Main;

use App\Http\Controllers\Controller;
use App\Base\Data\TypePaymentData;
use Illuminate\Http\Request;
use App\Models\Typayment;
use App\Base\System\TP\TypePayment;
use App\Base\Data\Currency;
use App\Base\Data\Bank;
use Illuminate\Support\Facades\Auth;
use App\Base\Data\RouteData;
use App\Base\UI\Message;
use App\Models\UserMeta;
use Illuminate\Support\Facades\Session;
use App\Base\System\TP\Cash;
use App\Base\System\Library\Comp\Util;
use App\Base\System\TP\CreditCard;
use Webklex\IMAP\Client;
use App\Base\System\Library\DataProviders\APIs\Mails\Gmail as GmailAPI;
use Google_Service_Gmail;
use App\Models\Auto\AutoRead;
use App\Base\System\Services\ReaderEmail\ReaderEmail;
use App\Models\Relations\TypaymentAutoRead as Typauto;
use App\Base\System\Services\ReaderEmail\DataParam;

/**
 * Description of TypaymentController
 *
 * @author jospina
 */
class TypaymentController extends Controller {

    const PATH = "content/main/typayment/";

    function index() {

        $data = [];

        $data["typayments"] = Auth::user()->typayments;


        return view(self::PATH . "index", $data);
    }

    function create() {

        $data = array();

        $data["typayments"] = TypePaymentData::getList();
        $data["currency"] = new Currency(Auth::user()->getMeta(UserMeta::KEY_CURRENCY));
        $data["app"] = json_decode(Session::get("app.setting"), true);
        $data["currencies"] = Currency::getList();

        $data["urls_autoread"] = [AutoRead::CLIENT_GMAIL => route(RouteData::SERVICES_API_GMAIL_AJAX_GET_URL_AUTH)];
        $data["autoread"] = Auth::user()->autoread_clients;

        return view(self::PATH . "createdit", $data);
    }

    function store(Request $request) {
        $data = $request->all();

        //Adecuación de campos
        foreach ($data as $index => $value) {
            if (strpos($index, "_submit") !== false) {
                $data[str_replace("_submit", "", $index)] = $value;
            }
        }

        $typayment = new Typayment;
        $typayment[Typayment::ATTR_TYPE] = $data[Typayment::ATTR_TYPE];
        $typayment[Typayment::ATTR_FK_USER_ID] = Auth::user()->id;
        $typayment[Typayment::ATTR_CURRENCY] = $data["currency"];

        $balance = Util::getValue($data["balance"], $data["currency"]);

        $payment = TypePayment::load($data[Typayment::ATTR_TYPE], floatval($balance), $data["currency"], $data);

        if (isset($data["bank-assoc"])) {
            $payment->setBank(new Bank($data["bank-assoc"]));
        }

        $typayment[Typayment::ATTR_FEATURES] = $payment;
        $typayment[Typayment::ATTR_BALANCE] = $balance;


        $typayment->save();

        //Si se asocia a una cuenta de correo
        if (isset($data["autoread-activate"])) {

            $autoparams = json_decode($data["autoread-params"], true);

            foreach ($autoparams as $type => $params) {
                if (count($params) == 0)
                    continue;

                $typauto = new Typauto();
                $typauto[Typauto::ATTR_FK_AUTOREAD_ID] = $data["autoread"];
                $typauto[Typauto::ATTR_FK_TYPAYMENT_ID] = $typayment->id;
                $typauto[Typauto::ATTR_TYPE] = $type;

                //Genera automaticamente el parametro de lectura de descripción de la transacción
                $reader = new ReaderEmail(AutoRead::find($data["autoread"]), $params);
                $params[DataParam::AUTOPARAM_INPUT_DESCRIPTION_FORMAT] = $reader->autogenerateParamForDescription();

                $typauto[Typauto::ATTR_PARAMS] = $params;

                $typauto->save();
            }
        }


        Message::alert(sprintf(trans("ui.msg.success.create"), trans("ui.label.typayment")), Message::ALERT_TYPE_SUCCESS);

        return redirect()->route(RouteData::TYPAYMENT_INDEX);
    }

    function edit($id) {

        if (!Typayment::isOwnerAuth($id))
            return redirect()->route(RouteData::TYPAYMENT_INDEX);

        $typayment = Typayment::find($id);

        if (!Typayment::isOwnerAuth($id))
            return redirect()->route(RouteData::TYPAYMENT_INDEX);

        $data = array();

        $data["typayment"] = $typayment;
        $data["typayments"] = TypePaymentData::getList();
        $data["currency"] = new Currency(Auth::user()->getMeta(UserMeta::KEY_CURRENCY));
        $data["app"] = json_decode(Session::get("app.setting"), true);
        $data["currencies"] = Currency::getList();

        $data["urls_autoread"] = [AutoRead::CLIENT_GMAIL => route(RouteData::SERVICES_API_GMAIL_AJAX_GET_URL_AUTH)];
        $data["autoread"] = Auth::user()->autoread_clients;

        $data["autoread_data"] = [];

        foreach ($typayment->autoreads as $autoread) {
            foreach (json_decode($autoread->pivot->params, true) as $param => $value) {
                if (isset(DataParam::getList()[$param])) {
                    $data["autoread_data"][$autoread->pivot->type][$param] = $value;
                }
            }
        }


        return view(self::PATH . "createdit", $data);
    }

    function update($id, Request $request) {

        if (!Typayment::isOwnerAuth($id))
            return redirect()->back();

        $data = $request->all();

        //Adecuación de campos
        foreach ($data as $index => $value) {
            if (strpos($index, "_submit") !== false) {
                $data[str_replace("_submit", "", $index)] = $value;
            }
        }

        $typayment = Typayment::find($id);
        $data["currency"] = $typayment[Typayment::ATTR_CURRENCY];

        $object = $typayment->getObjectType();

        if ($object->hasBankAssoc())
            $data[TypePaymentData::BANK] = $object->getBank()->getCode();


        switch ($object::ID) {
            case CreditCard::ID:
                $balance = $typayment->getBalance();
                $quota_new = Util::getValue($data[Typayment::ATTR_BALANCE], $data["currency"]);
                $typayment[Typayment::ATTR_BALANCE] = $quota_new - $balance;
                break;
        }

        if (isset($data[Typayment::ATTR_BALANCE]))
            $typayment[Typayment::ATTR_FEATURES] = TypePayment::load($object::ID, Util::getValue($data[Typayment::ATTR_BALANCE], $data["currency"]), $data["currency"], $data);

        $typayment->save();

        //Elimina la asociación
        Typauto::where(Typauto::ATTR_FK_TYPAYMENT_ID, $typayment->id)->delete();


        //Regenera la asociacion con los parametros modificados
        if (isset($data["autoread-activate"])) {

            $autoparams = json_decode($data["autoread-params"], true);


            foreach ($autoparams as $type => $params) {

                if (count($params) == 0)
                    continue;

                $typauto = new Typauto();
                $typauto[Typauto::ATTR_FK_AUTOREAD_ID] = $data["autoread"];
                $typauto[Typauto::ATTR_FK_TYPAYMENT_ID] = $typayment->id;
                $typauto[Typauto::ATTR_TYPE] = $type;

                //Genera automaticamente el parametro de lectura de descripción de la transacción
                $reader = new ReaderEmail(AutoRead::find($data["autoread"]), $params);
                $params[DataParam::AUTOPARAM_INPUT_DESCRIPTION_FORMAT] = $reader->autogenerateParamForDescription();

                $typauto[Typauto::ATTR_PARAMS] = $params;

                $typauto->save();
            }
        }

        Message::alert(sprintf(trans("ui.msg.success.edit"), trans("ui.label.typayment")), Message::ALERT_TYPE_INFO);
        return redirect()->route(RouteData::TYPAYMENT_INDEX);
    }

    /** Recibe una petición para eliminar un medio de pago
     * 
     * @param type $id
     * @return type
     */
    public function destroy($id, Request $request) {

        if (!Typayment::isOwnerAuth($id))
            return json_encode(array("response" => false));

        $typayment = Typayment::find($id);

        if (!$request->ajax())
            return json_encode(array("response" => false));

        //Verifica la validez de la transacción y el propietario
        if (empty($typayment) || $typayment[Typayment::ATTR_FK_USER_ID] != Auth::user()->id || count($typayment->transactions) > 0)
            return json_encode(array("response" => false));

        Typauto::where(Typauto::ATTR_FK_TYPAYMENT_ID, $typayment->id)->delete();

        $typayment->forceDelete();

        return json_encode(array("response" => true));
    }

    public function cancel($id, Request $request) {

        if (!Typayment::isOwnerAuth($id))
            return json_encode(array("response" => false));

        $typayment = Typayment::find($id);

        if (!$request->ajax())
            return json_encode(array("response" => false));

        //Verifica la validez de la transacción y el propietario
        if (empty($typayment) || $typayment[Typayment::ATTR_FK_USER_ID] != Auth::user()->id)
            return json_encode(array("response" => false));

        $typayment->delete();

        return json_encode(array("response" => true, "message" => trans("nav.route.typayment.cancel.confirm.success")));
    }

}
