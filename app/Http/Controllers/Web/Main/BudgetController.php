<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Web\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Budget;
use Illuminate\Support\Facades\Auth;
use App\Base\System\Library\Comp\Util;
use App\Base\Data\RouteData;
use App\Base\UI\Message;
use App\Base\Data\CategoryData;
use App\Jobs\Cache\RefreshCacheDataDashboard;

/**
 * Description of BudgetController
 *
 * @author jospina
 */
class BudgetController extends Controller {

    const PATH = "content/main/budget/";

    public function index() {
        $data["budgets"]=Auth::user()->budgets;
        return view(self::PATH . "index",$data);
    }

    public function create() {
        return view(self::PATH . "createdit");
    }

    public function store(Request $request) {
        $data = $request->all();

        $budget = new Budget();
        $budget[Budget::ATTR_BALANCE] = $data["balance"];
        $budget[Budget::ATTR_FK_USER_ID] = Auth::user()->id;
        $budget[Budget::ATTR_NAME] = $data["name"];
        $currency = $budget[Budget::ATTR_CURRENCY] = Auth::user()->getMeta("currency");

        $spend = array();
        $revenue = array();

        foreach ($data as $index => $value) {
            if (strpos($index, "spend-") !== false) {
                $subcat = str_replace("spend-", "", $index);
                $spend[CategoryData::getCatMain($subcat)][$subcat] = Util::getValue($value, $currency);
            }
            if (strpos($index, "revenue-") !== false) {
                $subcat = str_replace("revenue-", "", $index);
                $revenue[CategoryData::getCatMain($subcat)][$subcat] = Util::getValue($value, $currency);
            }
        }

        $date_end = new \DateTime($data["date_end_submit"] . "-01 00:00:00");
        $date_end->modify('last day of');

        $budget[Budget::ATTR_DATA_SPEND] = json_encode($spend);
        $budget[Budget::ATTR_DATA_REVENUE] = json_encode($revenue);


        $budget[Budget::ATTR_ACTIVE] = (Auth::user()->budgets->where(Budget::ATTR_ACTIVE, true)->count() == 0);
        $budget[Budget::ATTR_DATE_START] = $data["date_start_submit"] . "-01";
        $budget[Budget::ATTR_DATE_END] = $date_end;

        $budget->save();

        dispatch(new RefreshCacheDataDashboard(Auth::user()->id));


        Message::alert(sprintf(trans("ui.msg.success.create"), trans("ui.label.budget")), Message::ALERT_TYPE_SUCCESS);
        return redirect()->route(RouteData::BUDGET_INDEX);
    }

    public function edit($id) {

        if (!Budget::isOwnerAuth($id))
            return redirect()->route(RouteData::BUDGET_INDEX);

        $data = [];
        $data["budget"] = Budget::find($id);

        return view(self::PATH . "createdit", $data);
    }

    public function update($id, Request $request) {

        if (!Budget::isOwnerAuth($id))
            return redirect()->back();

        $budget = Budget::find($id);

        $data = $request->all();

        //Verifica la validez y el propietario
        if (empty($budget) || $budget[Budget::ATTR_FK_USER_ID] != Auth::user()->id)
            return redirect()->route(RouteData::BUDGET_INDEX);

        $spend = array();
        $revenue = array();

        $currency = $budget[Budget::ATTR_CURRENCY];

        foreach ($data as $index => $value) {
            if (strpos($index, "spend-") !== false) {
                $subcat = str_replace("spend-", "", $index);
                $spend[CategoryData::getCatMain($subcat)][$subcat] = Util::getValue($value, $currency);
            }
            if (strpos($index, "revenue-") !== false) {
                $subcat = str_replace("revenue-", "", $index);
                $revenue[CategoryData::getCatMain($subcat)][$subcat] = Util::getValue($value, $currency);
            }
        }
        $budget[Budget::ATTR_BALANCE] = $data["balance"];
        $budget[Budget::ATTR_NAME] = $data["name"];
        $budget[Budget::ATTR_DATA_SPEND] = json_encode($spend);
        $budget[Budget::ATTR_DATA_REVENUE] = json_encode($revenue);

        $date_end = new \DateTime($data["date_end_submit"] . "-01 00:00:00");
        $date_end->modify('last day of');

        $budget[Budget::ATTR_DATE_START] = $data["date_start_submit"] . "-01";
        $budget[Budget::ATTR_DATE_END] = $date_end;
        $budget->save();

        Message::alert(sprintf(trans("ui.msg.success.edit"), trans("ui.label.budget")), Message::ALERT_TYPE_INFO);

        dispatch(new RefreshCacheDataDashboard(Auth::user()->id));

        return redirect()->route(RouteData::BUDGET_INDEX);
    }

    public function destroy($id, Request $request) {

        if (!Budget::isOwnerAuth($id))
            return json_encode(array("response" => false));

        $budget = Budget::find($id);

        if (!$request->ajax())
            return json_encode(array("response" => false));

        //Verifica la validez y el propietario
        if (empty($budget) || $budget[Budget::ATTR_FK_USER_ID] != Auth::user()->id)
            return json_encode(array("response" => false));

        $budget->delete();

        return json_encode(array("response" => true));
    }

}
