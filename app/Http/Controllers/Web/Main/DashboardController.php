<?php

/*
  |--------------------------------------------------------------------------
  | DASHBOARD MAIN
  |--------------------------------------------------------------------------
 */

namespace App\Http\Controllers\Web\Main;

use App\Http\Controllers\Controller;
use App\Models\Flow;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\FlowCron;
use App\Base\System\Library\Comp\FineDiff;
use App\Models\Auto\AutoRead;
use App\Base\System\Services\ReaderEmail\ReaderEmail;

class DashboardController extends Controller {

    public function index() {
        return view("content/main/dashboard");
    }

}
