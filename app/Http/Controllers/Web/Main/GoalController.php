<?php

namespace App\Http\Controllers\Web\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Goal;
use Illuminate\Support\Facades\Auth;
use App\Base\UI\Message;
use App\Base\Data\RouteData;
use App\Base\System\Library\Comp\Util;
use App\Jobs\Cache\RefreshCacheDataWCGoals;

class GoalController extends Controller {

    const PATH = "content/main/goal/";

    public function index() {
        $data["goals"] = Auth::user()->goals()->orderBy("id", "DESC")->get();
        $data["goals_complete"] = Auth::user()->goals()->onlyTrashed()->orderBy("id", "DESC")->get();
        return view(self::PATH . "index", $data);
    }

    public function create() {
        $data = array();

        if (Goal::getRangeSavingAvailable(Auth::user()->id) == 0) {
            return Message::alert(trans("nav.route.goal.create.restriction.warning.1"), Message::ALERT_TYPE_ERROR);
        }

        return view(self::PATH . "createdit");
    }

    public function store(Request $request) {
        $data = $request->all();


        $goal = new Goal();
        $goal[Goal::ATTR_FK_USER_ID] = Auth::user()->id;
        $goal[Goal::ATTR_VALUE] = Util::getValue($data["value"], Auth::user()->getMetaCurrency());
        $goal[Goal::ATTR_NAME] = $data[Goal::ATTR_NAME];

        $date = new \DateTime($data["date_submit"] . "-01 00:00:00");
        $date->modify('last day of');

        $goal[Goal::ATTR_DATE] = $date->format("Y-m-d");


        $goal[Goal::ATTR_CURRENCY] = Auth::user()->getMetaCurrency();
        $goal[Goal::ATTR_RATE_SAVING] = intval($data["rate_saving"]) / 100;
        $goal->save();

        dispatch(new RefreshCacheDataWCGoals($goal->{Goal::ATTR_FK_USER_ID}));

        Message::alert(sprintf(trans("ui.msg.success.create"), trans("ui.label.goal")), Message::ALERT_TYPE_SUCCESS);
        return redirect()->route(RouteData::GOAL_INDEX);
    }

    public function edit($id) {
        $data = array();

        if (!Goal::isOwnerAuth($id))
            return redirect()->route(RouteData::GOAL_INDEX);

        $goal = Goal::find($id);

        $data["goal"] = $goal;
        return view(self::PATH . "createdit", $data);
    }

    public function update($id, Request $request) {

        if (!Goal::isOwnerAuth($id))
            return redirect()->back();

        $data = $request->all();

        $goal = Goal::find($id);
        $goal[Goal::ATTR_VALUE] = Util::getValue($data["value"], Auth::user()->getMetaCurrency());
        $goal[Goal::ATTR_NAME] = $data[Goal::ATTR_NAME];

        $date = new \DateTime($data["date_submit"] . "-01 00:00:00");
        $date->modify('last day of');

        $goal[Goal::ATTR_DATE] = $date->format("Y-m-d");

        $goal[Goal::ATTR_RATE_SAVING] = intval($data["rate_saving"]) / 100;
        $goal->save();

        dispatch(new RefreshCacheDataWCGoals($goal->{Goal::ATTR_FK_USER_ID}));

        Message::alert(sprintf(trans("ui.msg.success.edit"), trans("ui.label.goal")), Message::ALERT_TYPE_INFO);
        return redirect()->route(RouteData::GOAL_INDEX);
    }

    public function destroy($id, Request $request) {

        if (!Goal::isOwnerAuth($id))
            return json_encode(array("response" => false));

        $goal = Goal::find($id);

        if (!$request->ajax())
            return json_encode(array("response" => false));

        //Verifica la validez y el propietario
        if (empty($goal) || $goal[Goal::ATTR_FK_USER_ID] != Auth::user()->id)
            return json_encode(array("response" => false));

        if ($goal->getStatus() == Goal::STATUS_IN_PROGRESS)
            $goal->forceDelete();
        else
            $goal->delete();

        dispatch(new RefreshCacheDataWCGoals($goal->{Goal::ATTR_FK_USER_ID}));

        return json_encode(array("response" => true));
    }

}
