<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Models\UserMeta;
use App\Models\Flow;
use App\Base\Data\Currency;
use \App\Base\Data\ModelData;
use App\Base\Data\TypePaymentData;
use App\Models\FlowCron;
use App\Base\System\TP\{
    Cash,
    CreditCard,
    BankAccount
};
use App;
use App\Base\Data\RouteData;
use App\Base\Data\CategoryData;

class AppSetting {

    /**
     * CARGA EN UNA VARIABLE DE SESSION LOS PARAMETROS CONFIGURADOS DEL USUARIO
     * QUE SON UTILIZADOS EN TODA LA APLICACIÓN
     */
    public function handle($request, Closure $next, $guard = null) {

        if (!Auth::check())
            return $next($request);


        FlowCron::handler();
        Flow::checkAutoread();

        //Set Language
        App::setLocale(Auth::user()->getLang());


        $app = [];

        $currency = new Currency(Auth::user()->getMeta(UserMeta::KEY_CURRENCY));

        $app["user"]["data"]["id"] = Auth::user()->id;
        $app["route"]["ajax"]["set"]["meta"]["user"] = route(RouteData::AJAX_POST_SET_META_USER);
        $app["route"]["ajax"]["post"]["eloquent"]["toggle"]["active"] = route(RouteData::AJAX_POST_ELOQUENT_TOGGLE_ACTIVE);
        $app["route"]["ajax"]["post"]["ui"]["modal"]["render"]["view"] = route(RouteData::AJAX_POST_UI_RENDER_VIEW);

        /*
          |--------------------------------------------------------------------------
          | Tipo moneda
          |--------------------------------------------------------------------------
         */
        $app["currency"]["code"] = Auth::user()->getMeta(UserMeta::KEY_CURRENCY);
        $app["currency"]["format"] = $currency->getFormat();
        $app["currency"]["symbol"] = $currency->getSymbol();
        $app["currency"]["unify"] = Auth::user()->getMeta(UserMeta::KEY_CURRENCY_UNIFY);


        /*
          |--------------------------------------------------------------------------
          | MEDIOS DE PAGO
          |--------------------------------------------------------------------------
         */

        $app["typayment"]["list"] = TypePaymentData::getList();
        $app["typayment"]["type"]["cash"] = Cash::ID;
        $app["typayment"]["type"]["credit_card"] = CreditCard::ID;
        $app["typayment"]["type"]["bank_account"] = BankAccount::ID;


        /*
          |--------------------------------------------------------------------------
          | TIPO DE TRANSACCION
          |--------------------------------------------------------------------------
         */

        $app["flow"]["type"]["spend"] = Flow::TYPE_SPEND;
        $app["flow"]["type"]["revenue"] = Flow::TYPE_REVENUE;
        $app["flow"]["type"]["exchange"] = Flow::TYPE_EXCHANGE;
        $app["flow"]["type"]["investment"] = Flow::TYPE_INVESTMENT;

        /*
          |--------------------------------------------------------------------------
          | MODELOS - ENTIDADES
          |--------------------------------------------------------------------------
         */

        $app["model"]["type"]["flow"] = ModelData::ENT_FLOW;

        /*
          |--------------------------------------------------------------------------
          | CONJUNTO DE DATOS
          |--------------------------------------------------------------------------
         */

        $app["dataset"]["user"]["categories"]["spend"]["sub"] = CategoryData::getList("SP");
        $app["dataset"]["user"]["categories"]["revenue"]["sub"] = CategoryData::getList("EN");
        $app["dataset"]["user"]["categories"]["spend"]["main"] = CategoryData::getListCatMain("SP");
        $app["dataset"]["user"]["categories"]["revenue"]["main"] = CategoryData::getListCatMain("EN");


        $app = array_merge($app, App\Base\UI\AppTemplate::getDataUI());


        Session::set("app.setting", json_encode($app));
        config(['app.setting' => $app]);

        return $next($request);
    }

}
