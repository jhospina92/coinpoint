<?php

namespace App\Http\Middleware;

use App\Wildo\Wildo;
use Closure;

class WildoMiddleware {

    public function handle($request, Closure $next, $guard = null) {

        Wildo::run();

        return $next($request);
    }

}
