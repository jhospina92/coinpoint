<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Closure;
use Illuminate\Session\TokenMismatchException;
use App\Base\Data\RouteData;

class VerifyCsrfToken extends BaseVerifier {

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
            //
    ];

    public function handle($request, Closure $next) {

        if ($this->isExceptionRoute($request) ||
                $this->isReading($request) ||
                $this->runningUnitTests() ||
                $this->shouldPassThrough($request) ||
                $this->tokensMatch($request)
        ) {
            return $this->addCookieToResponse($request, $next($request));
        }

        throw new TokenMismatchException;
    }

    /** Indica si la ruta es una excepción 
     * 
     * @param \Illuminate\Http\Request $request
     */
    private function isExceptionRoute($request) {

        $routes_get = [];
        $routes_post = [];

        if ($request->isMethod('post')) {
            return (in_array($request->route()->getName(), $routes_post));
        }


        if ($request->isMethod('get')) {
            return (in_array($request->route()->getName(), $routes_get));
        }
    }

}
