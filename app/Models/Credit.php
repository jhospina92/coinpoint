<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use App\Models\User;
use App\Base\Data\CategoryData as CategoryData;
use App\Models\CreditTrack;
use App\Base\Data\Currency;
/**
 * Description of Credit
 *
 * @author jospina
 */
use App\Custom\Model;

class Credit extends Model {

    const DB_TABLE = "credits";
    //************************
    const ATTR_NAME = "name";
    const ATTR_ORGANIZATION = "organization";
    const ATTR_STATUS = "status";
    const ATTR_BALANCE = "balance"; //El valor faltante por pagar en el que se encuentra el prestamo
    const ATTR_VALUE = "value"; //El valor por el cual se realizo el prestamo
    const ATTR_FEES = "fees"; //Número de cuotas
    const ATTR_METADATA = "metadata"; //Información adicional
    const ATTR_CATEGORY = "category"; //Categoria asociada
    const ATTR_CURRENCY = "currency";
    //********* FK
    const ATTR_FK_USER_ID = "user_id";
    //Propiedades - Status
    const STATUS_ACTIVE = "AC"; //Activo
    const STATUS_CONCLUDE = "CO"; //Concluido

    /**
     *  Retorna un objeto array con el nombre de caracteristicas adicionales que contiene un credito. 
     */

    public static function getFeatures($id = null) {
        $data = array();
        //Ratio interes
        $data[] = ["id" => "feature_interests_ratio", "name" => trans("attr.credit.features.1.name"), "attrs" => ["type" => "text", "data" => "decimal"]];
        $data[] = ["id" => "feature_date_limit_fee", "name" => trans("attr.credit.features.2.name"), "attrs" => ["type" => "select", "data" => "number", "min" => "1", "max" => "30"]];

        if (!is_null($id)) {
            foreach ($data as $feature) {
                if ($feature["id"] == $id) {
                    return $feature;
                }
            }
        }

        return $data;
    }

    public function getBalance($format = false) {
        $is_unify = config("app.setting.currency.unify");

        $value = $this->balance;

        if ($is_unify) {
            $value = Currency::convert($value, $this->currency, config("app.setting.currency.code"));
            if ($format) {
                return Currency::getFormattedValue($value, config("app.setting.currency.code"));
            }
        }

        //Retorna el valor en formato moneda
        if ($format) {
            return Currency::getFormattedValue($value, $this->currency);
        }

        return $value;
    }

    public function track() {
        return $this->hasMany(CreditTrack::class);
    }

    /*
      |--------------------------------------------------------------------------
      | MUTATORS
      |--------------------------------------------------------------------------
     */

    public function getCategoryAttribute($code) {
        return CategoryData::getName($code);
    }

    /*
      |--------------------------------------------------------------------------
      | RELACTIONES
      |--------------------------------------------------------------------------
     */

    function user() {
        return $this->belongsTo(User::class);
    }

    function getFeesNumberPaid() {
        return CreditTrack::getFeesNumberPaid($this->id);
    }

}
