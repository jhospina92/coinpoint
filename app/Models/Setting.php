<?php

namespace App\Models;

use App\Custom\Model;

class Setting extends Model {

    const DB_TABLE = "settings";
    const ATTR_KEY = "key";
    const ATTR_VALUE = "value";
    const ATTR_FK_USER_ID = "user_id";
    //Meta keys
    const KEY_CURRENCY="currency";
    

    protected $table = self::DB_TABLE;
    public $timestamps = false;

}
