<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use App\Custom\Model;
use App\Models\Flow;
use App\Base\Data\CategoryData;
use App\Base\System\Library\Comp\DateUtil;
use App\Base\Data\Currency;

class Budget extends Model {

    const DB_TABLE = "budgets";
    //Attrs
    const ATTR_NAME = "name";
    const ATTR_ACTIVE = "active";
    const ATTR_DATA_REVENUE = "data_revenue";
    const ATTR_DATA_SPEND = "data_spend";
    const ATTR_BALANCE = "balance";
    const ATTR_DATE_START = "date_start";
    const ATTR_DATE_END = "date_end";
    const ATTR_CURRENCY = "currency";
    //FK
    const ATTR_FK_USER_ID = "user_id";

    protected $table = self::DB_TABLE;

    /** Retorna el valor total de una categoria principal dado por el codigo
     * 
     * @param string $catmain
     * @return int
     */
    function getTotalCategoryMain(string $catmain) {
        $data = array_merge($this->data_revenue, $this->data_spend);
        $total = 0;
        foreach ($data[$catmain] as $cat => $value) {
            $total += $value;
        }

        return $total;
    }

    /** Obtiene el total de ingresos
     * 
     * @return int
     */
    function getTotalRevenue($year = null, $month = null) {
        $year = (empty($year)) ? date("Y") : $year;
        $month = (empty($month)) ? date("m") : $month;
        $date = new DateUtil($year . "-" . $month . "-01", "Y-m-d");

        $date_start = $this->{self::ATTR_DATE_START};
        $date_end = $this->{self::ATTR_DATE_END};

        if (!(DateUtil::difSec($date, $date_start) <= 0 && DateUtil::difSec($date, $date_end) > 0)) {
            return 0;
        }

        $data = $this->data_revenue;
        $total = 0;
        foreach ($data as $catmain => $cat) {
            foreach ($data[$catmain] as $index => $value) {
                $total += $value;
            }
        }

        return $total;
    }

    /** Obtiene el total de gastos
     * 
     * @return int
     */
    function getTotalSpend($year = null, $month = null) {
        $year = (empty($year)) ? date("Y") : $year;
        $month = (empty($month)) ? date("m") : $month;

        $date = new DateUtil($year . "-" . $month . "-01", "Y-m-d");

        $date_start = $this->{self::ATTR_DATE_START};
        $date_end = $this->{self::ATTR_DATE_END};

        if (!(DateUtil::difSec($date, $date_start) <= 0 && DateUtil::difSec($date, $date_end) > 0)) {
            return 0;
        }

        $data = $this->data_spend;
        $total = 0;
        foreach ($data as $catmain => $cat) {
            foreach ($data[$catmain] as $index => $value) {
                $total += $value;
            }
        }

        return $total;
    }

    function setActive($boolean = true) {
        $this->active = $boolean;
        $this->save();
    }

    /** Retorna el número de meses de vigencia del presupuesto
     * 
     * @return int
     */
    function getCountMonthsValidity() {
        $segs = DateUtil::difSec($this->{self::ATTR_DATE_START}, $this->{self::ATTR_DATE_END});

        if ($segs < 0) {
            return 0;
        }

        return floor(((($segs / 60) / 60) / 24) / 30);
    }

    /** Retorna el valor real de la categoria de un prepuesto
     * 
     * @param type $category
     * @param type $year
     * @param type $month
     * @return type
     */
    function getRealValueCategory($category, $year = null, $month = null) {

        $type = CategoryData::getType($category);
        $transactions = Flow::getForMonth($year, $month, $type, [], $this->{self::ATTR_FK_USER_ID});
        $total = 0;

        foreach ($transactions as $transaction) {

            $value = Currency::convert($transaction->value, $transaction->typayment->getObjectType()->getCurrency()->getCode(), $this->currency);

            //Categorias especiales - Prestamos
            if (CategoryData::EXT_CREDITS_TRACK == $transaction[Flow::ATTR_CATEGORY] && $category == CategoryData::EXT_CREDITS_TRACK) {
                $total += $value;
                continue;
            }

            if (strpos($category, "-") === false && strpos($transaction[Flow::ATTR_CATEGORY], $category) !== false) {
                $total += $value;
                continue;
            }

            //Subcategoria
            if ($transaction[Flow::ATTR_CATEGORY] == $category) {
                $total += $value;
            }
        }

        return $total;
    }

    /**
     * Retorna el ahorro esperado actual del presupuesto cruzando 
     */
    public function getSavingExpected($year = null, $month = null) {
        $year = (empty($year)) ? date("Y") : $year;
        $month = (empty($month)) ? date("m") : $month;

        $date = new DateUtil($year . "-" . $month . "-01", "Y-m-d");

        $date_start = $this->{self::ATTR_DATE_START};
        $date_end = $this->{self::ATTR_DATE_END};

        if (!(DateUtil::difSec($date, $date_start) <= 0 && DateUtil::difSec($date, $date_end) > 0)) {
            return 0;
        }

        $revenues = $this->data_revenue;
        $spends = $this->data_spend;
        $total_spend = $this->getTotalSpend();
        $total_revenue = $this->getTotalRevenue();
        $saving_projection = 0;

        foreach ($revenues as $catmain => $data) {

            $total_category = $this->getTotalCategoryMain($catmain);
            $total_category_current = $this->getRealValueCategory($catmain, $year, $month);
            $progress_current = ($total_category_current / $total_revenue) * 100;
            $progress_budget = (($total_category / $total_revenue) * 100);

            if ($progress_current > $progress_budget) {
                $saving_projection += $total_category_current;
            } else {
                $saving_projection += $total_category;
            }
        }

        foreach ($spends as $catmain => $data) {

            $total_category = $this->getTotalCategoryMain($catmain);
            $total_category_current = $this->getRealValueCategory($catmain, $year, $month);
            $progress_current = ($total_category_current / $total_spend) * 100;
            $progress_budget = (($total_category / $total_spend) * 100);

            if ($progress_current > $progress_budget) {
                $saving_projection -= $total_category_current;
            } else {
                $saving_projection -= $total_category;
            }
        }

        return $saving_projection;
    }

    /*
      |--------------------------------------------------------------------------
      | RELACIONES
      |--------------------------------------------------------------------------
     */

    function user() {
        return $this->belongsTo(User::class);
    }

    /*
      |--------------------------------------------------------------------------
      | MUTATORS
      |--------------------------------------------------------------------------
     */

    function getDataRevenueAttribute($data) {
        return json_decode($data, true);
    }

    function getDataSpendAttribute($data) {
        return json_decode($data, true);
    }

}
