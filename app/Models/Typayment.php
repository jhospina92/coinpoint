<?php

namespace App\Models;

use App\Custom\Model;
use App\Models\User;
use App\Base\System\TP\TypePayment;
use Illuminate\Support\Facades\Auth;
use App\Models\Flow;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Base\System\TP\CreditCard;
use App\Base\Data\Currency;
use App\Base\System\TP\Cash;
use App\Jobs\Cache\RefreshCacheDataDashboard;
use App\Jobs\Cache\RefreshCacheDataWCGoals;
use App\Models\Relations\TypaymentAutoRead as Typauto;

class Typayment extends Model {

    use SoftDeletes;

    const DB_TABLE = "typayments";
    const ATTR_BALANCE = "balance";
    const ATTR_FEATURES = "features";
    const ATTR_CURRENCY = "currency";
    const ATTR_TYPE = "type";
    //FK
    const ATTR_FK_USER_ID = "user_id";

    protected $table = self::DB_TABLE;
    protected $dates = ['deleted_at'];

    /*
      |--------------------------------------------------------------------------
      | RELACIONES
      |--------------------------------------------------------------------------
     */

    function user() {
        return $this->belongsTo(User::class);
    }

    function getObjectType() {
        if (is_null($this))
            return array();

        $is_unify = config("app.setting.currency.unify");
        $data = json_decode($this->features, true);

        if ($is_unify)
            $data["balance"] = floatval(Currency::convert($data["balance"], $this->{self::ATTR_CURRENCY}, config("app.setting.currency.code")));

        return TypePayment::load($this->type, $this->getBalance(), $this->currency, $data);
    }

    /** Retorna el balance actual del medio pago
     *  
     * @param type $format Si retorna en forma de formato
     * @return [int|string]
     */
    function getBalance($format = false) {

        $is_unify = config("app.setting.currency.unify");

        $value = $this->balance;

        if ($this->{self::ATTR_TYPE} == CreditCard::ID) {
            $coup = json_decode($this->{self::ATTR_FEATURES}, true)["balance"];
            $value = $coup - $this->balance;
        }

        if ($is_unify) {
            $value = Currency::convert($value, $this->currency, config("app.setting.currency.code"));
            if ($format) {
                return Currency::getFormattedValue($value, config("app.setting.currency.code"));
            }
        }

        //Retorna el valor en formato moneda
        if ($format) {
            return Currency::getFormattedValue($value, $this->currency);
        }

        return $value;
    }

    /** Retorna el valor disponible del medio de pago
     * 
     * @param type $format
     * @return type
     */
    function getAvailableValue($format = false) {

        $is_unify = config("app.setting.currency.unify");

        $value = $this->balance;

        if ($is_unify) {
            $value = Currency::convert($value, $this->currency, config("app.setting.currency.code"));
            if ($format) {
                return Currency::getFormattedValue($value, config("app.setting.currency.code"));
            }
        }

        if ($format) {
            return Currency::getFormattedValue($value, $this->currency);
        }

        return $value;
    }

    public function updateBalance($type, int $value) {
        
        //Gastos
        if ($type == Flow::TYPE_SPEND) {
            $this->balance -= $value;
        }
        //Ingresos
        if ($type == Flow::TYPE_REVENUE) {
            $this->balance += $value;
        }

        //Transferencias
        if ($type == Flow::TYPE_EXCHANGE) {
            $this->balance -= $value;
        }

        $response = $this->save();

        dispatch(new RefreshCacheDataDashboard($this->{self::ATTR_FK_USER_ID}));
        dispatch(new RefreshCacheDataWCGoals($this->{self::ATTR_FK_USER_ID}));

        return $response;
    }

    public function hasRestrictions() {
        $object = $this->getObjectType();

        switch ($object::ID) {
            case CreditCard::ID:
                return $object->getBalance() != 0;
                break;
        }

        return false;
    }

    /** Retorna un array con HTML por cada nombre pasado como array
     * 
     * @return string
     */
    public static function getHtmlSelect($names = array()) {

        $data = array();


        $currencies = Auth::user()->getCurrencies();


        if (!Auth::check())
            return null;

        /**
         * Construye unos campos de SELECT para seleccionar medios de pago
         */
        foreach ($names as $name => $value) {

            $name = (is_numeric($value)) ? $name : $value;

            $data["select"][$name] = '<select class="mdb-select" name="' . $name . '" id="' . $name . '" data-required><option value="" disabled selected>' . trans("ui.label.select") . '</option>';

            
            /**
             * Construye el item html del select
             */
            $item_select = function($id, $value, $available_value, TypePayment $typepayment) {
                $format = $typepayment->getCurrency()->getFormat();
                return '<option value="' . $id . '" data-type="' . $typepayment::ID . '" data-features="'.str_replace("\"","'",json_encode($typepayment->getDataFeatures(),JSON_FORCE_OBJECT)).'" data-currency-symbol="' . $typepayment->getCurrency()->getSymbol() . '" data-currency-code="' . $typepayment->getCurrency()->getCode() . '" data-currency-format-decimals="' . $format[0] . '" data-currency-format-sep-millar="' . $format[1] . '" data-currency-format-sep-decimal="' . $format[2] . '" data-balance="' . $available_value . '" ' . ((is_numeric($value) && $value == $id) ? "selected" : "") . '>' . $typepayment->getFullName() . '</option>';
            };

            if (count($currencies) == 1) {
                foreach (Auth::user()->typayments()->orderBy(Typayment::ATTR_TYPE, "ASC")->get() as $typayment) {
                    $data["select"][$name] .= $item_select($typayment->id, $value, $typayment->getAvailableValue(), $typayment->getObjectType());
                }
            } else {

                /**
                 * ORGANIZA EL SELECT AGRUPANDOLO POR EL TIPO DE MONEDA
                 */
                for ($f = 0; $f < count($currencies); $f++) {
                    $currency = $currencies[$f];
                    //Cabecera del grupo
                    $data["select"][$name] .= '<optgroup label="' . $currency . '">';
                    //Filtra los tipos de pagos por el tipo de moneda indicada
                    $filter = str_replace(array("{", "}"), "%", json_encode(array("currency" => $currency)));
                    //Asigna cada opción
                    foreach (Auth::user()->typayments()->where(Typayment::ATTR_FEATURES, "LIKE", $filter)->orderBy(Typayment::ATTR_TYPE, "ASC")->get() as $typayment) {
                        $data["select"][$name] .= $item_select($typayment->id, $value, $typayment->getAvailableValue(), $typayment->getObjectType());
                    }
                    $data["select"][$name] .= '</optgroup>';
                }
            }

            $data["select"][$name] .= '</select>';
        }

        return $data;
    }

    /*
      |--------------------------------------------------------------------------
      | RELACIONES
      |--------------------------------------------------------------------------
     */

    function transactions() {
        return $this->hasMany(Flow::class, Flow::ATTR_FK_TYPAYMENT_ID);
    }

    function autoreads() {
        return $this->belongsToMany(Auto\AutoRead::class, Typauto::DB_TABLE, Typauto::ATTR_FK_TYPAYMENT_ID, Typauto::ATTR_FK_AUTOREAD_ID)->withPivot(Typauto::ATTR_TYPE, Typauto::ATTR_PARAMS);
    }

}
