<?php

namespace App\Models;

use App\Custom\Model;
use App\Models\Typayment;
use Illuminate\Support\Facades\Auth;
use App\Base\System\Library\Comp\Util;
use App\Models\User;
use \Illuminate\Http\Request;
use App\Base\System\TP\CreditCard;
use App\Base\Data\CategoryData as CategoryData;
use App\Models\Credit;
use App\Models\FlowCron;
use App\Base\Data\Currency;
use App\Base\System\Library\Comp\DateUtil;
use App\Base\UI\Widget;
use App\Models\Logs\LogAutoReadProcessed as LogSuccess;
use App\Base\UI\Message;

class Flow extends Model {

    const DB_TABLE = "flow";
    const ATTR_DATE = "date";
    const ATTR_DESCRIPTION = "description";
    const ATTR_VALUE = "value";
    const ATTR_TYPE = "type";
    const ATTR_CATEGORY = "category";
    const ATTR_METADATA = "metadata";
    //Foreanos
    const ATTR_FK_USER_ID = "user_id";
    const ATTR_FK_TYPAYMENT_ID = "typayment_id";
    //Tipos
    const TYPE_SPEND = "SP"; //Gasto
    const TYPE_REVENUE = "EN"; // Ingreso
    const TYPE_INVESTMENT = "IN"; //Inversión
    const TYPE_EXCHANGE = "EX"; //Intercambio

    protected $table = self::DB_TABLE;

    /** Crea en la base de datos un registro basado en el tipo de transacción
     * 
     * @param array $data
     * @param string $type
     * @return type
     */
    public static function createFromType($data, string $type = null) {

        $flow = new Flow();
        if (isset($data["id"]))
            $flow->id = $data["id"];


        $flow[Flow::ATTR_FK_TYPAYMENT_ID] = (isset($data["typayment"])) ? $data["typayment"] : $data["origin"];
        $currency = $flow->typayment->getObjectType()->getCurrency()->getCode();

        $flow[Flow::ATTR_VALUE] = Util::getValue($data["value"], $currency);
        $flow[Flow::ATTR_DATE] = $data["date_submit"];
        $flow[Flow::ATTR_DESCRIPTION] = trim($data["description"]);


        if (Auth::check())
            $flow[Flow::ATTR_FK_USER_ID] = Auth::user()->id;

        if (isset($data["user_id"]))
            $flow[Flow::ATTR_FK_USER_ID] = $data["user_id"];


        if (empty($flow[Flow::ATTR_FK_USER_ID]))
            throw new \Exception("User ID not has been defined");


        $flow[Flow::ATTR_TYPE] = $data["type"];
        $metadata = array();


        $typayment = Typayment::find($flow[Flow::ATTR_FK_TYPAYMENT_ID])->getObjectType();

        //Si la transaccion esta asociada a una tarjeta de credito
        if ($typayment::ID == CreditCard::ID) {
            if (isset($data["fees"]))
                $metadata["fees"] = $data["fees"];
            if (isset($data["cc-fees"]))
                $metadata["fees"] = $data["cc-fees"];
        }

        switch ((is_null($type)) ? $flow[Flow::ATTR_TYPE] : $type) {

            case self::TYPE_EXCHANGE:

                $metadata["dest"] = $data["dest"];
                $typayment_dest = Typayment::find($data["dest"]);
                //Actualiza el balance del medio de pago
                $typayment_dest->updateBalance(Flow::TYPE_REVENUE, Currency::convert($flow[Flow::ATTR_VALUE], $currency, $typayment_dest->getObjectType()->getCurrency()->getCode()));

                /**
                 * SI EXISTE UN COSTO ASOCIADO A LA TRANSACCIÓN REGISTRA EL CORRESPONDIENTE EGRESO
                 */
                if (isset($data["exchange-cost"]) && Util::getValue($data["exchange-cost"], $currency) > 0) {
                    $attr = array(
                        "value" => Util::getValue($data["exchange-cost"], $currency),
                        "date_submit" => $data["date_submit"],
                        Flow::ATTR_DESCRIPTION => trans("ui.label.cost.assoc.exchange"),
                        "type" => self::TYPE_SPEND,
                        "typayment" => $data["origin"],
                        Flow::ATTR_CATEGORY => CategoryData::SP_SERVICES_COMMISSIONS_INTERESTS
                    );

                    //Si se ha  realizado un avance de la tarjeta de credito
                    if ($typayment::ID == CreditCard::ID) {
                        $attr["fees"] = $typayment->getDefaultFees();
                    }

                    $flow->typayment->balance -= $attr["value"];

                    $metadata["transaction"] = self::createFromType($attr);
                }

                break;
            // GASTO - INGRESO
            case self::TYPE_REVENUE || self::TYPE_SPEND:

                $flow[Flow::ATTR_CATEGORY] = $category = $data["category"];


                /*
                  |--------------------------------------------------------------------------
                  | PAGO DE CUOTAS DE PRESTAMOS O CREDITOS
                  |--------------------------------------------------------------------------
                 */

                //Si el gasto esta asociado al pago de un credito
                if (strpos($category, CategoryData::EXT_CREDITS_TRACK) !== false) {

                    if (isset($data["credit_id"])) {
                        $credit_id = $data["credit_id"];
                    } else {
                        $credit_id = str_replace(CategoryData::EXT_CREDITS_TRACK . "-", "", $category);
                    }

                    $flow[Flow::ATTR_CATEGORY] = CategoryData::EXT_CREDITS_TRACK;
                    $metadata["credit_id"] = $credit_id;
                    $credit = Credit::find($credit_id);
                    $metadata["credit_id"] = $credit_id;
                    $realvalue = Currency::convert($flow[Flow::ATTR_VALUE], $currency, $credit[Credit::ATTR_CURRENCY]);

                    //Procesa el balance del credit
                    $data["credit_id"] = $credit_id;
                    $data["type"] = CreditTrack::TYPE_FEE;
                    CreditTrack::register(array_merge($data, ["value" => $realvalue]));
                }


                /*
                  |--------------------------------------------------------------------------
                  | PAGO DE CUOTAS DE TARJETAS DE CREDITO
                  |--------------------------------------------------------------------------
                 */

                if (strpos($category, CategoryData::EXT_CREDITS_CARD) !== false) {

                    if (isset($data["credit_card"])) {
                        $typayment_id = $data["credit_card"];
                    } else {
                        $typayment_id = str_replace(CategoryData::EXT_CREDITS_CARD . "-", "", $category);
                    }
                    $flow[Flow::ATTR_CATEGORY] = CategoryData::EXT_CREDITS_CARD;
                    $metadata["credit_card"] = $typayment_id;
                    $typayment = Typayment::find($typayment_id);
                    $typayment_origin = Typayment::find($data["typayment"])->getObjectType();

                    //Convierte a la moneda el valor ingresado del medio de pago al valor de la tarjeta de credito
                    $realvalue = Currency::convert(Util::getValue($data["value"], $typayment_origin->getCurrency()->getCode()), $typayment_origin->getCurrency()->getCode(), $typayment->getObjectType()->getCurrency()->getCode());
                    // exit("VALOR= > ".$realvalue);
                    $typayment[Typayment::ATTR_BALANCE] = +$typayment[Typayment::ATTR_BALANCE] + $realvalue;
                    $coup = intval(json_decode($typayment[Typayment::ATTR_FEATURES], true)["balance"]);
                    if ($typayment[Typayment::ATTR_BALANCE] >= $coup)
                        $typayment[Typayment::ATTR_BALANCE] = $coup;

                    $typayment->save();
                }
        }
        $flow[Flow::ATTR_METADATA] = $metadata;
        $flow->save();

        /*
          |--------------------------------------------------------------------------
          | Transacciones recurrentes
          |--------------------------------------------------------------------------
         */

        if (isset($data["repeat"])) {
            $repeat = explode("|", $data["repeat"]);
            $cron = new FlowCron();
            $cron[FlowCron::ATTR_FK_USER_ID] = Auth::user()->id;
            $cron[FlowCron::ATTR_FK_REF_ID] = $flow->id;
            $cron[FlowCron::ATTR_REPEAT] = $repeat;
            $cron[FlowCron::ATTR_DATE_NEXT] = $cron->getDateNextExec();
            $cron[FlowCron::ATTR_CONFIRM] = (isset($data["confirm"]));
            $cron->save();
        }

        //Actualiza el balance de los medios de pagos
        $flow->typayment->updateBalance($flow[Flow::ATTR_TYPE], $flow[Flow::ATTR_VALUE]);

        return $flow->id;
    }

    /** Elimina una transacción del sistema y reajusta el balance asociado aplicando un reversa
     * 
     * @param type $id
     */
    public function reverse() {
        $flow = $this;
        $meta = $flow[Flow::ATTR_METADATA];

        /**
         * Flujo normal
         */
        switch ($flow[Flow::ATTR_TYPE]) {
            case Flow::TYPE_REVENUE:
                $flow->typayment->updateBalance(Flow::TYPE_SPEND, $flow[Flow::ATTR_VALUE]);
                break;
            case Flow::TYPE_SPEND:
                $flow->typayment->updateBalance(Flow::TYPE_REVENUE, $flow[Flow::ATTR_VALUE]);
                break;
            case Flow::TYPE_EXCHANGE:

                $typayment = Typayment::find($meta["dest"]);
                $currency_dest = $typayment->getObjectType()->getCurrency()->getCode();
                $currency_orig = $flow->typayment->getObjectType()->getCurrency()->getCode();

                //Si esta asociado a una transacción de costo
                if (isset($meta["transaction"])) {

                    $sp = Flow::find($meta["transaction"]);

                    if (!empty($sp)) {

                        if ($flow->typayment->getObjectType()::ID == CreditCard::ID) {
                            $flow[Flow::ATTR_VALUE] += $sp[Flow::ATTR_VALUE];
                        }

                        $sp->reverse();
                    }
                }

                $flow->typayment->updateBalance(Flow::TYPE_REVENUE, $flow[Flow::ATTR_VALUE]);
                $typayment->updateBalance(Flow::TYPE_SPEND, Currency::convert($flow[Flow::ATTR_VALUE], $currency_orig, $currency_dest));
                break;
        }

        /*
          |--------------------------------------------------------------------------
          | PAGO DE CUOTAS DE TARJETAS DE CREDITO
          |--------------------------------------------------------------------------
         */
        if (strpos($flow[Flow::ATTR_CATEGORY], CategoryData::EXT_CREDITS_CARD) !== false) {
            $typayment = Typayment::find($meta["credit_card"]);
            $realvalue = Currency::convert($flow[Flow::ATTR_VALUE], $flow->typayment->getObjectType()->getCurrency()->getCode(), $typayment->getObjectType()->getCurrency()->getCode());
            $typayment->updateBalance(Flow::TYPE_SPEND, $realvalue);
        }

        /*
          |--------------------------------------------------------------------------
          | PAGO DE CUOTAS DE PRESTAMOS O CREDITOS
          |--------------------------------------------------------------------------
         */

        //Si el gasto esta asociado al pago de un credito
        if (strpos($flow[Flow::ATTR_CATEGORY], CategoryData::EXT_CREDITS_TRACK) !== false) {
            $credit = Credit::find($meta["credit_id"]);
            $realvalue = Currency::convert($flow[Flow::ATTR_VALUE], $flow->typayment->getObjectType()->getCurrency()->getCode(), $credit[Credit::ATTR_CURRENCY]);
            $credit[Credit::ATTR_BALANCE] += $realvalue;
            $credit->save();
        }


        return $flow->delete();
    }

    /** Indica si la transacción es dependiente de otra
     * 
     * @return boolean
     */
    public function isDependent() {
        //Solo los gastos pueden ser dependientes
        if ($this->type != self::TYPE_SPEND)
            return false;

        $user = User::find($this[self::ATTR_FK_USER_ID]);

        foreach ($user->transactions->where("id", ">", $this->id) as $transaction) {
            if (isset($transaction[self::ATTR_METADATA]["transaction"]) && $transaction[self::ATTR_METADATA]["transaction"] == $this->id)
                return true;
        }

        $object = $this->typayment->getObjectType();

        //Verifica que si se ha pagado contarjeta de credito y es la ultima transacción registrada se pueda eliminar
        if ($object::ID == CreditCard::ID && $user->transactions()->orderBy("id", "DESC")->first()->id != $this->id) {
            return true;
        }

        return false;
    }

    /** Retorna un array con el listado de transacciones realizadas por usuario durante un mes
     * 
     * @param int $year El año de las transacciones (Por defecto el actual)
     * @param int $month El mes de las transcciones (Por defecto el actual)
     * @param string $type El tipo de transacción (Ingreso, Gasto, Transferencia, Inversión) Por defecto, todas los tipos de transacciones. 
     * @param int $user_id El id del usuario. (Por defecto: Usuario en sesión)
     * @return Objects 
     */
    public static function getForMonth($year = null, $month = null, $type = null, $filters = array(), $user_id = null, $final = true) {

        $year = (is_null($year)) ? date("Y") : $year;
        $month = (is_null($month)) ? date("m") : $month;

        $month = Util::addZeroLeft($month);

        $user_id = $user_id ?? Auth::user()->id;

        $query = User::find($user_id)->transactions()->where(Flow::ATTR_DATE, "like", $year . "-" . $month . "%");

        if (!is_null($type))
            $query = $query->where(Flow::ATTR_TYPE, $type);

        foreach ($filters as $i => $filter) {
            list($index, $comparation, $value) = $filter;
            $query = $query->where($index, $comparation, $value);
        }


        if ($final)
            return $query->orderBy(Flow::ATTR_DATE, "DESC")->orderBy("id", "DESC")->get();
        return $query;
    }

    public static function getCategoryValue($category, $year = null, $month = null, $user_id = null) {

        $filter = [array(Flow::ATTR_CATEGORY, "=", $category)];

        $value = 0;

        foreach (self::getForMonth($year, $month, null, $filter, $user_id) as $transaction) {
            $value += $transaction->getValue();
        }

        return $value;
    }

    public static function getSpend($year, $month, $user_id = null) {
        $total = 0;
        foreach (self::getForMonth($year, $month, self::TYPE_SPEND, [], $user_id) as $transaction) {

            if (isset($transaction->metadata["credit_card"]))
                continue;

            $total += $transaction->getValue();
        }

        return $total;
    }

    public static function getRevenue($year = null, $month = null, $user_id = null) {
        $total = 0;
        foreach (self::getForMonth($year, $month, self::TYPE_REVENUE, [], $user_id) as $transaction) {
            $total += $transaction->getValue();
        }

        return $total;
    }

    /** Retorna el nombre de la categoria
     * 
     * @return Stribg
     */
    public function getCategoryName() {
        return CategoryData::getName($this[self::ATTR_CATEGORY]);
    }

    public function getValue($format = false) {

        $is_unify = config("app.setting.currency.unify") ?? $this->user->getMetaCurrencyUnify();
        $cucu = config("app.setting.currency.code") ?? $this->user->getMetaCurrency();

        $value = $this->value;

        if ($is_unify) {
            $value = Currency::convert($value, $this->typayment->getObjectType()->getCurrency()->getCode(), $cucu);
            if ($format) {
                return Currency::getFormattedValue($value, $cucu);
            }
        }

        //Retorna el valor en formato moneda
        if ($format) {
            return Currency::getFormattedValue($value, $this->typayment->getObjectType()->getCurrency()->getCode());
        }

        return $value;
    }

    /**
     *  Retorna el nombre del tipo de transacción
     */
    public function getTypeName() {
        return trans("attr.flow.type." . $this->{self::ATTR_TYPE});
    }

    public static function getUtilTypeName($type) {
        return trans("attr.flow.type." . $type);
    }

    /** Retorna un array asociativos con información relevante del usuario en sesión, usado como dashboard.
     * 
     * @return type
     */
    public static function getDataDashboard($user_id = null) {

        if ($user_id == null && !Auth::check())
            return array();

        $user_id = $user_id ?? Auth::user()->id;

        $user = User::find($user_id);

        $data = [];

        $month_current = 0;
        $month_previous = 0;
        $budget = $user->getBudgetActive();

        //Currency Current
        $cucu = $user->getMetaCurrency();


        //Almacenas los gastos por categorias
        $data["spend"]["categories"]["month"]["current"] = array();
        $data["spend"]["categories"]["month"]["previous"] = array();

        foreach (CategoryData::getSpendList() as $code => $cats) {
            $data["spend"]["categories"]["month"]["current"][$code] = 0;
            $data["spend"]["categories"]["month"]["previous"][$code] = 0;
        }

        $data["spend"]["categories"]["month"]["current"][CategoryData::EXT_CREDITS_TRACK] = 0;
        $data["spend"]["categories"]["month"]["current"][CategoryData::EXT_CREDITS_CARD] = 0;
        $data["spend"]["categories"]["month"]["previous"][CategoryData::EXT_CREDITS_TRACK] = 0;
        $data["spend"]["categories"]["month"]["previous"][CategoryData::EXT_CREDITS_CARD] = 0;

        //Gastos del mes actual
        foreach (Flow::getForMonth(null, null, Flow::TYPE_SPEND, [], $user_id) as $transaction) {

            if (isset($transaction->metadata["credit_card"]))
                continue;

            $value = Currency::convert($transaction->value, $transaction->typayment->getObjectType()->getCurrency()->getCode(), $cucu);

            $month_current += $value;
            $cat = CategoryData::getCatMain($transaction[Flow::ATTR_CATEGORY]);
            $data["spend"]["categories"]["month"]["current"][$cat] += $value;
        }

        $date = new DateUtil();
        $date->subtractMonth(1);

        //Gastos del mes anterior
        foreach (Flow::getForMonth($date->year, $date->month, Flow::TYPE_SPEND, [], $user_id) as $transaction) {

            if (isset($transaction->metadata["credit_card"]))
                continue;

            $value = Currency::convert($transaction->value, $transaction->typayment->getObjectType()->getCurrency()->getCode(), $cucu);

            $month_previous += $value;

            $cat = CategoryData::getCatMain($transaction[Flow::ATTR_CATEGORY]);
            $data["spend"]["categories"]["month"]["previous"][$cat] += $value;
        }

        $data["spend"]["month"]["current"] = $month_current;
        $data["spend"]["month"]["previous"] = $month_previous;


        $month_current = 0;
        $month_previous = 0;

        //Ingresos mes actual
        foreach (Flow::getForMonth(null, null, Flow::TYPE_REVENUE, [], $user_id) as $transaction) {
            $month_current += Currency::convert($transaction->value, $transaction->typayment->getObjectType()->getCurrency()->getCode(), $cucu);
        }

        $date = new DateUtil();
        $date->subtractMonth(1);

        //Ingresos mes anterior
        foreach (Flow::getForMonth($date->year, $date->month, Flow::TYPE_REVENUE, [], $user_id) as $transaction) {
            $month_previous += Currency::convert($transaction->value, $transaction->typayment->getObjectType()->getCurrency()->getCode(), $cucu);
        }


        $data["revenue"]["month"]["current"] = $month_current;
        $data["revenue"]["month"]["previous"] = $month_previous;

        $data["currency"] = $cucu;

        $date->addMonths(1);

        $data["saving"]["month"]["current"] = $data["revenue"]["month"]["current"] - $data["spend"]["month"]["current"];
        $data["saving"]["month"]["previous"] = $data["revenue"]["month"]["previous"] - $data["spend"]["month"]["previous"];


        /**
         * PARAMETROS DE LAS ESTADISTICAS
         */
        $data["graph"]["labels"] = [];


        $date->subtractMonth(1);


        //Obtiene las etiquetas a mostrar de los meses
        for ($i = $date->month; $i > 0; $i--) {

            $data["graph"]["labels"][] = DateUtil::getNameMonth($date->month);

            //Gastos del mes
            $value = 0;
            foreach (Flow::getForMonth($date->year, $date->month, Flow::TYPE_SPEND, [], $user_id) as $transaction) {
                if (isset($transaction->metadata["credit_card"]))
                    continue;
                $value += Currency::convert($transaction->value, $transaction->typayment->getObjectType()->getCurrency()->getCode(), $cucu);
            }


            $data["graph"]["data"]["spend"][] = $spend = $value;


            //Ingresos del mes
            $value = 0;
            foreach (Flow::getForMonth($date->year, $date->month, Flow::TYPE_REVENUE, [], $user_id) as $transaction) {
                $value += Currency::convert($transaction->value, $transaction->typayment->getObjectType()->getCurrency()->getCode(), $cucu);
            }
            $data["graph"]["data"]["revenue"][] = $revenue = $value;

            $data["graph"]["data"]["saving"][] = $saving = $revenue - $spend;

            if (!empty($budget)) {
                $data["graph"]["data"]["waited"]["revenue"][] = $budget->getTotalRevenue($date->year, $date->month);
                $data["graph"]["data"]["waited"]["spend"][] = $budget->getTotalSpend($date->year, $date->month);
                $data["graph"]["data"]["waited"]["saving"][] = $budget->getSavingExpected($date->year, $date->month);
            }


            $date->subtractMonth(1);

            if ($i == 1 && count($data["graph"]["labels"] < 12)) {
                $i = 13 - count($data["graph"]["labels"]);
            }


            if ($spend == 0) {
                break;
            }
        }


        /*         * ******************************+
         * CRECIMIENTO DE DINERO A TRAVES DEL TIEMPO
         */
        $date = new DateUtil();
        $date->subtractMonth(count($data["graph"]["data"]["saving"]));
        //Obtiene el balance desde el inicio del mes indicado
        $mount_balance_base = ceil($user->getAbsBalance($date->year, $date->month));
        $mount_balance_total = $mount_balance_base;
        // y luego va sumando cada nivel de ahorro/perdida mes tras mes
        foreach (array_reverse($data["graph"]["data"]["saving"]) as $value) {

            $mount_balance_total += ceil($value);

            if ($mount_balance_base == $mount_balance_total) {
                $data["graph"]["data"]["growth"][] = 0;
                continue;
            }

            $data["graph"]["data"]["growth"][] = $mount_balance_total;
        }

        //Adecua los datos para ser mostrados en la vista
        $data["graph"]["labels"] = implode(",", array_reverse($data["graph"]["labels"]));
        $data["graph"]["data"]["revenue"] = implode(",", array_reverse($data["graph"]["data"]["revenue"]));
        $data["graph"]["data"]["spend"] = implode(",", array_reverse($data["graph"]["data"]["spend"]));
        $data["graph"]["data"]["saving"] = implode(",", array_reverse($data["graph"]["data"]["saving"]));
        $data["graph"]["data"]["growth"] = implode(",", $data["graph"]["data"]["growth"]);

        if (!empty($budget)) {
            $data["graph"]["data"]["waited"]["spend"] = implode(",", array_reverse($data["graph"]["data"]["waited"]["spend"]));
            $data["graph"]["data"]["waited"]["revenue"] = implode(",", array_reverse($data["graph"]["data"]["waited"]["revenue"]));
            $data["graph"]["data"]["waited"]["saving"] = implode(",", array_reverse($data["graph"]["data"]["waited"]["saving"]));
        }


        /**
         * TARJETAS DE CREDITO
         */
        foreach ($user->typayments->where(Typayment::ATTR_TYPE, \App\Base\System\TP\CreditCard::ID) as $creditcard)
            $data["typayments"]["creditcard"][] = $creditcard;

        $widgets = Widget::getList();
        $data["widgets"] = array_chunk($widgets, ceil(count($widgets) / 2));


        return $data;
    }

    /** Determina si un usuario tiene transacciones autoleidas que no han sido procesadas, si es asi, creara una petición para mostrar un modal con la información
     * 
     * @param type $user_id
     * @return boolean
     */
    public static function checkAutoread($user_id = null) {
        $user_id = $user_id ?? ((Auth::check()) ? Auth::user()->id : null);
        $user = User::find($user_id);

        $autoreads = $user->autoread_clients;

        $autids = [];
        $emails = [];

        foreach ($autoreads as $autoread) {
            $autids[] = $autoread->id;
            $emails[] = $autoread->getClientObject()->getProfileEmail();
        }

        $logs = LogSuccess::whereIn(LogSuccess::ATTR_FK_AUTOREAD_ID, $autids)->where(LogSuccess::ATTR_VALIDATE, false)->orderBy(LogSuccess::ATTR_FK_AUTOREAD_ID, "DESC")->get();

        if (count($logs) > 0) {
            Message::modal(view(LogSuccess::MODAL_VIEW_NOTIFICATION_PATH, ["logs" => $logs, "emails" => $emails])->render(), trans("ui.label.flow.autoread.notify.modal.title"), false);
            return true;
        }

        return false;
    }

    /** Determina la categoria de una transacción dada una descripción. 
     * 
     * @param type $description La descripción para determina la categoria 
     * @param type $type [null] El tipo de transacción
     * @param type $user_id [null] El id del usuario, por defecto el usuario en sesión
     * @return String
     */
    public static function detectCategory($description, $type = null, $user_id = null) {
        $user_id = $user_id ?? ((Auth::check()) ? Auth::user()->id : null);
        $user = User::find($user_id);

        $query = $user->transactions()->where(self::ATTR_DESCRIPTION, $description);

        if (!empty($type)) {
            $query = $query->where(self::ATTR_TYPE, $type);
        }

        $query = $query->orderBy("id", "DESC")->first();

        return (!empty($query)) ? $query[self::ATTR_CATEGORY] : null;
    }

    /*
      |--------------------------------------------------------------------------
      | RELACIONES
      |--------------------------------------------------------------------------
     */

    public function typayment() {
        return $this->belongsTo(Typayment::class)->withTrashed();
    }

    public function cron() {
        return $this->hasOne(FlowCron::class, FlowCron::ATTR_FK_REF_ID);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    /*
      |--------------------------------------------------------------------------
      | RELACIONES OPCIONALES
      |--------------------------------------------------------------------------
     */

    /** Retorna un modelo del Credito asociado a la transacción si existe
     * 
     * @return type
     */
    public function credit() {
        if (isset($this->metadata["credit_id"]) && $this->category == CategoryData::EXT_CREDITS_TRACK) {
            return Credit::find($this->metadata["credit_id"]);
        }
        return null;
    }

    /** Retorna un modelo del medio de pago de tipo "Tarjeta de credito" asociado a la transacción si existe
     * 
     * @return type
     */
    public function creditCard() {
        if (isset($this->metadata["credit_card"]) && $this->category == CategoryData::EXT_CREDITS_CARD) {
            return Typayment::withTrashed()->find($this->metadata["credit_card"]);
        }
        return null;
    }

    /*
      |--------------------------------------------------------------------------
      | Mutadores
      |--------------------------------------------------------------------------
     */

    public function getMetadataAttribute($data) {
        return json_decode($data, true);
    }

    public function setMetadataAttribute($data) {
        $this->attributes[self::ATTR_METADATA] = json_encode($data);
    }

    /*
      |--------------------------------------------------------------------------
      | ESPECIALES
      |--------------------------------------------------------------------------
     */

    public function __toString() {
        return "[<" . self::ATTR_CATEGORY . ":" . $this->{self::ATTR_CATEGORY} . ">" .
                "<" . self::ATTR_DATE . ":" . $this->{self::ATTR_DATE} . ">" .
                "<" . self::ATTR_TYPE . ":" . $this->{self::ATTR_TYPE} . ">" .
                "<" . self::ATTR_DESCRIPTION . ":" . $this->{self::ATTR_DESCRIPTION} . ">" .
                "<" . self::ATTR_VALUE . ":" . $this->{self::ATTR_VALUE} . ">" .
                "<" . self::ATTR_FK_USER_ID . ":" . $this->{self::ATTR_FK_USER_ID} . "]" .
                "<typayment:" . $this->typayment->getObjectType()->getName() . "]"
        ;
    }

}
