<?php

namespace App\Models;

use App\Custom\Model;
use ReflectionClass;

class UserMeta extends Model {

    const DB_TABLE = "users_meta";
    const ATTR_KEY = "key";
    const ATTR_VALUE = "value";
    const ATTR_FK_USER_ID = "user_id";
    //Keys
    const KEY_CURRENCY = "currency";
    const KEY_CURRENCY_UNIFY = "currency-unify";
    const KEY_LANG = "lang";
    const KEY_MODE_SECURE = "mode-secure";
    const KEY_HASH_EMAIL_CONFIRM = "hash-email-confirm";
    const KEY_NOTF_FLOWCRON_AUTO_EMAIL = "notf-flowcron-auto-email";
    const KEY_NOTF_FLOWCRON_CONFIRM_EMAIL = "notf-flowcron-confirm-email";
    const KEY_NOTF_FLOW_AUTOREAD_SUCCESS_AUTO_EMAIL = "notf-flow-autoread-success-auto-email";
    const KEY_NOTF_FLOW_AUTOREAD_SUCCESS_CONFIRM_EMAIL = "notf-flow-autoread-success-confirm-email";
    const KEY_NOTF_FLOW_AUTOREAD_ERROR_EMAIL = "notf-flow-autoread-error-email";

    protected $table = self::DB_TABLE;
    public $timestamps = false;

    public static function val($val) {
        if ($val == "true")
            return true;

        if ($val == "false")
            return false;

        return $val;
    }

    /** Obtiene un array con el listado de claves
     * 
     * @return array
     */
    public static function getList() {
        $class = new ReflectionClass(__CLASS__);

        $list = array();

        foreach ($class->getConstants() as $index => $value) {
            if (strpos($index, "KEY_") === 0) {
                $list[] = $value;
            }
        }

        return $list;
    }

}
