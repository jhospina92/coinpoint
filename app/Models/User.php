<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\UserMeta;
use Illuminate\Support\Facades\Auth;
use App\Models\Typayment;
use App\Models\Flow;
use App\Models\Credit;
use App\Models\Budget;
use App\Base\Data\Lang;
use App\Base\System\TP\CreditCard;
use App\Base\System\Library\Comp\DateUtil;
use App\Base\Data\Currency;
use DateTime;
use App\Models\Goal;
use App\Base\System\Library\Networks\Email;
use App\Base\Data\RouteData;
use App\Models\Auto\AutoRead;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable {
    
     use SoftDeletes;

    const DB_TABLE = "users";
    const ATTR_NAME = "name";
    const ATTR_LASTNAME = "lastname";
    const ATTR_EMAIL = "email";
    const ATTR_PASSWORD = "password";
    const ATTR_TYPE = "type";
    const ATTR_LOGIN_LAST = "login_last";
    const ATTR_ACTIVE = "active"; //Indica si la cuenta ha sido activada
    const ATTR_API_TOKEN = "api_token"; //Token para acceder a los datos del usuario a traves de la api
    //Type
    const TYPE_FREE = "FR";
    const TYPE_PREMIUM = "PR";
    //Auth
    const AUTH_REMEMBER = "remember";

    use Notifiable;

    //Parametros a retornar en la API
    const API_PARAMS = [
        #Atributos permitos a mostrar del usuario
        "attrs_show_allowed" => [self::ATTR_NAME, self::ATTR_LASTNAME, self::ATTR_EMAIL, self::ATTR_TYPE, self::ATTR_LOGIN_LAST, self::ATTR_ACTIVE,self::UPDATED_AT,self::CREATED_AT],
        "_meta" => [UserMeta::class, UserMeta::ATTR_FK_USER_ID],
        #Cada valor relacional corresponde al nombre de la funcion que retorna la relacion
        "_relations" => ["typayments", "transactions", "credits", "budgets", "autoread_clients", "goals"]];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /** Obtiene una meta asociada al usuario dado por la key
     * 
     * @param type $key
     * @return string
     */
    function getMeta($key) {
        $meta = UserMeta::where(UserMeta::ATTR_FK_USER_ID, $this->id)->where(UserMeta::ATTR_KEY, $key)->first();

        //Si un parametro de notificación
        if (strpos($key, "notf-") !== false) {
            return (!empty($meta)) ? $meta[UserMeta::ATTR_VALUE] : true;
        }

        return (!empty($meta)) ? $meta[UserMeta::ATTR_VALUE] : null;
    }

    function setMeta($key, $value) {

        $meta = UserMeta::where(UserMeta::ATTR_FK_USER_ID, $this->id)->where(UserMeta::ATTR_KEY, $key)->first();

        if (empty($meta)) {
            $meta = new UserMeta();
            $meta[UserMeta::ATTR_FK_USER_ID] = $this->id;
        }

        $meta[UserMeta::ATTR_KEY] = $key;
        $meta[UserMeta::ATTR_VALUE] = UserMeta::val($value);
        return $meta->save();
    }

    function removeMeta($key) {
        $meta = UserMeta::where(UserMeta::ATTR_FK_USER_ID, $this->id)->where(UserMeta::ATTR_KEY, $key)->first();

        if (empty($meta)) {
            return false;
        }

        $meta->delete();

        return true;
    }

    /** Retorna un array de objetos con los creditos activos del usuario
     * 
     * @return type
     */
    function getCurrentCredits() {
        return $this->credits()->where(Credit::ATTR_STATUS, Credit::STATUS_ACTIVE)->get();
    }

    /** Retorna el idioma del usuario
     * 
     * @return type
     */
    function getLang() {
        return (is_null($lang = $this->getMeta(UserMeta::KEY_LANG))) ? Lang::SPANISH : $lang;
    }

    /** Indica si el usuario esta en modo seguro
     * 
     * @return bool
     */
    function isModeSecure() {
        return (is_null($bool = $this->getMeta(UserMeta::KEY_MODE_SECURE))) ? false : $bool;
    }

    /*
      |--------------------------------------------------------------------------
      | RELACIONES - EXTRAS
      |--------------------------------------------------------------------------
     */

    /** Retorna el presupuesto activo del usuario si existe
     * 
     * @return type
     */
    function getBudgetActive() {
        return $this->budgets->where(Budget::ATTR_ACTIVE, true)->first();
    }

    /** Retorna un array con los codigos de las monedas usadas por el usuario
     * 
     * @return type
     */
    function getCurrencies() {
        $currencies = array();
        foreach ($this->typayments as $typayment) {
            $currency = $typayment->getObjectType()->getCurrency()->getCode();
            if (!in_array($currency, $currencies)) {
                $currencies[] = $currency;
            }
        }

        return $currencies;
    }

    public static function register(array $data, bool $sendmail = false) {
        $attrs = [self::ATTR_NAME, self::ATTR_LASTNAME, self::ATTR_PASSWORD, self::ATTR_EMAIL];

        //Verifica que los datos existen
        foreach ($attrs as $attr) {
            if (!isset($data[$attr]))
                return null;
        }


        $user = new User();
        $user[self::ATTR_NAME] = title_case($data[self::ATTR_NAME]);
        $user[self::ATTR_LASTNAME] = title_case($data[self::ATTR_LASTNAME]);
        $user[self::ATTR_EMAIL] = strtolower($data[self::ATTR_EMAIL]);
        $user[self::ATTR_PASSWORD] = bcrypt($data[self::ATTR_PASSWORD]);
        $user[self::ATTR_TYPE] = self::TYPE_FREE;
        $user[self::ATTR_ACTIVE] = $data[self::ATTR_ACTIVE] ?? false;

        $hash = str_random(40);


        //Envia un correo de confirmación
        if ($sendmail) {
            $email = new Email(trans("mail.signup.activation.account.subject"), $user[self::ATTR_EMAIL]);
            $email->setDescription(sprintf(trans("mail.signup.activation.account.description"), $user[self::ATTR_NAME], route(RouteData::AUTH_CONFIRM, $hash)));
            $email->send();
        }

        $user->save();

        //Variables de configuración del usuario
        $user->setMeta(UserMeta::KEY_HASH_EMAIL_CONFIRM, $hash);
        $user->setMeta(UserMeta::KEY_CURRENCY, Currency::COP);
        $user->setMeta(UserMeta::KEY_LANG, Lang::SPANISH);
        $user->setMeta(UserMeta::KEY_CURRENCY_UNIFY, true);
        $user->setMeta(UserMeta::KEY_MODE_SECURE, false);

        return $user->id;
    }

    
    /*
      |--------------------------------------------------------------------------
      | RELACIONES
      |--------------------------------------------------------------------------
     */

    //Medios de pagos
    function typayments() {
        return $this->hasMany(Typayment::class);
    }

    //Transacciones
    function transactions() {
        return $this->hasMany(Flow::class);
    }

    //Creditos
    function credits() {
        return $this->hasMany(Credit::class);
    }

    //Presupuestos
    function budgets() {
        return $this->hasMany(Budget::class);
    }

    function autoread_clients() {
        return $this->hasMany(AutoRead::class);
    }

    function goals() {
        return $this->hasMany(Goal::class);
    }

    /**  Retorna el balance absoluto del usuario indicado hasta el mes y año indicado
     * 
     * @param type $year
     * @param type $month
     */
    function getAbsBalance($year = null, $month = null) {

        $year = (is_null($year)) ? date("Y") : $year;
        $month = (is_null($month)) ? date("m") : $month;

        //Currency Current
        $cucu = config("app.setting.currency.code");
        $total = $this->getAmountStart();

        $date = DateTime::createFromFormat('Y-m-d H:i:s', $year . '-' . $month . '-1 23:59:59');
        $date->modify('last day of');


        foreach ($this->transactions()->where(Flow::ATTR_DATE, "<=", $date->format(DateUtil::FORMAT_STANDARD))->get() as $transaction) {

            if (isset($transaction->metadata["credit_card"]))
                continue;

            if ($transaction->type == Flow::TYPE_REVENUE)
                $total += Currency::convert($transaction->value, $transaction->typayment->currency, $cucu);
            if ($transaction->type == Flow::TYPE_SPEND)
                $total -= Currency::convert($transaction->value, $transaction->typayment->currency, $cucu);
        }

        return $total;
    }

    /** Retorna el valor dinero inicial registrado del usuario
     * 
     * @return type
     */
    function getAmountStart() {
        $total = 0;
        //Currency Current
        $cucu = config("app.setting.currency.code");

        foreach ($this->typayments as $typayment) {
            if ($typayment->{Typayment::ATTR_TYPE} == CreditCard::ID)
                continue;
            $data = json_decode($typayment->{Typayment::ATTR_FEATURES}, true);
            $created = new DateUtil($typayment->created_at);

            $total += Currency::convert($data["balance"], $typayment->currency, $cucu);
        }

        return $total;
    }

    /** Retorna el valor total de dinero que tiene el usuario actualmente
     * 
     * @return type
     */
    function getAmountCurrent() {
        $amount_current = 0;
        $cucu = config("app.setting.currency.code");
        foreach ($this->typayments as $typayment) {
            if ($typayment->type == CreditCard::ID)
                continue;

            $amount_current += Currency::convert($typayment->balance, $typayment->currency, $cucu);
        }

        return $amount_current;
    }

    /**
     *  Retorna el valor actual en deuda por concepto asociados de medios de pagos (Por ejemplo: Tarjeta de credito)
     */
    function getValuePassivesForTypayments() {
        $amount_current = 0;
        $cucu = config("app.setting.currency.code");
        foreach ($this->typayments as $typayment) {
            if ($typayment->type != CreditCard::ID)
                continue;

            $amount_current += Currency::convert($typayment->getBalance(), $typayment->currency, $cucu);
        }

        return $amount_current;
    }

    /*
      |--------------------------------------------------------------------------
      |  Metodos magicos
      |--------------------------------------------------------------------------
     */

    public function __call($method, $parameters) {
        if (strpos($method, "getMeta") !== false && strpos($method, "getMeta") == 0) {
            return $this->magicGetMeta($method, $parameters);
        }
        return parent::__call($method, $parameters);
    }

    /** Retorna el valor de un meta del usuario llamado desde una funcion magica en formato getMeta{meta-key}()
     * 
     * @param type $method
     * @param type $params
     */
    private function magicGetMeta($method, $params) {
        $key = str_replace("getMeta", "", $method);
        if (preg_match('/[A-Z]/', substr($key, 1), $matches, PREG_OFFSET_CAPTURE)) {
            $key = substr($key, 0, $matches[0][1] + 1) . "-" . substr($key, $matches[0][1] + 1);
        }

        $key = strtolower($key);

        return $this->getMeta($key);
    }

}
