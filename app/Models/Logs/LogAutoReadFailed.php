<?php

namespace App\Models\Logs;

use App\Custom\Model;

class LogAutoReadFailed extends Model {

    const DB_TABLE = "log_autoread_failed";
    const ATTR_FK_AUTOREAD_ID = "autoread_id";
    const ATTR_FK_TYPAYMENT_ID = "typayment_id";
    const ATTR_MESSAGE_ID = "message_id";
    const ATTR_DESCRIPTION = "description";
    const ATTR_TYPE = "type";

    protected $table = self::DB_TABLE;

    const ERROR_DATE_NOT_FOUND = "date-not-found";
    const ERROR_HOUR_NOT_FOUND = "hour-not-found";
    const ERROR_VALUE_NOT_FOUND = "value-not-found";
    const ERROR_VALUE_NOT_IDENTIFIED = "value-not-identified";
    const ERROR_DESCRIPTION_FORMAT_INVALID = "description-format-invalid";
    const ERROR_DESCRIPTION_NOT_FOUND = "description-not-found";
    const ERROR_TYPAYMENT_CASH_NOT_EXITS="typayment-cash-not-exist";

    /*
      |--------------------------------------------------------------------------
      | MUTADORES DE ATRIBUTOS
      |--------------------------------------------------------------------------
     */

    public function getDescriptionAttribute($value) {
        return trans("attr.log.autoread.error." . $value);
    }

}
