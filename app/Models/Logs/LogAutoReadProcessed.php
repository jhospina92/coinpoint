<?php

namespace App\Models\Logs;

use App\Custom\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Auto\AutoRead;
use App\Models\Flow;
use App\Models\Typayment;
use App\Base\System\Library\Networks\Email;
use App\Models\User;
use App\Models\UserMeta;
use App\Base\Data\Currency;
use App\Base\Data\CategoryData;
use App\Models\Logs\LogAutoReadFailed as LogError;
use \App\Base\System\TP\Cash;

class LogAutoReadProcessed extends Model {

    use SoftDeletes;

    const DB_TABLE = "log_autoread_processed";
    const ATTR_FK_AUTOREAD_ID = "autoread_id";
    const ATTR_MESSAGE_ID = "message_id";
    const ATTR_DATA = "data";
    const ATTR_VALIDATE = "validate";
    const MODAL_VIEW_NOTIFICATION_PATH = "ui/modal-contents/flow-autoread-notification";

    protected $table = self::DB_TABLE;
    protected $dates = ['deleted_at'];

    /** Agrega un registro y en el camino realiza una notificación si es necesario
     * 
     * @param int $autoread_id
     * @param string $message_id
     * @param array $data
     * @return type
     */
    public static function add(int $autoread_id, string $message_id, array $data) {

        $log = new LogAutoReadProcessed;
        $log[self::ATTR_FK_AUTOREAD_ID] = $autoread_id;
        $log[self::ATTR_MESSAGE_ID] = $message_id;
        $log[self::ATTR_DATA] = $data;
        $log[self::ATTR_VALIDATE] = false;


        $autoread = AutoRead::find($autoread_id);
        $user_id = $autoread->user_id;

        $user = User::find($user_id);
        $client = $autoread->getClientObject();
        $typayment_id = $data["typayment"];
        $typayment = Typayment::find($data["typayment"])->getObjectType();
        $currency = $typayment->getCurrency()->getCode();
        $category = Flow::detectCategory($data["description"], $data["type"], $user_id);
        $value = Currency::getFormattedValue($data["value"], $currency);
        $type = $data["type"]; //Tipo de transacción

        $datamail = [];
        $datamail[] = $client->getProfileEmail(); //Correo electrónico asociado
        $datamail[] = $value; // Valor de la transacción
        $datamail[] = Flow::getUtilTypeName($type); //Tipo de transacción
        if (!is_null($category))
            $datamail[] = CategoryData::getName($category);
        $datamail[] = $typayment->getFullName(); //Medio de pago
        $datamail[] = $data["description"]; //Descripción de la transacción
        $datamail[] = $value;


        /**
         * ENVIAR UN CORREO ELECTRÓNICO DE NOTIFICACIÓN
         */
        $message = sprintf(trans("mail.autoread.success." . ((is_null($category)) ? "confirm." : "") . "description"), ...$datamail);
        $email = new Email(trans("mail.autoread.success." . ((is_null($category)) ? "confirm." : "") . "subject"), $user[User::ATTR_EMAIL]);
        $email->setDescription($message);


        if (!is_null($data["category"] = $category) || $type == Flow::TYPE_EXCHANGE) {

            $data["cc-fees"] = 1;
            $data["date_submit"] = $data["date"];
            $data[Flow::ATTR_FK_USER_ID] = $user_id;
            //Transferencia
            $data["origin"] = $typayment_id;

            $typayment_cash = Typayment::where(Typayment::ATTR_CURRENCY, $currency)->where(Typayment::ATTR_TYPE, Cash::ID)->first();

            //Registra un error, si no existe el medio de pago en efectivo de la misma moneda del medio de pago.
            if (empty($typayment_cash)) {
                $autoread->addLogError($message_id, LogError::ERROR_TYPAYMENT_CASH_NOT_EXITS, $type, $typayment_id);
                return false;
            }

            $data["dest"] = $typayment_cash->id;

            //Trata de obtener una descripcion para la transacción de tipo "Transferencia"
            if (empty($data["description"]) && $type == Flow::TYPE_EXCHANGE) {
                $query = $user->transactions()
                        ->where(Flow::ATTR_FK_TYPAYMENT_ID, $typayment_id)
                        ->where(Flow::ATTR_TYPE, Flow::TYPE_EXCHANGE)
                        ->where(Flow::ATTR_DESCRIPTION, "!=", "")->orderBy("id", "DESC")
                        ->first();

                if (!empty($query))
                    $data["description"] = $query[Flow::ATTR_DESCRIPTION];
            }

            if ($type == Flow::TYPE_EXCHANGE) {
                $email->setSubject(trans("mail.autoread.success.subject"));
                $email->setHeader(trans("mail.autoread.success.subject"));
                $email->setDescription(sprintf(trans("mail.autoread.success.exchange.description"), $typayment->getFullName(), $value, Flow::getUtilTypeName($type), $typayment->getFullName(), Typayment::find($data["dest"])->getObjectType()->getFullName(), $data["description"], $value, $user[User::ATTR_EMAIL]));
            }


            Flow::createFromType($data);
            $log[self::ATTR_VALIDATE] = true;

            //Si el usuario tiene activadas las notificaciones por correo
            if ($user->getMeta(UserMeta::KEY_NOTF_FLOW_AUTOREAD_SUCCESS_AUTO_EMAIL)) {
                $email->queue();
            }
        } elseif ($user->getMeta(UserMeta::KEY_NOTF_FLOW_AUTOREAD_SUCCESS_CONFIRM_EMAIL)) {
            $email->queue();
        }

        return $log->save();
    }

    /*
      |--------------------------------------------------------------------------
      | Mutadores
      |--------------------------------------------------------------------------
     */

    public function getDataAttribute($data) {
        return json_decode($data, true);
    }

    public function setDataAttribute($data) {
        $this->attributes[self::ATTR_DATA] = json_encode($data);
    }

    /*
      |--------------------------------------------------------------------------
      | Relaciones
      |--------------------------------------------------------------------------
     */

    function autoread() {
        return $this->belongsTo(AutoRead::class);
    }

}
