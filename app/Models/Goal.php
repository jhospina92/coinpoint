<?php

namespace App\Models;

use App\Custom\Model;
use Illuminate\Support\Facades\Auth;
use App\Base\System\Library\Comp\DateUtil;
use App\Base\Data\Currency;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;

class Goal extends Model {

    use SoftDeletes;

    const DB_TABLE = "goals";
    const ATTR_NAME = "name";
    const ATTR_VALUE = "value";
    const ATTR_CURRENCY = "currency";
    const ATTR_DATE = "date"; //Fecha de cumplimiento
    const ATTR_DATE_COMPLETE = "date_complete"; //Fecha cuando se alcanca la meta
    const ATTR_RATE_SAVING = "rate_saving"; //Porcentaje de ahorro que van al cumplimiento de la meta.
    //FK
    const ATTR_FK_USER_ID = "user_id";
    //STATUS
    const STATUS_IN_PROGRESS = "in_progress";
    const STATUS_COMPLETED = "completed";
    const STATUS_TERMINATED = "terminated";

    protected $table = self::DB_TABLE;
    protected $dates = ['deleted_at'];

    public function getSavingMin($year = null, $month = null) {


        $user = User::find($this->user_id);

        $year = (is_null($year)) ? date("Y") : $year;
        $month = (is_null($month)) ? date("m") : $month;

        $date_current = new DateUtil();
        $date_current->subtractMonth(1);

        $date = new DateUtil($year . "-" . $month . "-01 00:00:00");

        $amount_month = $user->getAbsBalance($date->year, $date->month) * floatval($this->rate_saving);

        if (($year >= $date_current->year && $month > $date_current->month) || $year > $date_current->year) {
            $date = new DateUtil();
            $date->subtractMonth(1);
            $amount_month = $user->getAbsBalance($date->year, $date->month) * floatval($this->rate_saving);
        }

        $date = ($year == $date_current->year && $month == $date_current->month) ? $date_current->subtractMonth(1) : $date;

        $months = $this->getMonthsNumber($date);


        /**
         * Adecua el valor fraccionado de la deuda en base al dinero
         */
        if ($this->rate_saving < 1) {
            $amount_month -= $user->getValuePassivesForTypayments() * (1 - floatval($this->rate_saving));
        }

        return ceil(($this->getValue() - $amount_month) / $months);
    }

    public function getMonthsNumber($date_start) {
        $months = ceil(DateUtil::difSec($date_start, $this->date) / 60 / 60 / 24 / 30);
        return ($months > 0) ? $months : 0;
    }

    public function getValue($format = false) {
        $value = $this->value;

        $cucu = config("app.setting.currency.code") ?? $this->user->getMetaCurrency();

        $value = Currency::convert($value, $this->currency, $cucu);
        if ($format) {
            return Currency::getFormattedValue($value, $cucu);
        }

        return $value;
    }

    /** Retorna el porcentaje de progreso del cumplimiento de la meta
     * 
     * @return type
     */
    public function getProgress() {

        if (!is_null($this->date_complete))
            return 100;

        $amount_current = (Auth::user()->getAbsBalance() * floatval($this->rate_saving));
        $progress = ($amount_current > $this->getValue()) ? 100 : ($amount_current / $this->getValue()) * 100;

        return number_format($progress, 2);
    }

    /** Obtiene el estado en el que se encuentra la meta
     * 
     * @return type
     */
    public function getStatus() {
        $progress = $this->getProgress();

        //Terminado
        if ($this->trashed()) {
            return self::STATUS_TERMINATED;
        }

        if ($progress == 100 || !is_null($this->date_complete)) {
            if (is_null($this->date_complete)) {
                $this->date_complete = date(DateUtil::FORMAT_STANDARD);
                $this->save();
            }
            return self::STATUS_COMPLETED;
        }

        return self::STATUS_IN_PROGRESS;
    }

    /** Retorna el nombre del estado dado por su valor ID
     * 
     * @param type $status
     * @return type
     */
    public static function getStatusText($status) {

        switch ($status) {
            case self::STATUS_TERMINATED:
                return trans("ui.label.status.terminated");
            case self::STATUS_COMPLETED:
                return trans("ui.label.status.completed");
            case self::STATUS_IN_PROGRESS:
                return trans("ui.label.status.in.progress");
        }

        return null;
    }

    /** Retorna un valor de 0-100 que indica el porcentaje de disponibles para el ahorro que pueden ser asignadas a las metas, dado por el Id del usuario.
     * 
     * @param type $user_id
     * @return int
     */
    public static function getRangeSavingAvailable($user_id) {

        //Verifica que el usuario exista
        if (User::where("id", $user_id)->count() == 0)
            return null;

        $goals = Goal::where(Goal::ATTR_FK_USER_ID, $user_id)->get();

        //Esta variable indica el porcentaje ahorro disponible para asignar a las metas
        $val = 100;

        if (count($goals) == 0)
            return 100;

        foreach ($goals as $goal) {
            $val -= $goal->rate_saving * 100;
        }

        return $val;
    }

    
      public function user() {
        return $this->belongsTo(User::class);
    }
}
