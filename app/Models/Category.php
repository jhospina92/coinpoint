<?php

namespace App\Models;

use App\Custom\Model;

class Category extends Model {

    const DB_TABLE="categories";
    const ATTR_NAME = "name";

    protected $table = self::DB_TABLE;
    protected $timestamp=false;

}
