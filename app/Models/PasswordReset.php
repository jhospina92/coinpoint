<?php

namespace App\Models;

use App\Custom\Model;
use App\Base\System\Library\Networks\Email;
use App\Models\User;
use App\Base\System\Library\Comp\DateUtil;
use App\Base\Data\RouteData;

class PasswordReset extends Model {

    const DB_TABLE = "password_resets";
    const ATTR_TOKEN = "token";
    const ATTR_EXPIRATION = "expiration";
    const ATTR_VALIDATE = "validate";
    //Foreings Key
    const ATTR_FK_USER_ID = "user_id";
    //Properties
    const UPDATED_AT = null;

    protected $table = self::DB_TABLE;

    //Genera una solicitud de reinicio de contraseña dado por el Id de usuario
    public static function generate($user_id) {

        do {
            $token = str_random(100);
        } while (PasswordReset::where(PasswordReset::ATTR_TOKEN, $token)->exists());

        $user = User::find($user_id);

        if (empty($user))
            return false;

        $pass = new PasswordReset();
        $pass[self::ATTR_FK_USER_ID] = $user_id;
        $pass[self::ATTR_TOKEN] = $token;
        $pass[self::ATTR_EXPIRATION] = (new DateUtil())->addDays(1);
        $pass[self::ATTR_VALIDATE] = false;
        $pass->save();

        $email = new Email(trans("mail.forgotten.subject"), $user[User::ATTR_EMAIL]);
        $email->setDescription(sprintf(trans("mail.forgotten.description"), $user[User::ATTR_NAME], route(RouteData::AUTH_FORGOTTEN_RESET, $token)));
        $email->send();

        return true;
    }

    /** Valida un token de verificación
     * 
     * @param type $token
     * @return boolean
     */
    public static function validate($token) {

        $reset = PasswordReset::where(PasswordReset::ATTR_TOKEN, $token)->first();

        if (empty($reset)) {
            return trans("msg.forgotten.reset.token.invalid");
        }

        if ($reset->{PasswordReset::ATTR_VALIDATE}) {
            return trans("msg.forgotten.reset.token.validated");
        }

        if (DateUtil::difSec(new DateUtil(), $reset->{PasswordReset::ATTR_EXPIRATION}) < 0) {
            return trans("msg.forgotten.reset.token.expirated");
        }

        return $reset;
    }

    public static function getUserFromToken($token) {
        $reset = PasswordReset::where(PasswordReset::ATTR_TOKEN, $token)->first();
        return User::find($reset->user_id);
    }

}
