<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use App\Custom\Model;
use ReflectionClass;
use App\Base\System\Library\Comp\Util;
use App\Models\Credit;
use App\Base\Data\Currency;

class CreditTrack extends Model {

    const DB_TABLE = "credits_track";
    const ATTR_TYPE = "type";
    const ATTR_VALUE = "value";
    // FK
    const ATTR_FK_CREDIT_ID = "credit_id";
    // TYPES
    const TYPE_CURRENT_INTEREST = "CI";
    const TYPE_ARREARS_INTEREST = "AC";
    const TYPE_FEE = "FE";

    protected $table = self::DB_TABLE;

    public static function getTypes() {
        $class = new ReflectionClass(__CLASS__);

        $return = [];

        foreach ($class->getConstants() as $name => $data) {
            if (strpos($name, "TYPE_") !== false) {
                $return[$data] = trans("attr.credit.track.type." . $data);
            }
        }

        return $return;
    }

    public static function register(array $data) {

        $credit = Credit::find($data["credit_id"]);

        $track = new CreditTrack();
        $track[CreditTrack::ATTR_FK_CREDIT_ID] = $data["credit_id"];
        $type = $track[CreditTrack::ATTR_TYPE] = $data["type"];
        $value = $track[CreditTrack::ATTR_VALUE] = Util::getValue($data["value"], $credit[Credit::ATTR_CURRENCY]);

        $track->save();

        if ($type == CreditTrack::TYPE_ARREARS_INTEREST || $type == CreditTrack::TYPE_CURRENT_INTEREST)
            $credit[Credit::ATTR_BALANCE] += $value;
        else
            $credit[Credit::ATTR_BALANCE] -= $value;

        $credit->save();
    }

    public function getValue($format = false) {
        $is_unify = config("app.setting.currency.unify");

        $value = $this->value;
        $currency = $this->typayment->getObjectType()->getCurrency()->getCode();

        if ($is_unify) {
            $value = Currency::convert($value, $currency, config("app.setting.currency.code"));
            if ($format) {
                return Currency::getFormattedValue($value, config("app.setting.currency.code"));
            }
        }

        //Retorna el valor en formato moneda
        if ($format) {
            return Currency::getFormattedValue($value, $currency);
        }

        return $value;
    }

    /** Retorna el numero de cuotas pagadas del credito
     * 
     * @param type $credit_id
     * @return type
     */
    public static function getFeesNumberPaid($credit_id) {
        return CreditTrack::where(self::ATTR_FK_CREDIT_ID, $credit_id)->where(self::ATTR_TYPE, self::TYPE_FEE)->count();
    }

    /*
      |--------------------------------------------------------------------------
      | Relaciones
      |--------------------------------------------------------------------------
     */

    public function typayment() {
        return $this->belongsTo(Typayment::class, self::ATTR_FK_CREDIT_ID);
    }

    /*
      |--------------------------------------------------------------------------
      | MUTATORS
      |--------------------------------------------------------------------------
     */

    public function getTypeAttribute($value) {
        return self::getTypes()[$value];
    }

}
