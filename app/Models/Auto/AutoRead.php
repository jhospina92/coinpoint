<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models\Auto;

use App\Custom\Model;
use Illuminate\Support\Facades\Auth;
use App\Base\System\Library\DataProviders\APIs\Mails\Build\MailClient;
use App\Models\Typayment;
use App\Models\Relations\TypaymentAutoRead as Typauto;
use App\Models\Logs\LogAutoReadFailed as LogError;
use App\Models\Logs\LogAutoReadProcessed as LogSuccess;
use App\Base\System\Library\Networks\Email;
use App\Models\User;
use App\Models\UserMeta;
use App\Base\System\Library\Comp\DateUtil;
use App\Models\Flow;

/**
 * Description of AutoRead
 *
 * @author jospina
 */
class AutoRead extends Model {

    const DB_TABLE = "autoread";
    //Attrs
    const ATTR_ACTIVE = "active"; //Indica si esta enlazado y autenticado correctamente
    const ATTR_AUTH = "auth"; //Almacena la información de permisos de autenticación
    const ATTR_CLIENT = "client"; //Indica el tipo de cliente de correo Gmail, Outlook, Yahoo etc...
    const ATTR_PROCESSED = "processed"; //Indica el numero de transacciones procesadas exitosamente
    const ATTR_ERRORS = "errors"; //Indica el numero de transacciones erradas
    //FK
    const ATTR_FK_USER_ID = "user_id";
    //Clients
    const CLIENT_GMAIL = "Gmail";

    protected $table = self::DB_TABLE;

    /** Agrega una nueva cuenta cliente
     * 
     * @param string $client
     * @param array $token_auth
     * @param int $user_id
     * @return \App\Models\Auto\Autoread
     */
    public static function add(string $client, array $token_auth, int $user_id = null): Autoread {

        $user_id = $user_id ?? Auth::user()->id;

        $autoread = new AutoRead();
        $autoread[AutoRead::ATTR_FK_USER_ID] = $user_id;
        $autoread[AutoRead::ATTR_CLIENT] = AutoRead::CLIENT_GMAIL;
        $autoread[AutoRead::ATTR_ACTIVE] = true;
        $autoread[AutoRead::ATTR_AUTH] = $token_auth;
        $autoread->save();

        return $autoread;
    }

    public function getClientObject() {
        return MailClient::load($this->client, $this->id);
    }

    /** Agrega un registro de procesamiento erroneo de lectura
     * 
     * @param type $message_id
     * @param type $description
     * @return void
     */
    public function addLogError($message_id, $description, $type, $typayment_id) {
        $log = new LogError;

        $log[LogError::ATTR_FK_AUTOREAD_ID] = $this->id;
        $log[LogError::ATTR_FK_TYPAYMENT_ID] = $typayment_id;
        $log[LogError::ATTR_MESSAGE_ID] = $message_id;
        $log[LogError::ATTR_DESCRIPTION] = $description;
        $log[LogError::ATTR_TYPE] = $type;


        $log->save();

        $this->{self::ATTR_ERRORS} = LogError::where(LogError::ATTR_FK_AUTOREAD_ID, $this->id)->count();
        $this->save();

        $user = User::find($this->{self::ATTR_FK_USER_ID});
        $client = $this->getClientObject();
        $typayment = Typayment::find($typayment_id)->getObjectType();
        

        $datamail = [
            $client->getProfileEmail(), //Correo electrónico asociado
            $log[LogError::ATTR_DESCRIPTION], // Descripción del error
            Flow::getUtilTypeName($type), //Tipo de transacción
            $typayment->getFullName(), //Medio de pago
            (new DateUtil())->getString("d m Y h:i a") // Fecha y hora
        ];

        $message = sprintf(trans("mail.autoread.failed.description"), ...$datamail);

        $email = new Email(trans("mail.autoread.failed.subject"), $user[User::ATTR_EMAIL]);
        $email->setDescription($message);

        if ($user->getMeta(UserMeta::KEY_NOTF_FLOW_AUTOREAD_ERROR_EMAIL)) {
            $email->queue();
        }
    }

    /** Agrega un registro de procesamiento exitoso de lectura
     *  
     * @param type $message_id
     * @param array $data
     * @return void
     */
    public function addLogSuccess($message_id, array $data) {

        LogSuccess::add($this->id, $message_id, $data);

        $this->{self::ATTR_PROCESSED} = LogSuccess::where(LogSuccess::ATTR_FK_AUTOREAD_ID, $this->id)->count();
        $this->save();
    }

    /** Indica si un message ID existe en el registro de procesamiento existoso
     * 
     * @param type $message_id
     * @return bool
     */
    public function existsMessageIdInLogSuccess($message_id): bool {
        return LogSuccess::where(LogSuccess::ATTR_FK_AUTOREAD_ID, $this->id)->where(LogSuccess::ATTR_MESSAGE_ID, $message_id)->withTrashed()->exists();
    }

    /** Indica si un message ID existe en el registro de procesamientos fallidos
     * 
     * @param type $message_id
     * @return bool
     */
    public function existsMessageIdInLogFail($message_id): bool {
        return LogError::where(LogError::ATTR_FK_AUTOREAD_ID, $this->id)->where(LogError::ATTR_MESSAGE_ID, $message_id)->exists();
    }

    /** Indica si un correo electrónico ya se encuentra asociado a la cuenta de un usuario 
     * 
     * @param type $email
     */
    public static function isEmailExist($email, $user_id = null): bool {
        $user_id = $user_id ?? Auth::user()->id;

        $auts = AutoRead::where(self::ATTR_FK_USER_ID, $user_id)->get();

        foreach ($auts as $aut) {
            $client = $aut->getClientObject();
            if ($client->getProfileEmail() == $email)
                return true;
        }

        return false;
    }

    /*
      |--------------------------------------------------------------------------
      | MUTADORES DE ATRIBUTOS
      |--------------------------------------------------------------------------
     */

    public function setAuthAttribute(array $array) {
        $this->attributes[self::ATTR_AUTH] = json_encode($array);
    }

    public function getAuthAttribute($json) {
        return json_decode($json, true);
    }

    /*
      |--------------------------------------------------------------------------
      | RELACIONES
      |--------------------------------------------------------------------------
     */

    function typayments() {
        return $this->belongsToMany(Typayment::class, Typauto::DB_TABLE, Typauto::ATTR_FK_AUTOREAD_ID, Typauto::ATTR_FK_TYPAYMENT_ID)->withPivot(Typauto::ATTR_TYPE, Typauto::ATTR_PARAMS);
    }

    function log_success() {
        return $this->hasMany(LogSuccess::class, LogSuccess::ATTR_FK_AUTOREAD_ID);
    }

    function log_error() {
        return $this->hasMany(LogError::class, LogError::ATTR_FK_AUTOREAD_ID);
    }

}
