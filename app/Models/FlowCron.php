<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use App\Models\Flow;
use App\Base\System\Library\Comp\DateUtil;
use App\Custom\Model;
use Illuminate\Support\Facades\Auth;
use App\Models\Wildo\WReport;
use App\Base\UI\Message;
use App\Base\System\Library\Comp\Util;
use App\Base\System\Library\Networks\Email;
use App\Base\Data\CategoryData;
use App\Base\Data\Currency;
use App\Base\Data\RouteData;

/**
 * Description of FlowCron
 *
 * @author jospina  
 */
class FlowCron extends Model {

    const DB_TABLE = "flow_cron";
    //ATTRS
    const ATTR_REPEAT = "repeat";
    const ATTR_DATE_NEXT = "date_next";
    const ATTR_CONFIRM = "confirm";
    const ATTR_HASH_EMAIL = "hash_email";
    const ATTR_LAST_NOTIFICATION = "last_notification";
    //FK 
    const ATTR_FK_USER_ID = "user_id";
    const ATTR_FK_REF_ID = "ref_id";
    //EXT
    const MODAL_VIEW_NOTIFICATION_PATH = "ui/modal-contents/flow-cron-notification";

    protected $table = self::DB_TABLE;
    public $timestamps = false;

    /**
     * Revisa las transacción programada de un usuario y verifica si deben ejecutarse.
     */
    public static function handler($user_id = null) {

        $user_id = $user_id ?? ((Auth::check()) ? Auth::user()->id : null);
        $user = User::find($user_id);

        if (empty($user))
            throw new Exception("The user id is undenfined");


        $crons = FlowCron::where(self::ATTR_FK_USER_ID, $user_id)->get();

        foreach ($crons as $cron) {
            $date = new DateUtil();
            $data["cron"] = $cron;
            $data["transaction"] = $transaction = $cron->transaction;
            $data["transaction_cron"] = $cron;

            $catname = CategoryData::getName($transaction[Flow::ATTR_CATEGORY]);
            $type = $transaction[Flow::ATTR_TYPE];
            $classtype = ($type == Flow::TYPE_REVENUE) ? "revenue" : "spend";

            if (empty($cron->transaction)) {
                $cron->delete();
                return;
            }

            $payment = $transaction->typayment->getObjectType();

            //Verifica que el tiempo se haya cumplido
            if (DateUtil::difSec($date, $cron[self::ATTR_DATE_NEXT]) > 0)
                continue;

            if ($cron[self::ATTR_CONFIRM]) {
                //Verifica si hay en sesión y este es el mismo propietario de las transacción en revision
                if (Auth::check() && Auth::user()->id == $user_id) {
                    Message::modal(view(self::MODAL_VIEW_NOTIFICATION_PATH, $data)->render(), trans("ui.label.flow.cron.for.today"), false);
                    break;
                } else {

                    $last_not = $cron[self::ATTR_LAST_NOTIFICATION];

                    //Verifica no se haya enviado una notificacion al usuario las ultimas 24 horas
                    if (!is_null($last_not) && DateUtil::difSec($last_not, $date) < (60 * 60 * 24))
                        continue;

                    $hash = $cron->generateHashEmail();

                    /**
                     * Si el usuario no esta en sesión se enviara un correo electrónico al usuario con enlace para la verficiación de la transacción
                     */
                    $datamail = [
                        $user[User::ATTR_NAME], //Nombre de la persona
                        $catname, //Categoria de la transacción
                        $transaction->getValue(true), //Valor de la transacción en formato moneda
                        $classtype, //Clase CSS del tipo de transacción
                        $transaction->getTypeName(), //Tipo de transacción
                        $catname, //Categoria de la transacción
                        $payment->getName(), //Medio de pago utilizado
                        $transaction[Flow::ATTR_DESCRIPTION], //Descripción personalizada de la transacción
                        $transaction->getValue(true), //Valor de la transacción en formato moneda
                        route(RouteData::SERVICES_FLOW_CRON_EMAIL, [$user->id, strtolower($transaction->typayment->currency), $user->getMetaLang(), $hash]), //URL donde se recibe la confirmación de la transacción
                    ];

                    $description = sprintf(trans("mail.flowcron.confirm.description"), ...$datamail);

                    $email = new Email(sprintf(trans("mail.flowcron.confirm.subject"), $catname), $user[User::ATTR_EMAIL]);
                    $email->setHeader(sprintf(trans("mail.flowcron.confirm.header"), $catname));
                    $email->setDescription($description);

                    if ($user->getMeta(UserMeta::KEY_NOTF_FLOWCRON_CONFIRM_EMAIL)) {
                        $email->queue();
                        $cron[self::ATTR_LAST_NOTIFICATION] = (new DateUtil());
                        $cron->save();
                    }
                }
            } else {
                $data = ["description" => $cron->transaction[Flow::ATTR_DESCRIPTION], "value" => $cron->transaction[Flow::ATTR_VALUE], "flag-date-next" => true, "user_id" => $user->id];

                /**
                 * Envia un correo electrónico cuando la transacción se ejecuta automaticamente
                 */
                $datamail = [
                    (new DateUtil())->getString("d m Y"),
                    $transaction->getValue(true), //Valor de la transacción en formato moneda
                    $catname, //Categoria de la transacción
                    $transaction->getTypeName(), //Tipo de transacción
                    $catname, //Categoria de la transacción
                    $payment->getName(), //Medio de pago utilizado
                    $transaction[Flow::ATTR_DESCRIPTION], //Descripción personalizada de la transacción
                    $transaction->getValue(true), //Valor de la transacción en formato moneda
                ];

                $description = sprintf(trans("mail.flowcron.exec.description"), ...$datamail);

                $email = new Email(sprintf(trans("mail.flowcron.exec.subject"), $catname), $user[User::ATTR_EMAIL]);
                $email->setHeader(sprintf(trans("mail.flowcron.exec.header"), $catname));
                $email->setDescription($description);

                if ($user->getMeta(UserMeta::KEY_NOTF_FLOWCRON_AUTO_EMAIL)) {
                    $email->queue();
                    $cron[self::ATTR_LAST_NOTIFICATION] = (new DateUtil());
                    $cron->save();
                }

                $cron->exec($data);
            }
        }
    }

    public function exec($data) {

        //Si han eliminado la transacción, la transacción programada se eliminara
        if (empty($this->transaction)) {
            $this->delete();
            return;
        }

        $dataflow = ["type" => $this->transaction->type,
            "date_submit" => date("Y/m/d"),
            "description" => $data["description"],
            "category" => $this->transaction->category,
            "typayment" => $this->transaction->typayment->id,
            "value" => Util::getValue($data["value"], $this->transaction->typayment->currency)];

        $dataflow = array_merge($dataflow, $this->transaction->{Flow::ATTR_METADATA});

        if (isset($data["user_id"]))
            $dataflow["user_id"] = $data["user_id"];


        //Actializa la transacción programada
        $this[FlowCron::ATTR_FK_REF_ID] = Flow::createFromType($dataflow);
        $this[FlowCron::ATTR_DATE_NEXT] = $this->getDateNextExec((isset($data["flag-date-next"])) ? $data["flag-date-next"] : false);

        $this->save();

        return $this[FlowCron::ATTR_DATE_NEXT];
    }

    /** Calcula la siguiente fecha de ejecución de la transacción de refencia
     * 
     * @param type $current (false) Indica si calcula la fecha en base al fecha actual
     * @return type
     */
    public function getDateNextExec($current = false) {

        $transaction = $this->transaction;

        if (empty($transaction) || empty($this->repeat))
            return null;

        $date = new DateUtil(explode(" ", $transaction[Flow::ATTR_DATE])[0] . " " . explode(" ", $transaction[Flow::CREATED_AT])[1]);

        if ($current)
            $date = new DateUtil();

        $count = $this->repeat[0];
        $time = $this->repeat[1];

        switch ($time) {
            case "d":
                $date = $date->addDays($count);
                break;
            case "m":
                $date = $date->addMonths($count);
                break;
            case "y":
                $date = $date->addMonths($count * 12);
                break;
        }
        return $date;
    }

    /** Genera un hash de email y lo asigna al objeto
     * 
     * @return type
     */
    private function generateHashEmail() {

        $attr = self::ATTR_HASH_EMAIL;

        do {
            $hash = str_random(100);
        } while (FlowCron::where($attr, $hash)->exists());

        $this->{$attr} = $hash;
        $this->save();

        return $hash;
    }

    /*
      |--------------------------------------------------------------------------
      | Relaciones
      |--------------------------------------------------------------------------
     */

    public function transaction() {
        return $this->belongsTo(Flow::class, self::ATTR_FK_REF_ID);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    /*
      |--------------------------------------------------------------------------
      | Mutadores
      |--------------------------------------------------------------------------
     */

    public function getRepeatAttribute($data) {
        return json_decode($data, true);
    }

    public function setRepeatAttribute($data) {
        $this->attributes[self::ATTR_REPEAT] = json_encode($data);
    }

}
