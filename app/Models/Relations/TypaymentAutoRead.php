<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models\Relations;

use App\Custom\Model;

/**
 * Description of TypaymentAutoRead
 *
 * @author jospina
 */
class TypaymentAutoRead extends Model {

    const DB_TABLE = "typayments_autoread";
    //FK
    const ATTR_FK_TYPAYMENT_ID = "typayment_id";
    const ATTR_FK_AUTOREAD_ID = "autoread_id";
    //ATTR
    const ATTR_PARAMS = "params";
    const ATTR_TYPE = "type";

    protected $table = self::DB_TABLE;
    public $timestamps = false;

     /*
      |--------------------------------------------------------------------------
      | MUTADORES DE ATRIBUTOS
      |--------------------------------------------------------------------------
     */

    public function setParamsAttribute(array $array) {
        $this->attributes[self::ATTR_PARAMS] = json_encode($array);
    }

    public function getParamsAttribute($json) {
        return json_decode($json, true);
    }
    
}
