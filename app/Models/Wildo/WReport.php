<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models\Wildo;

use \Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class WReport extends Model {

    const DB_TABLE = "wildo_reports";
    //Attrs
    const ATTR_MESSAGE = "message";
    const ATTR_DATA = "data";
    const ATTR_TYPE = "type";
    const ATTR_TIME = "time";
    const ATTR_VIEWED = "viewed";
    //Fk
    const ATTR_FK_USER_ID = "user_id";

    protected $table = self::DB_TABLE;
    public $timestamps = false;

    //Types
    const TYPE_INTERSTIAL = "IN";
    const TYPE_CRUM = "CR";
    const TYPE_MODAL = "MO";

    public static function get() {
        return WReport::where(self::ATTR_FK_USER_ID, Auth::user()->id)->where(self::ATTR_VIEWED, false)->get();
    }

}
