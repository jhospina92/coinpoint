<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Console\Task\CurrencyTask;
//Comands
use App\Console\Commands\CurrencyRefreshRates;
use App\Console\Commands\CheckFlowCron;
use App\Console\Commands\CheckAutoRead;

class Kernel extends ConsoleKernel {

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CurrencyRefreshRates::class,
        CheckFlowCron::class,
        CheckAutoRead::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {
        $schedule->command("currency:refresh-rates")->hourly();
        $schedule->command("flowcron:check")->hourly();
        $schedule->command("autoread:check")->everyTenMinutes();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands() {
        require base_path('routes/console.php');
    }

}
