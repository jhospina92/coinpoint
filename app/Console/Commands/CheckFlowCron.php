<?php

/**
 *  Verifica las transacciones programadas de los usuarios y las ejecuta y las notifica
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;

class CheckFlowCron extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'flowcron:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check the transations scheduled';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        /**
         *  Verifica las transacciones programadas asignando la tarea de a bloques de 1000 usuarios
         */
        $users = User::all(["id"]);
        $alluserids = [];

        foreach ($users as $user) {
            $alluserids[] = $user->id;
        }

        foreach (array_chunk($alluserids, 1000) as $blockusers) {
            dispatch(new \App\Jobs\Services\Email\SendFlowCronEmail($blockusers));
        }
    }

}
