<?php

/*
  Actualiza los ratios de conversion de monedas
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Console\Task\CurrencyTask;

class CurrencyRefreshRates extends Command {

    protected $signature = 'currency:refresh-rates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Do request to the API (https://openexchangerates.org) for reload the currency rates.';

    /**
     * Create a new command instance.
     *
     * @param  DripEmailer  $drip
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        dispatch(new \App\Jobs\Providers\RefreshCurrencyRates());
    }

}
