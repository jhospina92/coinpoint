<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;

class CheckAutoRead extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'autoread:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check the autoread transaction from emails';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $users = User::all(["id"]);
        $alluserids = [];

        foreach ($users as $user) {
            $alluserids[] = $user->id;
        }

        foreach (array_chunk($alluserids, 1000) as $blockusers) {
            dispatch(new \App\Jobs\Services\Auto\ProcessAutoRead($blockusers));
        }
    }

}
