# Registro de cambios #

Version 0.7.4
==================
Fecha: 2017 Agosto 14

- Se soluciona #47.
- Se soluciona #60.
- Se soluciona #61.
- Se corrige un bug que duplicaba los costos asociados a una transferencia cuando se editaba.
- Se implementa número de cuotas por defecto asociadas las transacciones con tarjeta de credito.


Version 0.7.3
==================
Fecha: 2017 Agosto 10

- Se agrega campos para los ultimos 4 numeros de la tarjeta de credito y la seleccion de la franquicia. #53
- Evita que se modifique el cupo de la tarjeta de credito por debajo del valor de la deuda pendiente. #56
- Se modifica el sistema de carga de los widgets en el dashboard, haciendo de manera asincrona.
- Se implementa y reemplaza los graficos mostrados en dashboard por las libreria de material desing de Google.

Version 0.7.2.3
==================
Fecha: 2017 Julio 05

- Correcion de algunos errores en el frontend. 
- SSe modifica el algoritmo que permite comprobar si una url existe.

Version 0.7.2.2
==================
Fecha: 2017 Junio 22

- Se modifica la frecuencia de verificación de transacción a través del correo electrónico a 10 minutos.
- Se realiza una modificación en el envio del correo electrónico de procesamiento automatico para el tipo de transacción "Transferencia".  

Version 0.7.2.1
==================
Fecha: 2017 Junio 21

- Se realiza un ajuste en la extracción del valor monetario de una transacción en la lectura automatica a traves del correo electrónico.  

Version 0.7.2.0
==================
Fecha: 2017 Junio 20

- Se mejora el algoritmo de autogeneración del formato de descripción en el sistema de lectura de transacción automaticas a traves del correo electrónico.  

Version 0.7.1.1
==================
Fecha: 2017 Junio 15

- Se estable un parametro para forzar el retorno del token de actualización de la API de GMail.
- Se implementa un mecanismo que evita que el sistema se bloquee si una cuenta de correo asociada para lectura automatica de transacciones no puede ser cargada. 

Version 0.7.1.0
==================
Fecha: 2017 Junio 14

- Se corrige un bug que calculaba mal el nuevo balance de la tarjeta de credito cuando se actualizaba. 
- Se habilita gestión de cuentas bancarias. 
- Se agrega procesamiento automatico de transacciones del tipo "Transferencia" cuando es leido del correo electrónico.


Version 0.7.0.1
==================
Fecha: 2017 Junio 13

- Se elimina dependencias IMAP.

Version 0.7.0.0
==================
Fecha: 2017 Junio 13

- Se agrega funcionalidad que permite activar/desactivar un atributo en un modelo Eloquent indicado si tiene la propiedad.
- Se corrige una duplicación de texto en la vista de configuración de notificaciones.
- Se agrega funcionalidad que permite renderizar una vista dentro de un contenedor a traves de ajax. 
- Se agrega funcionalidad que permite renderizar una vista dentro de un modal.
- Se agrega sistema de lectura de transacciones a traves de asociación de correo electrónicos a los medios de pagos. Con soporte unicamente para Gmail. 
- Se integra sistema y API de Gmail. 
- Se modifica sistema de información adicional para los medios de pagos.



Version 0.6.5.1
==================
Fecha: 2017 Mayo 15

- Se corregi un error el cual no permitia realizar la ejecución de una tarea programada por que estaba asociado al usuario en sesión.


Version 0.6.5.0
==================
Fecha: 2017 Mayo 12

- Se limita el tiempo de las notificaciones enviadas por correo electrónico para las transacciones programadas como minimo de 24 horas. #41
- Se corrige un error ocasionado cuando un usuario eliminaba un medio de pago.
- Se agrega un sistema de filtro y busquedas en el indice de transacciones. #20
- Algunas modificaciones menores. 



Version 0.6.4.1
==================
Fecha: 2017 Abril 28

- Se volvio a implementar un sistema de cache con variables de sesión para la carga de datos de tasas de conversión. 
- Corrección de algunos errores.


Version 0.6.4
==================
Fecha: 2017 Abril 27

- Se corrige #30. 
- Se desarrolla e implementa un nueva selección de fecha para seleccionar solamente año y mes, que aplican para las secciones de Metas y Presupuestos #33.
- Se agrega una nueva columna para indicar la fecha limite de las metas dentro de la vista del listado.
- Se corrige un error al tratar de actualizar información adicional de los medios de pagos.
- Se modifica el proceso de ejecución de la actualización de tasas de conversión de monedas.
- Se agrega envio de correo electrónico para las transacciones programadas.
- Se agrega pantalla de configuración de notificaciones y se incluyen las opciones correspondiente para las notificaciones de envio de correos por las transacciones programadas.
- Se integra el servicio de Amazon Storage Services System (S3) para el almacenamiento de archivos.
- Se implementa sistema de cache en carga de tasas de conversiones de moneda.
- Se aumenta la frecuencia de actualización de las tasas de conversiones de moneda. #32



Version 0.6.3.3
==================
Fecha: 2017 Abril 18

- Se agrega visualización de barra de carga al enviar los formulario en la parte de visitantes. #29
- Se agrega sistema de cache y procesamiento de tareas en segundo plano. #31
- Se optimiza la carga del dashboard usando cache. 
- Se cambia la manera de invocar los webcomponents.
- Se corrige un bug que nuevamente mostraba el mensaje modal de una transacción programada cuando ya esta se habia cancelado.
- Se agrega nuevo componente web, para mostrar un Loader de reloj de arena.
- Se agrega visualización de animación de carga antes de mostrarse el widget de metas.
- Se modifica la posición del botón del modo seguro.


Version 0.6.3.2
==================
Fecha: 2017 Abril 14

- Correción de bug, que no permitia ejecutar un acción invocada de confirmación.

Version 0.6.3.1
==================
Fecha: 2017 Abril 13

- Se agrega configuración para habilitar/deshabilitar el registro de nuevos usuarios y el envio de emails.

Version 0.6.3
==================
Fecha: 2017 Abril 12

- Se mejora la pantalla de inicio de sesión.
- Se agrega formulario de registro de usuario.
- Se agrega sistema de validación de usuarios mediante verificación de correo electrónico. 
- Se sistema de recuperación de contraseña.


Version 0.6.2.7
==================
Fecha: 2017 Abril 03

- Se corrige errores de visualización en el modal del presupuesto. 
- Se corrige un error de debug.


Version 0.6.2.6
==================
Fecha: 2017 Marzo 30

- Se corrige issue#22.
- Se corrige issue#23. 

Version 0.6.2.5
==================
Fecha: 2017 Marzo 23

- Corrección de errores asociados al loader que sale en pantalla cuando se cambia entre vistas.

Version 0.6.2.4
==================
Fecha: 2017 Marzo 23

- Corrección de errores asociados al loader que sale en pantalla cuando se cambia entre vistas.

Version 0.6.2.3
==================
Fecha: 2017 Marzo 21

- Se agregar loader en pantalla para la transación entre vistas.


Version 0.6.2.2
==================
Fecha: 2017 Marzo 16

- Se establece un comando para refrescar las tasas de conversión de las monedas. 
- Se adecua la ejecución de las tareas programadas en el servidor.
- Se actualiza los requerimientos del sistema desde composer.json para el uso de PHP desde la versión 7.0.0.

Version 0.6.2.1
==================
Fecha: 2017 Marzo 15

- Se optimiza el proceso de carga al gestionar el tipo de moneda en el sistema.
- Se modifica la clase proveedora de datos de las conversiones entre monedas, para evitar que entre en un bucle infinito si no hay archivos cacheados.

Version 0.6.2
==================
Fecha: 2017 Marzo 15

- Se agrega validación de propiedad de los registros.
- Se agrega gestión de metas. 
- Se agrega "Modo seguro": Permite ocultar los valores monetarios en pantalla.
- Se corrige un error en la ejecución de transacción programada cuando este estaba asociado al pago de un pestramo. Ya que no actualizaba el balance del prestamo.

Version 0.6.1
==================
Fecha: 2017 Marzo 01

- Reestructuración de carpetas de la estructura del sistema.
- Se agrega sección de gestión de cuenta de usuario.
- Se agrega vista de modificación de datos de perfil.
- Se agrega vista de cambio de contraseña.
- Se agrega vista de modificación de ajustes generales.
- Se agrega visualización de grafico estimado en los widgets de Gastos, Ingresos y Ahorros. Integrados con el presupuesto activo.
- Corrección de errores.
- Se corrige Issue#21.
- Se agregar atributos de timestamp al modelo de Typayment.
- Modificaciones en la manera de mostrar los graficos de balance en el dashboard.
- Se agrega widget para mostrar el nivel de crecimiento del dinero mes tras mes.

Version 0.6.0
==================
Fecha: 2017 Febrero 23

- Se agrega restricción que al seleccionar la categoria correspondiente al pago de la cuota de una tarjeta de credito esta no se desactive en el listado de medios de pagos.
- Se agrega requirimientos de campo en el creación de medios de pago.
- Se agrega una subcategoria en Educación llamada "Mensualidad Escolar/Educativa". Issue#14
- Se agrega una clase HTML personalizada para incluye un parametro de versión, para refrescar la importación de los archivos en cache del navegador una vez que se cambia la versión del sistema. 
- Se agregan paginas de errores HTTP 500 y 404 personalizadas.
- Se agrega atributo para determinar el tipo de moneda para los presupuestos y los prestamos.
- Se realiza una modificación en archivo mdb.min.js complemento externo.
- Se integra sistema de visualización y gestión en diferentes tipos de monedas. Issue#1
- Correción de errores.


Version 0.5.10
==================
Fecha: 2017 Febrero 15

- Se cambia el logo de la plataforma.
- Se modifica las subcategorias de Vestimenta. Issue#17
- Se agrega titulos personalizados para las paginas. 
- Se agrega atributo de SoftDelete para el modelo Typayment.
- Se agrega funcionalidad que permite eliminar y cancelar los medios de pagos.

Version 0.5.9
==================
Fecha: 2017 Febrero 13

- Se corrige un bug en la función de "getForMonth" del modelo Flow, que no obtenia correctamente el ID del usuario en sesión. 
- Se agrega visualización de rubros no presupuestado en el presupuesto.
- Se agrega visualización de vigencia de los presupuesto en la sección de prespuestos.
- Se corrige un bug que no mostraba las categorias correspondiente de Prestamos y Pago de cuotas de tarjetas de credito cuando se editaba una transacción.

Version 0.5.8
==================
Fecha: 2017 Febrero 09

- Se corrige un error en el widget de presupuesto cuando se calculaba el ahorro esperado. 
- Se realiza una modificacion en la forma de mostrar los widgets.
- Se corrige un error en la clase de utilidad de la fechas. (DateUtil)
- Se agrega modal para visualizar todos los datos del presupuesto acumulado del periodo.
- Se agregar uso de webcomponents.
- Se agrega webcomponent para visualizar el tiempo del progreso actual del presupuesto.

Version 0.5.7
==================
Fecha: 2017 Enero 31

- Se actualiza MDBoostrap a la versión 4.3.0
- Se agrega opción para confirmar la transacción programada, y si no que se ejecute automaticamente.

Version 0.5.6
==================
Fecha: 2017 Enero 30

- Se mejora la funcionalidad que permite la mostrar un mensaje de confirmación. 
- Se mejora la funcionalidad que permite validar los formulario antes de enviarlos, encapsulandolos.
- Se agrega funcionalidad que permite ejecutar las transacciones recurrentes cuando debe ser ejecutadas.

Version 0.5.5.1
==================
Fecha: 2017 Enero 24

- Se agrega funcionalidad que permite crear transacciones recurrentes.
- Se agrega visualización de transacciones programadas.

Version 0.5.5
==================
Fecha: 2017 Enero 24

- Se agrega funcionalidad que permite reproducir sonidos de notificación.
- Se agrega sistema de notificaciones "Wildo" que mostrar mensajes producidos por el sistema que son de ayuda del usuario.
- Se agrega modelo para la gestión de reportes tipo mensajes de Wildo en el sistema.

Version 0.5.4
==================
Fecha: 2017 Enero 20

- Se optimiza el algoritmo de la función que obtiene el listado de transacciones de un mes.
- Se reorganiza la manera de cargan los widgets en el dashboard.
- Se agrega visualización de porcentaje de gastos en el widget de categorias del dashboard en forma de tooltips.
- Se agrega información en el widget del Prespuesto en el Dashboard para mostrar el ahorro esperado, esto indicado por el nive de gasto actual. 

Version 0.5.3
==================
Fecha: 2017 Enero 14

- Se agrega gestión de medio de pago - tarjeta de credito.

Version 0.5.2.1
==================
Fecha: 2017 Enero 6

- Correcciones de error en el presupuesto.

Version 0.5.2
==================
Fecha: 2017 Enero 6

- Se agrega funcionalidad que permite editar los prespuestos.

Version 0.5.1
==================
Fecha: 2017 Enero 5

- Se corrige un error que no permitia obtener el gasto incurrido en los prestamo en el widget del presupuesto en el dashboard.
- Se agrega una validación en donde se solo muestra los tipos de categoria "Tarjetas de Credito" y "Prestamos" en el listado de categorias si el usuario los tiene. 
- Se agrega la funcionalidad que permite eliminar un presupuesto.
 
Version 0.5.0
==================
Fecha: 2017 Enero 4

- Se modifica el lugar donde se de click en la tarjeta de la transacción para voltearla.
- Se agrega funcionad de mensaje tipo "Dialog" para la confirmación de eventos. 
- Se agrega sistema de reversión de transacciones, permitiendo eliminar registro de transacciones y ajustar el balance de los componentes asociados. 
- Se agrega requerimiento de componente "Form" para la creación de formularios desde el backend.
- Se agrega funcionalidad que permite la modificación y edición de transacciones.
- Se agrega propiedad que permite indicar si una transacción es dependiente de otra.
- Se agrega validación de dependencia de transacción lo que no permite su eliminación de listado de transacciones.


Version 0.4.2.2
==================
Fecha: 2017 Enero 2

- Corrección de errores en la visualización de información en los widgets del dashboard.

Version 0.4.2.1
==================
Fecha: 2016 Diciembre 30

- Se modifica la visualización del presupuesto activo en el listado de presupuesto y se agrega menu desplegable para acciones asociadas.


Version 0.4.2
==================
Fecha: 2016 Diciembre 28

- Se agrega funcionalidad que permite desactivar un presupuesto.
- Se modifica el nombre de las rutas que activar/desactiva los presupuesto.
- Se agrega widget con visualización de cumplimiento del presupuesto activado.


Version 0.4.1.1
==================
Fecha: 2016 Diciembre 27

- Se modifica el estilo del botón de accesos directos del menu superior.

Version 0.4.1
==================
Fecha: 2016 Diciembre 27

- Se modifica la estructura de almacenamiento de los datos del presupuesto.
- Se agrega visualización de presupuestos.
- Se agrega funcionalidad en la visualización de presupuestos que permite activar.

Version 0.4.0
==================
Fecha: 2016 Diciembre 26

- Se termina el formulario de creación de presupuestos y gestión de guardado en la base de datos. #3
- Se modifica modelo de "Budget" referente al presupuesto.

Version 0.3.9.1
==================
Fecha: 2016 Diciembre 19

- Se modifica el evento en Jquery que formatea los números en tipo de moneda para tenerlo en persistencia.
- Se agrega el formulario de creación de Presupuestos.

Version 0.3.9
==================
Fecha: 2016 Diciembre 19

- Se agrega visualizador de uso de tarjetas de credito en el dashboard.

Version 0.3.8
==================
Fecha: 2016 Diciembre 17

- Se agrega visualizador de gastos desglosados por categorias del mes en el dashboard.

Version 0.3.7.1
==================
Fecha: 2016 Diciembre 15

- Se corrige #issue 11. 

Versión 0.3.7
==================
Fecha: 2016 Diciembre 13

- Se agrega pago de cuotas de tarjetas de credito en el formulario de transacciones. 
- Se agrega texto indicando la versión del sistema en la parte inferior del menu principal. 

Versión 0.3.6
==================
Fecha: 2016 Diciembre 12

- Se agrega balance de deuda en el menu principal d la interfaz. 
- Se agrega campo de Numero de cuotas al pagar con tarjeta de credito.

Versión 0.3.5
==================
Fecha: 2016 Diciembre 11

- Se integra las transacciones asociadas al pago cuotas de los prestamos.
- Se agrega formulario y almacenamiento del seguimiento de los prestamos.

Versión 0.3.4
==================
Fecha: 2016 Diciembre 10

- Se agrega modelo para el seguimiento de los creditos.
- Se corrige un error al agregar un nuevo medio de pago, en la que no se establecia el atributo "balance".
- Se atributo de relación con el usuario al modelo de "Credit" y el atributo "status".

Versión 0.3.3
==================
Fecha: 2016 Diciembre 7

- Se agrega seccion de caracteristicas adicionales al crear un prestamo.
- Se crea el algoritmo de guardado cuando se envia el formulario de creación de prestamo.

Versión 0.3.2
==================
Fecha: 2016 Diciembre 6

- Se agrega controlador y modelo para la gestión de creditos.
- Se independiza EL componente que permite seleccionar las categorias para poder usarlos en otras partes sin repetir codigo.
- Se eliminas las categorias asociadas a cuotas de creditos.
- Se crea el formulario para agregar prestamos.

Versión 0.3.1
==================
Fecha: 2016 Diciembre 2

- Se agrega botón de accesos directos en la barra de navegación superior.
- Se inicializa controlador de "Prestamos"

Versión 0.3.0
==================
Fecha: 2016 Diciembre 1

- Se agrega los parametros de la aplicación en la configuración global del sistema.
- Se agrega visualizador de balances de "Gastos" e "Ingresos" abreviados en el dashboard.

Versión 0.2.15
==================
Fecha: 2016 Noviembre 29

- Se modifica el algoritmo de lectura de balaces de saldos de los medios de pagos, haciendolo totalmente eficiente.
- Se agrega un nuevo atributo "Balance" al modelo "Typayment".

Versión 0.2.14
==================
Fecha: 2016 Noviembre 29

- Se agrega un elemento del menú para ir al "Dashboard"
- Se agrega campo para el registro del costo de asociado a la transferencia cuando se realiza una transacción del tipo "Transferencia"

Versión 0.2.13
==================
Fecha: 2016 Noviembre 25

- Se agrega sección en la barra de navegación para ver el balance general de las finanzas del usuario.

Versión 0.2.12
==================
Fecha: 2016 Noviembre 25

- Se agrega algoritmo de lectura de balaces de saldos de los medios de pagos.
- Se modificación el sistema de validación de formularios.
- Se agrega algunas validaciones de condiciones.
- Se corrige algunos errores.

Versión 0.2.11
==================
Fecha: 2016 Noviembre 24

- Se agrega el atributo Metadata al modelo "Flow" para almacenar información adicional en cada transacción.
- Se termina el soporte para agregar el tipo de transacción "Transferencia", entre tipos de pagos.

Versión 0.2.10
==================
Fecha: 2016 Noviembre 21

- Se inicializa el soporte para agregar el tipo de transacción "Transferencia de fondos".

Versión 0.2.9
==================
Fecha: 2016 Noviembre 21

- Se agrega listado de categorias para el tip de transacciones de "Ingreso".


Versión 0.2.8
==================
Fecha: 2016 Noviembre 19

- Se agrega efecto rotativo a los recuadros donde aparecen cada transacción en el listado de transacciones. 

Version 0.2.7
==================
Fecha: 2016 Noviembre 17

- Se agrega el proceso guardado de la al agregar una transacción de tipo "Gasto".
- Se modifica un tipo de atributo en el modelo de "Flow".
- Se agrega visualización de transacciones realizadas.

Version 0.2.6
==================
Fecha: 2016 Noviembre 17

- Se agregan los campo de "Valor", "Descripción" y "Medios de pago" en el formulario de "Agregar transacción".
- Se agregan validaciones de campos al sistema de validación de formulario. 

Version 0.2.5
==================
Fecha: 2016 Noviembre 16

- Se agrega campo de selección de Categorias del tipo "Gasto" con una ventana modal para su selección en el formulario de "Agregar transacción".

Version 0.2.4
==================
Fecha: 2016 Noviembre 12

- Se agrega en la clase "Category" las categorias de Impuestos, Transporte, Diversión y Ocio, Vestuario, Educación, Salud, Donaciones, Productos y Servicios Complementarios.  

Version 0.2.3
==================
Fecha: 2016 Noviembre 11

- Se agrega clase con listado de categorias de  Vivienda y Alimentación.
- Se realiza una modifaciones de algunos atributos (category_id, is_expense) de la estructura del modelo "Flow" para el registro de transacciones. 


Version 0.2.2
==================
Fecha: 2016 Noviembre 9

- Se agrega parametros de texto traducidos a español de la funcionalidad del campo de tipo calendario.
- Se agrega controlador de flujo de transacciones. 
- Se inicia el formulario de creación de transacciones.

Version 0.2.1
==================
Fecha: 2016 Noviembre 8

- Se realiza una adecuación en el listado de Medios de pagos.

Version 0.2.0
==================
Fecha: 2016 Noviembre 4

- Se modifica un error que en la clase "Message" que no permitia mostrar el color de "error" en la alerta de error.
- Se agrega una nueva plantilla visual para los usuarios visitantes.
- Adecua la pantalla de login.
- Se cambia la versión de Bootstrap a la alfa v4.0.0-alpha.5. 
- Se cambia la versión de MDBootsrap (Material Design para Bootstrap) a la 4.2.0

Version 0.1.8
==================
Fecha: 2016 Noviembre 3

- Se adecua la cabecera y el menu de la interfaz a material Design. 
- Se agrega listado de medios de pagos.

Version 0.1.7
==================
Fecha: 2016 Noviembre 2

- Se termina el proceso de adición de un medio de pago.
- Se agrega sistema de validación de campos de formulario.

Version 0.1.6
==================
Fecha: 2016 Noviembre 1
- Se soporte para material design.
- Se agrega funcionalidad que permite indicar en pantalla un loader cuando se envia un formulario. 
- Se adecua el formualario de agregar medio de pago para material design.

Version 0.1.5
==================
Fecha: 2016 Octubre 31

- Se agrega campo para seleccion el banco asocicado al medio de pago.
- Se agrega clase cn el listado de bancos de Colombia.

Version 0.1.4
==================
Fecha: 2016 Octubre 28

- Se agrega parte del formulario para agregar medios de pago.
- Se agrega Capa de aplicación para cargar los parametros configuración del usuario.
- Se agrega controlador de gestión de medios de pago.

Version 0.1.3
==================
Fecha: 2016 Octubre 26

- Se agrega estructura de clases que definen los tipos de pagos.
- Se agrega clase que define el tipo de moneda.

Version 0.1.2
==================
Fecha: 2016 Octubre 25

- Se modifica la barra superior.
- Se agrega sección de menu en la parte derecha de la pantalla.
- Se elimina el modelo Product.
- Se modifica el modelo Typayment.

Version 0.1.1
==================
Fecha: 2016 Octubre 24

- Se implementa logotipo y favicon.
- Se cambia color de la barra de navegación superior.
- Se agrega funcionalidad que permite mostrar mensajes de tipo alerta y modal al usuario.