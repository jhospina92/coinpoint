<?php

return [
    "section.my.profile" => "Mi perfil",
    "profile.name" => "Nombre(s)",
    "profile.lastname" => "Apellido(s)",
    "section.security" => "Seguridad",
    "section.security.password.current" => "Contraseña Actual",
    "section.security.password.new" => "Contraseña nueva",
    "section.security.password.repeat" => "Repetir Contraseña nueva",
    "section.settings" => "Configuración",
    "section.settings.language" => "Idioma",
    "section.settings.currency" => "Moneda",
    "section.settings.currency.unify" => "Unificación de monedas",
    "section.settings.notf.flowcron.auto.email" => "Enviarme un correo electrónico cada vez que una transacción programada sea procesada de manera automatica.",
    "section.settings.notf.flowcron.confirm.email" => "Enviarme un correo electrónico como recordatorio cuando tengo que confirmar una transacción programada.",
    "section.settings.notf.flow.autoread.success.auto.email" => "Enviarme un correo electrónico cuando una transacción leido automaticamente sea procesada sin confirmar.",
    "section.settings.notf.flow.autoread.success.confirm.email" => "Enviarme un correo electrónico cuando deba confirmar una transacción leida automaticamente.",
    "section.settings.notf.flow.autoread.error.email" => "Enviarme un correo electrónico cuando se haya producido un fallo en la lectura automática de mis transacciones.",
    ];
