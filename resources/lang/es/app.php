<?php

return [
    "website.title" => "Myrald",
    "website.description" => "Control Financiero Inteligente",
    "website.copyright" => "Myrald Inc. &copy; %s - Todos los derechos reservados",
    "date.format.submit" => "yyyy/mm/dd",
    "date.format.text" => "dd mmmm, yyyy",
    "version" => "0.7.3",
    "error.500" => "<h1>¡OOUPS!</h1><h3>Algo ha salido mal en nuestro sistema</h3><p>Por favor ten paciencia, pronto lo solucionaremos. =)</p><p>Si el problema persiste, envíanos un correo electrónico ha <a href='mailto:myrald.inc@gmail.com'>myrald.inc@gmail.com</a></p>",
    "error.404.title" => "404 Pagina no encontrada",
    "error.404" => "<h1 class='big'>404</h1><h2>¡Esta página no está disponible!</h2><h3>Es posible que el enlace que has seguido esté roto o que se haya eliminado la página.</h3>",
    "error.400" => "<h1>¡OOUPS!</h1><h3>Algo ha salido mal</h3>",
];
