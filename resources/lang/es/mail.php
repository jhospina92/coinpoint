<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Activación de cuenta
      |--------------------------------------------------------------------------
     */

    "signup.activation.account.subject" => "¡Activa tu cuenta!",
    "signup.activation.account.description" => "
        <p>Hola %s:</p>
        <p>¡Gracias por registrarte en Myrald!</p>
        <p>Estas apunto de iniciar el proceso que te permitirá llevar el control de tus finanzas y mejorar tu calidad de vida. Activa tu cuenta haciendo clic en el siguiente botón:</p><p class='text-center'><a href='%s' class='btn-action'>Activar cuenta</a></p>",
    "gen.help" => "<p>Si tienes algún problema, comunícate con nosotros enviando un mensaje al correo electrónico <b>support@myrald.com</b> y encantados estaremos de ayudarte.</p>",
    "gen.terms" => "<p>Te enviamos este email como parte de tu suscripción en Myrald.com. No respondas a este email, ya que no podemos contestarte desde esta dirección.</p><p>Este mensaje se envió a %s de parte de Myrald.com.</p><p>El uso del servicio sujeto a nuestros <a target='_blank' href='#'>Términos y condiciones</a> y a nuestra <a target='_blank' href='#'>Politica de privacidad</a>.</p>",
    "gen.copyright" => "<p>Copyright Myrald Inc. © " . date("Y") . " - Todos los derechos reservados. <br/>Te encuestras suscrito a nuestro newsletter a través de <a target='_blank' class='hd' href='https://myrald.com'>Myrald.com</a></p>",
    /*
      |--------------------------------------------------------------------------
      | Recuperación de contraseña
      |--------------------------------------------------------------------------
     */
    "forgotten.subject" => "Recuperación de contraseña",
    "forgotten.description" => ""
    . "<p>Hola %s:</p>"
    . "<p>Hemos recibido una solicitud para restablecerla contraseña de tu cuenta de usuario que tienes con nosotros en Myrald.com. Si no has solicitado esto por favor ignora este mensaje.</p>"
    . "<p>Para reestablecer tu contraseña pulsa en el siguiente botón:</p>"
    . "<p class='text-center'><a href='%s' class='btn-action'>Reestablecer contraseña</a></p>",
    /*
      |--------------------------------------------------------------------------
      | Confirmar transacción programada
      |--------------------------------------------------------------------------
     */
    "flowcron.confirm.subject" => "Recordatorio de transacción | %s",
    "flowcron.confirm.header" => "Recordatorio de transacción<br/><span class='tag'>%s</span>",
    "flowcron.confirm.description" => ""
    . "<p>Hola %s:</p>"
    . "<p>Este mensaje es para recordarte que tienes una transacción programada para hoy de <b>%s</b>, por un valor estimado de <i><b>%s</b></i>.</p>"
    . "<p>A través de este correo puedes confirmar los datos de la transacción o iniciar sesión en <a href='" . url("") . "'>Myrald.com</a> y hacerlo directamente en la plataforma.</a></p>"
    . "<div class='form'>"
    . "<h2 class='text-center'>Datos de la transacción</h2><br/>"
    . "<p><b>Tipo de transacción:</b> <span class='%s'>%s</span></p>"
    . "<p><b>Categoria:</b> <span>%s</span></p>"
    . "<p><b>Medio de pago:</b> <span>%s</span></p>"
    . "<p><b>Descripción:</b> <span>%s</span></p>"
    . "<p><b>Valor:</b> <span>%s</span></p><br/>"
    . "<small>Pulsa en el botón para \"Procesar\" para ir a confirmar la transacción.</small>"
    . "<div><a class='btn-submit' href='%s'>Procesar</a></div></div>"
    . "<small>* Siempre puedes cambiarlo despúes.</small>",
    /*
      |--------------------------------------------------------------------------
      | Transacción ejecutada automaticamente
      |--------------------------------------------------------------------------
     */
    "flowcron.exec.subject" => "Transacción automática | %s",
    "flowcron.exec.header" => "Transacción automática<br/><span class='tag'>%s</span>",
    "flowcron.exec.description" => ""
    . "<p>Se ha realizado una transacción automática el día <b><i>%s</i></b> por un valor de <b>%s</b> por concepto de <b>%s</b> en tu cuenta de Myrald.</p>" .
    "<div class='form'>"
    . "<h2 class='text-center'>Detalles de la transacción</h2><br/>"
    . "<p><b>Tipo de transacción:</b> %s</p>"
    . "<p><b>Categoria:</b> <span>%s</span></p>"
    . "<p><b>Medio de pago:</b> <span>%s</span></p>"
    . "<p><b>Descripción:</b> <span>%s</span></p>"
    . "<p><b>Valor:</b> <span>%s</span></p></div>"
    . "<small>* Si no quieres recibir más esta notificación ingresa a tu cuenta en <a href='" . url("") . "'>Myrald.com</a> e ingresa a la sección de <b>Notificaciones</b> de la gestión de tu cuenta y desactiva la opción correspondiente.</small>"
    . "<small>* Si quieres eliminar esta transacción automática ingresa a tu cuenta y dirigete a la sección de <b>Transacciones Programadas</b></small>",
    /*
      |--------------------------------------------------------------------------
      | Autoread: Lectura Fallida
      |--------------------------------------------------------------------------
     */
    "autoread.failed.subject" => "Lectura de transacción fallida",
    "autoread.failed.description" => ""
    . "<p>Este mensaje es para infórmate que hemos intentado procesar un transacción leída desde tu correo electrónico <b>%s</b>, pero al parecer ha ocurrido un problema con el análisis de los datos.</p>" .
    "<div class='form'>"
    . "<h2 class='text-center'>Informe del problema</h2><br/>"
    . "<p><b>Descripción del error:</b> <span>%s</span></p>"
    . "<p><b>Tipo de transacción:</b> %s</p>"
    . "<p><b>Medio de pago:</b> %s</p>"
    . "<p><b>Fecha:</b> <span>%s</span></p></div>"
    . "<small>* Si no quieres recibir más esta notificación ingresa a tu cuenta en <a href='" . url("") . "'>Myrald.com</a> e ingresa a la sección de <b>Notificaciones</b> de la gestión de tu cuenta y desactiva la opción correspondiente.</small>",
    /*
      |--------------------------------------------------------------------------
      | Autoread: Lectura procesada - Confirmar
      |--------------------------------------------------------------------------
     */
    "autoread.success.confirm.subject" => "¡Hemos detectado una transacción!",
    "autoread.success.confirm.description" => ""
    . "<p>Hemos detectado una transacción en tu correo electrónico <b>%s</b> por valor de <b>%s</b>. Ingresa a tu cuenta de Myrald para confirmar la transacción.</p>"
    . "<div class='form'>"
    . "<h2 class='text-center'>Datos de la transacción</h2><br/>"
    . "<p><b>Tipo de transacción:</b> <span>%s</span></p>"
    . "<p><b>Medio de pago:</b> <span>%s</span></p>"
    . "<p><b>Descripción:</b> <span>%s</span></p>"
    . "<p><b>Valor:</b> <span>%s</span></p><br/>"
    . "<small>Pulsa en el botón \"Ingresar\" para ir a confirmar la transacción desde tu cuenta de Myrald.</small>"
    . "<div><a class='btn-submit' href='" . url("") . "'>Ingresar</a></div></div>"
    . "<small>* Si no quieres recibir más esta notificación ingresa a tu cuenta en <a href='" . url("") . "'>Myrald.com</a> e ingresa a la sección de <b>Notificaciones</b> de la gestión de tu cuenta y desactiva la opción correspondiente.</small>",
    /*
      |--------------------------------------------------------------------------
      | Autoread: Lectura procesada
      |--------------------------------------------------------------------------
     */
    "autoread.success.subject" => "¡Hemos procesado una transacción!",
    "autoread.success.description" => ""
    . "<p>Hemos procesado una transacción que fue leida automáticamente desde tu correo electrónico <b>%s</b> por valor de <b>%s</b>.</p>"
    . "<div class='form'>"
    . "<h2 class='text-center'>Datos de la transacción</h2><br/>"
    . "<p><b>Tipo de transacción:</b> <span>%s</span></p>"
    . "<p><b>Categoria:</b> <span>%s</span></p>"
    . "<p><b>Medio de pago:</b> <span>%s</span></p>"
    . "<p><b>Descripción:</b> <span>%s</span></p>"
    . "<p><b>Valor:</b> <span>%s</span></p><br/></div>"
    . "<small>* Si no quieres recibir más esta notificación ingresa a tu cuenta en <a href='" . url("") . "'>Myrald.com</a> e ingresa a la sección de <b>Notificaciones</b> de la gestión de tu cuenta y desactiva la opción correspondiente.</small>",
    //Descripción : Transferencia
    "autoread.success.exchange.description" => ""
    . "<p>Hemos procesado una transferencia de tu cuenta <b>%s</b> por valor de <b>%s<b>.</p>"
    . "<div class='form'>"
    . "<h2 class='text-center'>Datos de la transacción</h2><br/>"
    . "<p><b>Tipo de transacción:</b> <span>%s</span></p>"
    . "<p><b>Origen:</b> <span>%s</span></p>"
    . "<p><b>Destino:</b> <span>%s</span></p>"
    . "<p><b>Descripción:</b> <span>%s</span></p>"
    . "<p><b>Valor:</b> <span>%s</span></p><br/></div>"
    . "<small>* Esta transacción fue leida automáticamente desde tu correo electrónico <b>%s</b> que tienes asociada en tu cuenta de Myrald.</small>"
    . "<small>* Si no quieres recibir más esta notificación ingresa a tu cuenta en <a href='" . url("") . "'>Myrald.com</a> e ingresa a la sección de <b>Notificaciones</b> de la gestión de tu cuenta y desactiva la opción correspondiente.</small>",
];
