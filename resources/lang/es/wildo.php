<?php

use App\Models\Wildo\WReport;

return [
    "creditcard.date.cut" => "La fecha de recorte de tu tarjeta de credito %s esta pronto a llegar, te recomiendo que no realices ninguna compra con ella. Ya que si lo haces tendras que empezarla a pagar justo este mes.",
    "creditcard.date.pay" => "La fecha de pago de tu tarjeta de credito %s pronto llegara, te recomiendo que canceles la deuda que tienes."
];
