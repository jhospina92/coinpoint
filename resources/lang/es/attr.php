<?php

use App\Models\User;
use App\Base\System\TP\{
    Cash,
    CreditCard,
    BankAccount
};
use App\Base\Data\CategoryData;
use App\Models\CreditTrack;
use \App\Base\Data\Lang;
use App\Models\Flow;
use \App\Base\System\Services\ReaderEmail\DataParam;
use App\Models\Logs\LogAutoReadFailed;

$data = [
    "user.email" => "Correo electrónico",
    "user.password" => "Contraseña",
    "user.name" => "Nombre",
    "user.lastname" => "Apellidos",
    "flow.type.entry" => "Ingreso",
    "flow.type.spend" => "Gasto",
    "flow.type.investment" => "Inversión",
    "flow.type.exchange" => "Transferencia",
    //[0.6.4]
    "flow.type." . Flow::TYPE_SPEND => "Gasto",
    "flow.type." . Flow::TYPE_REVENUE => "Ingreso",
    "flow.type." . Flow::TYPE_EXCHANGE => "Transferencia",
];


$data = array_merge($data, [
    "typepayment.name." . Cash::ID => "Efectivo",
    "typepayment.name." . BankAccount::ID => "Cuenta bancaria",
    "typepayment.name." . CreditCard::ID => "Tarjeta de credito",
    "typepayment." . CreditCard::ID . ".field." . CreditCard::FIELD_DATE_DAY_CUT["name"] => "Día de facturación / corte",
    "typepayment." . CreditCard::ID . ".field." . CreditCard::FIELD_DATE_DAY_CUT["name"] . ".help" => "Corresponde al día en que el periodo de facturación de los gastos realizados son facturados por la entidad financiera.",
    "typepayment." . CreditCard::ID . ".field." . CreditCard::FIELD_DATE_DAY_PAY["name"] => "Día de pago",
    "typepayment." . CreditCard::ID . ".field." . CreditCard::FIELD_DATE_DAY_PAY["name"] . ".help" => "Corresponde al día maximo en el que debes pagar la factura mensual de los consumos realizados.",
    "typepayment." . CreditCard::ID . ".field." . CreditCard::FIELD_END_NUMBERS["name"] => "4 últimos números de la tarjeta",
    "typepayment." . CreditCard::ID . ".field." . CreditCard::FIELD_FRANCHISE["name"] => "Franquicia",
    "typepayment." . CreditCard::ID . ".field." . CreditCard::FIELD_FRANCHISE["name"] . ".help" => "Selecciona la franquicia internacional a la que pertenece tu tarjeta de credito.",
    "typepayment." . CreditCard::ID . ".field." . CreditCard::FIELD_END_NUMBERS["name"] => "4 últimos números de la tarjeta",
    "typepayment." . CreditCard::ID . ".field." . CreditCard::FIELD_DEFAULT_FEES["name"] => "Cuotas por defecto",
    "typepayment." . CreditCard::ID . ".field." . CreditCard::FIELD_DEFAULT_FEES["name"] . ".help" => "El banco siempre tiene asignado un número de cuotas por defecto asociada a una tarjeta de credito, si no conoces este valor consulta con tu banco, pero por lo general son 12 cuotas.",
        ]);

/*
  |--------------------------------------------------------------------------
  | CATEGORIAS - Impuestos
  |--------------------------------------------------------------------------
 */

$data = array_merge($data, [
    "category." . CategoryData::SP_TAXES => "Impuestos",
    "category." . CategoryData::SP_TAXES_VEHICLE => "Impuesto vehicular",
    "category." . CategoryData::SP_TAXES_BANK_FINANCIAL => "Impuestos bancarios y financieros"
        ]);


/*
  |--------------------------------------------------------------------------
  | CATEGORIAS - Vivienda
  |--------------------------------------------------------------------------
 */

$data = array_merge($data, [
    "category." . CategoryData::SP_HOME => "Vivienda",
    "category." . CategoryData::SP_HOME_RENT => "Arriendo",
    "category." . CategoryData::SP_HOME_SERVICE_ENERGY => "Servicio público - Energia Eléctrica",
    "category." . CategoryData::SP_HOME_SERVICE_WATER_SEWERAGE => "Servicio público - Agua/Alcantarillado",
    "category." . CategoryData::SP_HOME_SERVICE_CLEAN => "Servicio público - Aseo/Recolección de basuras",
    "category." . CategoryData::SP_HOME_SERVICE_GAS => "Servicio público - Gas domiciliario",
    "category." . CategoryData::SP_HOME_SERVICE_PACK_INTERNET_TELEVISION_PHONE => "Servicio de Internet/Televisión/Teléfonia Fija",
    "category." . CategoryData::SP_HOME_REFORMS_MANTENANCE_REPAIRS => "Reformas, Mantenimiento y Reparaciones",
    "category." . CategoryData::SP_HOME_FURNITURE_DECORATION => "Mobiliario y Decoración",
    "category." . CategoryData::SP_HOME_APPLIANCES => "Electródomesticos"
        ]);

/*
  |--------------------------------------------------------------------------
  | CATEGORIAS - Alimentación
  |--------------------------------------------------------------------------
 */

$data = array_merge($data, [
    "category." . CategoryData::SP_FOOD => "Alimentación",
    "category." . CategoryData::SP_FOOD_MARKET => "Mercado",
    "category." . CategoryData::SP_FOOD_JUNK => "Comida Chatarra/Chucheria",
    "category." . CategoryData::SP_FOOD_RESTAURANT => "Restaurante",
        ]);


/*
  |--------------------------------------------------------------------------
  | CATEGORIAS - Transporte
  |--------------------------------------------------------------------------
 */

$data = array_merge($data, [
    "category." . CategoryData::SP_TRANSPORT => "Transporte",
    "category." . CategoryData::SP_TRANSPORT_ARTICULATED_BUS => "Bus articulado",
    "category." . CategoryData::SP_TRANSPORT_URBAN_BUS => "Bus Urbano",
    "category." . CategoryData::SP_TRANSPORT_TAXI => "Taxi",
    "category." . CategoryData::SP_TRANSPORT_MANTENANCE_REPAIR_VEHICLE => "Mantenimiento/Reparación vehicular",
    "category." . CategoryData::SP_TRANSPORT_TICKET_AIRWAYS => "Pasaje de avión",
    "category." . CategoryData::SP_TRANSPORT_TICKET_TRAIN => "Pasaje de tren",
    "category." . CategoryData::SP_TRANSPORT_TICKET_SUBWAY => "Pasaje de metro",
    "category." . CategoryData::SP_TRANSPORT_TICKET_INTERSTATE_BUS => "Pasaje de bus interestatal",
    "category." . CategoryData::SP_TRANSPORT_PRIVATE_APP => "Servicio Privado a través de Aplicación Móvil",
    "category." . CategoryData::SP_TRANSPORT_BUY_VEHICLE => "Compra de vehiculo personal",
    "category." . CategoryData::SP_TRANSPORT_FUEL => "Gasolina/Combustible",
    "category." . CategoryData::SP_TRANSPORT_VEHICLE_INSURANCE => "Seguro de vehiculo",
    "category." . CategoryData::SP_TRANSPORT_VEHICLE_INFRACTIONS => "Multas e infracciones de transito",
    "category." . CategoryData::SP_TRANSPORT_VEHICLE_TOLL => "Peaje",
        ]);

/*
  |--------------------------------------------------------------------------
  | CATEGORIAS - Diversión y Ocio
  |--------------------------------------------------------------------------
 */



$data = array_merge($data, [
    "category." . CategoryData::SP_FUN => "Diversión y Ocio",
    "category." . CategoryData::SP_FUN_CINEMA_TEATHER_CONCERT => "Salida a Cine/Teatro/Concierto",
    "category." . CategoryData::SP_FUN_MUSEUM_EXPOSITION => "Salida a Museo/Exposiciones",
    "category." . CategoryData::SP_FUN_SPORT_EVENT => "Salida a Evento deportivo",
    "category." . CategoryData::SP_FUN_BOOK_MAGZ_NEWSLETTER => "Adquisición de libros/revista/periodicos/comics",
    "category." . CategoryData::SP_FUN_VIDEOGAME => "Adquisición de consola/videojuegos ",
    "category." . CategoryData::SP_FUN_MUSIC => "Adquisición de canción/música/melodia",
    "category." . CategoryData::SP_FUN_APP => "Compra/Suscripción de aplicación móvil/web/destop",
    "category." . CategoryData::SP_FUN_SUSCRIPTION_MEDIA => "Suscripción a plataforma multimedia",
    "category." . CategoryData::SP_FUN_TRAVEL => "Viaje/Hotel/Turismo",
    "category." . CategoryData::SP_FUN_DANCE_BAR => "Salida a comer/bar/bailar",
    "category." . CategoryData::SP_FUN_TOYS => "Juguetes/Figuras/Colecciones",
        ]);


/*
  |--------------------------------------------------------------------------
  | CATEGORIAS - Vestuario
  |--------------------------------------------------------------------------
 */


$data = array_merge($data, [
    "category." . CategoryData::SP_LOCKER => "Vestuario",
    "category." . CategoryData::SP_LOCKER_SPORT => "Ropa Deportiva",
    "category." . CategoryData::SP_LOCKER_CASUAL => "Ropa Casual",
    "category." . CategoryData::SP_LOCKER_KIDS => "Ropa infantil o para niños",
    "category." . CategoryData::SP_LOCKER_ELEGANT => "Ropa Elegante",
    "category." . CategoryData::SP_LOCKER_UNDERWEAR => "Ropa Interior",
    "category." . CategoryData::SP_LOCKER_SHOES => "Zapatos",
    "category." . CategoryData::SP_LOCKER_PROPS => "Complementos y Accesorios",
    //Deprecated
    "category.LOK-HEAD" => "Vestimenta de Cabeza",
    "category.LOK-TORS" => "Vestimenta de Torso",
    "category.LOK-LEGS" => "Vestimenta de Piernas",
    "category.LOK-FOOT" => "Vestimenta de Pies",
        ]);


/*
  |--------------------------------------------------------------------------
  | CATEGORIAS - Educación
  |--------------------------------------------------------------------------
 */


$data = array_merge($data, [
    "category." . CategoryData::SP_EDUCATION => "Educación",
    "category." . CategoryData::SP_EDUCATION_BUY_BOOKS => "Compra de libros",
    "category." . CategoryData::SP_EDUCATION_COURSES_CERTIFICATIONS => "Cursos/Certificaciones",
    "category." . CategoryData::SP_EDUCATION_CONFERENCES => "Conferencias/Seminarios",
    "category." . CategoryData::SP_EDUCATION_PAY_ENROLLMENT => "Pago de matricula",
    "category." . CategoryData::SP_EDUCATION_MATERIALS_IMPLEMENTS => "Papeleria, materiales e implementos",
    "category." . CategoryData::SP_EDUCATION_MONTHLY => "Mensualidad escolar/educativa"
        ]);

/*
  |--------------------------------------------------------------------------
  | CATEGORIAS - Salud
  |--------------------------------------------------------------------------
 */


$data = array_merge($data, [
    "category." . CategoryData::SP_HEALTH => "Salud",
    "category." . CategoryData::SP_HEALTH_INSURANCE => "Seguro médico",
    "category." . CategoryData::SP_HEALTH_CONSULTATION => "Consulta médica",
    "category." . CategoryData::SP_HEALTH_MEDICATIONS => "Medicamentos/Inyeccions/Vacunas/Cremas",
    "category." . CategoryData::SP_HEALTH_THERAPIES => "Tratamientos/Terapias/Hospitalizaciones",
    "category." . CategoryData::SP_HEALTH_SURGERY => "Cirujias",
    "category." . CategoryData::SP_HEALTH_BEATY => "Estética y belleza",
    "category." . CategoryData::SP_HEALTH_OPTOMETRY => "Optometria/Óptica",
    "category." . CategoryData::SP_HEALTH_ODONTOLOGY => "Odontología",
    "category." . CategoryData::SP_HEALTH_ORTOPEDY => "Ortopedia",
    "category." . CategoryData::SP_HEALTH_AUDIOLOGY => "Audiologia",
    "category." . CategoryData::SP_HEALTH_ALTERNATIVE => "Médicina Alternativa",
    "category." . CategoryData::SP_HEALTH_IMPLEMENTS => "Implementos médicos",
        ]);

/*
  |--------------------------------------------------------------------------
  | CATEGORIAS - Productos y Servicios
  |--------------------------------------------------------------------------
 */


$data = array_merge($data, [
    "category." . CategoryData::SP_SERVICES => "Productos y servicios complementarios",
    "category." . CategoryData::SP_SERVICES_INSURANCES => "Seguros",
    "category." . CategoryData::SP_SERVICES_MOBILE => "Teléfonia Móvil/Celular",
    "category." . CategoryData::SP_SERVICES_LOTTERY => "Loterias y Apuestas",
    "category." . CategoryData::SP_SERVICES_COMMISSIONS_INTERESTS => "Comisiones e Intereses",
    "category." . CategoryData::SP_SERVICES_BANKS => "Servicios Bancarios",
    "category." . CategoryData::SP_SERVICES_SOFTWARE => "Software",
    "category." . CategoryData::SP_SERVICES_LEGALS => "Servicios Legales",
    "category." . CategoryData::SP_SERVICES_NOTARIALS => "Servicios Notariales",
    "category." . CategoryData::SP_SERVICES_PAPERS => "Papeleria",
        ]);

/*
  |--------------------------------------------------------------------------
  | CATEGORIAS - Donaciones/Regalos/Contribuciones
  |--------------------------------------------------------------------------
 */


$data = array_merge($data, [
    "category." . CategoryData::SP_DONATIONS => "Donaciones/Regalos/Contribuciones",
    "category." . CategoryData::SP_DONATIONS_CHARITY => "Caridad",
    "category." . CategoryData::SP_DONATIONS_CONTRIBUTION_FAMILY => "Contribución familiar",
    "category." . CategoryData::SP_DONATIONS_CONTRIBUTION_COUPLE => "Contribución matrimonial/sentimental",
    "category." . CategoryData::SP_DONATIONS_GIFS => "Regalo de Cumpleaños/Navidad/Aniversario/Especial",
    "category." . CategoryData::SP_DONATIONS_HELP_SUSTENANCE => "Ayuda de sostenimiento",
    "category." . CategoryData::SP_DONATIONS_COURTESY => "Cortesia",
        ]);

/*
  |--------------------------------------------------------------------------
  | CATEGORIAS - Empleo formal
  |--------------------------------------------------------------------------
 */

$data = array_merge($data, [
    "category." . CategoryData::EN_JOB => "Empleo",
    "category." . CategoryData::EN_JOB_SALARY => "Salario",
    "category." . CategoryData::EN_JOB_BONUS => "Bonos",
    "category." . CategoryData::EN_JOB_PERFOMANCE => "Prestaciones",
        ]);

/*
  |--------------------------------------------------------------------------
  | CATEGORIAS - Actividad Profesional
  |--------------------------------------------------------------------------
 */

$data = array_merge($data, [
    "category." . CategoryData::EN_PROFESSIONAL => "Actividades Profesionales",
    "category." . CategoryData::EN_PROFESSIONAL_FEE => "Honorarios",
    "category." . CategoryData::EN_PROFESSIONAL_COMMISSIONS => "Comisiones",
        ]);

/*
  |--------------------------------------------------------------------------
  | CATEGORIAS - Actividades Empresariales e Inversiones
  |--------------------------------------------------------------------------
 */

$data = array_merge($data, [
    "category." . CategoryData::EN_COMPANY => "Actividades Empresariales e Inversiones",
    "category." . CategoryData::EN_COMPANY_DIVIDENS => "Dividendos",
    "category." . CategoryData::EN_COMPANY_COMMISSIONS => "Comisiones",
    "category." . CategoryData::EN_COMPANY_SETTLEMENT => "Liquidaciones",
        ]);


/*
  |--------------------------------------------------------------------------
  | CATEGORIAS - Liquidación de bienes
  |--------------------------------------------------------------------------
 */

$data = array_merge($data, [
    "category." . CategoryData::EN_PROPERTIES => "Propiedades de Finca Raiz",
    "category." . CategoryData::EN_PROPERTIES_SALE => "Venta",
    "category." . CategoryData::EN_PROPERTIES_RENTAL => "Arrendamiento",
        ]);

/*
  |--------------------------------------------------------------------------
  | CATEGORIAS - Liquidación de bienes
  |--------------------------------------------------------------------------
 */

$data = array_merge($data, [
    "category." . CategoryData::EN_SETTLEMENT => "Liquidación de bienes",
    "category." . CategoryData::EN_SETTLEMENT_FURNITE => "Mobiliario",
    "category." . CategoryData::EN_SETTLEMENT_HOME_APPLIANCE => "Electródomesticos",
    "category." . CategoryData::EN_SETTLEMENT_CLOTHING => "Vestimenta",
    "category." . CategoryData::EN_SETTLEMENT_VEHICLES => "Vehiculos",
    "category." . CategoryData::EN_SETTLEMENT_OTHERS => "Otros",
        ]);


/*
  |--------------------------------------------------------------------------
  | CATEGORIAS - Otros
  |--------------------------------------------------------------------------
 */

$data = array_merge($data, [
    "category." . CategoryData::EN_OTHERS => "Otros",
    "category." . CategoryData::EN_OTHERS_AWARDS => "Obtención de premio",
    "category." . CategoryData::EN_OTHERS_INTERESTS => "Intereses",
    "category." . CategoryData::EN_OTHERS_GIFT => "Regalo",
    "category." . CategoryData::EXT_CREDITS_CARD => "Tarjetas de credito",
    "category." . CategoryData::EXT_CREDITS_TRACK => "Pago de prestamos",
        ]);


/*
  |--------------------------------------------------------------------------
  | Caracteriticas - Credito
  |--------------------------------------------------------------------------
 */

$data = array_merge($data, [
    "credit.features.1.name" => "% Ratio Interes anual",
    "credit.features.2.name" => "Dia del mes de pago de la cuota"
        ]);

/*
  |--------------------------------------------------------------------------
  | Tipos - Credit Track
  |--------------------------------------------------------------------------
 */

$data = array_merge($data, [
    "credit.track.type." . CreditTrack::TYPE_CURRENT_INTEREST => "Intereses corrientes",
    "credit.track.type." . CreditTrack::TYPE_ARREARS_INTEREST => "Intereses de mora",
    "credit.track.type." . CreditTrack::TYPE_FEE => "Cuota",
        ]);


/*
  |--------------------------------------------------------------------------
  | IDIOMAS
  |--------------------------------------------------------------------------
 */

$data = array_merge($data, [
    "lang." . Lang::SPANISH => "Español",
        //"lang." . Lang::ENGLISH => "Ingles",
        ]);

/*
  |--------------------------------------------------------------------------
  | PARAMETROS DE LECTURA
  |--------------------------------------------------------------------------
 */

$data = array_merge($data, [
    "reader.email.param." . DataParam::PARAM_FROM => "Correo electrónico de origen",
    "reader.email.param." . DataParam::PARAM_FROM . ".help" => "Especifica la cuenta de correo electrónico de donde provienen los mensajes de cada una de las transacciones que realizas con tu medio de pago.",
    "reader.email.param." . DataParam::PARAM_INPUT_SUBJECT => "Asunto del mensaje",
    "reader.email.param." . DataParam::PARAM_INPUT_SUBJECT . ".help" => "Especifica el asunto que recibes cuando te llega un mensaje al correo electrónico.",
    "reader.email.param." . DataParam::AUTOPARAM_INPUT_DESCRIPTION_FORMAT => "Formato de entrada de la descripción",
    "reader.email.param." . DataParam::AUTOPARAM_INPUT_DESCRIPTION_FORMAT . ".help" => '<p>Especifica la parte del contenido donde se encuentra la descripción de la transaccón realizada con el medio de pago. Los mensajes recibidos siempre contiene algun tipo de descripción del establecimiento donde fue realizada la transacción. Aquí se debe especificar el fragmento que contiene dicha descripción, por ejemplo si el mensaje contiene algo como esto:</p><p><b>"...realizaste compra en establecimiento(GOOGLE *Play ), con la tarjeta..."</b></p><p>Entonces debera poner en el campo el siguiente texto:</p><div class="bg-inverse text-white">realizaste compra en establecimiento(<b>XYZ</b>), con la tarjeta</div><br/><p>Indicando con el texto <b>XYZ</b> donde se encuentre la descripción de la transacción.</p>',
        ]);

/*
  |--------------------------------------------------------------------------
  | MENSAJES DE ERROR
  |--------------------------------------------------------------------------
 */

$data = array_merge($data, [
    "log.autoread.error." . LogAutoReadFailed::ERROR_DATE_NOT_FOUND => "La fecha no pudo ser leida",
    "log.autoread.error." . LogAutoReadFailed::ERROR_DESCRIPTION_FORMAT_INVALID => "El formato de identificación de la descripción no pudo ser generada correctamente.",
    "log.autoread.error." . LogAutoReadFailed::ERROR_DESCRIPTION_NOT_FOUND => "La descripción de la transacción no pudo ser leida",
    "log.autoread.error." . LogAutoReadFailed::ERROR_HOUR_NOT_FOUND => "La hora no pudo ser leida",
    "log.autoread.error." . LogAutoReadFailed::ERROR_VALUE_NOT_FOUND => "El valor de la transacción no pudo ser leida",
    "log.autoread.error." . LogAutoReadFailed::ERROR_VALUE_NOT_IDENTIFIED => "El valor de la transacción no pudo ser identificada correctamente",
    "log.autoread.error." . LogAutoReadFailed::ERROR_TYPAYMENT_CASH_NOT_EXITS => "El medio de pago \"Efectivo\" no existe",
        ]);

return $data;

