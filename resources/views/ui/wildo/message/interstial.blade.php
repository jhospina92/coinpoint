<?php

use App\Models\Wildo\WReport;
?>

<li id="wildo-interstial" title="{{trans('ui.label.ask.help.wildo')}}">
    <img id="wildo" draggable="false" src="{{URL::to("assets/images/app/wildo/st.png")}}" @if(Session::has("wildo.report.type")==WReport::TYPE_INTERSTIAL) data-placement="right" data-custom-class="cloud" data-original-title="{{Session::get("wildo.report.message")}}" @endif>
</li>
