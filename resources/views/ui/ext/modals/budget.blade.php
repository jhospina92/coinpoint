<?php

use App\Models\Budget;
use App\Base\System\Library\Comp\DateUtil;
use App\Base\Data\CategoryData;
use App\Base\Data\Currency;

//Obtiene la diferencia en meses del mes actual al mes de inicio del presupuesto
$count = $budget->getCountMonthsValidity();
$revenues = $budget->data_revenue;
$spends = $budget->data_spend;
$date = new DateUtil($budget->{Budget::ATTR_DATE_START});
$total_revenue = $budget->getTotalRevenue() * $count;
$total_spend = $budget->getTotalSpend() * $count;
$total_revenue_current = 0;
$total_spend_current = 0;
?>
<div class="modal fade in" id="modal-budget" role="dialog" aria-labelledby="modal-message" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{$budget->name}}</h4>
            </div>
            <div class="modal-body">
                
                <h3>{{trans("ui.label.budget.period")}}</h3>
                {!!WebComponent::invoke(WebComponent::AS_BUDGET_PROGRESS_TIME)!!}
                
                <h3>{{trans("ui.label.revenue")}} </h3>
                <ul class="list-group categories revenue">
                    @foreach($revenues as $catmain => $data)
                    <?php
                    $total_category = $budget->getTotalCategoryMain($catmain) * $count;
                    $total_category_current = 0;
                    for ($i = 0; $i < $count; $i++) {
                        $total_category_current += $budget->getRealValueCategory($catmain, $date->year, $date->month);
                        $date->addMonths(1);
                    }

                    $total_revenue_current += $total_category_current;

                    $date->subtractMonth($count);

                    $progress_current = ($total_revenue > 0) ? ($total_category_current / $total_category) * 100 : 0;
                    $progress_budget = ($total_revenue > 0) ? (($total_category / $total_revenue) * 100) : 0;
                    ?>
                    <li class="list-group-item {{($progress_current>$progress_budget)?"success":"fail"}}">
                        <h6>{{CategoryData::getName($catmain)}} <span class="tag bg-revenue">{!!Currency::getFormattedValue($total_category_current, $budget->currency)!!}</span></h6>
                        <span class="tag bg-primary pull-right">{!!Currency::getFormattedValue($total_category, $budget->currency)!!}</span>
                        <div class="progress-coin">
                            <div class="progress-bar progress-current {{($progress_current>0)?"active":""}}" style="width:{{($progress_current>100)?"100":$progress_current}}%;{{($progress_current<$progress_budget)?"z-index:2;":""}}"></div>
                            <div class="progress-bar progress-budget" style="width:100%"></div>
                        </div>
                    </li>
                    @endforeach
                </ul>
                </br>
                <!-- SPEND -->
                <h3>{{trans("ui.label.spend")}} </h3>
                <ul class="list-group categories spend">
                    @foreach($spends as $catmain => $data)
                    <?php
                    $total_category = $budget->getTotalCategoryMain($catmain) * $count;
                    $total_category_current = 0;
                    for ($i = 0; $i < $count; $i++) {
                        $total_category_current += $budget->getRealValueCategory($catmain, $date->year, $date->month);
                        $date->addMonths(1);
                    }
                    $total_spend_current += $total_category_current;
                    $date->subtractMonth($count);

    
                    $progress_current = ($total_spend > 0) ? ($total_category_current / $total_category) * 100 : 0;
                    ?>
                    <li class="list-group-item {{($progress_current>$progress_budget)?"success":"fail"}}" data-toggle="tooltip" data-placement="left" title="" data-custom-class="spend" data-original-title="{!!strip_tags(Currency::getFormattedValue($total_category_current, $budget->currency))!!}">
                        <h6>{{CategoryData::getName($catmain)}} <span class="tag {{(($total_category-$total_category_current)>0)?"bg-revenue":"bg-spend"}}">{!!Currency::getFormattedValue($total_category-$total_category_current, $budget->currency)!!}</span></h6>
                        <span class="tag bg-primary pull-right">{!!Currency::getFormattedValue($total_category, $budget->currency)!!}</span>
                        <div class="progress-coin">
                            <div class="progress-bar progress-current {{($progress_current>0)?"active":""}}" style="width:{{($progress_current>100)?"100":$progress_current}}%;{{($progress_current<$progress_budget)?"z-index:2;":""}}"></div>
                            <div class="progress-bar progress-budget" style="width:100%"></div>
                        </div>
                    </li>
                    @endforeach
                </ul>
                <br/>
                <div class="row group-visual-data">
                    <div class="col-md-6 bg-revenue">
                        <h5>{{sprintf(trans("ui.label.valf.expected"),trans("ui.label.revenue"))}}</h5>
                        <span>{!!Currency::getFormattedValue($total_revenue, $budget->currency)!!}</span>
                    </div>
                    <div class="col-md-6 bg-revenue">
                        <h5>{{sprintf(trans("ui.label.valf.obtained"),trans("ui.label.revenue"))}}</h5>
                        <span>{!!Currency::getFormattedValue($total_revenue_current, $budget->currency)!!}</span>
                    </div>
                </div>
                <div class="row group-visual-data">
                    <div class="col-md-6 bg-spend">
                        <h5>{{sprintf(trans("ui.label.valf.expected"),trans("ui.label.spend"))}}</h5>
                        <span>{!!Currency::getFormattedValue($total_spend, $budget->currency)!!}</span>
                    </div>
                    <div class="col-md-6 bg-spend">
                        <h5>{{sprintf(trans("ui.label.valf.obtained"),trans("ui.label.spend"))}}</h5>
                        <span>{!!Currency::getFormattedValue($total_spend_current, $budget->currency)!!}</span>
                    </div>
                </div>
                <div class="row group-visual-data">
                    <div class="col-md-6 bg-saving">
                        <h5>{{sprintf(trans("ui.label.valf.expected"),trans("ui.label.saving"))}}</h5>
                        <span>{!!Currency::getFormattedValue($total_revenue-$total_spend, $budget->currency)!!}</span>
                    </div>
                    <div class="col-md-6 bg-saving">
                        <h5>{{sprintf(trans("ui.label.valf.obtained"),trans("ui.label.saving"))}}</h5>
                        <span>{!!Currency::getFormattedValue($total_revenue_current-$total_spend_current, $budget->currency)!!}</span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default ml-auto" data-dismiss="modal">{{trans("ui.label.close")}}</button>
            </div>
        </div>
    </div>
</div>

