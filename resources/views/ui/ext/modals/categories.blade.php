<?php

use App\Base\Data\CategoryData;
use App\Models\Flow;
use App\Base\System\TP\CreditCard;

$categories = [array("ID" => Flow::TYPE_SPEND, "list" => CategoryData::getSpendList()), array("ID" => Flow::TYPE_REVENUE, "list" => CategoryData::getEntryList())];
$credits = Auth::user()->getCurrentCredits();
?>

<!-- Modal -->
<div class="modal fade" id="modal-category" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"><i class="fa fa-tags" aria-hidden="true"></i> {{trans("ui.label.select.a.category")}}</h4>
            </div>
            <!--Body-->
            <div class="modal-body">
                @foreach($categories as $type => $cats)
                <div class="row list-categories" id="categories-{{$cats["ID"]}}">
                    <div class="col-md-6">
                        <ul class="list-group list-group-hover">
                            @foreach($cats["list"] as $code => $info)
                            <li class="list-group-item" data-code="{{$code}}">{{$info["name"]}}</li>
                            @endforeach

                            @if(Flow::TYPE_SPEND==$cats["ID"])
                                @if(count($credits)>0)
                                <li class="list-group-item" data-code="{{CategoryData::EXT_CREDITS_TRACK}}"><i class="fa fa-gg-circle" aria-hidden="true"></i> {{trans("ui.label.credit")}}</li>
                                @endif
                            
                                @if(Auth::user()->typayments()->where("type",CreditCard::ID)->count()>0)
                                <li class="list-group-item" data-code="{{CategoryData::EXT_CREDITS_CARD}}"><i class="fa fa-credit-card" aria-hidden="true"></i> {{trans("ui.label.credits.card")}}</li>
                                @endif
                            @endif


                        </ul>
                    </div>
                    <div class="col-md-6">
                        @foreach($cats["list"] as $code => $info)
                        <ul class="list-group list-group-hover list-subcat" id="subcat-{{$code}}" style="display: none;">

                            @foreach($info["sub"] as $codesub => $name)
                            <li class="list-group-item" data-code="{{$codesub}}">{{$name}}</li>
                            @endforeach
                        </ul>
                        @endforeach

                        <!-- CREDITS TRACK -->
                        <ul class="list-group list-group-hover list-subcat" id="subcat-{{CategoryData::EXT_CREDITS_TRACK}}" style="display: none;">

                            @foreach($credits as $credit)
                            <li class="list-group-item" data-code="{{CategoryData::EXT_CREDITS_TRACK}}-{{$credit->id}}"><span class="tag bg-info label-pill float-xs-right tag-currency" style="margin-left:5px;">{{$credit->currency}}</span> {{trans("ui.label.fee")}} {{$credit->organization}}</li>
                            @endforeach
                        </ul>

                        <!-- CREDITS CARD -->
                        <ul class="list-group list-group-hover list-subcat" id="subcat-{{CategoryData::EXT_CREDITS_CARD}}" style="display: none;">
                            @foreach(Auth::user()->typayments as $typayment)
                            @if($typayment->getObjectType()::ID==CreditCard::ID)
                            <li class="list-group-item" data-code="{{CategoryData::EXT_CREDITS_CARD}}-{{$typayment->id}}"> <span class="tag bg-info label-pill float-xs-right tag-currency" style="margin-left:5px;">{{$typayment->getObjectType()->getCurrency()->getCode()}}</span><span class="tag bg-primary label-pill float-xs-right">{{$typayment->getObjectType()->getBankName()}}</span> {{trans("ui.label.fee")}}</li>
                            @endif
                            @endforeach
                        </ul>


                    </div>
                </div>
                @endforeach

            </div>
            <!--Footer-->
            <div class="modal-footer">
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
<!-- /.Live preview-->
