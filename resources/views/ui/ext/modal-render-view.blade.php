<div class="modal fade in" id="ui-modal-render-view" role="dialog" aria-labelledby="modal-message" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="{{trans("ui.label.close")}}"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" data-btn="close">{{trans("ui.label.close")}}</button>
            </div>
        </div>
    </div>
</div>