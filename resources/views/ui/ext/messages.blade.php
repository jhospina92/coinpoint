<!--
  |--------------------------------------------------------------------------
  |  MODAL MESSAGE
  |--------------------------------------------------------------------------
 -->

@if(Session::has("ui.message.modal.message"))
<div class="modal fade in" id="ui-message-modal" role="dialog" aria-labelledby="modal-message" aria-hidden="true">
    <div class="modal-dialog modal-{!!Session::get("ui.message.modal.size")!!}" role="document">
         <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">{{Session::get("ui.message.modal.title")}}</h4>
                                </div>
                                <div class="modal-body">
                                    {!!Session::get("ui.message.modal.message")!!}
                                </div>
                                @if(!empty(Session::get("ui.message.modal.footer")))
                                <div class="modal-footer">
                                    {!!Session::get("ui.message.modal.footer")!!}
                                </div>
                                @endif
                            </div>
                        </div>
    </div>

    <script>

        ready(function () {
            $("#ui-message-modal").modal("show");
        });

    </script>
    @endif


    <!--
      |--------------------------------------------------------------------------
      |  ALERT MESSAGE
      |--------------------------------------------------------------------------
     -->

    @if(Session::has("ui.message.alert.message"))

    <div id="ui-message-alert" class="alert alert-{{Session::get("ui.message.alert.type")}} alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {!!Session::get("ui.message.alert.message")!!}
    </div>

    @endif


    <!--
      |--------------------------------------------------------------------------
      |  CONFIRM DIALOG
      |--------------------------------------------------------------------------
     -->


    <div class="modal fade" id="dialog-confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <h5 class="text-center"></h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="cancel btn btn-default" data-dismiss="modal">{{trans("ui.label.cancel")}}</button>
                    <button type="button" class="accept btn btn-success">{{trans("ui.label.accept")}}</button>
                </div>
            </div>
        </div>
    </div>
