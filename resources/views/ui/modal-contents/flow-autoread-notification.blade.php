<?php

use App\Models\Flow;
use App\Models\Typayment;
use App\Base\System\Library\Comp\DateUtil;
use App\Base\Data\Currency;
use App\Models\Auto\AutoRead;
?>

 {!!trans("ui.label.flow.autoread.notify.modal.description")!!}

<div class="row">
    <div class="col-md-12">
        
        
        <table class="table table-bordered" id="table-aut-log">
            <thead class="thead-inverse">
                <tr>
                    <th>{{trans("ui.label.typayment")}}</th>
                    <th>{{trans("ui.label.type")}}</th>
                    <th>{{trans("ui.label.date")}}</th>
                    <th>{{trans("ui.label.description")}}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($logs as $log)
                @php
                $data=$log->data;
                $typayment=Typayment::find($data["typayment"])->getObjectType();
                $autoread_client=AutoRead::find($log->autoread_id)->getClientObject();
                $email=$autoread_client->getProfileEmail();
                @endphp

                @if(in_array($email,$emails))
                <tr>
                    <td colspan="6" class="text-center" style="background: #656565;color: white;">{{$email}}</td>
                </tr>
                @php
                unset($emails[array_search($email,$emails)]);
                @endphp
                @endif

                <tr id="aut-log-{{$log->id}}-1" class="tr-log init">
                    <td>{!!$typayment->getFullName()!!}</td>
                    <td class="color-{{$data["type"]}}">{!!Flow::getUtilTypeName($data["type"])!!}</td>
                    <td>{!!(new DateUtil($data["date"]))->getString("d m Y")!!}</td>
                    <td>{!!$data["description"]!!}</td>
                    <td class="text-center" rowspan="2" style="height: 100%;">
                        <div class="btn-group flex-center">
                            <label class="btn btn-danger btn-flowaut-skip" title="{{trans("ui.label.skip")}}" data-id="{{$log->id}}"><i class="fa fa-remove"></i></label>
                            <label class="btn btn-success btn-flowaut-confirm" title="{{trans("ui.label.confirm")}}" data-id="{{$log->id}}"><i class="fa fa-check"></i></label>
                        </div>
                    </td>
                </tr>
                <tr id="aut-log-{{$log->id}}-2" class="tr-log end">
                    <td colspan="2" style="position: relative;">
                        <div class="md-form" style="margin-bottom: 0px;">
                            <input type="search" id="input-aut-category-log-{{$log->id}}" class="form-control mdb-autocomplete aut-flow-input-category" data-type="{{$data["type"]}}" data-id="{{$log->id}}">
                            <button class="mdb-autocomplete-clear">
                                <svg fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="https://www.w3.org/2000/svg">
                                <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z" />
                                <path d="M0 0h24v24H0z" fill="none" />
                                </svg>
                            </button>
                            <label for="input-aut-category-log-{{$log->id}}" class="active">{{trans("ui.label.category")}}</label>
                        </div>
                        <div class="tag-cat-main" style="display: none;"></div>
                    </td>
                    <td class="align-middle" colspan="2" style="font-size: 1.3em;text-align: center;">{!!Currency::getFormattedValue($data["value"],$typayment->getCurrency()->getCode())!!}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<script>
    var url_ajax_aut_confirm = "{{route(RouteData::AUTOREAD_AJAX_LOG_CONFIRM)}}";
    var url_ajax_aut_skip = "{{route(RouteData::AUTOREAD_AJAX_LOG_SKIP)}}";
    var msg_flow_aut_confirm = "{{trans('msg.autoread.modal.notf.confirm.success')}}";
    var msg_flow_aut_skip = "{{trans('msg.autoread.modal.notf.skip')}}";
    var msg_flow_aut_error_input_cat = "{{trans('msg.autoread.modal.error.input.cat')}}";
</script>

{{ HTML::script('assets/js/app/main/flow/autoread-notify.js') }}

