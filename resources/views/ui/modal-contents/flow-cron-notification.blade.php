<?php

use App\Models\Flow;

if (empty($transaction)) {
    $transaction_cron->delete();
    ?>
    <script>
        ready(function () {
            $('#ui-message-modal').modal("hide");
        });
    </script>
    <?php
    return;
}
?>

<form action="" method="ajax" id="form-fc-not">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-md-12">
            <div class="btn-group" data-toggle="buttons" style="width: 100%;">
                @if($transaction->type==Flow::TYPE_SPEND)
                <label class="btn btn-danger active" style="width: 100%;">
                    <input type="radio" name="type" id="type-spend" checked="checked" value="{{Flow::TYPE_SPEND}}">  {{trans("attr.flow.type.spend")}}
                </label>
                @endif
                
                @if($transaction->type==Flow::TYPE_REVENUE)
                <label class="btn btn-success" style="width: 100%;" checked="checked">
                    <input type="radio" name="type" id="type-entry" value="{{Flow::TYPE_REVENUE}}"> {{trans("attr.flow.type.entry")}}
                </label>
                @endif
            </div>
        </div>
    </div>
    <br/>
    <br/>
    <div class="row">
        <div class="col-md-6">
            <div class="md-form input-group">
                <i class="prefix"><i class="fa fa-tags"></i></i>
                <input type="text" class="form-control" data-required="text" readonly="readonly" value="{{$transaction->getCategoryName()}}">
            </div>
        </div>
        <div class="col-md-6">
            <div class="md-form">
                <select class="mdb-select" disabled="disabled">
                    <option>{{$transaction->typayment->getObjectType()->getFullName()}}</option>
                </select>
                <label>{{trans("ui.label.typayment")}}</label>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="md-form">
                <input id="fc-input-description" type="text" name="description" class="form-control" value="{{$transaction->description}}" maxlength="100">
                <label for="fc-input-description">{{trans("ui.label.description")}}</label>
            </div>
        </div>
        <div class="col-md-6">
            <div class="md-form">
                <i class="prefix currency-symbol">{{$transaction->typayment->getObjectType()->getCurrency()->getSymbol()}}</i>
                <input id="fc-input-value" data-error-attr="0" type="text" data-required="balance" name="value" class="form-control input-currency" value="{{$transaction->value}}" maxlength="20">
                <label for="fc-input-value">{{trans("ui.label.value")}}</label>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-center">
            <button class="btn btn-danger btn-destroy" type="button" data-msg="{{trans("nav.route.flow.cron.delete.confirm.exec")}}" data-action="{{URL::route(RouteData::FLOW_CRON_DELETE,$cron->id)}}" data-msg-success="{{trans("nav.route.flow.cron.delete.confirm.exec.success")}}" data-tag="#ui-message-modal"><i class="fa fa-ban" aria-hidden="true"></i> {{trans("ui.label.cancel")}}</button>
            <button class="btn btn-deep-purple" type="submit" data-func-success="fc_defer_success" data-action="{{URL::route(RouteData::FLOW_CRON_DEFER,$cron->id)}}" id="btn-flow-cron-defer"><span class="fa fa-clock-o"></span> {{trans("ui.label.postpone.tomorrow")}}</button>
            <button class="btn btn-primary" type="submit" data-func-success="fc_exec_success" data-action="{{URL::route(RouteData::FLOW_CRON_EXEC,$cron->id)}}" id="btn-flow-cron-exec"><span class="fa fa-send-o"></span> {{trans("ui.label.register")}}</button>
        </div>
    </div>

</form>
<script>

    var msg_flow_cron_defer_success = "{{trans('ui.msg.flow.cron.defer.success')}}";
    var msg_flow_cron_exec_success = "{{trans('ui.msg.flow.cron.exec.success')}}"

    ready(function () {
        $("#fc-input-value").keyup();
    });
</script>

{{ HTML::script('assets/js/app/main/flow/cron.js') }}

