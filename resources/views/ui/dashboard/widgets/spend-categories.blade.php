<?php

use App\Base\Data\CategoryData;
use App\Models\Flow;
use App\Base\Data\Currency;
use Illuminate\Support\Facades\Auth;

$cucu = config("app.setting.currency.code") ?? Auth::user()->getMetaCurrency();
?>
<div class="card dashaboard-widget">
    <h3 class="card-header danger-color white-text">{{trans("ui.label.categories")}}</h3>
    <div class="card-block">
        <ul class="list-group categories">
            @foreach($spend["categories"]["month"]["current"] as $code => $value)
            @php 
            $porcent_current=number_format(($spend["month"]["current"]>0)?($value*100)/$spend["month"]["current"]:0,2);
            @endphp
            <li class="list-group-item" data-toggle="tooltip" data-placement="left" title="{{($revenue["month"]["current"]>0)?number_format(($value*100)/$revenue["month"]["current"],2):0}}%" data-custom-class="spend">
                @if(CategoryData::EXT_CREDITS_CARD==$code)
                <span class="tag bg-inverse label-pill float-xs-right">{!!Currency::getFormattedValue(Flow::getCategoryValue(CategoryData::EXT_CREDITS_CARD), $cucu)!!}</span>
                @else
                <span class="tag bg-danger label-pill float-xs-right">{!!Currency::getFormattedValue($value, $cucu)!!}</span>
                @endif
                {{CategoryData::getName($code)}}

                <progress class="progress progress-danger" value="{{$porcent_current}}" max="100">
                    <div class="progress">
                        <span class="progress-bar" style="width: {{$porcent_current}}%;">{{$porcent_current}}%</span>
                    </div>
                </progress>
            </li>
            @endforeach
        </ul>
    </div>
</div>