<?php

use App\Base\Data\Currency;
use App\Base\System\Library\Comp\DateUtil;
?> 
@if(count(explode(",", $graph["labels"]))>1)
<div class="card dashaboard-widget">
    <h3 class="card-header bg-coinpoint white-text">{{trans("ui.label.flow.growth")}}</h3>
    <div class="card-block">
        <div class="graph" data-type="line" data-labels="{{$graph["labels"]}}" data-cols='{{trans("ui.label.date.month")}},{{trans("ui.label.patrimony")}}'>          
            <item data-values="{{$graph["data"]["growth"]}}" data-attr-fillColor="#00bb9b" data-attr-pointColor="#00bb9b" data-attr-strokeColor="#00bb9b"></data>
        </div>
    </div>
</div>
@endif