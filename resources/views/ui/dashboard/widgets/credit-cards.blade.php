<?php

use App\Base\Data\Currency;
use App\Base\System\Library\Comp\DateUtil;

$is_unify = config("app.setting.currency.unify");

$date_current = new DateUtil();
?>
@if(isset($typayments))
<div class="card dashaboard-widget">
    <!-- USO DE LAS TARJETAS DE CREDITO -->
    <h3 class="card-header  white-text">{{trans("ui.label.credits.card")}}</h3>
    <div class="card-block">
        @foreach($typayments["creditcard"] as $creditcard)
        @php 
        $object=$creditcard->getObjectType(); 
        $progress=($object->getBalance()/$object->getQuota())*100;
        @endphp
        <div class="row credit-card">
            <div class="col-sm-12">{{$object->getBankName()}}</div>
            <div class="col-sm-12">
                <div class="balance">
                    @if($is_unify)
                    {!!Currency::getFormattedValue($object->getBalance(), config("app.setting.currency.code"))!!}
                    @else
                    {!!Currency::getFormattedValue($object->getBalance(), $object->getCurrency()->getCode())!!}
                    @endif
                </div>
                <progress class="progress progress-striped" value="{{$progress}}" max="100">
                    <div class="progress">
                        <span class="progress-bar" style="width: {{$progress}}%;">{{$progress}}%</span>
                    </div>
                </progress>
                <div class="quota">
                    @if($is_unify)
                    {!!Currency::getFormattedValue($object->getQuota(), config("app.setting.currency.code"))!!}
                    @else
                    {!!Currency::getFormattedValue($object->getQuota(), $object->getCurrency()->getCode())!!}
                    @endif
                </div>
                <div class="row">
                    @if(!empty($object->getDateDayCut()))
                    @php
                    $date=new DateUtil(date("Y-m-".$object->getDateDayCut())." 00:00:00");

                    if(DateUtil::difSec($date,$date_current)>0)
                    $date->addMonths(1);

                    @endphp
                    <div class="col-md-6 date">
                        <i class="fa fa-calendar" aria-hidden="true"></i> {{trans("ui.label.date.cutoff")}} <span class="bg-info">{{$date->getDay()}} {{$date->getMonth()}} {{$date->getYear()}}</span>
                    </div>
                    @endif
                    @if(!empty($object->getDateDayPay()))
                    @php 

                    $date=new DateUtil(date("Y-m-".$object->getDateDayPay())." 00:00:00");
                    if(DateUtil::difSec($date,$date_current)>0)
                    $date->addDays(30);

                    @endphp
                    <div class="col-md-6 date">
                        <i class="fa fa-calendar" aria-hidden="true"></i> {{trans("ui.label.date.pay")}} <span class="bg-info">{{$date->getDay()}} {{$date->getMonth()}} {{$date->getYear()}}</span>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endif