@if(count(Auth::user()->goals)>0)
<div class="card dashboard-widget goals-widget">
    <h3 class="card-header bg-coinpoint white-text">{{trans("nav.route.goal.index")}}</h3>
    <div class="card-block">
        {!!WebComponent::invoke("goal-progress")!!}
    </div>
</div>

@endif