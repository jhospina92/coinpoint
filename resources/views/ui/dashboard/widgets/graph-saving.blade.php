<?php

use App\Base\Data\Currency;
use App\Base\System\Library\Comp\DateUtil;

$cucu = config("app.setting.currency.code") ?? Auth::user()->getMetaCurrency();
$active_budget = (!empty($budget = Auth::user()->getBudgetActive()) && DateUtil::difSec(new DateUtil(), $budget->date_start) < 0 && $budget->currency == $cucu);
?> 
<div class="card dashaboard-widget">
    <h3 class="card-header bg-saving white-text">{{trans("ui.label.saving")}}</h3>
    <div class="card-block">
        <h4 class="card-title">{{trans("ui.date.ui.current.month")}} <span class="tag bg-saving pull-right">{!!Currency::getFormattedValue($saving["month"]["current"], $currency)!!}</span></h4>
        <h4 class="card-title">{{trans("ui.date.ui.prev.month")}} <span class="tag bg-saving pull-right">{!!Currency::getFormattedValue($saving["month"]["previous"], $currency)!!}</span></h4>
        @if (count(explode(",", $graph["labels"]))>1)
        <div class="graph" data-type="line" data-labels="{{$graph["labels"]}}" data-cols='{{trans("ui.label.date.month")}},{{trans("ui.label.saving")}}{{($active_budget)?",".trans("ui.label.budgeted"):""}}'>

            <item data-values="{{$graph["data"]["saving"]}}" data-attr-fillColor="#062A64" data-attr-pointColor="#062A64" data-attr-strokeColor="#062A64" />

            @if($active_budget)
            <item data-values="{{$graph["data"]["waited"]["saving"]}}" data-attr-fillColor="#062a65" data-attr-strokeColor="#062a65"></item>
            @endif
        </div>
        @endif
    </div>
</div>