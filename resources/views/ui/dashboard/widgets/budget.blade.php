<?php

use App\Base\Data\Currency;
use App\Base\Data\CategoryData;
use App\Base\System\Library\Comp\DateUtil;

$cucu = config("app.setting.currency.code") ?? Auth::user()->getMetaCurrency();

?>
<!-- WIDGET BUDGET -->
@if(!empty($budget=Auth::user()->getBudgetActive()) && DateUtil::difSec(new DateUtil(),$budget->date_start)<0 && $budget->currency==$cucu)
<?php
$revenues = $budget->data_revenue;
$spends = $budget->data_spend;
$total_spend = $budget->getTotalSpend();
$total_revenue = $budget->getTotalRevenue();
$total_spend_month_current = $spend["month"]["current"];
$total_spend_not_budged = 0; //TOTAL Gasto no presupuestado
$total_revenue_not_budged = 0; //TOTAL Ingresos no presupuestado
$saving_projection = $budget->getSavingExpected();

?>
<div class="card dashaboard-widget budget">
    <h3 class="card-header white-text">{{trans("ui.label.budget")}} <span class="card-subtitle">{{$budget->name}}</span> <span class="pull-right btn-icon" data-toggle="modal" data-target="#modal-budget"><i class="fa fa-area-chart" aria-hidden="true"></i></span></h3>
    <div class="card-block">
        {!!WebComponent::invoke(WebComponent::AS_BUDGET_PROGRESS_TIME)!!}
        <h3>{{trans("ui.label.revenue")}} </h3>
        <ul class="list-group categories revenue">
            @foreach($revenues as $catmain => $data)
            @php
            $total_category=$budget->getTotalCategoryMain($catmain);
            $total_revenue_not_budged+=$total_category_current=$budget->getRealValueCategory($catmain);
            $progress_current=($total_category_current/$total_revenue)*100;
            $progress_budget=(($total_category/$total_revenue)*100);
            $total=$total_category-$total_category_current;

            @endphp
            <li class="list-group-item {{($progress_current>$progress_budget)?"success":"fail"}}">
                {!!($total_category_current>=$total_category)?"<div class='indicator success color-revenue'><i class='fa fa-check' aria-hidden='true'></i></div>":"<div class='indicator fail color-spend'><i class='fa fa-remove' aria-hidden='true'></i></div>"!!}
                {{CategoryData::getName($catmain)}}
                <span class="tag bg-revenue pull-right">{{($total<0)?"+":""}}{!!Currency::getFormattedValue(($total<0)?abs($total):$total, $cucu)!!}</span>
                <div class="progress-coin">
                    <div class="progress-bar progress-current {{($progress_current>0)?"active":""}}" style="width:{{($progress_current>100)?"100":$progress_current}}%;{{($progress_current<$progress_budget)?"z-index:2;":""}}"></div>
                    <div class="progress-bar progress-budget" style="width:{{$progress_budget}}%"></div>
                </div>
            </li>
            @endforeach

            @if(($revenue["month"]["current"]-$total_revenue_not_budged)>0)
            <!-- PRESUPUESTO NO DETERMINADO -->
            <li class="list-group-item undenfined">
                {{trans("ui.label.budget.undenfined")}}
                <span class="tag bg-revenue pull-right">+{!!Currency::getFormattedValue(($revenue["month"]["current"]-$total_revenue_not_budged), $cucu)!!}</span>
            </li>
            @endif

        </ul>
        <br/>
        <!-- SPEND -->
        <h3>{{trans("ui.label.spend")}} </h3>
        <ul class="list-group categories spend">
            @foreach($spends as $catmain => $data)
            @php
            $total_category=$budget->getTotalCategoryMain($catmain);
            $total_spend_not_budged+=$total_category_current=$budget->getRealValueCategory($catmain);
            $progress_current=($total_category_current/$total_spend)*100;
            $progress_budget=(($total_category/$total_spend)*100);
            $total_spend_not_budged+=$total_category_current;

            @endphp
            <li class="list-group-item {{($progress_current<$progress_budget)?"success":"fail"}}">
                {!!($total_category-$total_category_current>=0)?"<div class='indicator success color-revenue'><i class='fa fa-check' aria-hidden='true'></i></div>":"<div class='indicator fail color-spend'><i class='fa fa-remove' aria-hidden='true'></i></div>"!!}
                {{CategoryData::getName($catmain)}}
                <span class="tag bg-revenue pull-right">{!!Currency::getFormattedValue($total_category-$total_category_current, $cucu)!!}</span>
                <div class="progress-coin">
                    <div class="progress-bar progress-current {{($progress_current>0)?"active":""}}" style="width:{{($progress_current>100)?"100":$progress_current}}%;{{($progress_current<$progress_budget)?"z-index:2;":""}}"></div>
                    <div class="progress-bar progress-budget" style="width:{{$progress_budget}}%"></div>
                </div>
            </li>
            @endforeach

            @if(($spend["month"]["current"]-$total_spend_not_budged)>0)
            <!-- PRESUPUESTO NO DETERMINADO -->
            <li class="list-group-item undenfined">
                {{trans("ui.label.budget.undenfined")}}
                <span class="tag bg-spend pull-right">-{!!Currency::getFormattedValue(($spend["month"]["current"]-$total_spend_not_budged), $cucu)!!}</span>
            </li>
            @endif
        </ul>

        <div class="row">
            <div class="col-md-12"><h3>{{trans("ui.label.balance.alt")}}</h3></div>
            <div class="col-md-12" id='balance-budget'>
                <ul class="list-group">
                    <li href="#" class="list-group-item">
                        <h4 class="list-group-item-heading">{{trans("ui.label.revenue")}}</h4>
                        <span class="list-group-item-text color-revenue">{!!Currency::getFormattedValue($revenue["month"]["current"], $currency)!!}<small> / {!!Currency::getFormattedValue($budget->getTotalRevenue(),$cucu)!!}</small></span>
                    </li>
                    <li href="#" class="list-group-item">
                        <h4 class="list-group-item-heading">{{trans("ui.label.spend")}} <span class="tag bg-spend label-pill float-xs-right">{{number_format((($revenue["month"]["current"]>0)?($spend["month"]["current"]/$revenue["month"]["current"]):0)*100,2)}}%<small> / {{($budget->getTotalRevenue()>0)?number_format(($budget->getTotalSpend()/$budget->getTotalRevenue())*100,2):0}}%</small></span></h4>
                        <span class="list-group-item-text color-spend">{!!Currency::getFormattedValue($spend["month"]["current"], $currency)!!}<small> / {!!Currency::getFormattedValue($budget->getTotalSpend(),$cucu)!!}</small></span>
                    </li>
                    <li href="#" class="list-group-item">
                        <h4 class="list-group-item-heading">{{trans("ui.label.saving")}} <span class="tag bg-saving bg-primary label-pill float-xs-right">@if($spend["month"]["current"]>0){{number_format((($revenue["month"]["current"]-$spend["month"]["current"])/$spend["month"]["current"])*100,2)}}%<small> / {{($budget->getTotalRevenue()>0)?number_format(($budget->balance/$budget->getTotalRevenue())*100,2):0}}@else{{"0.00"}}@endif%</small></span></h4>
                        <span class="list-group-item-text color-saving">{!!Currency::getFormattedValue(($revenue["month"]["current"]-$spend["month"]["current"]),$cucu)!!}<small> / {!!Currency::getFormattedValue($budget->balance,$cucu)!!}</small></span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row" style="margin-bottom: 30px;">
            <div class="col-md-12 text-center"><h3>{{trans("ui.label.saving.expected")}}</h3></div>
        </div>
        <div id="expected-saving" class="bg-saving" data-toggle="tooltip" data-placement="left" title="" data-custom-class="saving" data-original-title="{{($budget->getTotalRevenue()>0)?number_format(((($budget->getTotalRevenue()-$saving_projection-$total_spend_month_current)/$budget->getTotalRevenue())*100),2):0}}%">
            {!!Currency::getFormattedValue(($saving_projection),$cucu)!!}
        </div>
    </div>
</div>

@include("ui/ext/modals/budget")

@endif