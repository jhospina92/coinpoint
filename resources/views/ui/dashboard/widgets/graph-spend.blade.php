<?php

use App\Base\Data\Currency;
use App\Base\System\Library\Comp\DateUtil;

$cucu = config("app.setting.currency.code") ?? Auth::user()->getMetaCurrency();
$active_budget = (!empty($budget = Auth::user()->getBudgetActive()) && DateUtil::difSec(new DateUtil(), $budget->date_start) < 0 && $budget->currency == $cucu);
?>
<div class="card dashaboard-widget">
    <h3 class="card-header danger-color white-text">{{trans("ui.label.spend")}}</h3>
    <div class="card-block">
        <h4 class="card-title">{{trans("ui.date.ui.current.month")}} <span class="tag red pull-right">{!!Currency::getFormattedValue($spend["month"]["current"], $currency)!!}</span></h4>
        <h4 class="card-title">{{trans("ui.date.ui.prev.month")}} <span class="tag red pull-right">{!!Currency::getFormattedValue($spend["month"]["previous"], $currency)!!}</span></h4>
        @if (count(explode(",", $graph["labels"]))>1)
        <div class="graph" data-type="line" data-labels="{{$graph["labels"]}}" data-cols='{{trans("ui.label.date.month")}},{{trans("ui.label.spend")}}{{($active_budget)?",".trans("ui.label.budgeted"):""}}'>
            <item data-values="{{$graph["data"]["spend"]}}" data-attr-fillColor="#f44" data-attr-pointColor="#f44" data-attr-strokeColor="#f44"></item>
            @if($active_budget)
            <item data-values="{{$graph["data"]["waited"]["spend"]}}" data-attr-fillColor="#ff8989" ddata-attr-strokeColor="#ff8989"></item>
            @endif
        </div>
        @endif
    </div>
</div>