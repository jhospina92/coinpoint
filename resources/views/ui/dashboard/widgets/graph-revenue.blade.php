<?php

use App\Base\Data\Currency;
use App\Base\System\Library\Comp\DateUtil;

$cucu = config("app.setting.currency.code") ?? Auth::user()->getMetaCurrency();
$active_budget = (!empty($budget = Auth::user()->getBudgetActive()) && DateUtil::difSec(new DateUtil(), $budget->date_start) < 0 && $budget->currency == $cucu);
?> 
<div class="card dashaboard-widget">
    <h3 class="card-header success-color white-text">{{trans("ui.label.revenue")}}</h3>
    <div class="card-block">
        <h4 class="card-title">{{trans("ui.date.ui.current.month")}} <span class="tag success-color pull-right">{!!Currency::getFormattedValue($revenue["month"]["current"], $currency)!!}</span></h4>
        <h4 class="card-title">{{trans("ui.date.ui.prev.month")}} <span class="tag success-color pull-right">{!!Currency::getFormattedValue($revenue["month"]["previous"], $currency)!!}</span></h4>
        @if (count(explode(",", $graph["labels"]))>1)
        <div class="graph" data-type="line" data-labels="{{$graph["labels"]}}" data-cols='{{trans("ui.label.date.month")}},{{trans("ui.label.revenue")}}{{($active_budget)?",".trans("ui.label.budgeted"):""}}'>
            <item data-values="{{$graph["data"]["revenue"]}}" data-attr-fillColor="#00C851" data-attr-pointColor="#00C851" data-attr-strokeColor="#00C851"></item>
                @if($active_budget)
                <item data-values="{{$graph["data"]["waited"]["revenue"]}}" data-attr-fillColor="#71dd9d" data-attr-strokeColor="#71dd9d"></item>
                @endif      
        </div>
        @endif
    </div>
</div>