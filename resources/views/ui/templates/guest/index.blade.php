@include("ui/templates/guest/header")

<section class="container-fluid container">


    @if ($__env->yieldContent('content-box'))
    <div class="container-fluid card">
        @yield("content-box")
    </div>
    @endif

    @if ($__env->yieldContent('content'))
    @yield("content")
    @endif

</section>

@include("ui/templates/guest/footer")