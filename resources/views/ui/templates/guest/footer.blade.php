<footer class="text-center">
    {!!sprintf(trans("app.website.copyright"),date("Y"))!!}
</footer>


{{-- jQuery (necessary for Bootstraps JavaScript plugins) --}}
{{ HTML::script('assets/ext/jquery/jquery-3.1.1.min.js') }}
{{ HTML::script('assets/js/app/extJQuery.js') }}

{{ HTML::script('assets/ext/md/js/tether.min.js') }}

{{-- Include all compiled plugins (below), or include individual files as needed --}}
{{ HTML::script('assets/ext/bootstrap/js/bootstrap.min.js') }}

{{ HTML::script('assets/js/templates/guest/theme.js') }}
{{ HTML::script('assets/js/templates/share.js') }}

{{ HTML::script('assets/ext/md/js/mdb.min.js') }}


<script>
var app=<?php echo json_encode(AppTemplate::getDataUI()); ?>;
var sg_rse = "{{URL::route(RouteData::AUTH_AJAX_RESEND_EMAIL_CONFIRM)}}";
</script>

{{--OTROS SCRIPTS--}}
@yield("script")



</body>

</html>

