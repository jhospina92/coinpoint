<!DOCTYPE html>
<html lang="{{App::getLocale()}}">
    <head>
        <title>@yield("website.title",AppTemplate::getTitle())</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name='language' content='{{App::getLocale()}}' />
        <meta http-equiv='content-language' content='{{App::getLocale()}}' />
        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
        <meta name="author" content="Ospisoft Technologies LLC">
        <meta name='owner' content="Ospisoft Technologies LLC" />
        <meta name='copyright' content="Ospisoft Technologies LLC - All Rights Reserved" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,100,300,500">

        {{-- Bootstrap --}}
        {{ HTML::style('assets/ext/bootstrap/css/bootstrap.min.css', array('media' => 'screen')) }}
        {{ HTML::style('assets/ext/font-awesome/css/font-awesome.min.css', array('media' => 'screen')) }}

        {{-- Material Design --}}
        {{ HTML::style('assets/ext/md/css/mdb.min.css', array('media' => 'screen')) }}

        {{ HTML::style('assets/css/templates/guest/reset.css', array('media' => 'screen')) }}

        {{ HTML::style('assets/css/templates/guest/style.css', array('media' => 'screen')) }}

        {{ HTML::style('assets/css/templates/shared.css', array('media' => 'screen')) }}

        {{ HTML::script('assets/js/app/functions.js') }}

        <link rel="shortcut icon" href="{{URL::to("assets/images/app/favicon.png")}}">


        <script>
            var app_token = "{{ csrf_token() }}";
            var app_url_site = "{{URL::to('/')}}";
            var message_modal_id = "#ui-message-modal";
        </script>



        <script src='https://www.google.com/recaptcha/api.js'></script>

    </head>
    <body class="fixed-sn coinpoint-skin" id="page-{{str_replace(".","-",(!is_null(Request::route()))?Request::route()->getName():"")}}">
        <div id="overlay-bg"></div>

