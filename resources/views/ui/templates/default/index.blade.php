@include("ui/templates/default/header")

@include("ui/templates/default/nav/bar-nav")

<!--Main layout-->
<main>

    @include("ui/ext/messages")

    @if ($__env->yieldContent('content-box'))
    <div id="container-main" class="container-fluid card card-block">
        @yield("content-box")
    </div>
    @endif

    @if ($__env->yieldContent('content'))
    @yield("content")
    @endif

</main>
<!--/Main layout-->

@include("ui/ext/modal-render-view")

@include("ui/templates/default/footer")