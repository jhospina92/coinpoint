

{{-- jQuery (necessary for Bootstraps JavaScript plugins) --}}
{{ HTML::script('assets/ext/jquery/jquery-3.1.1.min.js') }}
{{ HTML::script('assets/js/app/extJQuery.js') }}

{{ HTML::script('assets/ext/md/js/tether.min.js') }}

{{-- Include all compiled plugins (below), or include individual files as needed --}}
{{ HTML::script('assets/ext/bootstrap/js/bootstrap.min.js') }}

{{ HTML::script('assets/ext/md/js/mdb.min.js') }}

{{ HTML::script('assets/js/templates/default/theme.js') }}
{{ HTML::script('assets/js/templates/share.js') }}

{{ HTML::script('assets/js/app/modules/DatePickerMonth.js') }}

<!-- WILDO -->

{{ HTML::script('assets/app/wildo/message/interstial/script.js') }}

{{--OTROS SCRIPTS--}}
@yield("script")

<div id="sound"></div>
<script>
    var app = {!!Session::get('app.setting')!!};
</script>

{!!WebComponent::invoke("page-loader")!!}



</body>

</html>

