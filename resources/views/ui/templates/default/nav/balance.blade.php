<?php

use App\Base\Data\Currency;
use App\Base\System\TP\CreditCard;
use App\Base\System\Library\Comp\Util;

$balance["patrimony"] = 0;
$balance["credit"] = 0;
$typayments = Auth::user()->typayments;

$app = json_decode(Session::get("app.setting"), true);
$is_unify = config("app.setting.currency.unify");
$curcur = config("app.setting.currency.code");

$list = array();
$list["patrimony"] = array();
$list["credit"] = array();

/*
  |--------------------------------------------------------------------------
  | BALANCE DE MEDIOS DE PAGOS
  |--------------------------------------------------------------------------
 */
foreach ($typayments as $typayment) {
    $object = $typayment->getObjectType();
    //Tarjeta de credito

    $name = $object->getName();

    if ($is_unify && $curcur != $object->getCurrency()->getCode()) {
        $name .= " (" . $object->getCurrency()->getCode() . ")";
    }

    if ($object::ID == CreditCard::ID) {
        $coup = intval(json_decode($typayment["features"], true)["balance"]);

        if ($typayment->getBalance() == 0) {
            continue;
        }

        $value = $typayment->getBalance();
        $balance["credit"] += $value;

        $bankname = $object->getBankName();

        if ($is_unify && $curcur != $object->getCurrency()->getCode()) {
            $bankname .= " (" . $object->getCurrency()->getCode() . ")";
        }

        $list["credit"][] = array("name" => $name, "balance" => $typayment->getBalance(true), "bank" => $bankname);
        continue;
    }
    $list["patrimony"][] = array("name" => $name, "balance" => $typayment->getBalance(true), "bank" => $object->getBankName());

    $balance["patrimony"] += $typayment->getBalance();
}

/*
  |--------------------------------------------------------------------------
  | BALANCE DE PRESTAMOS
  |--------------------------------------------------------------------------
 */


foreach (Auth::user()->credits as $credit) {

    $name = $credit->name;
    $org = $credit->organization;
    $value = $credit->balance;

    if ($is_unify && $curcur != $credit->currency && isset($bankname)) {
        $bankname .= " (" . $credit->currency . ")";
    }

    if ($is_unify && $curcur != $credit->currency) {
        $org .= " (" . $credit->currency . ")";
    }

    $balance["credit"] += $credit->getBalance();
    $list["credit"][] = array("name" => $name, "balance" => $credit->getBalance(true), "bank" => $org);
}
?>

@if($balance["patrimony"]>0 || $is_unify)
<ul id="app-balance" class="collapsible app-balance collapsible-accordion">
    <li><a class="collapsible-header waves-effect arrow-r {{(!$is_unify)?"not-unify":""}}">{!!($is_unify)?Currency::getFormattedValue($balance["patrimony"],$app["currency"]["code"]):""!!} <label class="text-uppercase">{{trans("ui.label.patrimony")}}</label></a>
        <div class="collapsible-body">
            <ul>
                @foreach($list["patrimony"] as $index => $data)
                <li><a class="waves-effect" not-loader>{!!$data["balance"]!!}<label>{{$data["name"]}} {{(!is_null($data["bank"]))?" (".$data["bank"].")":""}}</label></a>
                </li>
                @endforeach
            </ul>
        </div>
    </li>
</ul>
@endif

@if($balance["credit"]>0)

<ul id="app-credit" class="collapsible app-balance collapsible-accordion">
    <li><a class="collapsible-header waves-effect arrow-r {{(!$is_unify)?"not-unify":""}}">{!!($is_unify)?Currency::getFormattedValue($balance["credit"],$app["currency"]["code"]):""!!} <label class="text-uppercase">{{trans("ui.label.credit.it")}}</label></a>
        <div class="collapsible-body">
            <ul>
                @foreach($list["credit"] as $index => $data)
                <li><a class="waves-effect" not-loader title="{{$data["name"]}} {{(!is_null($data["bank"]))?" (".$data["bank"].")":""}}">{!!$data["balance"]!!}<label>{{(!is_null($data["bank"]))?" ".$data["bank"]."":""}}</label></a>
                </li>
                @endforeach
            </ul>
        </div>
    </li>
</ul>

@endif

