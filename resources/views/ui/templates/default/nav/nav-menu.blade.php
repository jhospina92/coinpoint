<?php $nameroute = Request::route()->getName(); ?>      

<ul class="collapsible collapsible-accordion">
    @foreach(RouteData::getMenu() as $route => $item)
    <li {{($nameroute==$route)?"class=active":""}}><a href="{{URL::route($route)}}"><i class="{{$item["icon"]}}" aria-hidden="true"></i> {{$item["name"]}}</a></li>
    @endforeach
</ul>