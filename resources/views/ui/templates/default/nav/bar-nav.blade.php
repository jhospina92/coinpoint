<!--Double navigation-->
<header>
    <!-- Sidebar navigation -->
    <ul id="slide-out" class="side-nav fixed custom-scrollbar">

        <!-- Logo -->
        <li>
            <div class="logo-wrapper waves-light">
                <a href="#"><img src="{{URL::to("assets/images/app/brand-bar.png")}}" class="img-fluid flex-center"></a>
            </div>

            <div class="progress" id="progress-bar-g">
                <div class="indeterminate" style="width: 70%"></div>
            </div>
        </li>
        <!--/. Logo -->

        <!-- BALANCE -->
        <li>
            @if(Auth::check()) 
            @include("ui/templates/default/nav/balance")
            @endif
        </li>

        <!-- Side navigation links -->
        <li id="menu-main">
            @if(Auth::check()) 
            @include("ui/templates/default/nav/nav-menu")
            @endif
        </li>
        <!--/. Side navigation links -->

        <li id="mode-secure-content"><div class="switch" title="{{trans("ui.label.secure.mode")}}">
                <label>
                    <i class="fa fa-unlock {{(!Auth::user()->isModeSecure())?'active':''}}" aria-hidden="true"></i>
                    <input type="checkbox" id="mode-secure" {{(Auth::user()->isModeSecure())?"checked='checked'":''}}>
                    <span class="lever"></span>
                    <i class="fa fa-lock {{(Auth::user()->isModeSecure())?'active':''}}" aria-hidden="true"></i>
                </label>
            </div></li>
        <!--<li id="app-version">{{trans("app.version")}}</li>-->

        <!--@include("ui/wildo/message/interstial")-->

    </ul>
    <!--/. Sidebar navigation -->


    <!--Navbar-->
    <nav class="navbar navbar-fixed-top scrolling-navbar double-nav">

        <!-- SideNav slide-out button -->
        <div class="pull-left">
            <a href="#" data-activates="slide-out" class="button-collapse"><i class="fa fa-bars"></i></a>
        </div>



        <!-- Breadcrumb-->
        <div class="breadcrumb-dn">

            <div class="btn-group" id="btn-shortcut">
                <a class="btn-floating coinpoint" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-plus-circle"></i></a>
                <div class="dropdown-menu">
                    @foreach(RouteData::getShortcutes() as $route => $item)
                    @if(Request::route()->getName()!=$route)
                    <a class="dropdown-item"href="{{URL::route($route)}}"><i class="{{$item["icon"]}}" aria-hidden="true"></i> {{$item["name"]}}</a>
                    @endif
                    @endforeach
                </div>
            </div>

            <p> <!-- IA MESSAGE--></p>

        </div>

        <ul class="nav navbar-nav pull-right">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{Auth::user()->name}}</a>
                <div class="dropdown-menu dropdown-primary dd-right" aria-labelledby="dropdownMenu1" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                    <a class="dropdown-item" href="{{URL::route(RouteData::ACCOUNT_DASHBOARD)}}"><i class="fa fa-user" aria-hidden="true"></i> {{trans("nav.route.account.dashboard")}}</a>
                    <a class="dropdown-item" href="{{URL::route(RouteData::USER_LOGOUT)}}"><i class="fa fa-sign-out" aria-hidden="true"></i> {{trans("ui.label.logout")}}</a>
                </div>
            </li>
        </ul>

    </nav>
    <!--/.Navbar-->

</header>
<!--/Double navigation-->

