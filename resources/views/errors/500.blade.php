@extends("ui/templates/guest/index")

@section("content")
<div class="card card-block" id="http-error-message">
    <div class="col-md-12 text-center">
        {!!HTML::icon("restore")!!}
    </div>
    <div class="col-md-12 text-center">
        {!!trans("app.error.500")!!}
    </div>
</div>
@stop