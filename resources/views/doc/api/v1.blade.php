<!DOCTYPE html>
<html>
    <head>
        <title>Myrald API</title>
        <!-- needed for adaptive design -->
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" href="{{"/assets/images/app/favicon.png"}}">

        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
        <!--
        ReDoc doesn't change outer page styles
        -->
        <style>
            body {
                background: #00456C;
                margin: 0;
                padding: 0;
                font-family: 'Roboto Condensed', sans-serif;
            }

            .btn {
                font-size: 1em;
                text-decoration: none;
                padding: .85rem 2.13rem;
                border-radius: 2px;
                border: 0;
                color: #fff!important;
                margin: 6px;
                white-space: normal!important;
                background: #00ba9b;
                box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
            }
        </style>

    </head>
    <body>

        <div style="text-align: center;">
            <img src="assets/images/app/brand-full.png">
            <h1 style="color: white;font-size: 4em;">MYRALD API DOCUMENTATION</h1>
            <h2 style="color: white;font-size: 2em;">VERSION 1.0</h2>
            <br/>
            <br/>
            <a class="btn" href="https://bitbucket.org/jhospina92/coinpoint/wiki/API/v1">GO HERE!</a>
        </div>

    </body>
</html>