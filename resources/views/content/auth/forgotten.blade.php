@extends("ui/templates/guest/index")

@section("content")


<header>
    <img src="{{URL::to("assets/images/app/brand-full.png")}}" alt="{{trans('website.title')}} | {{trans('website.description')}}" title="{{trans('website.title')}} | {{trans('website.description')}}">
</header>

{!!AppTemplate::getState($view_name,array("form","confirm"))!!}

<div class="row">
    <div class="col-md-12 text-center" id="msg-register">
        <a href="{{URL::route("auth.login")}}" class="btn btn-coin-point">{{trans("ui.label.login")}}</a>
    </div>
</div>

@stop

@section("script")

{{ HTML::script('assets/js/app/modules/InputValidation.js') }}
{{ HTML::script('assets/js/templates/guest/main/auth/index.js') }}

@stop