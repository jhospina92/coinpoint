<div class="row">
    <br/>
    <div class="col-md-6 offset-md-3 card card-block">

        <h1 class="h1-responsive text-xs-center"><b>{{trans("msg.forgotten.confirm.title")}}</b></h1>

        <p class='text-center'>{!!sprintf(trans("msg.forgotten.confirm.description"),$_GET['email'])!!}</p>
    </div>
</div>

@include("ui/ext/messages")