<?php

use App\Base\System\Library\DataProviders\GCaptcha;
?>

<div class="row">
    <br/>
    <div class="col-md-6 offset-md-3 card card-block">

        @if(!env('APP_ENABLE_USER_REGISTER', true))
        <div class="alert alert-warning">
            {{trans("msg.signup.disabled")}}
        </div>
        @endif


        <h1 class="h1-responsive text-xs-center">{{trans("ui.label.register.you")}}</h1>

        @include("ui/ext/messages")

        {{Form::open(['route' => [RouteData::AUTH_SIGNUP],'method' => 'post',"class"=>"form-main"])}} 

        <div class="md-form">
            <input type="text" name="{{User::ATTR_NAME}}" id="name" value="{{old(User::ATTR_NAME)}}" class="form-control" validation="name"  maxlength="100">
            <label for="name">{{trans("attr.user.name")}}</label>
        </div>

        <div class="md-form">
            <input type="text" name="{{User::ATTR_LASTNAME}}" id="lastname" value="{{old(User::ATTR_LASTNAME)}}"class="form-control" validation="name"  maxlength="100">
            <label for="lastname">{{trans("attr.user.lastname")}}</label>
        </div>

        <div class="md-form">
            <i class="fa fa-envelope prefix"></i>
            <input type="email" name="{{User::ATTR_EMAIL}}" id="email" value="{{old(User::ATTR_EMAIL)}}"class="form-control" validation="email" maxlength="100">
            <label for="email">{{trans("attr.user.email")}}</label>
        </div>

        <div class="md-form">
            <i class="fa fa-lock prefix"></i>
            <input  type="password" name="{{User::ATTR_PASSWORD}}" id="password" class="form-control" validation="password" maxlength="50">
            <label for="password">{{trans("attr.user.password")}}</label>
        </div>    
        <div class="md-form">
            <i class="fa fa-lock prefix"></i>
            <input  type="password" name="{{User::ATTR_PASSWORD}}-repeat" id="password-repeat" class="form-control" validation="compare" data-compare="#password"  maxlength="50">
            <label for="password-repeat">{{ucfirst(trans("ui.label.repeat")." ".trans("attr.user.password"))}}</label>
        </div>    

        <div class="row">
            <div class="col-md-12">
                <fieldset class="form-group" style="margin-top: 17px;">
                    <input type="checkbox" name="terms" class="filled-in" id="terms" validation="check" data-msg-error="{{trans("msg.signup.accept.terms.error")}}">
                    <label for="terms">{!!trans("msg.signup.accept.terms")!!}</label>
                </fieldset>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-xs-center">
                <button id="subtn" type="submit" data-sitekey="{{GCaptcha::KEY_PUBLIC}}" data-callback="sg_onSubmit" class="btn btn-lg btn-block btn-primary center-block g-recaptcha">{{trans("ui.label.register.alt")}}</button>
            </div>
        </div>
        </form>  

        <div class="progress" id="progress-bar-g" style="margin-top: 1rem;margin-bottom: 0px;">
            <div class="indeterminate" style="width: 100%"></div>
        </div>

    </div>

</div>

