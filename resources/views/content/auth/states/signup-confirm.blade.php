<div class="row">
    <br/>
    <div class="col-md-6 offset-md-3 card card-block">

        <h1 class="h1-responsive text-xs-center"><b>{{trans("ui.label.welcome")}}</b></h1>

        {!!trans("msg.signup.confirm.pz.success")!!}
        <br/>
        <br/>
        <div class="text-xs-center">
            <a href="#" id="resend-mail" data-msg-title="{{trans("msg.signup.confirm.resent.link")}}" data-msg-content="{{trans("msg.signup.confirm.resent.link.content")}}" data-callback="sg_success_dialog_resend_email" data-btn-ok="{{trans('ui.label.resend')}}" open-dialog-confirm>{{trans("msg.signup.confirm.resent.link")}}</a>
        </div>
    </div>
</div>

@include("ui/ext/messages")