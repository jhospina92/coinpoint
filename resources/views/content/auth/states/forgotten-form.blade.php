<?php

use App\Base\System\Library\DataProviders\GCaptcha;
?>

<div class="row">
    <div class="col-md-6 offset-md-3 card card-block">
        <div class="progress" id="progress-bar-g">
            <div class="indeterminate" style="width: 100%"></div>
        </div>
        <h1 class="h1-responsive text-xs-center"><span class="glyphicon glyphicon-user"></span> {{trans("nav.route.".Request::route()->getName())}}</h1>

        <p>{{trans("msg.forggotten.description.default")}}</p>

        @include("ui/ext/messages")

        {{Form::open(['route' => [RouteData::AUTH_FORGOTTEN],'method' => 'post',"class"=>"form-main"])}} 

        <div class="md-form">
            <i class="fa fa-envelope prefix"></i>
            <input type="email" name="{{User::ATTR_EMAIL}}" id="email" value="{{old(User::ATTR_EMAIL)}}"class="form-control" validation="email" maxlength="100">
            <label for="email">{{trans("attr.user.email")}}</label>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-xs-center">
                <button id="subtn" type="submit" data-sitekey="{{GCaptcha::KEY_PUBLIC}}" data-callback="sg_onSubmit" class="btn btn-lg btn-block btn-primary center-block g-recaptcha">{{trans("ui.label.request")}}</button>
            </div>
        </div>
        </form>  
    </div>

</div>

