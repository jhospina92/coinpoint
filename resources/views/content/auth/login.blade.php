<?php

use App\Base\System\Library\DataProviders\GCaptcha;
?>

@extends("ui/templates/guest/index")

@section("content")

<header>
    <img src="{{URL::to("assets/images/app/brand-full.png")}}" alt="{{trans('website.title')}} | {{trans('website.description')}}" title="{{trans('website.title')}} | {{trans('website.description')}}">
</header>

<div class="row">
    <div class="col-md-6 offset-md-3 card card-block">


        <div class="progress" id="progress-bar-g">
            <div class="indeterminate" style="width: 100%"></div>
        </div>


        <h1 class="h1-responsive text-xs-center"><span class="glyphicon glyphicon-user"></span> {{trans("ui.label.join.account")}}</h1>


        @include("ui/ext/messages")

        <form action="" method="POST">
            {{ csrf_field() }}

            <!--Email validation-->
            <div class="md-form">
                <i class="fa fa-envelope prefix"></i>
                <input type="email" name="{{User::ATTR_EMAIL}}" id="email" value="{{old(User::ATTR_EMAIL)}}"class="form-control" validation="email">
                <label for="email" data-error="{{trans("ui.msg.error.email.invalid")}}" data-success="{{trans('ui.msg.suc.fine')}}">{{trans("attr.user.email")}}</label>
            </div>

            <div class="md-form">
                <i class="fa fa-lock prefix"></i>
                <input  type="password" name="{{User::ATTR_PASSWORD}}" id="password" class="form-control">
                <label for="password">{{trans("attr.user.password")}}</label>
            </div>      

            <div class="row"> 
                <div class="col-xs-6 col-sm-6 col-md-6 text-xs-left">
                    <fieldset class="form-group" style="margin-top: 17px;">
                        <input type="checkbox" name="remember" class="filled-in" id="remember" checked="checked">
                        <label for="remember">{{trans("ui.msg.remember")}}</label>
                    </fieldset>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 text-xs-right">
                    <button id="subtn" type="submit" data-sitekey="{{GCaptcha::KEY_PUBLIC}}" data-callback="sg_onSubmit" class="btn btn-lg btn-block btn-primary center-block g-recaptcha">{{trans("ui.label.join")}}</button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <br/>
                    <a href="{{URL::route(RouteData::AUTH_FORGOTTEN)}}">{{trans("ui.msg.remember.password.question")}}</a>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-center" id="msg-register">
        <h4>{{trans("ui.msg.singup.question")}}</h4>
        <a href="{{URL::route("auth.signup")}}" class="btn btn-coin-point">{{trans("ui.label.register.you")}}</a>
    </div>
</div>

@stop

@section("script")

{{ HTML::script('assets/js/app/modules/InputValidation.js') }}
{{ HTML::script('assets/js/templates/guest/main/auth/index.js') }}

@stop