@extends("ui/templates/guest/index")

@section("content")


<header>
    <img src="{{URL::to("assets/images/app/brand-full.png")}}" alt="{{trans('website.title')}} | {{trans('website.description')}}" title="{{trans('website.title')}} | {{trans('website.description')}}">
</header>

<div class="row">
    <div class="col-md-6 offset-md-3 card card-block">
        
        <div class="progress" id="progress-bar-g">
            <div class="indeterminate" style="width: 100%"></div>
        </div>

        <h1 class="h1-responsive text-xs-center"><span class="glyphicon glyphicon-user"></span> {{trans("nav.route.".Request::route()->getName())}}</h1>

        <p>{{trans("msg.forgotten.reset.token.description")}}</p>

        @include("ui/ext/messages")

        {{Form::open(['route' => [RouteData::AUTH_FORGOTTEN_RESET,$token],'method' => 'post',"class"=>"form-main"])}} 


        <div class="md-form">
            <input type="password" class="form-control" data-required="text" name="new" id="pwd-new" validation="password" maxlength="50">
            <label for="pwd-new">{{trans("ac.section.security.password.new")}}</label>
        </div>
        <div class="md-form">
            <input type="password" class="form-control"  name="repeat" data-compare="#pwd-new" id='pwd-repeat' validation="compare">
            <label for='pwd-repeat'>{{trans("ac.section.security.password.repeat")}}</label>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-xs-center">
                <input type='hidden' name='token' value='{{$token}}'>
                <button id="subtn" type="button" id='subtn' class="btn btn-lg btn-block btn-primary center-block">{{trans("ui.label.register")}}</button>
            </div>
        </div>
        </form>  
    </div>
</div>

@stop

@section("script")

{{ HTML::script('assets/js/app/modules/InputValidation.js') }}
{{ HTML::script('assets/js/templates/guest/main/auth/forgotten-reset.js') }}

@stop