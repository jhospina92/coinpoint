@extends("ui/templates/guest/index")

@section("content")


<header>
    <img src="{{URL::to("assets/images/app/brand-full.png")}}" alt="{{trans('website.title')}} | {{trans('website.description')}}" title="{{trans('website.title')}} | {{trans('website.description')}}">
</header>

<div class="row">
    <br/>
    <div class="col-md-6 offset-md-3 card card-block">

        <h1 class="h1-responsive text-xs-center"><b>{!!trans("msg.signup.title.account.has.active")!!}</b></h1>

        {!!sprintf(trans("msg.signup.confirm.welcome.success"),$user->name)!!}
        <br/>
        <br/>
        <div class="text-xs-center">
            <h3 class="text-center">{!!trans("msg.signup.title.start")!!}</h3>
            <a href="{{URL::route("auth.login")}}" class="btn btn-coin-point">{{trans("ui.label.login")}}</a>
        </div>

    </div>
</div>

@stop

@section("script")

@stop