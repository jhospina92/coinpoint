@extends("ui/templates/guest/index")

@section("content")


{!!AppTemplate::getState($view_name,array("form","confirm"))!!}

<div class="row">
    <div class="col-md-12 text-center" id="msg-register">
        <h4>{{trans("ui.msg.login.question")}}</h4>
        <a href="{{URL::route("auth.login")}}" class="btn btn-coin-point">{{trans("ui.label.login")}}</a>
    </div>
</div>


@stop

@section("script")

{{ HTML::script('assets/js/app/modules/InputValidation.js') }}
{{ HTML::script('assets/js/templates/guest/main/auth/index.js') }}

@stop