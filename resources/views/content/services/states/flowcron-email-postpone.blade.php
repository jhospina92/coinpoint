<?php

use App\Base\System\Library\Comp\DateUtil;

$date = new DateUtil(date(DateUtil::FORMAT_STANDARD, $_GET["dt"] ?? null));

?>
<div class="row">
    <div class="col-md-10 offset-md-1 card card-block">
        <br/>
        <h2 class="text-center"><i class="fa fa-check-circle" aria-hidden="true" style="color:mediumseagreen;"></i> {!!sprintf(trans("msg.services.flow.cron.confirm.email.postpone.success"),$date->getDay(),$date->getMonth(),$date->getYear())!!}</h2>
        <br/>
        <p class="text-center">{{trans("msg.services.flow.cron.confirm.email.postpone.success.subtitle")}}</p>
    </div>
</div>