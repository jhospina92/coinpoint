<?php
use App\Models\Flow;
use App\Base\System\Library\Comp\DateUtil;
?>

<div class="row">
    <div class="col-md-10 offset-md-1 card card-block">
        
        <h2 class="text-center">{{sprintf(trans("ui.label.cron.for.date"),(new DateUtil($cron->date_next))->getString("d m Y"))}}
            @if($transaction->type==Flow::TYPE_SPEND)<span class="badge red">{{$transaction->getTypeName()}}</span>@endif
            @if($transaction->type==Flow::TYPE_REVENUE)<span class="badge green">{{$transaction->getTypeName()}}</span>@endif
        </h2>
        
        <form action="" method="POST" class="form-main">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12">
                    <div class="btn-group pull-right" data-toggle="buttons">
                        <input type="hidden" name="type" value="{{$transaction->type}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="md-form input-group">
                        <i class="prefix"><i class="fa fa-tags"></i></i>
                        <input type="text" class="form-control" data-required="text" readonly="readonly" value="{{$transaction->getCategoryName()}}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="md-form">
                        <select class="mdb-select" disabled="disabled">
                            <option>{{$transaction->typayment->getObjectType()->getFullName()}}</option>
                        </select>
                        <label>{{trans("ui.label.typayment")}}</label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="md-form">
                        <input id="fc-input-description" type="text" name="description" class="form-control" value="{{$transaction->description}}" maxlength="100">
                        <label for="fc-input-description">{{trans("ui.label.description")}}</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="md-form">
                        <i class="prefix currency-symbol">{{$transaction->typayment->getObjectType()->getCurrency()->getSymbol()}}</i>
                        <input id="fc-input-value" data-error-attr="0" type="text" data-required="balance" name="value" class="form-control input-currency" value="{{$transaction->value}}" maxlength="20">
                        <label for="fc-input-value">{{trans("ui.label.value")}}</label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-center">
                    <button class="btn btn-deep-purple" type="submit" style="width: 45%;" onclick="javascript:form.action='?act=postpone';"><span class="fa fa-clock-o"></span> {{trans("ui.label.postpone.tomorrow")}}</button>
                    <button class="btn btn-primary" type="submit" style="width: 45%;" onclick="javascript:form.action='?act=register';"><span class="fa fa-send-o"></span> {{trans("ui.label.register")}}</button>
                </div>
            </div>

        </form>

    </div>
</div>
<div class="row">
    <div class="col-md-10 offset-md-1 text-center">
        <small style="color:gainsboro;">{!!sprintf(trans("msg.services.flow.cron.confirm.email.warning"),$email)!!}</small>
    </div>
</div>