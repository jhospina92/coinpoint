@extends("ui/templates/guest/index")

@section("content")


<header>
    <img style="max-width:300px;" src="{{URL::to("assets/images/app/brand-full.png")}}" alt="{{trans('website.title')}} | {{trans('website.description')}}" title="{{trans('website.title')}} | {{trans('website.description')}}">
</header>


{!!AppTemplate::getState($view_name,array("form","register","postpone"),["cron"=>$cron,"transaction"=>$transaction,"email"=>$email])!!}

@stop

@section("script")
<script>
    {!!$frontapp!!}
</script>
@stop