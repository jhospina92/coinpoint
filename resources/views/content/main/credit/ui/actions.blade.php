@if(Request::route()->getName()!=RouteData::CREDIT_CREATE)

<section class="pull-right">
    <a class="btn btn-default waves-effect waves-light" href="{{URL::route(RouteData::CREDIT_CREATE)}}"><span class="fa fa-plus-circle"></span> {{trans("nav.route.credit.create")}}</a>
</section>
@endif

