<?php

use App\Models\Credit;
use App\Base\Data\Currency;
use App\Base\System\Library\Comp\DateUtil;
?>
@extends("ui/templates/default/index")

@section("content")

@include("content/main/credit/ui/actions")

<h1 class="h1-responsive"><i class="fa fa-history" aria-hidden="true"></i> {{trans("nav.route.credit.record")}} | {{$credit->name}} - {{$credit->organization}}</h1>

<ul class="list-group">
    @foreach($credit->track as $track)
    <li class="list-group-item">
        <span class="tag bg-SP big label-pill float-xs-right"> {!!$track->getValue(true)!!}</span> 
        <h4 class="list-group-item-heading">{{$track->type}}</h4>
        <p class="list-group-item-text">
            <span class="tag"><span class="fa fa-calendar"></span> {{(new DateUtil($track->created_at))->getString("d m Y")}}</span>
        </p>
    </li>
    @endforeach
</ul>


@stop