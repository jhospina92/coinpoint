<?php

use App\Models\Credit;
?>
@extends("ui/templates/default/index")
@section("content-box")


<h1 class="h1-responsive">{{trans("nav.route.credit.create")}}</h1>

<form action="{{URL::route(RouteData::CREDIT_STORE)}}" method="POST" class="form-main">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-md-6">
            <div  class="md-form">
                <input id="input-name" type="text" class="form-control" data-required="text" name="{{Credit::ATTR_NAME}}">
                <label for="input-name">{{trans("ui.label.description")}}</label>
            </div>
        </div>
        <div class="col-md-6">
            <div class="md-form">
                <input id="input-organization" type="text" class="form-control" data-required="text" name="{{Credit::ATTR_ORGANIZATION}}">
                <label for="input-organization">{{trans("ui.label.organization")}}</label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="md-form input-group">
                <i class="prefix"><i class="fa fa-tags"></i></i>
                <input type="text" class="form-control" data-required="text" readonly="readonly"  id="category-text" placeholder="{{trans("ui.label.credit.objetive")}}">
                <span class="input-group-btn">
                    <button class="btn btn-cyan btn-lg" type="button" data-toggle="modal" data-target="#modal-category">{{trans("ui.label.categories")}}</button>
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="md-form">
                <input id="input-fees" type="text" class="form-control" data-required="number" name="{{Credit::ATTR_FEES}}">
                <label for="input-fees">{{trans("ui.label.fees")}}</label>
            </div>
        </div>
        <div class="col-md-4">
            <div class="md-form">
                <select id="select-currency" name="currency" class="mdb-select" data-required>
                    @foreach($currencies as $code)
                    <option value="{{$code}}" {{($currency->getCode()==$code)?"selected":""}}>{{$code}}</option>
                    @endforeach
                </select>
                <label>{{trans("ui.label.currency")}}</label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="md-form">
                <i class="prefix">{!!config("app.setting.currency.symbol")!!}</i>
                <input id="input-currency" data-error-attr="0" type="text" data-required="balance" name="{{Credit::ATTR_VALUE}}" class="form-control input-currency" value="" maxlength="20">
                <label for="input-currency">{{trans("ui.label.value")}}</label>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header default-color white-text">
                    {{trans("ui.label.features.optionals")}}
                </div>
                <div class="card-block">
                    @foreach(Credit::getFeatures() as $feature)
                    @php $attrs=$feature["attrs"]; @endphp
                    <div class="row">
                        <div class="col-md-6">
                            {{$feature["name"]}}
                        </div>
                        <div class="col-md-6">
                            @if($attrs["type"]=="select")
                            <div class="md-form">
                                <select class="mdb-select" name="{{$feature["id"]}}">
                                    <option value="" disabled selected>{{trans("ui.label.select")}}</option>
                                    @if($attrs["data"]=="number")
                                    @for ($i = $attrs["min"]; $i <= $attrs["max"]; $i++)
                                    <option value="{{$i}}">{{$i}}</option>
                                    @endfor
                                    @endif
                                </select>
                            </div>
                            @endif

                            @if($attrs["type"]=="text")
                            <div class="md-form">
                                <input id="input-organization input-optional" type="text" class="form-control" data-required="{{$attrs["data"]}}" name="{{$feature["id"]}}">
                            </div>
                            @endif
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="offset-md-10 col-md-2 ">
            <button class="btn btn-primary pull-right" type="submit">{{trans("ui.label.send")}}</button>
        </div>
    </div>
</form>


<!-- MODAL CATEGORIES --->
@include("ui/ext/modals/categories")

@stop

