<?php

use App\Models\Credit;
use App\Models\CreditTrack;
use App\Base\Data\Currency;
?>
@extends("ui/templates/default/index")
@section("content-box")


<h1 class="h1-responsive">{{trans("ui.label.add.track.credit")}}</h1>

<form action="" method="POST" class="form-main">
    <input type="hidden" name="credit_id" value="{{$credit->id}}">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-md-12">
            {{$credit->name}} - {{$credit->organization}}
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="md-form">
                <select class="mdb-select" name="type" data-required>
                    <option value="" disabled selected>{{trans("ui.label.select")}}</option>
                    @foreach(CreditTrack::getTypes() as $index => $value)
                    <option value="{{$index}}">{{$value}}</option>
                    @endforeach
                </select>
                <label>{{trans("ui.label.type")}}</label>
            </div>
        </div>
        <div class="col-xs-5">
            <div class="md-form">
                <i class="prefix currency-symbol">{{(new Currency($credit->currency))->getSymbol()}}</i>
                <input id="input-value" data-error-attr="0" type="text" data-required="balance" name="value" class="form-control input-currency" value="" maxlength="20">
                <label for="input-value">{{trans("ui.label.value")}}</label>
            </div>
        </div>
        <div class="col-xs-1 currency-code">
            {{(new Currency($credit->currency))->getCode()}}
        </div>
    </div>

    <div class="row">
        <div class="offset-md-10 col-md-2 ">
            <button class="btn btn-primary pull-right" type="submit">{{trans("ui.label.send")}}</button>
        </div>
    </div>
</form>


<!-- MODAL CATEGORIES --->
@include("ui/ext/modals/categories")

@stop

