<?php

use App\Models\Credit;
use App\Base\Data\Currency;
?>
@extends("ui/templates/default/index")

@section("content")

@include("content/main/credit/ui/actions")

<h1 class="h1-responsive">{{trans("nav.route.credit.index")}}</h1>

@if(count($credits)==0)
<div class="card">
    <div class="card-block center-block text-xs-center not-show-data">
        <h2><i class="fa fa-info-circle" aria-hidden="true"></i> {{trans("ui.msg.not.show.data")}}</h2>
    </div>
</div>
@endif

<div class="row">
    @foreach($credits as $credit)
    @php $features=json_decode($credit->metadata,true); @endphp

    <div class="col-md-6">

        <div class="card">

            <div class="card-block">
                <!--Title-->
                <h4 class="card-title">{{$credit->name}} - {{$credit->organization}}
                    <div class="pull-right">
                        <div class="dropdown">
                            <a class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                            <div class="dropdown-menu dropdown-primary" aria-labelledby="dropdownMenu1" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                <a class="dropdown-item" href="{{URL::route(RouteData::CREDIT_TRACK_CREATE,$credit->id)}}"><i class="fa fa-flash" aria-hidden="true"></i> {{trans("ui.label.add.track.credit")}}</a>
                                <a class="dropdown-item" href="{{URL::route(RouteData::CREDIT_RECORD,$credit->id)}}"><i class="fa fa-history" aria-hidden="true"></i> {{trans("nav.route.credit.record")}}</a>
                            </div>
                        </div>
                    </div>
                </h4>

                <div class="card-text">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <span class="tag bg-primary label-pill float-xs-right" style="font-size: 1em;">{{$credit->category}}</span> {{trans("ui.label.credit.objetive")}}
                        </li>
                        <li class="list-group-item">
                            <span class="tag bg-primary label-pill float-xs-right" style="font-size: 1em;">{{$credit->fees}}</span>
                            <span class="tag bg-warning label-pill float-xs-right" style="font-size: 1em;margin-right:5px;">{{$credit->getFeesNumberPaid()}}</span> {{trans("ui.label.fees")}}
                        </li>
                        @foreach($features as $index => $value)
                        <li class="list-group-item">
                            <span class="tag bg-primary label-pill float-xs-right" style="font-size: 1em;">{{$value}}</span> {{Credit::getFeatures($index)["name"]}}
                        </li>
                        @endforeach
                    </ul>
                </div>

            </div>

            <div class="card-data">
                {!!$credit->getBalance(true)!!}
            </div>

        </div>
    </div>

    @endforeach
</div>

@stop