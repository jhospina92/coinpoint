<?php

use App\Base\Data\TypePaymentData;
use App\Models\Typayment;
use App\Base\Data\Currency;
use App\Models\UserMeta;
use App\Base\Data\Bank;
use App\Models\Flow;
use App\Base\System\TP\TypePayment;
use App\Base\System\TP\Cash;
?>
@extends("ui/templates/default/index")

@section("css")
{{ HTML::style('assets/css/app/main/flow/create.css', array('media' => 'screen')) }}
@stop

@section("content-box")

@include("content/main/flow/ui/actions")

<h1 class="h1-responsive">{{(isset($transaction))?trans("nav.route.flow.edit"):trans("nav.route.flow.create")}}</h1>

@if(isset($transaction))
{{Form::open(['route' => ["flow.update",$transaction["id"]],'method' => 'put',"class"=>"form-main"])}} 
@else
{{Form::open(['route' => "flow.store",'method' => 'post',"class"=>"form-main"])}}
@endif 
<div class="row">
    <div class="col-md-12">
        <div class="btn-group pull-right" data-toggle="buttons">
            <label class="btn btn-danger active" onclick="flow_push_state_type(this, 'spend')" data-class-style="spend">
                <input type="radio" name="type" id="type-spend" checked="checked" value="{{Flow::TYPE_SPEND}}">  {{trans("attr.flow.type.spend")}}
            </label>
            <label class="btn btn-success" onclick="flow_push_state_type(this, 'entry')" data-class-style="entry">
                <input type="radio" name="type" id="type-entry" value="{{Flow::TYPE_REVENUE}}"> {{trans("attr.flow.type.entry")}}
            </label>
            <label class="btn btn-warning" onclick="flow_push_state_type(this, 'exchange')" data-class-style="exchange">
                <input type="radio" name="type" id="type-exchange" value="{{Flow::TYPE_EXCHANGE}}"> {{trans("attr.flow.type.exchange")}}
            </label>
        </div>
    </div>
</div>
<br/>
<br/>
<div class="row">
    <div class="col-md-6">
        <div class="md-form">
            <i class="prefix"><li class="fa fa-calendar"></li></i>
            <input id="flow-date" data-value="{{(isset($transaction))?$transaction["date"]:'today'}}" type="text" data-attr-format="yyyy/mm/dd" name="date" class="form-control datepicker" value="" maxlength="20">
            <label for="flow-date">{{trans("ui.label.date")}}</label>
        </div>
    </div>
    <div class="col-md-6" data-show-type="{{Flow::TYPE_SPEND}},{{Flow::TYPE_REVENUE}}">
        <div class="md-form input-group">
            <i class="prefix"><i class="fa fa-tags"></i></i>
            <input type="text" class="form-control" data-required="text" readonly="readonly"  id="category-text" placeholder="{{trans("ui.label.select.category")}}">
            <span class="input-group-btn">
                <button class="btn btn-cyan btn-lg" type="button" data-toggle="modal" data-target="#modal-category">{{trans("ui.label.categories")}}</button>
            </span>
        </div>
    </div>
</div>

<!-- TYPE EXCHANGE -->
@include("content/main/flow/sections/create-exchange")

<div class="row" data-show-type="{{Flow::TYPE_SPEND}},{{Flow::TYPE_REVENUE}}">
    <div class="col-md-12">
        <div class="md-form">
            {!!$html["select"]["typayment"]!!}
            <label>{{trans("ui.label.typayment")}}</label>
        </div>
    </div>
</div>
<div class="row content-type-cc" style="display: none;">
    <div class="col-md-12">
        <div class="md-form">
            <input id="input-fees" type="text" name="cc-fees" class="form-control" value="" maxlength="2" data-required="number">
            <label for="input-fees">{{trans("ui.label.fees")}}</label>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="md-form">
            <input id="input-description" type="text" name="description" class="form-control" value="" maxlength="100">
            <label for="input-description">{{trans("ui.label.description")}}</label>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-11">
        <div class="md-form">
            <i class="prefix currency-symbol"></i>
            <input id="input-value" data-error-attr="0" type="text" data-required="balance" name="value" class="form-control input-currency" value="" maxlength="20">
            <label for="input-value">{{trans("ui.label.value")}}</label>
        </div>
    </div>
    <div class="col-xs-1 currency-code"></div>
</div>


@include("content/main/flow/sections/cron")

<div class="row">
    <div class="offset-md-10 col-md-2 ">
        <button class="btn btn-primary pull-right" type="submit">{{(isset($transaction))?trans("ui.label.update"):trans("ui.label.register")}}</button>
    </div>
</div>
</form>


<!-- MODAL CATEGORIES --->
@include("ui/ext/modals/categories")

@stop

@section("script")
@if(isset($transaction))
<script>
    var transaction = {!!json_encode($transaction, true)!!}
    ;
    var ajax_get_data_model_by_id = "{{URL::route(RouteData::AJAX_GET_DATA_MODEL_BY_ID)}}";
</script>
@endif
{{ HTML::script('assets/js/app/main/flow/create.js') }}
@stop
