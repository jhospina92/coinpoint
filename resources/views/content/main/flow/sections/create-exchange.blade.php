<?php

use App\Models\Flow;
use App\Base\System\TP\TypePayment;
use App\Models\Typayment;
use App\Base\System\TP\Cash;
?>

<div class="card text-xs-center" data-show-type="{{Flow::TYPE_EXCHANGE}}" style="display: none;">
    <div class="card-header primary-color  white-text">
        {{trans("ui.text.header.exchange")}}
    </div>
    <div class="card-block">
        <div class="row" >
            <div class="col-md-5">
                <div class="md-form">
                    {!!$html["select"]["origin"]!!}
                    <label>{{trans("ui.label.origin")}}</label>
                </div>
            </div>
            <div class="col-md-2 center-block h3 align-middle" style="text-align:center;">
                <div class="fa fa-arrow-right"></div>
            </div>
            <div class="col-md-5">
                <div class="md-form">
                    {!!$html["select"]["dest"]!!}
                    <label>{{trans("ui.label.destiny")}}</label>
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer text-muted primary-color white-text" id="exchange-footer"></div>
</div>

<div class="row" data-show-type="{{Flow::TYPE_EXCHANGE}}" style="display: none;">
        <div class="col-xs-11">
            <div class="md-form">
                <i class="prefix currency-symbol"></i>
                <input id="exchange-cost" data-error-attr="0" type="text" data-required="balance" name="exchange-cost" class="form-control input-currency" value="" maxlength="20">
                <label for="exchange-cost">{{trans("ui.label.cost.assoc.exchange")}}</label>
            </div>
        </div>
        <div class="col-xs-1 currency-code"></div>
</div>