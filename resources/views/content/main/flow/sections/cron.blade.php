<?php

use App\Models\Flow;
?>
<div class="row" id="cron-content" data-show-type="{{Flow::TYPE_SPEND}},{{Flow::TYPE_REVENUE}}">
    <div class="col-md-12">
        <div class="switch">
            <label>
                <span class="title">{{sprintf(trans("ui.label.ask.cron"),trans("ui.label.transaction"))}}</span>
                <input id="cron-check" name="cron" type="checkbox">
                <span class="lever"></span>
            </label>
        </div>
    </div>
    <div class="col-md-12" id="form-cron" style="display: none;">
        <div class="col-md-12" style="margin-top:20px;">
            <div class="md-form">
                <select class="mdb-select" name="repeat" id="repeat" data-required>
                    <option value="" disabled selected>{{trans("ui.label.select")}}</option>
                    <option value="1|d">{{trans("ui.label.every.day")}}</option>
                    <option value="7|d">{{trans("ui.label.every.week")}}</option>
                    <option value="15|d">{{sprintf(trans("ui.label.every.num.days"),15)}}</option>
                    <option value="1|m">{{trans("ui.label.every.month")}}</option>
                    <option value="2|m">{{sprintf(trans("ui.label.every.num.month"),2)}}</option>
                    <option value="3|m">{{sprintf(trans("ui.label.every.num.month"),3)}}</option>
                    <option value="6|m">{{sprintf(trans("ui.label.every.num.month"),6)}}</option>
                    <option value="9|m">{{sprintf(trans("ui.label.every.num.month"),9)}}</option>
                    <option value="12|y">{{sprintf(trans("ui.label.every.year"),1)}}</option>
                </select>
                <label>{{trans("ui.label.repeat")}}</label>
            </div>
        </div>
        <div class="col-md-12">
            <div class="md-form">
                <fieldset class="form-group">
                    <input type="checkbox" name="confirm" id="confirm">
                    <label for="confirm">{{trans("ui.label.flow.cron.confirm.after.exec")}}</label>
                </fieldset>
            </div>
        </div>
    </div>
</div>

<script>
    var trans_label_cron_disabled = "{{sprintf(trans('ui.label.ask.cron'),trans('ui.label.transaction'))}}";
    var trans_label_cron_enabled = "{{sprintf(trans('ui.label.cron.entity'),trans('ui.label.transaction'))}}";
</script>