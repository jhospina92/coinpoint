<?php

use App\Base\Data\CategoryData;
use App\Base\Data\Currency;
use App\Base\System\Library\Comp\DateUtil;
use App\Models\Flow;
use App\Models\Typayment;
use App\Base\System\TP\CreditCard;
use App\Models\Credit;
use App\Base\System\TP\Cash;

$filter_cats = explode(",", $_GET["cats"] ?? "");
$filter_type = explode(",", $_GET["type"] ?? "");
$filter_typayments = explode(",", $_GET["typayments"] ?? "");
?>
@extends("ui/templates/default/index")

@section("css")

{{ HTML::style('assets/css/app/main/flow/index.css', array('media' => 'screen')) }}

@stop

@section("content")

@include("content/main/flow/ui/actions")

<h1 class="h1-responsive">{{trans("nav.route.".Request::route()->getName())}}</h1></li>

<br/>

{{-- FORMULARIO DE BUSQUEDA Y FILTRO DE TRANSACCIONES ---}}

{{Form::open(['route' =>RouteData::FLOW_INDEX,'method' => 'GET',"class"=>"form-main"])}} 
<div class="filter" id="filter" data-status="{{(isset($_GET["search"]))?'open':'close'}}">
    <div id="open-filter" style="display: {{(isset($_GET["search"]))?'none':'block'}}" title="{{trans("ui.msg.flow.click.open.filters")}}" data-toggle="tooltip" data-placement="top"><i class="fa fa-search"></i></div>
    <div id="filter-content">
        <div class="row">
            <div class="col-md-3">
                <div class="md-form">
                    <i class="prefix"><li class="fa fa-calendar"></li></i>
                    <input id="date" data-set-y="{{"[".implode(",",$dataset_y)."]"}}" data-value="{{$_GET["datefrom_submit"]??""}}" type="text" data-attr-format="yyyy-mm" name="datefrom" class="form-control datepicker-month" value="" maxlength="20">
                    <label for="date"> {{trans("ui.label.date.from.alt")}}</label>
                </div>
            </div>
            <div class="col-md-3">
                <div class="md-form">
                    <i class="prefix"><li class="fa fa-calendar"></li></i>
                    <input id="date" data-value="{{$_GET["dateto_submit"]??"today"}}" type="text" data-attr-format="yyyy-mm" name="dateto" class="form-control datepicker-month" value="" maxlength="20">
                    <label for="date"> {{trans("ui.label.date.to.alt")}}</label>
                </div>
            </div>

            <div class="col-md-6">
                <div class="mdb-form-group">
                    <select class="mdb-select select-group select-category" id="select-category" multiple>
                        <option value="all" disabled selected>{{trans("ui.label.all")}}</option>
                        <!--CATEGORIAS DE GASTOS-->
                        @foreach($categories["spend"] as $catmain => $info)
                        <option data-type="{{Flow::TYPE_SPEND}}" value="{{$catmain}}" data-icon="{{URL::to("assets/images/app/icons/tag.png")}}" {{(in_array($catmain,$filter_cats)?"selected":"")}}>{{mb_strtoupper($info["name"])}}</option>
                        @foreach($info["sub"] as $code => $name)}
                        <option data-type="{{Flow::TYPE_SPEND}}" value="{{$code}}" {{(in_array($code,$filter_cats)?"selected":"")}}>{!!$name!!}</option>
                        @endforeach
                        @endforeach
                        <!--CATEGORIAS DE INGRESOS-->
                        @foreach($categories["revenue"] as $catmain => $info)
                        <option data-type="{{Flow::TYPE_REVENUE}}" value="{{$catmain}}" data-icon="{{URL::to("assets/images/app/icons/tag-revenue.png")}}" {{(in_array($catmain,$filter_cats)?"selected":"")}}>{{mb_strtoupper($info["name"])}}</option>
                        @foreach($info["sub"] as $code => $name)}
                        <option data-type="{{Flow::TYPE_REVENUE}}" value="{{$code}}" {{(in_array($code,$filter_cats)?"selected":"")}}>{!!$name!!}</option>
                        @endforeach
                        @endforeach
                    </select>
                    <label>{{trans("ui.label.categories")}}</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="mdb-form-group">
                    <select class="mdb-select select-group" id="select-typayments" multiple>
                        <option value="" disabled selected>{{trans("ui.label.all")}}</option>';
                        @if (count($currencies) == 1)
                        @foreach (Auth::user()->typayments()->orderBy(Typayment::ATTR_TYPE, "ASC")->get() as $typayment)
                        @php
                        $payment = $typayment->getObjectType();
                        $format = $payment->getCurrency()->getFormat();
                        @endphp
                        <option value="{{$typayment->id}}" {{(in_array($typayment->id,$filter_typayments)?"selected":"")}}>{{$payment->getName() . ' ' . (($payment->getID() != Cash::ID) ? " (" . $payment->getBankName() . ")" : "")}}</option>';
                        @endforeach
                        @else
                        @for ($f = 0; $f < count($currencies); $f++)
                        @php
                        $currency = $currencies[$f];
                        $filter = str_replace(array("{", "}"), "%", json_encode(array("currency" => $currency)));
                        @endphp 
                        <option value="{{$currency}}" data-icon="{{URL::to("assets/images/app/icons/blank.png")}}" {{(in_array($currency,$filter_typayments)?"selected":"")}}>{{$currency}}</option>
                        @foreach (Auth::user()->typayments()->where(Typayment::ATTR_FEATURES, "LIKE", $filter)->orderBy(Typayment::ATTR_TYPE, "ASC")->get() as $typayment)
                        @php
                        $payment = $typayment->getObjectType();
                        $format = $payment->getCurrency()->getFormat();
                        @endphp 
                        <option value="{{$typayment->id}}" {{(in_array($typayment->id,$filter_typayments)?"selected":"")}}>{{$payment->getName() . ' ' . (($payment->getID() != Cash::ID) ? " (" . $payment->getBankName() . ")" : "")}}</option>
                        @endforeach
                        </optgroup>
                        @endfor
                        @endif

                    </select>

                    <label>{{trans("ui.label.typayment")}}</label>
                </div>
            </div>
            <div class="col-md-3">
                <div class="mdb-form-group">
                    <select class="mdb-select select-type" id="select-type" multiple>
                        <option value="all" disabled selected>{{trans("ui.label.all")}}</option>
                        <option value="{{Flow::TYPE_SPEND}}" {{(in_array(Flow::TYPE_SPEND,$filter_type)?"selected":"")}}>{{trans("attr.flow.type.spend")}}</option>
                        <option value="{{Flow::TYPE_REVENUE}}" {{(in_array(Flow::TYPE_REVENUE,$filter_type)?"selected":"")}}>{{trans("attr.flow.type.entry")}}</option>
                        <option value="{{Flow::TYPE_EXCHANGE}}" {{(in_array(Flow::TYPE_EXCHANGE,$filter_type)?"selected":"")}}>{{trans("attr.flow.type.exchange")}}</option>
                    </select>
                    <label>{{trans("ui.label.type.transaction")}}</label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="md-form">
                    <input name="search" id="search" type="text" class="form-control" value="{{$_GET["search"]??""}}" maxlength="50">
                    <label for="search">{{trans("ui.label.search")}}</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 offset-md-3 text-center">
                <input id="type" name="type" type="hidden" value="{{$_GET["type"]??""}}">  
                <input id="cats" name="cats" type="hidden" value="{{$_GET["cats"]??""}}">  
                <input id="typayments" name="typayments" type="hidden" value="{{$_GET["typayments"]??""}}">  
                <button class="btn btn-primary" style="width:100%;"><i class="fa fa-filter" aria-hidden="true"></i> {{trans("ui.label.filter")}}</button>
            </div>
        </div>
    </div>
    {{Form::close()}}
</div>

{{-- FIN => FORMULARIO DE BUSQUEDA ---}}

<hr>
<hr>

<div id="list-flow-index">

    @foreach($transactions["month"] as $month)

    <div class="streak streak-photo streak-month" style="background-image:url('{{URL::to("assets/images/res/months/".$month["num"].".jpg")}}')">
        <div class="flex-center white-text pattern-4">
            <ul>
                <li><h1 class="h1-responsive wow fadeIn">{{$month["name"]}}</h1></li>
                <li><h5 class="text-xs-center font-italic wow fadeIn" data-wow-delay="0.2s"> {{$month["year"]}}</h5></li>
            </ul>
        </div>
    </div>

    <div class="row">

        @if(count($month["list"])>0)

        @foreach($month["list"] as $transaction)
        @php
        $typayment = $transaction->typayment->getObjectType();
        $metadata = $transaction->metadata;
        @endphp

        <div class="col-md-6" id="transaction-{{$transaction->id}}">

            <!--Rotating card-->
            <div class="card-wrapper card-transaction">
                <div id="card-{{$transaction->id}}" class="card-rotating {{(($transaction->type!=Flow::TYPE_EXCHANGE))?"effect__click":""}}">

                    <!--Front Side-->
                    <div class="face front hoverable">

                        <!--Avatar-->
                        <div class="avatar date-day {{strtolower($transaction->type)}}">
                            {{(new DateUtil($transaction->date))->day}}
                        </div>
                        <!--Content-->
                        <div class="card-block">
                            <h2 class="h2-responsive">
                                @if($transaction->type==Flow::TYPE_EXCHANGE)
                                <?php $dest = Typayment::find($metadata["dest"]); ?>
                                {{$typayment->getName()}}  <div class="fa fa-arrow-right"></div> {{$dest->getObjectType()->getName()}}
                                @elseif($transaction->category==CategoryData::EXT_CREDITS_TRACK)
                                {{trans("ui.label.fee")}} {{$transaction->credit()->organization}}
                                @elseif($transaction->category==CategoryData::EXT_CREDITS_CARD)
                                {{trans("ui.label.fee")}} {{$transaction->creditCard()->getObjectType()->getName()}} {{$transaction->creditCard()->getObjectType()->getBankName()}} 
                                @else
                                {{CategoryData::getName($transaction->category)}}
                                @endif
                            </h2>

                            <div class="pull-right btn-actions">
                                <div class="dropdown">
                                    <a class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                                    <div class="dropdown-menu dropdown-primary" aria-labelledby="dropdownMenu1" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                        <a class="dropdown-item" href="{{URL::route(RouteData::FLOW_EDIT,$transaction->id)}}"><i class="fa fa-edit" aria-hidden="true"></i> {{trans("ui.label.edit")}}</a>
                                        @if(!$transaction->isDependent())
                                        <a class="dropdown-item btn-destroy" data-msg="{{trans("nav.route.flow.delete.confirm")}}" data-action="{{URL::route(RouteData::FLOW_DELETE,$transaction->id)}}" data-tag="#transaction-{{$transaction->id}}"><i class="fa fa-trash" aria-hidden="true"></i> {{trans("ui.label.delete")}}</a>
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-data {{strtolower($transaction->type)}} rotate-btn" data-card="card-{{$transaction->id}}">
                            {{(Flow::TYPE_SPEND==$transaction->type)?"-":""}}{!!$transaction->getValue(true)!!}
                        </div>
                    </div>
                    <!--/.Front Side-->
                    @if(strlen($transaction->description)>0 || $transaction->type!=Flow::TYPE_EXCHANGE)
                    <!--Back Side-->
                    <div class="face back hoverable rotate-btn" data-card="card-{{$transaction->id}}" >
                        <table class="table {{strtolower($transaction->type)}}">
                            <tr><th>{{trans("ui.label.description")}}</th><td>{{$transaction->description}}</td></tr>
                            @if($transaction->type!=Flow::TYPE_EXCHANGE)
                            <tr><th>{{trans("ui.label.typayment")}}</th><td>{{$typayment->getName()}}  
                                    @if($typayment->hasBankAssoc())
                                    ({{$typayment->getBankName()}})
                                    @endif</td></tr>
                            @endif
                            @if($typayment::ID==CreditCard::ID)
                            <tr><th>{{trans("ui.label.fees")}}</th><td>{{$transaction->metadata["fees"]}}</td></tr>
                            @endif
                        </table>
                    </div>
                    @endif
                    <!--/.Back Side-->
                </div>
            </div>
            <!--/.Rotating card--> 

        </div>
        @endforeach

        @else

        <div class="col-md-12">
            <div class="card">
                <div class="card-block center-block text-xs-center">
                    <h2><i class="fa fa-info-circle" aria-hidden="true"></i> {{trans("ui.msg.not.show.data")}}</h2>
                </div>
            </div>
        </div>

        @endif

    </div>
    @endforeach

</div>


@section("script")
{{ HTML::script('assets/js/app/main/flow/index.js') }}
@stop


@stop