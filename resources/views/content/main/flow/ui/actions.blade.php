
<section class="pull-right">
    @if(Request::route()->getName()!=RouteData::FLOW_CREATE && Request::route()->getName()!=RouteData::FLOW_EDIT) 
    <a class="btn btn-default waves-effect waves-light" href="{{URL::route(RouteData::FLOW_CREATE)}}"><span class="fa fa-plus-circle"></span> {{trans("nav.route.flow.create")}}</a>
    @endif
    @if(Request::route()->getName()!=RouteData::FLOW_CRON_INDEX)
    <a class="btn btn-info waves-effect waves-light" href="{{URL::route(RouteData::FLOW_CRON_INDEX)}}"><span class="fa fa-clock-o"></span> {{trans("nav.route.flow.cron.index")}}</a>
    @endif
</section>


