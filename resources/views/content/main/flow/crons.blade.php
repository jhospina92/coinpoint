<?php

use App\Base\Data\CategoryData;
use App\Base\Data\Currency;
use App\Models\FlowCron;
use App\Base\System\Library\Comp\DateUtil;
?>
@extends("ui/templates/default/index")

@section("content")

@include("content/main/flow/ui/actions")

<h1 class="h1-responsive">{{trans("nav.route.".Request::route()->getName())}}</h1></li>

@if(count($crons)>0)

<ul class="list-group">
    @foreach($crons as $cron)
    @php
    $transaction = $cron->transaction;
    if (empty($transaction)) {
    $cron->delete();
    continue;
    }
    $date = new DateUtil($cron[FlowCron::ATTR_DATE_NEXT]);
    @endphp

    <li class="list-group-item" id="cron-{{$cron->id}}">

        <div class="pull-right btn-actions">
            <div class="dropdown">
                <a class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                <div class="dropdown-menu dropdown-primary" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                    <a class="dropdown-item btn-destroy" data-msg="{{trans("nav.route.flow.cron.delete.confirm")}}" data-action="{{URL::route(RouteData::FLOW_CRON_DELETE,$cron->id)}}" data-tag="#cron-{{$cron->id}}"><i class="fa fa-trash" aria-hidden="true"></i> {{trans("ui.label.delete")}}</a>
                </div>
            </div>
        </div>

        <span class="tag left-actions bg-{{$transaction->type}} big label-pill float-xs-right">{!!Currency::getFormattedValue($transaction->value,config("app.setting.currency.code"))!!}</span> 
        <h4 class="list-group-item-heading">{{CategoryData::getName($transaction->category)}}</h4>
        <p class="list-group-item-text">
            <span class="tag"><i class="fa fa-credit-card" aria-hidden="true"></i> {{$transaction->typayment->getObjectType()->getFullName()}}</span> 
            <span class="tag"><span class="fa fa-clock-o"></span> {{sprintf(trans("ui.label.count.down.format"),DateUtil::calculateDifference(DateUtil::getCurrentTime(),$date))}}</span>
            @if($cron->confirm)
            <span class="tag"><span class="fa fa-check-circle-o"></span> {{trans("ui.label.flow.cron.confirm.after.exec")}}</span>
            @endif
        </p>
    </li>
    @endforeach
</ul>

@else

<div class="col-md-12">
    <div class="card">
        <div class="card-block center-block text-xs-center">
            <h2><i class="fa fa-info-circle" aria-hidden="true"></i> {{trans("ui.msg.not.show.data")}}</h2>
        </div>
    </div>
</div>

@endif


@stop