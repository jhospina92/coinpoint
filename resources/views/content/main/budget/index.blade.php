<?php

use App\Models\Credit;
use App\Base\Data\Currency;
use App\Base\Data\CategoryData;
use App\Base\System\Library\Comp\DateUtil;

?>
@extends("ui/templates/default/index")

@section("content")

@include("content/main/budget/ui/actions")

<h1 class="h1-responsive">{{trans("nav.route.budget.index")}}</h1>

@if(count($budgets)==0)
<div class="card">
    <div class="card-block center-block text-xs-center not-show-data">
        <h2><i class="fa fa-info-circle" aria-hidden="true"></i> {{trans("ui.msg.not.show.data")}}</h2>
    </div>
</div>
@endif

<div class="row">
    @foreach($budgets as $budget)
    @php 
    $revenues=$budget->data_revenue; 
    $spends=$budget->data_spend; 
    @endphp
    <div class="col-md-6" id="content-budget-{{$budget->id}}">
        <div class="card">
            <div id="budget-{{$budget->id}}" class="card-block budget-index {{($budget->active)?"active":""}}">
                <div class="card-title">
                    <h2>{{$budget->name}} 
                        @if($budget->currency!=config("app.setting.currency.code"))
                        <span class="badge coinpoint" style="font-size: 0.4em;">{{$budget->currency}}</span>
                        @endif
                        <div class="pull-right">
                            <div class="dropdown">
                                <a class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                                <div class="dropdown-menu dropdown-primary" aria-labelledby="dropdownMenu1" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                    <a class="dropdown-item active-budget" data-id="{{$budget->id}}" not-loader><i class="fa fa-power-off" aria-hidden="true"></i> <span>{{($budget->active)?trans("ui.label.disable"):trans("ui.label.enable")}}</span></a>
                                    <a class="dropdown-item" href="{{URL::route(RouteData::BUDGET_EDIT,$budget->id)}}"><i class="fa fa-edit" aria-hidden="true"></i> {{trans("ui.label.edit")}}</a>
                                    <a class="dropdown-item btn-destroy" data-msg="{{trans("nav.route.budget.delete.confirm")}}" data-action="{{URL::route(RouteData::BUDGET_DELETE,$budget->id)}}" data-tag="#content-budget-{{$budget->id}}"><i class="fa fa-trash" aria-hidden="true"></i> {{trans("ui.label.delete")}}</a>
                                </div>
                            </div>
                        </div>
                    </h2>
                    @if($budget->active && $budget->currency!=config("app.setting.currency.code"))
                    <div class="alert alert-warning">
                        <i class="fa fa-warning"></i> {{trans("ui.msg.budget.currency.not.valid")}}
                    </div>
                    @endif
                </div>
                <br/>
                <ul class="collapsible collapsible-accordion budget revenue">
                    @foreach($revenues as $catmain => $data)
                    <li><a class="collapsible-header waves-effect arrow-r">{{CategoryData::getName($catmain)}} <span class="tag tag-success green pull-right">{!!Currency::getFormattedValue($budget->getTotalCategoryMain($catmain),$budget->currency)!!}</span></a>
                        <div class="collapsible-body">
                            <ul>
                                @foreach($data as $cat => $value)
                                <li><a href="#" class="waves-effect">{{CategoryData::getName($cat)}} <span class="tag tag-success green pull-right">{!!Currency::getFormattedValue($value,$budget->currency)!!}</span></a></li>
                                @endforeach
                            </ul>
                        </div>
                    </li>
                    @endforeach
                </ul>
                <ul class="collapsible collapsible-accordion budget spend">
                    @foreach($spends as $catmain => $data)
                    <li><a class="collapsible-header waves-effect arrow-r">{{CategoryData::getName($catmain)}} <span class="tag tag-danger red pull-right">{!!Currency::getFormattedValue($budget->getTotalCategoryMain($catmain),$budget->currency)!!}</span></a>
                        <div class="collapsible-body">
                            <ul>
                                @foreach($data as $cat => $value)
                                <li><a href="#" class="waves-effect">{{CategoryData::getName($cat)}} <span class="tag tag-danger red pull-right">{!!Currency::getFormattedValue($value,$budget->currency)!!}</span></a></li>
                                @endforeach
                            </ul>
                        </div>
                    </li>
                    @endforeach
                </ul>
                <div class="row">
                    <div class="col-md-12"><h3>{{trans("ui.label.balance.alt")}}</h3></div>
                    <div class="col-md-12">
                        <ul class="list-group">
                            <li href="#" class="list-group-item">
                                <h4 class="list-group-item-heading">{{trans("ui.label.revenue")}}</h4>
                                <span class="list-group-item-text color-revenue">{!!Currency::getFormattedValue($budget->getTotalRevenue(),$budget->currency)!!}</span>
                            </li>
                            <li href="#" class="list-group-item">
                                <h4 class="list-group-item-heading">{{trans("ui.label.spend")}} <span class="tag bg-spend label-pill float-xs-right">{{($budget->getTotalRevenue()>0)?number_format(($budget->getTotalSpend()/$budget->getTotalRevenue())*100,2):"0"}}%</span></h4>
                                <span class="list-group-item-text color-spend">{!!Currency::getFormattedValue($budget->getTotalSpend(),$budget->currency)!!}</span>
                            </li>
                            <li href="#" class="list-group-item">
                                <h4 class="list-group-item-heading">{{trans("ui.label.saving")}} <span class="tag bg-saving bg-primary label-pill float-xs-right">{{($budget->getTotalRevenue()>0)?number_format(($budget->balance/$budget->getTotalRevenue())*100,2):"0"}}%</span></h4>
                                <span class="list-group-item-text color-saving">{!!Currency::getFormattedValue($budget->balance,$budget->currency)!!}</span>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-3">
                        <label class="tag-arrow">{{(new DateUtil($budget->date_start,"Y-m-d"))->getString("d m Y")}}</label>
                    </div>
                    <div class="col-xs-6 arrow-right">

                    </div>
                    <div class="col-xs-3 text-right">
                        <label class="tag-arrow">{{(new DateUtil($budget->date_end,"Y-m-d"))->getString("d m Y")}}</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>

@stop

@section("script")
<script>
    var ajax_budget_enable = "{{URL::route(RouteData::AJAX_POST_BUDGET_ENABLE)}}";
    var ajax_budget_disable = "{{URL::route(RouteData::AJAX_POST_BUDGET_DISABLE)}}";
</script>
{{ HTML::script('assets/js/app/main/budget/index.js') }}
@stop