@if(Request::route()->getName()!=RouteData::BUDGET_CREATE)

<section class="pull-right">
    <a class="btn btn-default waves-effect waves-light" href="{{URL::route(RouteData::BUDGET_CREATE)}}"><span class="fa fa-plus-circle"></span> {{trans("nav.route.budget.create")}}</a>
</section>
@endif

