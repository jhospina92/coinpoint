<?php

use App\Models\Budget;
use App\Base\Data\Currency;
use App\Base\Data\CategoryData;
use App\Base\System\Library\Comp\DateUtil;

$currency_code = (isset($budget)) ? $budget->currency : config("app.setting.currency.code");
$currency_symbol = (isset($budget)) ? (new Currency($budget->currency))->getSymbol() : config("app.setting.currency.symbol");

$list_item_main = "<div class='row section' data-code='%code%'><div class='col-md-8'><h3>%name% <span class='tag %color-class% level'>" . $currency_symbol . " <span>0</span> " . $currency_code . "</span></h3></div><div class='col-md-4'><div class='progress-balance'><div class='bar %color-class%'><span>0%</span></div></div></div><div class='list-subcats-content col-md-12'></div></div>";
$list_item = "<div class='row'><div class='col-md-8' style='padding-top: 10px;' data-code='%code%'><h4><button class='btn btn-danger remove-item' type='button'><i class='fa fa-remove' aria-hidden='true'></i></button> %name%</h4></div><div class='col-md-4'><div class='md-form'><i class='prefix'>" . $currency_symbol . "</i><input id='input-currency-%code%' name='%input-name%-%code%' data-code='%code%' data-code-main='%code-main%' data-error-attr='0' type='text' data-required='balance' class='form-control input-currency' maxlength='20' value=''/><label for='input-currency-%code%'>" . $currency_code . "</label></div></div></div>";
?>
@extends("ui/templates/default/index")

@section("css")
{{ HTML::style('assets/css/app/main/budget/create.css', array('media' => 'screen')) }}
@stop

@section("content")

@include("content/main/budget/ui/actions")

<h1 class="h1-responsive">{{trans("nav.route.budget.create")}}</h1>


@if(isset($budget))
{{Form::open(['route' => [RouteData::BUDGET_UPDATE,$budget->id],'method' => 'put',"class"=>"form-main"])}} 
@else
{{Form::open(['route' => RouteData::BUDGET_STORE,'method' => 'post',"class"=>"form-main"])}}
@endif 

<input type="hidden" id="category" value="">
<input type="hidden" id="balance" name="balance" value="0">
<div class="card">
    <div class="card-block">
        <div class="md-form">
            <input id="input-name" type="text" class="form-control" data-required="text" name="{{Budget::ATTR_NAME}}" value="{{(isset($budget))?$budget[Budget::ATTR_NAME]:""}}">
            <label for="input-name">{{trans("ui.label.name")}}</label>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-block">
        <h2 class="card-title">{{trans("ui.label.validity")}}</h2>
        <div class="row">
            <div class="col-md-6">
                <div class="md-form">
                    <i class="prefix"><li class="fa fa-calendar"></li></i>
                    <input id="date_start" data-value="{{(isset($budget[Budget::ATTR_DATE_START]))?$budget[Budget::ATTR_DATE_START]:""}}" type="text" data-attr-format="yyyy-mm" name="date_start" class="form-control datepicker-month" value="" maxlength="20" data-required>
                    <label for="date_start"> {{trans("ui.label.date.start")}}</label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="md-form">
                    <i class="prefix"><li class="fa fa-calendar"></li></i>
                    <input id="date_end" data-value="{{(isset($budget[Budget::ATTR_DATE_END]))?$budget[Budget::ATTR_DATE_END]:""}}" type="text" data-attr-format="yyyy-mm" name="date_end" class="form-control datepicker-month" value="" maxlength="20" data-required>
                    <label for="date_end"> {{trans("ui.label.date.end")}}</label>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- REVENUE -->
<div class="card" data-type="list-revenue">
    <div class="card-block">
        <h2 class="card-title">{{trans("ui.label.revenue")}} <span class="pull-right total color-revenue">{!!Currency::getFormattedValue(0,$currency_code)!!}</span></h2>
        <div class="row list-budget" id="list-revenue">
            @if(isset($budget))
            @foreach($budget[Budget::ATTR_DATA_REVENUE] as $catmain => $subcat)

            @php
            $html_items="";
            $html_main=str_replace(array("%code%","%name%","%color-class%"),array($catmain,CategoryData::getName($catmain),"green"),$list_item_main);
            @endphp

            @foreach($subcat as $code => $value)

            @php
            $html_items.=str_replace(array("%code%","%name%","%input-name%","value=''","%code-main%"),array($code,CategoryData::getName($code),"revenue","value='$value'",$catmain),$list_item);
            @endphp
            @endforeach

            {!!str_replace("<div class='list-subcats-content col-md-12'></div>","<div class='list-subcats-content col-md-12'>".$html_items."</div>",$html_main)!!}

            @endforeach
            @endif
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="md-form input-group">
                    <input type="text" class="form-control" style="display: none;" data-required="text" readonly="readonly"  placeholder="{{trans("ui.label.add.type.revenue")}}">
                    <span class="input-group-btn">
                        <button class="btn btn-cyan btn-lg" type="button" data-toggle="modal" data-show-initial="EN" data-target="#modal-category">{{trans("ui.label.add.type.revenue")}}</button>
                    </span>
                </div>
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
</div>

<!-- SPEND -->
<div class="card" data-type="list-spend">
    <div class="card-block">
        <h2 class="card-title">{{trans("ui.label.spend")}} <span class="pull-right total color-spend">{!!Currency::getFormattedValue(0,$currency_code)!!}</span></h2>
        <div class="row list-budget" id="list-spend">

            @if(isset($budget))
            @foreach($budget[Budget::ATTR_DATA_SPEND] as $catmain => $subcat)

            @php
            $html_items="";
            $html_main=str_replace(array("%code%","%name%","%color-class%"),array($catmain,CategoryData::getName($catmain),"red"),$list_item_main);
            @endphp

            @foreach($subcat as $code => $value)

            @php
            $html_items.=str_replace(array("%code%","%name%","%input-name%","value=''","%code-main%"),array($code,CategoryData::getName($code),"spend","value='$value'",$catmain),$list_item);
            @endphp
            @endforeach

            {!!str_replace("<div class='list-subcats-content col-md-12'></div>","<div class='list-subcats-content col-md-12'>".$html_items."</div>",$html_main)!!}

            @endforeach
            @endif

        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="md-form input-group">
                    <input type="text" class="form-control" style="display: none;" data-required="text" readonly="readonly" placeholder="{{trans("ui.label.add.type.spend")}}">
                    <span class="input-group-btn">
                        <button class="btn btn-cyan btn-lg" type="button" data-toggle="modal" data-show-initial="SP" data-target="#modal-category">{{trans("ui.label.add.type.spend")}}</button>
                    </span>
                </div>
            </div>
            <div class="col-md-6"></div>
        </div>
    </div>
</div>
<!-- BALANCE -->
<div class="card">
    <div class="card-block">
        <h2 class="card-title">{{trans("ui.label.balance.alt")}}</h2>
        <div class="row">
            <div class="col-md-12">
                <div class="legend">
                    <h5><span class="tag green"> </span> {{trans("ui.label.revenue")}}</h5>
                    <h5><span class="tag primary-color"> </span> {{trans("ui.label.saving")}}</h5>
                    <h5><span class="tag red"> </span> {{trans("ui.label.spend")}}</h5>
                    <h5><span class="tag dark-red"> </span> {{trans("ui.label.deficit")}}</h5>
                </div>
                <div class="progress-balance" id="progress-balance">
                    <div class="bar revenue green" style="width:50%;"></div>
                    <div class="bar saving primary-color" style="width:0%;"></div>
                    <div class="bar deficit dark-red" style="width:0%;"></div>
                    <div class="bar spend red" style="width:50%;"></div>
                </div>
            </div> 
        </div>
    </div>
</div>

<div class="card">
    <div class="card-block">
        <h2 class="card-title pull-right">{{trans("ui.label.saving")}} <div class="tag pull-right" id="saving-porcent">0%</div></h2>
        <div class="row">
            <div class="col-md-12">
                <h2 class="color-saving pull-right" id="saving">{!!Currency::getFormattedValue(0,$currency_code)!!}</h2>
            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="offset-md-10 col-md-2 ">
        <button class="btn btn-primary pull-right" type="submit">{{trans("ui.label.send")}}</button>
    </div>
</div>
</form>


<!-- MODAL CATEGORIES --->
@include("ui/ext/modals/categories")

@stop

@section("script")

<script>
    var list_item_main = "{!!$list_item_main!!}";
    var list_item = "{!!$list_item!!}";
    var currency_code = "{{$currency_code}}";
    var currency_symbol = "{{$currency_symbol}}";
    var currency_format_sep_millar = "<?php echo (isset($budget)) ? (new Currency($budget->currency))->getFormat()[1] : config("app.setting.currency.format")[1]; ?>";
    var currency_format_sep_decimal = "<?php echo (isset($budget)) ? (new Currency($budget->currency))->getFormat()[2] : config("app.setting.currency.format")[2]; ?>";
    //CONTINUAR ARREGLANDO LA ASIGNACIÓN DE LA MONEDA
</script>   
{{ HTML::script('assets/js/app/main/budget/create.js') }}
@stop