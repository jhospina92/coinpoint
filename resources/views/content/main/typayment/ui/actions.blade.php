@if(Request::route()->getName()!=RouteData::TYPAYMENT_CREATE && Request::route()->getName()!=RouteData::TYPAYMENT_EDIT)

<section class="pull-right">
    <a class="btn btn-default waves-effect waves-light" href="{{URL::route(RouteData::TYPAYMENT_CREATE)}}"><span class="fa fa-plus-circle"></span> {{trans("nav.route.typayment.create")}}</a>
     <a class="btn btn-info waves-effect waves-light" href="{{URL::route(RouteData::AUTOREAD_INDEX)}}"><i class="fa fa-envelope-o" aria-hidden="true"></i> {{trans("nav.route.autoread.index")}}</a>
</section>
@endif

