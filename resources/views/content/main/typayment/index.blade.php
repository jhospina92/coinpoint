<?php

use App\Base\System\TP\TypePayment;
use App\Models\Typayment;
use App\Base\System\TP\CreditCard;
use App\Base\System\TP\Cash;

$is_unify = Auth::user()->getMetaCurrencyUnify();
$currency = Auth::user()->getMetaCurrency();
?>
@extends("ui/templates/default/index")

@section("content")

@include("content/main/typayment/ui/actions")

<h1 class="h1-responsive">{{trans("nav.route.".Request::route()->getName())}}</h1>

@if(count($typayments)==0)
<div class="card">
    <div class="card-block center-block text-xs-center not-show-data">
        <h2><i class="fa fa-info-circle" aria-hidden="true"></i> {{trans("ui.msg.not.show.data")}}</h2>
    </div>
</div>
@endif

<ul class="list-group">

    @foreach($typayments as $typayment)
    <?php
    $object = $typayment->getObjectType();
    ?>

    <li class="list-group-item list-typayment" id="typayment-{{$typayment->id}}">

        <div class="pull-right btn-actions">
            @if($object::ID!=Cash::ID)
            <div class="dropdown">
                <a class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                <div class="dropdown-menu dropdown-primary" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                    @if($object::ID!=Cash::ID)
                    <a class="dropdown-item" href="{{URL::route(RouteData::TYPAYMENT_EDIT,$typayment->id)}}"><i class="fa fa-edit" aria-hidden="true"></i> {{trans("ui.label.manage")}}</a>   
                    @endif

                    @if(!$typayment->hasRestrictions())

                    @if(count($typayment->transactions)>0)
                    <a class="dropdown-item btn-destroy" data-msg="{{trans("nav.route.typayment.cancel.confirm")}}" data-action="{{URL::route(RouteData::TYPAYMENT_CANCEL,$typayment->id)}}" data-tag="#typayment-{{$typayment->id}}"><i class="fa fa-ban" aria-hidden="true"></i> {{trans("ui.label.cancel")}}</a>
                    @else
                    <a class="dropdown-item btn-destroy" data-msg="{{trans("nav.route.typayment.delete.confirm")}}" data-action="{{URL::route(RouteData::TYPAYMENT_DELETE,$typayment->id)}}" data-tag="#typayment-{{$typayment->id}}"><i class="fa fa-trash" aria-hidden="true"></i> {{trans("ui.label.delete")}}</a>
                    @endif
                    @endif
                </div>
            </div>
            @endif
        </div>


        <span class="tag left-actions bg-info big label-pill float-xs-right">{!!$typayment->getAvailableValue(true)!!}</span> 
        <h4 class="list-group-item-heading">{{$object->getName()}} {!!($is_unify && $currency!=$object->getCurrency()->getCode())?"<label class='badge bg-coinpoint'>".$object->getCurrency()->getCode()."</label>":""!!}</h4>
        @if($object->hasBankAssoc())
        <p class="list-group-item-text">{{$object->getBankName()}}</p>
        @endif
    </li>
    @endforeach
</ul>


@stop