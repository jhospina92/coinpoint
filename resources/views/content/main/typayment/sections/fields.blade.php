<?php

use App\Base\Data\TypePaymentData;
use App\Base\System\TP\{
    BankAccount,
    CreditCard
};
?>

@foreach(TypePaymentData::getList("class") as $id => $class)

@php
$fields=$class::getFields();
$total_fields=count($fields);

@endphp

<div data-show-type="{{$id}}" data-vals="{{count($class::getFields())}}" style="display: none;">
    <div id="sec-fields" class="card">
        <div class="card-block">
            <h2>{{trans("ui.label.addional.info")}}</h2>

            <div class="row">
                @foreach($fields as $index => $data)

                <?php
                
                /**
                 * Calcula el tamaño de la columna de tal manera que el numero de campos en total siempre ocupe el total del ancho del contenedor, es decir 12-cols
                 */
                
                $cols_size = $cs = 12 / $total_fields;

                if ($loop->index == $total_fields - 3) {
                    $fc = ($cols_size - floor($cols_size)) * $total_fields;
                }

                if (isset($fc) && $fc > 0) {
                    $cols_size = $cs + 1;
                    $fc--;
                }
                ?>
                <div class="col-md-{{floor($cols_size)}}">
                    @if($data["type"]=="select")
                    {!!HTML::getSelectForm($index,$data["name"],$data["required"],$class::_parse($data["data"]),$data["help"],(isset($features) && isset($features[$index]))?$features[$index]:"")!!}
                    @else
                    {!!HTML::getInputForm($data["type"],$index,$data["name"],$data["required"],$data["help"],(isset($features) && isset($features[$index]))?$features[$index]:"",(isset($data["html"]["attrs"]))?$data["html"]["attrs"]:[])!!}
                    @endif

                </div>
                @endforeach
            </div>

        </div>
    </div>
</div>
@endforeach

