<?php

use App\Base\System\TP\{
    BankAccount,
    CreditCard
};
use \App\Base\System\Services\ReaderEmail\ReaderEmail;
use App\Models\Flow;
use \App\Base\System\Services\ReaderEmail\DataParam;

$types = [Flow::TYPE_SPEND, Flow::TYPE_REVENUE, Flow::TYPE_EXCHANGE];
?>

<div data-show-type="{{CreditCard::ID}},{{BankAccount::ID}}">
    <textarea name="autoread-params" style="display: none;">{{(isset($autoread_data) && count($autoread_data) > 0)?json_encode($autoread_data):""}}</textarea>
    <div class="row">
        <div class="col-md-12">
            <div class="switch">
                <label>
                    <span class="title">{{trans("msg.typayment.sec.autoread.switch")}}</span>
                    <input id="autoread-check" name="autoread-activate" type="checkbox" {{(isset($typayment) && count($typayment->autoreads)>0)?"checked='checked'":''}}>
                           <span class="lever"></span>
                </label>
            </div>
        </div>
    </div>



    <div id="sec-autoread" class="card" style="display: none;">
        <div class="card-block">

            <h2>{{trans("ui.label.autoread.title")}}</h2>
            <p>{{trans("ui.label.add.param.reader.help")}}<p>

                @if(count($autoread)>0)

            <div class="row" style="margin-top:40px;margin-bottom: 20px;">
                <div class='col-md-4 col-md-offset-4'>
                    <div class="md-form">
                        <select class="mdb-select" name="autoread" data-required>
                            <option value="" disabled selected>{{trans("ui.label.select")}}</option>
                            @foreach($autoread as $row)
                            <option value="{{$row->id}}" {{(isset($typayment) && count($typayment->autoreads)>0 && $typayment->autoreads[0]->pivot->autoread_id==$row->id)?"selected":""}}>{{$row->getClientObject()->getProfileEMail()}}</option>
                            @endforeach
                        </select>
                        <label>{{trans("ui.label.select.email.associate")}}</label>
                    </div>
                </div>
            </div>

            @foreach($types as $type)
            <div class="col-md-4" style="padding: 0px;padding-right:5px;">
                <h2 class="{{$type}}">
                    {{Flow::getUtilTypeName($type)}}
                </h2>
                <table class="table table-striped" style="margin-bottom:0px;">
                    <thead class="thead-inverse">
                        <tr>
                            <th>{{trans("ui.label.param")}}</th>
                            <th>{{trans("ui.label.value")}}</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="{{$type}}-table-params">
                        @if(isset($autoread_data) && isset($autoread_data[$type]))
                        @foreach($autoread_data[$type] as $index => $value)
                        @if(isset(DataParam::getList()[$index]))
                        <tr id="{{$type}}-param-{{$index}}" class="param">
                            <td>{{DataParam::getName($index)}}</td><td>{{$value}}</td>
                            <td><i class='fa fa-remove' onclick="aut_remove_param('{{$type}}-param-{{$index}}','{{$type}}')"></i></td
                        </tr>
                        @endif
                        @endforeach
                        @endif
                        
                        <tr id="{{$type}}-msg-not-params" style="display:{{(isset($autoread_data) && isset($autoread_data[$type]) && count($autoread_data[$type])>0)?"none":"table-row"}};">
                            <td class="text-center" colspan="3">
                                {{trans("msg.typayment.sec.autoread.not.params")}}
                            </td>
                        </tr>
                       
                    </tbody>
                </table>

                <button type="button" class="aut-btn-add-param btn btn-default btn-block btn-sm" data-type="{{$type}}">{{trans("ui.label.add.param.reader")}}</button>

            </div>
            @endforeach

            @else
            <div class='col-md-12'>
                <div class="alert alert-warning">
                    {!!sprintf(trans("msg.typayment.sec.autoread.warning.not.found.accounts"),route(RouteData::AUTOREAD_ADD))!!}
                </div>
            </div>
            @endif

        </div>
    </div>
</div>



</div>


<div class="modal fade in" id="modal-params-autoread" role="dialog" aria-labelledby="modal-message" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{trans("ui.label.params.reader")}}</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger" data-type="not-select" style="display: none;">{{trans("msg.typayment.sec.autoread.modal.select.param.error")}}</div>

                        <div class="md-form">
                            <select class="mdb-select" id="aut-select-param">
                                <option value="" disabled selected>{{trans("ui.label.select")}}</option>
                                @foreach(ReaderEmail::getParams() as $name => $text)
                                <option value="{{$name}}">{{$text}}</option>
                                @endforeach
                            </select>
                            <label>{{trans("ui.label.select.param")}}</label>
                        </div>
                    </div>
                </div>
                <div class="row well well-sm help-default-msg">
                    <div class="col-md-12">
                        {{trans("ui.label.add.param.reader.help")}}
                    </div>
                    @foreach(ReaderEmail::getParams(".help") as $name => $text)
                    <div class="col-md-12" data-name-help="{{$name}}" style="display: none;">{!!$text!!}</div>
                    @endforeach
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger" data-type="param-invalid" style="display: none;">{{trans("msg.typayment.sec.autoread.modal.input.param.error")}}</div>
                        <input type="text" class="form-control input-alternate" id="input-text-add"  placeholder="{{trans("msg.typayment.sec.autoread.modal.input.add.placeholder")}}"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" title="{{trans("ui.label.close")}}"><i class="fa fa-close"></i></button>
                <button type="button" class="btn btn-success" id="aut-btn-add" data-url-validate="{{route(RouteData::AUTOREAD_AJAX_VALIDATE_PARAM)}}" title="{{trans("ui.label.add")}}"><i class="fa fa-check"></i></button>
            </div>
        </div>
    </div>
</div>

<script>
    var msg_error_params_exists = "{{trans('msg.typayment.sec.autoread.modal.param.error.exists')}}";
    var msg_error_not_params="{{trans('msg.typayment.sec.autoread.error.not.params')}}";
    var msg_error_params_miss="{{trans('msg.typayment.sec.autoread.error.params.miss')}}"
</script>