@if(Request::route()->getName()!=RouteData::AUTOREAD_ADD)

<section class="pull-right">
    <a class="btn btn-default waves-effect waves-light" href="{{URL::route(RouteData::AUTOREAD_ADD)}}" title="{{trans("nav.route.autoread.add")}}"><span class="fa fa-plus-circle"></span></a>
</section>
@endif

