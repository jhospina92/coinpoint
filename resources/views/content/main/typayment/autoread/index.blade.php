<?php

use App\Base\System\Library\DataProviders\APIs\Mails\MailClient;
use App\Models\Auto\AutoRead;
?>

@extends("ui/templates/default/index")

@section("content")

@include("content/main/typayment/autoread/ui/actions")

<h1 class="h1-responsive">{{trans("nav.route.".Request::route()->getName())}}</h1>



<ul class="list-group">
    @foreach($clients as $row)
    @php
    $client=$row->getClientObject();
    $email=$client->getProfileEMail();
    @endphp

    <li class="list-group-item justify-content-between" id="client-{{$row->id}}">

        <div class="pull-right btn-actions">
            <div class="dropdown">
                <a class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                <div class="dropdown-menu dropdown-primary" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                    <a class="dropdown-item btn-toggle-active" data-text-state-true="<i class='fa fa-unlink' aria-hidden='true'></i> {{trans("ui.label.act.unlink")}}" data-text-state-false="<i class='fa fa-link' aria-hidden='true'></i>  {{trans("ui.label.act.link")}}" data-model="auto_read" data-id="{{$row->id}}" data-callback="callback_toggle_active"><i class="fa fa-{{trans(($row->active)?"unlink":"link")}}" aria-hidden="true"></i> {{trans(($row->active)?"ui.label.act.unlink":"ui.label.act.link")}}</a>
                    <a class="dropdown-item btn-destroy" data-msg="{{sprintf(trans("nav.route.autoread.index.delete.confirm"),$email)}}" data-action="{{route(RouteData::AUTOREAD_DELETE,$row->id)}}" data-tag="#client-{{$row->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i> {{trans("ui.label.delete")}}</a>
                </div>
            </div>
        </div>

        <span class="tag state left-actions {{($row->active)?"success-color":"danger-color"}} big label-pill float-xs-right" title="{{trans(($row->active)?"ui.label.linked":"ui.label.unlinked")}}"><i class="fa fa-{{($row->active)?"link":"unlink"}}" aria-hidden="true"></i></span>

        <h4 class="list-group-item-heading">{{$email}}</h4>

        <p class="list-group-item-text">
            <span class="tag tag-success btn-modal-render-view" data-view="log-success" data-params="{{json_encode(["autoread"=>$row->id])}}" data-title="{{trans('ui.label.log.autoread.processed.success')}}" data-footer="false" style="cursor:pointer;"><i class="fa fa-check-circle"></i> {{trans("ui.label.processed")}} <span class="badge" style="background: white;color: black;">{{$row[AutoRead::ATTR_PROCESSED]}}</span></span> 
            <span class="tag tag-danger btn-modal-render-view" data-view="log-error" data-params="{{json_encode(["autoread"=>$row->id])}}" data-title="{{trans('ui.label.log.autoread.processed.failed')}}" data-footer="false"  style="cursor:pointer;"><i class="fa fa-times-circle-o"></i> {{trans("ui.label.failed")}} <span class="badge" style="background: white;color: black;">{{$row[AutoRead::ATTR_ERRORS]}}</span></span>
        </p>
    </li>

    @endforeach
</ul>
<br/>


@if(count($clients)==0)
<div class="card">
    <div class="card-block center-block text-xs-center not-show-data">
        <h2><i class="fa fa-info-circle" aria-hidden="true"></i> {{trans("ui.msg.not.show.data")}}</h2>
    </div>
</div>
@else
<small>{!!trans("msg.autoread.index.description")!!}</small>
@endif

@stop

@section("script")
{{ HTML::script('assets/js/app/main/typayment/autoread/index.js') }}
@stop
