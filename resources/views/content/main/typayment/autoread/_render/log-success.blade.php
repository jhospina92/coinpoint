<?php

use App\Models\Auto\AutoRead;
use App\Base\System\Library\Comp\DateUtil;
use App\Models\Flow;
use App\Models\Typayment;
use App\Base\Data\Currency;

if (!AutoRead::isOwnerAuth($autoread)) {
    return;
}
$autoread_id = $autoread;
$autoread = AutoRead::find($autoread);

$result = $autoread->log_success()->withTrashed()->orderBy("id", "DESC")->paginate(10, ['*'], 'page', ($page ?? 1));
?>

@if(count($result)>0)

@if(!isset($currentpage))
<div id="content-log-success">
    @endif
    <table class="table table-striped">
        <thead>
            <tr><th>{{trans("ui.label.date")}}</th><th>{{trans("ui.label.typayment")}}</th><th>{{trans("ui.label.type")}}</th><th>{{trans("ui.label.value")}}</th><th></th></tr>
        </thead>
        <tbody>
            @foreach($result as $log)
            @php
            $data=$log->data;
            $typayment=Typayment::withTrashed()->find($data["typayment"])->getObjectType();
            $autoread_client=$autoread->getClientObject();
            $email=$autoread_client->getProfileEmail();
            @endphp
            <tr>
                <td>{{(new DateUtil($log->created_at))->getString("d m Y")}}</td>
                <td>{!!$typayment->getFullName()!!}</td>
                <td class="color-{{$data["type"]}}">{!!Flow::getUtilTypeName($data["type"])!!}</td>
                <td>{!!Currency::getFormattedValue(floatval($data["value"]),$typayment->getCurrency()->getCode(),false)!!}</td>
                <td>{!!($log->trashed())?"<i class='fa fa-minus-circle color-spend' title=\"".trans('ui.label.skipped')."\"></i>":"<i class='fa fa-check color-revenue' title=\"".trans('ui.label.validated')."\"></i>"!!}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

    @if(!isset($currentpage))
</div>
@endif


@if(!isset($currentpage))
{!!HTML::pagination($result,"btn-render-view",["view"=>"log-success","container"=>"#content-log-success","params"=>json_encode(["autoread"=>$autoread_id])])!!}
@endif

@else
<div class="text-center">
    <br/>
    <br/>
    <h2><i class="fa fa-info-circle" aria-hidden="true"></i> {{trans("ui.msg.not.show.data")}}</h2>
    <br/>
    <br/>
</div>
@endif