<?php

use App\Models\Auto\AutoRead;
use App\Base\System\Library\Comp\DateUtil;
use App\Models\Flow;
use App\Models\Typayment;
use App\Base\Data\Currency;

if (!AutoRead::isOwnerAuth($autoread)) {
    return;
}
$autoread_id = $autoread;
$autoread = AutoRead::find($autoread);

$result = $autoread->log_error()->orderBy("id", "DESC")->paginate(10, ['*'], 'page', ($page ?? 1));
?>

@if(count($result)>0)

@if(!isset($currentpage))
<div id="content-log-error">
    @endif
    <table class="table table-striped">
        <thead>
            <tr><th>{{trans("ui.label.date")}}</th>
                <th>{{trans("ui.label.type")}}</th>
                <th>{{trans("ui.label.typayment")}}</th>
                <th>{{trans("ui.label.description")}}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($result as $log)

            <tr>
                <td>{{(new DateUtil($log->created_at))->getString("d m Y")}}</td>
                <td class="color-{{$log->type}}">{!!Flow::getUtilTypeName($log->type)!!}</td>
                <td>{{Typayment::withTrashed()->find($log->typayment_id)->getObjectType()->getFullName()}}</td>
                <td>{{$log->description}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

    @if(!isset($currentpage))
</div>
@endif

@if(!isset($currentpage))
{!!HTML::pagination($result,"btn-render-view",["view"=>"log-error","container"=>"#content-log-error","params"=>json_encode(["autoread"=>$autoread_id])])!!}
@endif

@else
<div class="text-center">
    <br/>
    <br/>
    <h2><i class="fa fa-info-circle" aria-hidden="true"></i> {{trans("ui.msg.not.show.data")}}</h2>
    <br/>
    <br/>
</div>
@endif