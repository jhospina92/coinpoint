@extends("ui/templates/default/index")

@section("content")


<h1 class="h1-responsive">{{trans("nav.route.".Request::route()->getName())}}</h1>

<div class="card">
    <div class="card-block">
        <div class='row'>
            <div class='col-md-4'>
                <div class="md-form">
                    <select id="select-client" class="mdb-select" name="autoread-client" data-required>
                        <option value="" disabled selected>{{trans("ui.label.select")}}</option>
                        @foreach($clients as $mail => $url)
                        <option value="{{$mail}}" data-url="{{$url}}">{{$mail}}</option>
                        @endforeach
                    </select>
                    <label>{{trans("ui.label.select.provider.mail")}}</label>
                </div>
            </div>
            <div class='col-md-2'>
                <button type='button' class='btn btn-info btn-block' id='connect' disabled="disabled"><i class="fa fa-connectdevelop" aria-hidden="true"></i> {{trans("ui.label.connect")}}</button>
            </div>
        </div>
    </div>
</div>

@stop

@section("script")

<script>
    var msg_btn_state_connect = "{{trans('ui.label.connect')}}";
    var msg_btn_state_connecting = "{{trans('ui.label.state.connecting')}}";
    var msg_btn_state_connected = "{{trans('ui.label.connected')}}";
</script>

{{ HTML::script('assets/js/app/main/typayment/autoread/add.js') }}

@stop