<?php

use App\Base\Data\TypePaymentData;
use App\Models\Typayment;
use App\Base\Data\Currency;
use App\Models\UserMeta;
use App\Base\Data\Bank;

if (isset($typayment)) {
    $typepayment = $typayment->getObjectType();
    $features = json_decode($typayment->features, true);
    
}
?>
@extends("ui/templates/default/index")

@section("css")
{{ HTML::style('assets/css/app/main/typayment/create.css', array('media' => 'screen')) }}
@stop

@section("content")

@include("content/main/typayment/ui/actions")


<h1 class="h1-responsive">{{trans(((!isset($typayment))?"nav.route.typayment.create":"nav.route.typayment.edit"))}}</h1>

    @if(isset($typayment))
    {{Form::open(['route' => ["typayment.update",$typayment->id],'method' => 'put',"class"=>"form-main"])}} 
    @else
    {{Form::open(['route' => "typayment.store",'method' => 'post',"class"=>"form-main"])}}
    @endif 


    <div class="card">
        <div class="card-block">
            <div class="row">
                <div class="col-md-6">
                    <div class="md-form">
                        <select id="type" name="{{Typayment::ATTR_TYPE}}" class="mdb-select" data-required {{(isset($typepayment) && !$typepayment::paramManageEdit("type"))?"disabled='disabled'":""}}>
                                <option value="" disabled selected>{{trans("ui.label.select")}}</option>
                            @foreach($typayments as $code => $name)
                            <option value="{{$code}}" {{(isset($typepayment) && $typepayment::ID==$code)?"selected":""}}>{{$name}}</option>
                            @endforeach
                        </select>   
                        <label>{{trans("ui.typayment.select")}}</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="md-form">
                        <select id="select-currency" name="currency" class="mdb-select" data-required {{(isset($typepayment) && !$typepayment::paramManageEdit("currency"))?"disabled='disabled'":""}}>
                                @foreach($currencies as $code)
                                <option value="{{$code}}" {{(($currency->getCode()==$code && !isset($typepayment)) || (isset($typepayment) && $typayment->currency==$code))?"selected":""}}>{{$code}}</option>
                            @endforeach
                        </select>
                        <label>{{trans("ui.label.currency")}}</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="md-form">
                        <i class="prefix">{!!$currency->getSymbol()!!}</i>
                        <input id="input-currency" data-error-attr="0" type="text" data-required="balance" {{(isset($typayment))?sprintf("data-min-value=%s",$typepayment->getBalance()):""}} name="{{TypePaymentData::BALANCE}}" class="form-control input-currency" maxlength="20" {{(isset($typepayment) && !$typepayment::paramManageEdit("value"))?"disabled='disabled'":""}} value="{{(isset($typayment))?$features["balance"]:""}}">
                        <label for="input-currency">{{trans("ui.label.balance")}}</label>
                    </div>
                </div>
            </div>
            <div class="row" id="select-bank" style="display:none;">
                <div class="col-md-12">
                    <div class="md-form">
                        <select id="bank-assoc" name="bank-assoc" class="mdb-select" data-required data-value="{{(isset($typepayment) && $typepayment->hasBankAssoc())?$typepayment->getBank()->getCode():""}}" {{(isset($typepayment) && !$typepayment::paramManageEdit("bank"))?"disabled='disabled'":""}}></select>
                        <label>{{trans("ui.label.bank.assoc")}}</label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include("content/main/typayment/sections/fields")

    @include("content/main/typayment/sections/autoread")



    <div class="row">
        <div class="offset-md-10 col-md-2 ">
            <button class="btn btn-primary pull-right" type="submit"> {{(isset($typayment))?trans("ui.label.update"):trans("ui.label.register")}}</button>
        </div>
    </div>
</form>
@stop

@section("script")
<script>
    var ajax_get_banks = "{{URL::route(RouteData::AJAX_GET_BANKS)}}";
</script>
{{ HTML::script('assets/js/app/main/typayment/create.js') }}
{{ HTML::script('assets/js/app/main/typayment/create-exceptions.js') }}

@stop
