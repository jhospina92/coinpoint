<?php

use App\Models\Goal;
use App\Base\System\Library\Comp\DateUtil;
use App\Base\Data\Currency;

$currency_dif = false;

$cucu = config("app.setting.currency.code");
foreach ($goals as $goal) {
    if ($cucu != $goal->currency) {
        $currency_dif = true;
        break;
    }
}
?>
@extends("ui/templates/default/index")

@section("content")

@include("content/main/goal/ui/actions")

<h1 class="h1-responsive">{{trans("nav.route.".Request::route()->getName())}}</h1></li>

<div id="container-main" class="container-fluid card card-block">

    @if(count($goals)>0)
    <table class="table">
        <thead class="thead-inverse">
            <tr>
                <th>{{trans("ui.label.target")}}</th>
                <th>{{trans("ui.label.progress")}}</th>
                <th>{{trans("ui.label.money.rest")}}</th>
                <th>{{trans("ui.label.status")}}</th>
                <th>{{trans("ui.label.percent.saving")}}</th>
                <th>{{trans("ui.label.date.limit")}}</th>
                @if($currency_dif)
                <th>{{trans("ui.label.currency")}}</th>
                @endif
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($goals as $goal)
            <tr id="goal-{{$goal->id}}" class="goal">
                <th scope="row">{{$goal->name}}</th>
                <td>{{$goal->getProgress()}}%</td>
                <td>{!!Currency::getFormattedValue((1-($goal->getProgress()/100))*$goal->getValue(),$cucu)!!}</td>
                <td><span class="tag status {{$goal->getStatus()}}">{{Goal::getStatusText($goal->getStatus())}}</span></td>
                <td>{{$goal->rate_saving*100}}%</td>
                @if($currency_dif)
                <td>{{$goal->currency}}</td>
                @endif
                <td>{{(new DateUtil($goal->date))->getString("d m Y")}}</td>
                <td>
                    @if (!$goal->trashed())
                    <a class="btn btn-primary btn-sm" href="{{URL::route(RouteData::GOAL_EDIT,$goal->id)}}"><i class="fa fa-edit" aria-hidden="true"></i> {{trans("ui.label.edit")}}</a>
                    @if($goal->getStatus()==Goal::STATUS_COMPLETED)
                    <a class="btn btn-danger btn-sm btn-destroy" data-msg="false" data-msg-success="{{trans("nav.route.goal.delete.terminate")}}" data-action="{{URL::route(RouteData::GOAL_DELETE,$goal->id)}}" data-tag="#goal-{{$goal->id}}" data-func-success="goal_callback_success_index_terminate"><i class="fa fa-check-circle" aria-hidden="true"></i> {{trans("ui.msg.label.terminate")}}</a>
                    @endif
                    @if($goal->getStatus()==Goal::STATUS_IN_PROGRESS)
                    <a class="btn btn-danger btn-sm btn-destroy" data-msg="{{trans("nav.route.goal.delete.confirm")}}" data-action="{{URL::route(RouteData::GOAL_DELETE,$goal->id)}}" data-tag="#goal-{{$goal->id}}" data-func-success="goal_callback_success_index_terminate"><i class="fa fa-trash-o" aria-hidden="true"></i> {{trans("ui.label.delete")}}</a>
                    @endif
                    @else

                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @else
    <div class="card-block center-block text-xs-center">
        <h2><i class="fa fa-info-circle" aria-hidden="true"></i> {{trans("ui.msg.not.show.data")}}</h2>
    </div>
    @endif

</div>

@if(count($goals_complete)>0)

<h2 class="doc-title">Metas alcanzadas</h2>

<div class="container-fluid card card-block">
    <table class="table">
        <thead class="thead-inverse">
            <tr>
                <th>{{trans("ui.label.target")}}</th>
                <th>{{trans("ui.label.date.created")}}</th>
                <th>{{trans("ui.label.status")}}</th>
                <th>{{trans("ui.label.percent.saving")}}</th>
                @if($currency_dif)
                <th>{{trans("ui.label.currency")}}</th>
                @endif
                <th>{{trans("ui.label.reached")}}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($goals_complete as $goal)
            <tr>
                <td>{{$goal->name}}</td>
                <td>{{(new DateUtil($goal->created_at))->getString("d m Y")}}</td>
                <td><span class="tag status {{$goal->getStatus()}}">{{Goal::getStatusText($goal->getStatus())}}</span></td>
                <td>{{$goal->rate_saving*100}}%</td>
                @if($currency_dif)
                <td>{{$goal->currency}}</td>
                @endif
                <td>{{(new DateUtil($goal->date_complete))->getString("d m Y")}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

@stop

@section("script")
{{ HTML::script('assets/js/app/main/goal/index.js') }}
@stop