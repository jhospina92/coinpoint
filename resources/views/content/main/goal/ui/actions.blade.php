
<section class="pull-right">
    @if(Request::route()->getName()!=RouteData::GOAL_CREATE && Request::route()->getName()!=RouteData::GOAL_EDIT) 
    <a class="btn btn-default waves-effect waves-light" href="{{URL::route(RouteData::GOAL_CREATE)}}"><span class="fa fa-plus-circle"></span> {{trans("nav.route.goal.create")}}</a>
    @endif
</section>


