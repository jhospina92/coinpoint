<?php

use App\Base\Data\TypePaymentData;
use App\Models\Typayment;
use App\Base\Data\Currency;
use App\Models\Goal;

$currency = new Currency(config("app.setting.currency.code"));

$sa = (isset($goal)) ? ($goal->rate_saving * 100) + Goal::getRangeSavingAvailable(Auth::user()->id) : Goal::getRangeSavingAvailable(Auth::user()->id);
?>
@extends("ui/templates/default/index")

@section("content")

<div id="container-main" class="container-fluid card card-block">

    @include("content/main/goal/ui/actions")

    <h1 class="h1-responsive">{{(isset($goal))?trans("nav.route.goal.edit"):trans("nav.route.goal.create")}}</h1>

    @if(isset($goal))
    {{Form::open(['route' => ["goal.update",$goal["id"]],'method' => 'put',"class"=>"form-main"])}} 
    @else
    {{Form::open(['route' => "goal.store",'method' => 'post',"class"=>"form-main"])}}
    @endif 
    <div class="row">
        <div class="col-md-12">
            <div  class="md-form">
                <input id="input-name" type="text" class="form-control" data-required="text" name="name" placeholder="{{trans("ui.label.goal.attr.name.placeholder")}}" value="{{(isset($goal))?$goal->name:""}}">
                <label for="input-name">{{trans("ui.label.goal.attr.name")}}</label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="md-form">
                <i class="prefix"><li class="fa fa-calendar"></li></i>
                <input id="date" data-value="{{(isset($goal))?$goal["date"]:'today'}}" type="text" data-attr-format="yyyy-mm" name="date" class="form-control datepicker-month" value="" maxlength="20">
                <label for="date">{{trans("ui.label.goal.attr.date")}}</label>
            </div>
        </div>
        <div class="col-md-6">
            <h6>{{trans("ui.label.goal.attr.rate_saving")}} <span class="tag tag-info pull-right" id="display-rate-saving">{{(isset($goal))?$goal["rate_saving"]*100:$sa}}%</span></h6>
            <div class="range-field">
                <input type="range" min="0" max="{{$sa}}" name="rate_saving" id="rate_saving" label-min="0%" label-max="{{$sa}}%" value="{{(isset($goal))?$goal["rate_saving"]*100:$sa}}" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-11">
            <div class="md-form">
                <i class="prefix currency-symbol">{{$currency->getSymbol()}}</i>
                <input id="input-value" data-error-attr="0" type="text" data-required="balance" name="value" class="form-control input-currency" value="{{(isset($goal))?$goal["value"]:''}}" maxlength="20">
                <label for="input-value">{{trans("ui.label.goal.attr.value")}}</label>
            </div>
        </div>
        <div class="col-xs-1 currency-code">
            {{$currency->getCode()}}
        </div>
    </div>

    <div class="row">
        <div class="offset-md-10 col-md-2">
            <button class="btn btn-primary pull-right" type="submit" disabled="disabled">{{(isset($goal))?trans("ui.label.update"):trans("ui.label.register")}}</button>
        </div>
    </div>
    {{Form::close()}}
</div>

@include("content/main/goal/sections/calculate")

<!-- MODAL CATEGORIES --->
@include("ui/ext/modals/categories")

@stop

@section("script")
{{ HTML::script('assets/js/app/main/goal/create.js') }}
@stop
