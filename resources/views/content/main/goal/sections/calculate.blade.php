<?php

use App\Base\System\TP\CreditCard;
use App\Base\Data\Currency;

$amount_current = Auth::user()->getAmountCurrent();
?>
<div class="container-fluid card card-block" id="calculate" style="display: none;">
    <div class="row">
        <div class="col-xs-2 text-center"><span id="perc-saving">100%</span> {{trans("ui.label.current.money")}}</div>
        <div class="col-xs-2 text-center offset-xs-1">{{trans("ui.label.missings.month")}}</div>
        <div class="col-xs-2 text-center offset-xs-1">{{trans("ui.label.saving.min.month")}}</div>
        <div class="col-xs-3 text-center offset-xs-1">{{trans("ui.label.my.goal")}}</div>
    </div>
    <div class="row">
        <div class="col-xs-2">
            <label class="tag-arrow bg-info" data-value="{{$amount_current}}" id="st-current-amount">{!!Currency::getFormattedValue($amount_current, $currency->getCode())!!}</label>
        </div>
        <div class="col-xs-1 text-center">
            <i class="fa fa-plus-circle color-saving symbol" aria-hidden="true"></i>
        </div>
        <div class="col-xs-2">
            <label class="tag-arrow bg-info" id="st-months">0</label>
        </div>
        <div class="col-xs-1 text-center">
            <i class="fa fa-asterisk color-saving symbol" aria-hidden="true"></i>
        </div>
        <div class="col-xs-2">
            <label class="tag-arrow bg-info" id="st-min-saving"></label>
        </div>
        <div class="col-xs-1 arrow-right"></div>
        <div class="col-xs-3">
            <label class="tag-arrow" id="st-goal-value"></label>
        </div>
    </div>
    <div class="attempt">
        {{trans("ui.msg.estimation.warning")}}
    </div>
</div>
