
@extends("ui/templates/default/index")

@section("css")
{{ HTML::style('assets/css/app/main/dashboard.css', array('media' => 'screen')) }}
@stop

@section("content")

<div class="row onload-render-view" id="widgets" data-view='dashboard-widgets' data-container='#widgets'></div>

@stop