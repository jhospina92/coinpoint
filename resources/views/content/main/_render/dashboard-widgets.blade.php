<?php

use App\Base\System\Library\Comp\DateUtil;
use App\Base\System\Library\Comp\Util;
use App\Models\Flow;
use App\Base\Data\Currency;
use App\Base\Data\CategoryData;
use Illuminate\Support\Facades\Auth;
use App\Base\UI\Widget;

$cachekey = Auth::user()->id . "-dashboard";

extract(Cache::get($cachekey, Flow::getDataDashboard()));

?>

<div class="col-md-12">
    @include("ui/dashboard/widgets/goals-progress")
</div>
<div class="col-md-6">
    @foreach($widgets[0] as $index =>$path)
    @include($path)
    @endforeach
</div>
<div class="col-md-6">
    @foreach($widgets[1] as $index =>$path)
    @include($path)
    @endforeach
</div>