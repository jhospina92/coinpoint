<?php

use App\Models\UserMeta;
?>

@extends("ui/templates/default/index")

@section("css")
{{ HTML::style('assets/css/app/main/dashboard.css', array('media' => 'screen')) }}
@stop

@section("content-box")

<h1>{{trans("nav.route.account.settings.notifications")}}</h1>

<br/>

{{Form::open(['route' => RouteData::ACCOUNT_SETTINGS_NOTIFICATIONS,'method' => 'put',"class"=>"form-main"])}} 

<table class="table table-striped">
    <thead class="table-inverse">
        <tr>
            <th colspan="2">{{trans("nav.route.flow.cron.index")}}</th>
        </tr>
    </thead>
    <thead>
        <tr>
            <th>{{trans("ui.label.description")}}</th>
            <th>{{trans("ui.label.active")}}</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{trans("ac.section.settings.notf.flowcron.auto.email")}}</td>
            <td>
                <div class="switch">
                    <label>
                        <input type="checkbox" name="{{UserMeta::KEY_NOTF_FLOWCRON_AUTO_EMAIL}}" {{(Auth::user()->getMeta(UserMeta::KEY_NOTF_FLOWCRON_AUTO_EMAIL))?"checked=checked":""}}>
                        <span class="lever"></span>
                    </label>
                </div>
            </td>
        </tr>
         <tr>
            <td>{{trans("ac.section.settings.notf.flowcron.confirm.email")}}</td>
            <td>
                <div class="switch">
                    <label>
                        <input type="checkbox" name="{{UserMeta::KEY_NOTF_FLOWCRON_CONFIRM_EMAIL}}" {{(Auth::user()->getMeta(UserMeta::KEY_NOTF_FLOWCRON_CONFIRM_EMAIL))?"checked=checked":""}}>
                        <span class="lever"></span>
                    </label>
                </div>
            </td>
        </tr>
    </tbody>
     <thead class="table-inverse">
        <tr>
            <th colspan="2">{{trans("ui.label.autoread.title")}}</th>
        </tr>
    </thead>
    <thead>
        <tr>
            <th>{{trans("ui.label.description")}}</th>
            <th>{{trans("ui.label.active")}}</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{trans("ac.section.settings.notf.flow.autoread.success.auto.email")}}</td>
            <td>
                <div class="switch">
                    <label>
                        <input type="checkbox" name="{{UserMeta::KEY_NOTF_FLOW_AUTOREAD_SUCCESS_AUTO_EMAIL}}" {{(Auth::user()->getMeta(UserMeta::KEY_NOTF_FLOW_AUTOREAD_SUCCESS_AUTO_EMAIL))?"checked=checked":""}}>
                        <span class="lever"></span>
                    </label>
                </div>
            </td>
        </tr>
         <tr>
            <td>{{trans("ac.section.settings.notf.flow.autoread.success.confirm.email")}}</td>
            <td>
                <div class="switch">
                    <label>
                        <input type="checkbox" name="{{UserMeta::KEY_NOTF_FLOW_AUTOREAD_SUCCESS_CONFIRM_EMAIL}}" {{(Auth::user()->getMeta(UserMeta::KEY_NOTF_FLOW_AUTOREAD_SUCCESS_CONFIRM_EMAIL))?"checked=checked":""}}>
                        <span class="lever"></span>
                    </label>
                </div>
            </td>
        </tr>
        <tr>
            <td>{{trans("ac.section.settings.notf.flow.autoread.error.email")}}</td>
            <td>
                <div class="switch">
                    <label>
                        <input type="checkbox" name="{{UserMeta::KEY_NOTF_FLOW_AUTOREAD_ERROR_EMAIL}}" {{(Auth::user()->getMeta(UserMeta::KEY_NOTF_FLOW_AUTOREAD_ERROR_EMAIL))?"checked=checked":""}}>
                        <span class="lever"></span>
                    </label>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<br/>
<br/>

<div class="row">
    <div class="col-md-12 text-center">
        <button class="btn btn-primary" type="submit">{{trans("ui.label.update")}}</button>
    </div>
</div>
</form>
@stop