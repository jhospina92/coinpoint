<?php

use App\Models\UserMeta;
?>

@extends("ui/templates/default/index")

@section("css")
{{ HTML::style('assets/css/app/main/dashboard.css', array('media' => 'screen')) }}
@stop

@section("content-box")

<h1>{{trans("nav.route.account.settings.general")}}</h1>

<hr>

{{Form::open(['route' => RouteData::ACCOUNT_SETTINGS_GENERAL,'method' => 'put',"class"=>"form-main"])}} 


<div class="row">
    <div class="col-md-6">
        <div class="md-form">
            <select name="{{UserMeta::KEY_LANG}}" class="mdb-select" data-required>
                @foreach($langs as $code => $lang)
                <option value="{{$code}}" {{($code==Auth::user()->getLang())?"selected":""}}>{{$lang}}</option>
                @endforeach
            </select>
            <label>{{trans("ac.section.settings.language")}}</label>
        </div>
    </div>
    <div class="col-md-6">

    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="md-form">
            <select name="{{UserMeta::KEY_CURRENCY}}" class="mdb-select" data-required>
                @foreach($currencies as $code)
                <option value="{{$code}}" {{($code==Auth::user()->getMeta(UserMeta::KEY_CURRENCY))?"selected":null}}>{{$code}}</option>
                @endforeach
            </select>
            <label>{{trans("ac.section.settings.currency")}}</label>
        </div>
    </div>
    <div class="col-md-6">

    </div>
</div>

<div class="row">
    <div class="col-md-6">
        {{trans("ac.section.settings.currency.unify")}}
        <div class="switch">
            <label>
                {{trans("ui.label.not")}}
                <input type="checkbox" name="{{UserMeta::KEY_CURRENCY_UNIFY}}" {{(Auth::user()->getMeta(UserMeta::KEY_CURRENCY_UNIFY))?"checked='checked'":""}}>
                <span class="lever"></span>
                {{trans("ui.label.yes")}}
            </label>
        </div>
    </div>
    <div class="col-md-6">

    </div>
</div>

<div class="row">
    <div class="col-md-12 text-center">
        <button class="btn btn-primary" type="submit">{{trans("ui.label.update")}}</button>
    </div>
</div>
</form>
@stop