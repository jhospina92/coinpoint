@extends("ui/templates/default/index")

@section("content")

<h1>{{trans("nav.route.account.dashboard")}}</h1>

<hr>

<div class="row">
    <div class="col-sm-6">
        <h3><i class="fa fa-user"></i> {{trans("ac.section.my.profile")}}</h3>
        <div class="list-group">
            <a href="{{URL::route(RouteData::ACCOUNT_PROFILE_EDIT)}}" class="list-group-item">{{trans("nav.route.account.profile.edit")}}</a>
        </div>
        <br/>
        <h3><i class="fa fa-gears"></i> {{trans("ac.section.settings")}}</h3>
        <div class="list-group">
            <a href="{{URL::route(RouteData::ACCOUNT_SETTINGS_GENERAL)}}" class="list-group-item">{{trans("nav.route.account.settings.general")}}</a>
            <a href="{{URL::route(RouteData::ACCOUNT_SETTINGS_NOTIFICATIONS)}}" class="list-group-item">{{trans("nav.route.account.settings.notifications")}}</a>
        </div>
    </div>
    <div class="col-sm-6">
        <h3><i class="fa fa-lock"></i> {{trans("ac.section.security")}}</h3>
        <div class="list-group">
            <a href="{{URL::route(RouteData::ACCOUNT_SECURY_RESET_PASSWORD)}}" class="list-group-item">{{trans("nav.route.account.security.reset.password")}}</a>
        </div>
    </div>
</div>

@stop