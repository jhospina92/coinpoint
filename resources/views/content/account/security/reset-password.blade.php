@extends("ui/templates/default/index")


@section("css")
{{ HTML::style('assets/css/app/main/dashboard.css', array('media' => 'screen')) }}
@stop

@section("content")


<div class="row text-center">
    <h1>{{trans("nav.route.account.security.reset.password")}}</h1>
    <hr>
</div>
{{Form::open(['route' => [RouteData::ACCOUNT_SECURY_RESET_PASSWORD],'method' => 'put',"class"=>"form-main"])}} 

<div class="row">
    <div class="offset-md-4 col-md-4">
        <div class="md-form">
            {{trans("ac.section.security.password.current")}}
            <input type="password" class="input-alternate" data-required="text" name="current" min-length="8">
        </div>
        <div class="md-form">
            {{trans("ac.section.security.password.new")}}
            <input type="password" class="input-alternate" data-required="text" name="new" id="pwd-new" min-length="8">
        </div>
        <div class="md-form">
            {{trans("ac.section.security.password.repeat")}}
            <input type="password" class="input-alternate" data-required="text" name="repeat" data-compare="#pwd-new" min-length="8">
        </div>
    </div>
</div>

<div class="row">
    <div class="offset-md-4 col-md-4 text-center">
        <button class="btn btn-primary" type="submit">{{trans("ui.label.update")}}</button>
    </div>
</div>
</form>
@stop