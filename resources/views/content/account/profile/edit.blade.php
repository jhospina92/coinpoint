@extends("ui/templates/default/index")


@section("css")
{{ HTML::style('assets/css/app/main/dashboard.css', array('media' => 'screen')) }}
@stop

@section("content")

<h1>{{trans("nav.route.account.profile.edit")}}</h1>

<hr>

{{Form::open(['route' => RouteData::ACCOUNT_PROFILE_EDIT,'method' => 'put',"class"=>"form-main"])}} 

<div class="row">
    <div class="col-md-6">
        <div class="md-form">
            {{trans("ac.profile.name")}}
            <input type="text" class="input-alternate" value="{{Auth::user()->name}}" data-required="text" name="name">
        </div>
    </div>
    <div class="col-md-6">
        <div class="md-form">
            {{trans("ac.profile.lastname")}}
            <input type="text" class="input-alternate" value="{{Auth::user()->lastname}}" data-required="text" name="lastname">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-center">
        <button class="btn btn-primary" type="submit">{{trans("ui.label.update")}}</button>
    </div>
</div>
</form>
@stop