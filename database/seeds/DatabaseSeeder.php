<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\UserMeta;
use App\Base\Data\Currency;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        
        /**
         * GENERA Y REGISTRA 200 usuarios aleatoriamente
         */

        for ($i = 1000; $i < 1200; $i++) {
            //User
            DB::table(User::DB_TABLE)->insert([
                "id" => $i,
                User::ATTR_EMAIL => str_random(20) . "@gmail.com",
                User::ATTR_PASSWORD => bcrypt('test.myrald'),
                User::ATTR_NAME => str_random(10),
                User::ATTR_LASTNAME => str_random(10),
                User::ATTR_TYPE => User::TYPE_FREE,
                User::ATTR_API_TOKEN => str_random(50),
                User::ATTR_ACTIVE => true
            ]);

            //UserMeta User - Currency
            DB::table(UserMeta::DB_TABLE)->insert([
                UserMeta::ATTR_FK_USER_ID => $i,
                UserMeta::ATTR_KEY => UserMeta::KEY_CURRENCY,
                UserMeta::ATTR_VALUE => Currency::COP
            ]);

            DB::table(UserMeta::DB_TABLE)->insert([
                UserMeta::ATTR_FK_USER_ID => $i,
                UserMeta::ATTR_KEY => UserMeta::KEY_CURRENCY_UNIFY,
                UserMeta::ATTR_VALUE => true
            ]);

            DB::table(UserMeta::DB_TABLE)->insert([
                UserMeta::ATTR_FK_USER_ID => $i,
                UserMeta::ATTR_KEY => UserMeta::KEY_LANG,
                UserMeta::ATTR_VALUE => "es"
            ]);

            DB::table(UserMeta::DB_TABLE)->insert([
                UserMeta::ATTR_FK_USER_ID => $i,
                UserMeta::ATTR_KEY => UserMeta::KEY_MODE_SECURE,
                UserMeta::ATTR_VALUE => false
            ]);
        }
    }

}
