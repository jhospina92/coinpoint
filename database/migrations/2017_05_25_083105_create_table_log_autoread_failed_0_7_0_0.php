<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Logs\LogAutoReadFailed;

class CreateTableLogAutoreadFailed0700 extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create(LogAutoReadFailed::DB_TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->integer(LogAutoReadFailed::ATTR_FK_AUTOREAD_ID);
            $table->integer(LogAutoReadFailed::ATTR_FK_TYPAYMENT_ID);
            $table->string(LogAutoReadFailed::ATTR_MESSAGE_ID);
            $table->string(LogAutoReadFailed::ATTR_TYPE, 2);
            $table->string(LogAutoReadFailed::ATTR_DESCRIPTION)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop(LogAutoReadFailed::DB_TABLE);
    }

}
