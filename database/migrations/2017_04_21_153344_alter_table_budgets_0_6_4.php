<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Budget;

class AlterTableBudgets064 extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table(Budget::DB_TABLE, function(Blueprint $table) {
            $table->date(Budget::ATTR_DATE_START)->change();
            $table->date(Budget::ATTR_DATE_END)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table(Budget::DB_TABLE, function (Blueprint $table) {
            $table->dateTime(Budget::ATTR_DATE_START)->change();
            $table->dateTime(Budget::ATTR_DATE_END)->change();
        });
    }

}
