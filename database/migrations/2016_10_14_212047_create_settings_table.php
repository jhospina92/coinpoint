<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Setting;

class CreateSettingsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create(Setting::DB_TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->string(Setting::ATTR_KEY);
            $table->string(Setting::ATTR_VALUE);
            $table->integer(Setting::ATTR_FK_USER_ID);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop(Setting::DB_TABLE);
    }

}
