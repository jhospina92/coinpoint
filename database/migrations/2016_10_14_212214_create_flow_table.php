<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Flow;

class CreateFlowTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create(Flow::DB_TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->integer(Flow::ATTR_FK_USER_ID);
            $table->integer(Flow::ATTR_FK_TYPAYMENT_ID);
            $table->dateTime(Flow::ATTR_DATE);
            $table->string(Flow::ATTR_CATEGORY, 8)->nullable();
            $table->string(Flow::ATTR_TYPE,2);
            $table->string((Flow::ATTR_DESCRIPTION));
            $table->integer(Flow::ATTR_VALUE);
            $table->text(Flow::ATTR_METADATA)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop(Flow::DB_TABLE);
    }

}
