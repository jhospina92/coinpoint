<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\FlowCron;

class CreateTableFlowCron extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create(FlowCron::DB_TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->integer(FlowCron::ATTR_FK_USER_ID);
            $table->bigInteger(FlowCron::ATTR_FK_REF_ID);
            $table->json(FlowCron::ATTR_REPEAT);
            $table->dateTime(FlowCron::ATTR_DATE_NEXT);
            $table->boolean(FlowCron::ATTR_CONFIRM);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop(FlowCron::DB_TABLE);
    }

}
