<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Credit;

class CreateCreditsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create(Credit::DB_TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->integer(Credit::ATTR_FK_USER_ID);
            $table->string(Credit::ATTR_NAME);
            $table->string(Credit::ATTR_ORGANIZATION);
            $table->integer(Credit::ATTR_VALUE);
            $table->integer(Credit::ATTR_BALANCE);
            $table->tinyInteger(Credit::ATTR_FEES);
            $table->string(Credit::ATTR_CATEGORY, 8)->nullable();
            $table->text(Credit::ATTR_METADATA);
            $table->string(Credit::ATTR_STATUS, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop(Credit::DB_TABLE);
    }

}
