<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Auto\AutoRead;
use App\Models\Typayment;

class CreateTableAutoread0700 extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create(AutoRead::DB_TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->integer(AutoRead::ATTR_FK_USER_ID);
            $table->string(AutoRead::ATTR_CLIENT);
            $table->boolean(AutoRead::ATTR_ACTIVE);
            $table->text(AutoRead::ATTR_AUTH);
            $table->integer(AutoRead::ATTR_PROCESSED)->default(0);
            $table->integer(AutoRead::ATTR_ERRORS)->default(0);
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop(AutoRead::DB_TABLE);
    }

}
