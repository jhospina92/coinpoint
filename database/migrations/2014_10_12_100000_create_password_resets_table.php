<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\PasswordReset;

class CreatePasswordResetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(PasswordReset::DB_TABLE, function (Blueprint $table) {
            $table->increments("id");
            $table->integer(PasswordReset::ATTR_FK_USER_ID);
            $table->string(PasswordReset::ATTR_TOKEN)->index();
            $table->boolean(PasswordReset::ATTR_VALIDATE);
            $table->timestamp(PasswordReset::ATTR_EXPIRATION);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(PasswordReset::DB_TABLE);
    }
}
