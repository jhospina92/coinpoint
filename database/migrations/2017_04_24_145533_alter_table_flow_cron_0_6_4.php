<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\FlowCron;

class AlterTableFlowCron064 extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table(FlowCron::DB_TABLE, function(Blueprint $table) {
            $table->string(FlowCron::ATTR_HASH_EMAIL,100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table(FlowCron::DB_TABLE, function(Blueprint $table) {
            $table->dropColumn(FlowCron::ATTR_HASH_EMAIL);
        });
    }

}
