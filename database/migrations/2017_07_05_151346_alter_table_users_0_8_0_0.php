<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\User;

class AlterTableUsers0800 extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table(User::DB_TABLE, function(Blueprint $table) {
            $table->string(User::ATTR_API_TOKEN,50)->unique()->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table(User::DB_TABLE, function(Blueprint $table) {
            $table->dropColumn(User::ATTR_API_TOKEN);
            $table->dropColumn("deleted_at");
        });
    }

}
