<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Typayment;
use App\Base\System\Library\Comp\DateUtil;

class AlterTableTypaymentsTimestamps061 extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table(Typayment::DB_TABLE, function(Blueprint $table) {
            $table->timestamps();
        });

        Typayment::where("id", "!=", 0)->update([Typayment::CREATED_AT => "2017-01-01 00:00:00"]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table(Typayment::DB_TABLE, function (Blueprint $table) {
            $table->dropTimestamps();
        });
    }

}
