<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\User;

class CreateUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create(User::DB_TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->string(User::ATTR_EMAIL)->unique();
            $table->string(User::ATTR_PASSWORD);
            $table->string(User::ATTR_NAME);
            $table->string(User::ATTR_LASTNAME);
            $table->string(User::ATTR_TYPE, 2)->default(User::TYPE_FREE);
            $table->timestamp(User::ATTR_LOGIN_LAST)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop(User::DB_TABLE);
    }

}
