<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Typayment;
use App\Models\Flow;
use App\Models\Credit;
use App\Models\CreditTrack;
use App\Models\Budget;

class AlterAllAttrBalance0511 extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table(Typayment::DB_TABLE, function (Blueprint $table) {
            $table->float(Typayment::ATTR_BALANCE, 12, 2)->change();
        });

        Schema::table(Flow::DB_TABLE, function (Blueprint $table) {
            $table->float(Flow::ATTR_VALUE, 12, 2)->change();
        });

        Schema::table(Credit::DB_TABLE, function (Blueprint $table) {
            $table->float(Credit::ATTR_VALUE, 12, 2)->change();
        });

        Schema::table(Budget::DB_TABLE, function (Blueprint $table) {
            $table->float(Budget::ATTR_BALANCE, 12, 2)->change();
        });

        Schema::table(CreditTrack::DB_TABLE, function (Blueprint $table) {
            $table->float(CreditTrack::ATTR_VALUE, 12, 2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table(Typayment::DB_TABLE, function (Blueprint $table) {
            $table->integer(Typayment::ATTR_BALANCE)->change();
        });

        Schema::table(Flow::DB_TABLE, function (Blueprint $table) {
            $table->integer(Flow::ATTR_VALUE)->change();
        });

        Schema::table(Credit::DB_TABLE, function (Blueprint $table) {
            $table->integer(Credit::ATTR_VALUE)->change();
        });

        Schema::table(CreditTrack::DB_TABLE, function (Blueprint $table) {
            $table->integer(CreditTrack::ATTR_VALUE)->change();
        });

        Schema::table(Budget::DB_TABLE, function (Blueprint $table) {
            $table->integer(Budget::ATTR_BALANCE)->change();
        });
    }

}
