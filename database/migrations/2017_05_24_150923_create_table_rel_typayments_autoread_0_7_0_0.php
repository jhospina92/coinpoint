<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Relations\TypaymentAutoRead;

class CreateTableRelTypaymentsAutoread0700 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create(TypaymentAutoRead::DB_TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->integer(TypaymentAutoRead::ATTR_FK_TYPAYMENT_ID);
            $table->integer(TypaymentAutoRead::ATTR_FK_AUTOREAD_ID);
            $table->string(TypaymentAutoRead::ATTR_TYPE);
            $table->text(TypaymentAutoRead::ATTR_PARAMS);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
           Schema::drop(TypaymentAutoRead::DB_TABLE);
    }
}
