<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Logs\LogAutoReadProcessed;

class CreateTableLogAutoreadProcessed0700 extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create(LogAutoReadProcessed::DB_TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->integer(LogAutoReadProcessed::ATTR_FK_AUTOREAD_ID);
            $table->string(LogAutoReadProcessed::ATTR_MESSAGE_ID);
            $table->text(LogAutoReadProcessed::ATTR_DATA);
            $table->boolean(LogAutoReadProcessed::ATTR_VALIDATE);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop(LogAutoReadProcessed::DB_TABLE);
    }

}
