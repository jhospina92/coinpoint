<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Typayment;

class CreateTypaymentsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create(Typayment::DB_TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->string(Typayment::ATTR_TYPE, 2);
            $table->text(Typayment::ATTR_FEATURES);
            $table->integer(Typayment::ATTR_BALANCE);
            $table->integer(Typayment::ATTR_FK_USER_ID);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop(Typayment::DB_TABLE);
    }

}
