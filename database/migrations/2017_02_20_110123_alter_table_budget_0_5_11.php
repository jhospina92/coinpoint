<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Budget;
use App\Base\Data\Currency;

class AlterTableBudget0511 extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table(Budget::DB_TABLE, function(Blueprint $table) {
            $table->string(Budget::ATTR_CURRENCY, 3)->default(Currency::COP);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table(Budget::DB_TABLE, function ($table) {
            $table->dropColumn(Budget::ATTR_CURRENCY);
        });
    }

}
