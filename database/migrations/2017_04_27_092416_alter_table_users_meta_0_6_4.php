<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\UserMeta;
use App\Models\User;

class AlterTableUsersMeta064 extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        foreach (User::all() as $user) {
            $user->setMeta(UserMeta::KEY_NOTF_FLOWCRON_AUTO_EMAIL, true);
            $user->setMeta(UserMeta::KEY_NOTF_FLOWCRON_CONFIRM_EMAIL, true);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        foreach (User::all() as $user) {
            $user->removeMeta(UserMeta::KEY_NOTF_FLOWCRON_AUTO_EMAIL);
            $user->removeMeta(UserMeta::KEY_NOTF_FLOWCRON_CONFIRM_EMAIL);
        }
    }

}
