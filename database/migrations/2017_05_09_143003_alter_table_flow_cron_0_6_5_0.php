<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\FlowCron;

class AlterTableFlowCron0650 extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table(FlowCron::DB_TABLE, function(Blueprint $table) {
            $table->dateTime(FlowCron::ATTR_LAST_NOTIFICATION)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table(FlowCron::DB_TABLE, function(Blueprint $table) {
            $table->dropColumn(FlowCron::ATTR_LAST_NOTIFICATION);
        });
    }

}
