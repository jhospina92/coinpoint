<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\UserMeta;

class CreateUsersMetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create(UserMeta::DB_TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->integer(UserMeta::ATTR_FK_USER_ID);
            $table->string(UserMeta::ATTR_KEY);
            $table->string(UserMeta::ATTR_VALUE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(UserMeta::DB_TABLE);
    }
}
