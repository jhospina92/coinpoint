<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\User;

class AlterTableUsers063 extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table(User::DB_TABLE, function(Blueprint $table) {
            $table->boolean(User::ATTR_ACTIVE)->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table(User::DB_TABLE, function (Blueprint $table) {
            $table->dropColumn(User::ATTR_ACTIVE);
        });
    }

}
