<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Budget;

class CreateBudgetsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create(Budget::DB_TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->string(Budget::ATTR_NAME);
            $table->boolean(Budget::ATTR_ACTIVE);
            $table->text(Budget::ATTR_DATA_REVENUE);
            $table->text(Budget::ATTR_DATA_SPEND);
            $table->integer(Budget::ATTR_BALANCE);
            $table->dateTime(Budget::ATTR_DATE_START)->nullable();
            $table->dateTime(Budget::ATTR_DATE_END)->nullable();
            $table->integer(Budget::ATTR_FK_USER_ID);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop(Budget::DB_TABLE);
    }

}
