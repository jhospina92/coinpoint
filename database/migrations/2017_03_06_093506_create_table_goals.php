<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Goal;

class CreateTableGoals extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create(Goal::DB_TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->integer(Goal::ATTR_FK_USER_ID);
            $table->string(Goal::ATTR_NAME);
            $table->double(Goal::ATTR_VALUE);
            $table->date(Goal::ATTR_DATE);
            $table->dateTime(Goal::ATTR_DATE_COMPLETE)->nullable();
            $table->string(Goal::ATTR_CURRENCY, 3);
            $table->decimal(Goal::ATTR_RATE_SAVING, 3, 2);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop(Goal::DB_TABLE);
    }

}
