<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Wildo\WReport;

class CreateTableWildoReports extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create(WReport::DB_TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->integer(WReport::ATTR_FK_USER_ID);
            $table->string(WReport::ATTR_MESSAGE);
            $table->text(WReport::ATTR_DATA);
            $table->dateTime(WReport::ATTR_TIME)->nullable();
            $table->string(WReport::ATTR_TYPE, 2)->default(WReport::TYPE_CRUM);
            $table->boolean(WReport::ATTR_VIEWED)->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop(Budget::DB_TABLE);
    }

}
