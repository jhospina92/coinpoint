<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Credit;
use App\Base\Data\Currency;

class AlterTableCredits0511 extends Migration
{
      /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table(Credit::DB_TABLE, function(Blueprint $table) {
            $table->string(Credit::ATTR_CURRENCY, 3)->default(Currency::COP);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table(Credit::DB_TABLE, function ($table) {
            $table->dropColumn(Credit::ATTR_CURRENCY);
        });
    }
   
}
