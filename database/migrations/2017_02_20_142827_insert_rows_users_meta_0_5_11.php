<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\UserMeta;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Base\Data\Currency;

class InsertRowsUsersMeta0511 extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        foreach (User::all() as $user) {
            DB::table(UserMeta::DB_TABLE)->insert([
                UserMeta::ATTR_FK_USER_ID => $user->id,
                UserMeta::ATTR_KEY => UserMeta::KEY_CURRENCY_UNIFY,
                UserMeta::ATTR_VALUE => true
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        UserMeta::where(UserMeta::ATTR_KEY, UserMeta::KEY_CURRENCY_UNIFY)->delete();
    }

}
