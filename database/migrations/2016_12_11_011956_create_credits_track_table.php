<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\CreditTrack;

class CreateCreditsTrackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
           Schema::create(CreditTrack::DB_TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->integer(CreditTrack::ATTR_FK_CREDIT_ID);
            $table->string(CreditTrack::ATTR_TYPE);
            $table->integer(CreditTrack::ATTR_VALUE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CreditTrack::DB_TABLE);
    }
}
