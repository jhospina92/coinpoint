<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Typayment;
use App\Base\Data\Currency;

class AlterTableTypayments0511 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::table(Typayment::DB_TABLE, function(Blueprint $table) {
            $table->string(Typayment::ATTR_CURRENCY, 3)->default(Currency::COP);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table(Typayment::DB_TABLE, function ($table) {
            $table->dropColumn(Typayment::ATTR_CURRENCY);
        });
    }
}
