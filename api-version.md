# API Myrald #

Version 0.1.0
==================
Fecha: 2017 Julio 30

- Implementación de EndPoint
- Usuarios
    - [GET] Listar usuarios
    - [POST] Crear usuario
    - [PUT] Actualizar usuario
    - [DELETE] Eliminar usuario
- Filtración
- Ordenación